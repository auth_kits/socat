import { createSelector } from "@reduxjs/toolkit";
import { difference, includes } from "lodash";
import { RootState } from "../app/store";

const selectNotificationState = ({ notification }: RootState) => notification;

export const selectStatus = createSelector(
  selectNotificationState,
  (state) => state.status
);

export const selectSnacks = createSelector(
  selectNotificationState,
  (state) => state.snacks
);

export const selectNotifications = createSelector(
  selectNotificationState,
  (state) => state.notifications
);
