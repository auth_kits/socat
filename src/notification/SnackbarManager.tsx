import { Alert, Box, Snackbar } from "@material-ui/core";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { removeSnackMessage } from "./action";
import { selectSnacks } from "./selector";

export const SnackbarManager = () => {
  const snacks = useAppSelector(selectSnacks);
  const dispatch = useAppDispatch();
  const handleClose = (snackId) => {
    console.log();
    dispatch(
      removeSnackMessage({
        snackId,
      })
    );
  };

  return (
    <Box>
      {snacks?.map((s) => (
        <Snackbar
          id={s.id}
          key={s.id}
          open
          autoHideDuration={3000}
          onClose={() => handleClose(s.id)}
          message={s.message}
        >
          <Alert
            onClose={() => handleClose(s.id)}
            severity={s.notificationType}
            sx={{ width: "100%" }}
          >
            {s.message}
          </Alert>
        </Snackbar>
      ))}
    </Box>
  );
};
