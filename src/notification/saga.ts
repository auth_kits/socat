import { all, call, put, takeLatest } from "redux-saga/effects";
import { getApi } from "../app/axios";
import { history } from "../app/history";
import * as types from "./action";

export function* getNotificationSaga() {
  try {
    const { data } = yield call(getApi().get, "/notifications");
    yield put(
      types.getNotificationsSuccess({
        notifications: data.data,
      })
    );
  } catch (e) {
    // yield put(
    //   types.addSnackMessage({
    //     message: "Error fetching notification results",
    //     notificationType: "error",
    //   })
    // );
  }
}

export function* initSaga() {
  yield put(types.getNotifications());
}

export default function* watchNotificationSaga() {
  yield all([
    takeLatest("@@SOCAT@@INIT@@", initSaga),
    takeLatest(types.getNotifications, getNotificationSaga),
  ]);
}
