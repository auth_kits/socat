import { createReducer } from "@reduxjs/toolkit";
import { filter, without } from "lodash";
import { v4 as uuidv4 } from "uuid";
import * as types from "./action";

export interface SnackState extends types.SnackMessage {
  id: string;
}
export interface NotificationState {
  // Changing this will trigger
  snacks: SnackState[];
  status: types.ELoadingStatus;
  notifications: types.INotification[];
}

export default createReducer<NotificationState>(
  {
    snacks: [],
    status: types.ELoadingStatus.INITIAL,
    notifications: [],
  },
  (builder) => {
    builder.addCase(types.getNotifications, (state) => {
      state.status = types.ELoadingStatus.LOADING;
    });

    builder.addCase(types.setNotificationsError, (state) => {
      state.status = types.ELoadingStatus.ERROR;
    });

    builder.addCase(
      types.getNotificationsSuccess,
      (state, { payload: { notifications } }) => {
        state.status = types.ELoadingStatus.SUCCESS;
        state.notifications = notifications;
      }
    );

    builder.addCase(
      types.addSnackMessage,
      (state, { payload: { message, notificationType } }) => {
        state.snacks.push({
          id: uuidv4() as string,
          message,
          notificationType,
        });
      }
    );

    builder.addCase(
      types.removeSnackMessage,
      (state, { payload: { snackId } }) => {
        state.snacks = state.snacks.filter((s) => s.id !== snackId);
      }
    );
  }
);
