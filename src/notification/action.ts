import { AlertColor } from "@material-ui/core";
import { createAction } from "@reduxjs/toolkit";

const PREFIX = "socat/notification";

export enum ENotificationType {
  NONE = "none",
}

export interface INotification {
  id: string;
  title: string;
  description: string;
  avatar?: string;
  type: ENotificationType;
  createdAt: Date;
  unread: boolean;
}

export enum ELoadingStatus {
  INITIAL,
  LOADING,
  SUCCESS,
  ERROR,
}

export const getNotifications = createAction(`${PREFIX}/getNotifications`);

export interface NotificationSuccessPayload {
  notifications: INotification[];
}
export const getNotificationsSuccess = createAction<NotificationSuccessPayload>(
  `${PREFIX}/getNotificationsSuccess`
);

interface NotificationError {
  message: string;
}
export const setNotificationsError = createAction<NotificationError>(
  `${PREFIX}/setNotificationsError`
);

export interface SnackMessage {
  message: string;
  notificationType: AlertColor;
}

export const addSnackMessage = createAction<SnackMessage>(
  `${PREFIX}/addSnackMessage`
);
export const removeSnackMessage = createAction<{ snackId: string }>(
  `${PREFIX}/removeSnackMessage`
);
