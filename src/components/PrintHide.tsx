import { Box } from "@material-ui/core";
import { SxProps } from "@material-ui/system";

export const PrintView: React.FC<{
  sx?: SxProps;
  pageBreak?: "after" | "before" | "none";
  hide?: boolean;
  show?: boolean;
}> = ({ hide, show, children, sx }) => {
  if (show || hide === false) {
    if (sx) {
      return <Box sx={sx}>{children}</Box>;
    }
    return <>{children}</>;
  }
  return null;
};
