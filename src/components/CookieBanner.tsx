import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import { Button, Grid, Paper, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  banner: {
    position: "fixed",
    left: "0px",
    bottom: "2px",
    width: "100%",
    display: "flex",
    zIndex: 1200,
  },
  bannerItem: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
    padding: "10px 50px",
    width: "100%",
  },
  bannerText: {
    flex: 1,
  },
  bannerBtn: {
    textTransform: "none",
  },
}));

function CoockieBanner() {
  const styles = useStyles();
  const [showCookie, setShowCookie] = useState(true);

  useEffect(() => {
    const isCookieEnabled = localStorage.getItem("cookie_enabled");
    if (isCookieEnabled) setShowCookie(false);
  }, []);

  const onAccept = () => {
    localStorage.setItem("cookie_enabled", JSON.stringify(true));
    setShowCookie(false);
  };

  return (
    <Grid
      visibility={showCookie ? "visible" : "hidden"}
      sm={12}
      md={12}
      xl={12}
      className={styles.banner}
    >
      <Paper square elevation={3} className={styles.bannerItem}>
        <Typography variant="body2" className={styles.bannerText}>
          This website uses cookies. We use cookies to ensure that we give you
          the best experience on our website to personalise content and
          advertisement.
        </Typography>
        <Button className={styles.bannerBtn} onClick={onAccept}>
          I understand
        </Button>
      </Paper>
    </Grid>
  );
}

export default CoockieBanner;
