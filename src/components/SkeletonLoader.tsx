import { Skeleton } from "@material-ui/core";
import React from "react";

export const SkeletonLoader: React.FC<{
  loading: boolean;
  variant?: "text" | "circular" | "rectangular";
}> = ({ children, variant, loading }) => {
  if (loading) {
    return (
      <Skeleton
        sx={{
          borderRadius: "10px",
        }}
        height="100%"
        width="100%"
        variant={variant}
      >
        {children}
      </Skeleton>
    );
  }
  return <>{children}</>;
};
