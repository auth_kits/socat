import {
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  DialogProps,
  Box,
  Typography,
} from "@material-ui/core";
import React from "react";

export const WipDialog: React.FC<DialogProps> = (props) => (
  <Dialog {...props}>
    <DialogContent>
      <Box sx={{ margin: "auto", textAlign: "center" }}>
        <Typography sx={{ color: "text.secondary" }}>
          Our engineers are working on it, please check again.
        </Typography>

        <Box
          component="img"
          src="/images/under_construction.svg"
          sx={{ height: 260, mx: "auto", my: { xs: 5, sm: 10 } }}
        />
      </Box>
    </DialogContent>
    <DialogActions>
      <Button
        onClick={(ev) => {
          props?.onClose?.(ev, "backdropClick");
        }}
        autoFocus
      >
        Close
      </Button>
    </DialogActions>
  </Dialog>
);
