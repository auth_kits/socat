import { createBrowserHistory } from "history";

// this is just for navigating in the history
export const history = createBrowserHistory();
