import axios, { AxiosInstance } from "axios";
import { logout } from "../auth/action";
import { initializeMockApi } from "./mocks";
import { store } from "./store";

let axiosClient: AxiosInstance;

const createClient = () => {
  axiosClient = axios.create({
    baseURL: process.env.REACT_APP_BASE_BACKEND_URL,
    // timeout: 30000,
  });
  const token = localStorage?.getItem(process.env.REACT_APP_TOKEN_NAME ?? "");
  if (token) {
    axiosClient.defaults.headers.common["Authorization"] = token;
  }

  axiosClient.interceptors.response.use(
    (resp) => resp,
    (err) => {
      // logout and reset all the statuses if the server sends 401
      // note: As RBAC is implemented 403's should be handled on case by case basis
      if (err.response.status === 401) {
        store.dispatch(logout(err?.response?.data?.msg));
      }

      return Promise.reject(err);
    }
  );

  // if (process.env.REACT_APP_ENABLE_MOCK) {
  // initializeMockApi(axiosClient);
  // }
  return axiosClient;
};

export const getApi = (reset = false) => {
  if (!reset && axiosClient) {
    return axiosClient;
  }

  axiosClient = createClient();
  return axiosClient;
};
