import { alpha } from "@material-ui/core/styles";

// ----------------------------------------------------------------------

function createGradient(color1, color2) {
  return `linear-gradient(to bottom, ${color1}, ${color2})`;
}

// SETUP COLORS
const GREY = {
  0: "#FFFFFF",
  100: "#F9FAFB",
  200: "#F4F6F8",
  300: "#DFE3E8",
  400: "#C4CDD5",
  500: "#919EAB",
  600: "#637381",
  700: "#454F5B",
  800: "#212B36",
  900: "#161C24",
  500_8: alpha("#919EAB", 0.08),
  500_12: alpha("#919EAB", 0.12),
  500_16: alpha("#919EAB", 0.16),
  500_24: alpha("#919EAB", 0.24),
  500_32: alpha("#919EAB", 0.32),
  500_48: alpha("#919EAB", 0.48),
  500_56: alpha("#919EAB", 0.56),
  500_80: alpha("#919EAB", 0.8),
};

const PRIMARY = {
  // lighter: "#C8FACD",
  lighter: "#f6a5a1",
  // light: "#5BE584",
  light: "#f48a85",
  main: "#f05d56",
  // main: "#00AB55",
  dark: "#ba4842",
  contrastText: "#fff",
  darker: "#6a2926",
  // dark: "#007B55",
  // darker: "#005249",
  // contrastText: "#fff",
};
const SECONDARY = {
  lighter: "#D6E4FF",
  light: "#84A9FF",
  main: "#3366FF",
  dark: "#1939B7",
  darker: "#091A7A",
  contrastText: "#fff",
};
const INFO = {
  lighter: "#D0F2FF",
  light: "#74CAFF",
  main: "#1890FF",
  dark: "#0C53B7",
  darker: "#04297A",
  contrastText: "#fff",
};
const SUCCESS = {
  lighter: "#E9FCD4",
  light: "#AAF27F",
  main: "#54D62C",
  dark: "#229A16",
  darker: "#08660D",
  contrastText: GREY[800],
};
const WARNING = {
  lighter: "#FFF7CD",
  light: "#FFE16A",
  main: "#FFC107",
  dark: "#B78103",
  darker: "#7A4F01",
  contrastText: GREY[800],
};
const ERROR = {
  lighter: "#FFE7D9",
  light: "#FFA48D",
  main: "#FF4842",
  dark: "#B72136",
  darker: "#7A0C2E",
  contrastText: "#fff",
};

const PINK = {
  lighter: "#f8bbd0",
  light: "#f06292",
  main: "#e91e63",
  dark: "#c2185b",
  darker: "#880e4f",
  contrastText: GREY[800],
};
const PURPLE = {
  lighter: "#f3e5f5",
  light: "#ba68c8",
  main: "#9c27b0",
  dark: "#7b1fa2",
  darker: "#4a148c",
  contrastText: GREY[800],
};
const DEEPPURPLE = {
  lighter: "#d1c4e9",
  light: "#9575cd",
  main: "#673ab7",
  dark: "#512da8",
  darker: "#311b92",
  contrastText: GREY[800],
};
const INDIGO = {
  lighter: "#c5cae9",
  light: "#7986cb",
  main: "#3f51b5",
  dark: "#303f9f",
  darker: "#1a237e",
  contrastText: GREY[800],
};
const LIGHTBLUE = {
  lighter: "#b3e5fc",
  light: "#4fc3f7",
  main: "#03a9f4",
  dark: "#0288d1",
  darker: "#01579b",
  contrastText: GREY[800],
};
const BLUE = {
  lighter: "#bbdefb",
  light: "#64b5f6",
  main: "#2196f3",
  dark: "#1976d2",
  darker: "#0d47a1",
  contrastText: GREY[800],
};
const CYAN = {
  lighter: "#b2ebf2",
  light: "#4dd0e1",
  main: "#00bcd4",
  dark: "#0097a7",
  darker: "#006064",
  contrastText: GREY[800],
};
const TEAL = {
  lighter: "#edf7f6",
  light: "#4db6ac",
  main: "#009688",
  dark: "#004d40",
  darker: "#013129",
  variant: "#b2dfdb",
  contrastText: GREY[800],
};
const LIGHTGREEN = {
  lighter: "#dcedc8",
  light: "#aed581",
  main: "#8bc34a",
  dark: "#689f38",
  darker: "#33691e",
  contrastText: GREY[800],
};
const GREEN = {
  lighter: "#c8e6c9",
  light: "#81c784",
  main: "#4caf50",
  dark: "#388e3c",
  darker: "#0b420f",
  contrastText: GREY[800],
};
const LIME = {
  lighter: "#e6ee9c",
  light: "#dce775",
  main: "#cddc39",
  dark: "#afb42b",
  darker: "#827717",
  contrastText: GREY[800],
};
const YELLOW = {
  lighter: "#fff9c4",
  light: "#fff176",
  main: "#ffeb3b",
  dark: "#fbc02d",
  darker: "#f57f17",
  contrastText: GREY[800],
};
const AMBER = {
  lighter: "#ffecb3",
  light: "#ffd54f",
  main: "#ffc107",
  dark: "#ffa000",
  darker: "#ff6f00",
  contrastText: GREY[800],
};
const ORANGE = {
  lighter: "#ffe0b2",
  light: "#ffb74d",
  main: "#ff9800",
  dark: "#f57c00",
  darker: "#e65100",
  contrastText: GREY[800],
};
const DEEPORANGE = {
  lighter: "#ffccbc",
  light: "#ff8a65",
  main: "#ff5722",
  dark: "#e64a19",
  darker: "#bf360c",
  contrastText: GREY[800],
};
const BROWN = {
  lighter: "#d7ccc8",
  light: "#a1887f",
  main: "#795548",
  dark: "#5d4037",
  darker: "#3e2723",
  contrastText: GREY[800],
};
const BLUEGREY = {
  lighter: "#cfd8dc",
  light: "#90a4ae",
  main: "#607d8b",
  dark: "#455a64",
  darker: "#263238",
  contrastText: GREY[800],
};
const RED = {
  lighter: "#ffcdd2",
  light: "#e57373",
  main: "#f44336",
  dark: "#d32f2f",
  darker: "#b71c1c",
  contrastText: GREY[800],
};
const WHITE = {
  main: "#fff",
};
const BLACK = {
  main: "#000",
};

const GRADIENTS = {
  primary: createGradient(PRIMARY.light, PRIMARY.main),
  info: createGradient(INFO.light, INFO.main),
  success: createGradient(SUCCESS.light, SUCCESS.main),
  warning: createGradient(WARNING.light, WARNING.main),
  error: createGradient(ERROR.light, ERROR.main),
  lime: createGradient(LIME.dark, LIME.darker),
  lightlime: createGradient(LIME.lighter, LIME.light),
  teal: createGradient(TEAL.light, TEAL.dark),
  lightteal: createGradient(TEAL.variant, TEAL.light),
  orange: createGradient(ORANGE.lighter, ORANGE.light),
  amber: createGradient(AMBER.dark, AMBER.darker),
  deeporange: createGradient(DEEPORANGE.light, DEEPORANGE.darker),
  purple: createGradient(PURPLE.light, PURPLE.darker),
  bluegrey: createGradient(BLUEGREY.lighter, BLUEGREY.light),
  red: createGradient(RED.light, RED.main),
  lightred: createGradient(RED.lighter, RED.light),
  cyan: createGradient(CYAN.light, CYAN.darker),
  blue: createGradient(BLUE.main, BLUE.darker),
  indigo: createGradient(INDIGO.light, INDIGO.darker),
  pink: createGradient(PINK.light, PINK.darker),
  deeppurple: createGradient(DEEPPURPLE.light, DEEPPURPLE.darker),
  green: createGradient(GREEN.light, GREEN.main),
  brown: createGradient(BROWN.lighter, BROWN.light),
  lightblue: createGradient(LIGHTBLUE.lighter, LIGHTBLUE.light),
};

const palette = {
  common: { black: "#000", white: "#fff" },
  primary: { ...PRIMARY },
  secondary: { ...SECONDARY },
  info: { ...INFO },
  success: { ...SUCCESS },
  warning: { ...WARNING },
  error: { ...ERROR },
  pink: { ...PINK },
  bluegrey: { ...BLUEGREY },
  brown: { ...BROWN },
  deeporange: { ...DEEPORANGE },
  orange: { ...ORANGE },
  amber: { ...AMBER },
  yellow: { ...YELLOW },
  lightgreen: { ...LIGHTGREEN },
  green: { ...GREEN },
  lightblue: { ...LIGHTBLUE },
  indigo: { ...INDIGO },
  deeppurple: { ...DEEPPURPLE },
  purple: { ...PURPLE },
  lime: { ...LIME },
  teal: { ...TEAL },
  cyan: { ...CYAN },
  red: { ...RED },
  white: { ...WHITE },
  black: { ...BLACK },
  blue: { ...BLUE },
  grey: GREY,
  gradients: GRADIENTS,
  divider: GREY[500_24],
  text: { primary: GREY[800], secondary: GREY[600], disabled: GREY[500] },
  background: { paper: "#fff", default: GREY[200], neutral: GREY[200] },
  action: {
    active: GREY[600],
    hover: GREY[500_8],
    selected: GREY[500_16],
    disabled: GREY[500_80],
    disabledBackground: GREY[500_24],
    focus: GREY[500_24],
    hoverOpacity: 0.08,
    disabledOpacity: 0.48,
  },
};

export default palette;
