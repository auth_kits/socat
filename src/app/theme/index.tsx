// material
import { CssBaseline, ThemeOptions, ThemeProvider } from "@material-ui/core";
import { createTheme, StyledEngineProvider } from "@material-ui/core/styles";
import React, { useMemo } from "react";
import GlobalStyles from "./globalStyles";
import componentsOverride from "./overrides";
import breakpoints from "./breakpoints";
import palette from "./palette";
import shadows, { customShadows } from "./shadows";
//
import shape from "./shape";
import typography from "./typography";

// ----------------------------------------------------------------------

export const ThemeConfig: React.FC = ({ children }) => {
  const themeOptions: ThemeOptions = useMemo(
    () => ({
      palette,
      shape,
      typography,
      shadows,
      customShadows,
      breakpoints,
    }),
    []
  );

  const theme = createTheme(themeOptions);
  theme.components = componentsOverride(theme);

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <GlobalStyles />
        {children}
      </ThemeProvider>
    </StyledEngineProvider>
  );
};
