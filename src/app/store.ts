import { configureStore } from "@reduxjs/toolkit";
import {
  FLUSH,
  PAUSE,
  PERSIST,
  persistReducer,
  PURGE,
  REGISTER,
  REHYDRATE,
} from "redux-persist";
import storage from "redux-persist/lib/storage";
import createSagaMiddleware from "redux-saga";
import rootReducer from "./reducers";
import { rootSaga } from "./sagas";

export default function configureAppStore(initialState = {}) {
  const reduxSagaMonitorOptions = {};
  const sagaMiddleware = createSagaMiddleware(reduxSagaMonitorOptions);

  const persistConfig = {
    key: "root",
    version: 1,
    storage,
    throttle: 3000,
    blacklist: ["notification", "admin"],
    ...(process.env.NODE_ENV !== "production"
      ? {
          whitelist: ["none"], // this will makesure persistance is disabled
        }
      : {}),
  };

  const persistedReducer = persistReducer(persistConfig, rootReducer);

  const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
        serializableCheck: {
          ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
          // ignoredPaths: ["auth.*", "admin.*"],
        },
      }).concat(sagaMiddleware),
    devTools: process.env.NODE_ENV !== "production",
    preloadedState: initialState,
  });

  sagaMiddleware.run(rootSaga);

  return store;
}

export const store = configureAppStore();

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
