/**
 * Combine all reducers in this file and export the combined reducers.
 */
import { combineReducers } from "@reduxjs/toolkit";
import counterSlice from "../counter/counterSlice";
import authReducer from "../auth/reducer";
import influencerReducer from "../dashboard/influencers/reducer";
import adminReducer from "../dashboard/admin/reducer";
import notificationReducer from "../notification/reducer";

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */

const rootReducer = combineReducers({
  counter: counterSlice.reducer,
  auth: authReducer,
  admin: adminReducer,
  influencers: influencerReducer,
  notification: notificationReducer,
});

const resettableRootReducer = (state, action) => {
  if (action.type === "store/reset") {
    // cleanup
    localStorage.removeItem("persist:root");
    return rootReducer(undefined, action);
  }
  return rootReducer(state, action);
};

export default resettableRootReducer;
