import watchUserAuthentication from "../auth/saga";
import { adminSaga } from "../dashboard/admin/saga";
import watchInfluencerSaga from "../dashboard/influencers/saga";
import watchNotificationSaga from "../notification/saga";
import combineSaga from "../utils/sagaCreator";

export const rootSaga = combineSaga([
  watchUserAuthentication,
  adminSaga,
  watchInfluencerSaga,
  watchNotificationSaga,
]);
