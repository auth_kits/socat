import { AxiosInstance } from "axios";
import MockAdapter from "axios-mock-adapter";

const getRandomId = () => Date.now().toString(36);

export const initializeMockApi = (instance: AxiosInstance) => {
  instance.interceptors.response.use((resp) => {
    // perform a task before the request is sent
    console.groupCollapsed(
      `${resp.config.method} ${resp.config.baseURL}${resp.config.url}`
    );
    if (typeof resp.data !== "function") {
      console.log(resp.data);
    }
    console.groupEnd();
    return resp;
  });

  const adapter = new MockAdapter(instance);

  adapter
    .onPost("/login", { email: "asd@asd.asd", password: "asdasd" })
    .reply(200, {
      token: "Some token value",
      user: {
        email: "test@example.com",
        name: "Example",
        imageUrl: "https://via.placeholder.com/150",
        roles: ["admin"],
      },
    })
    // testing invalid credentials
    .onPost("/login")
    .reply(400);

  // For Admin Pages
  adapter
    .onGet("/admin/users")
    .reply(200, [
      {
        id: "id-1",
        email: "email1@test.com",
        status: "verified",
      },
    ])
    .onPost("/admin/users/new")
    .reply((config) => {
      // `config` is the axios config and contains things like the url
      const data = JSON.parse(config.data);
      if (data.name === "error") {
        return [
          400,
          {
            message:
              "This is an exmple of an error message that is to be shown.",
          },
        ];
      }
      const user = {
        roles: [],
        imageUrl: "",
        email: data.email,
        name: data.name,
        phone: data.phoneNo,
        address: data.address,
        id: getRandomId(),
      };
      return [200, user];
    });

  // For influencerprofile page
  adapter
    .onGet("/influencer/profile")
    .reply(200, {
      detail: {
        displayName: "Virat Kohli",
        id: "virat",
        imageUrl: "https://picsum.photos/150/150/?blur=2",
        location: {
          label: "Delhi, India",
        },
        tagLine: "I am virat",
        socialProfiles: [
          {
            count: 621601654,
            profileType: "instagram",
          },
          {
            count: 654949846,
            profileType: "youtube",
          },
          {
            count: 98465465,
            profileType: "tiktok",
          },
        ],
      },
    })
    .onGet("/influencer/overview")
    .reply(200, {})
    .onGet("/influencer/engagement")
    .reply(200, {})
    .onGet("/influencer/audience")
    .reply(200, {})
    .onGet("/influencer/posts")
    .reply(200, {});
};
