import { all, call, spawn } from "redux-saga/effects";

export default function combineSaga(sagas: any[]) {
  return function* combinedSagas() {
    yield all(
      sagas.map((saga) =>
        spawn(function* restartIfFailed() {
          while (true) {
            try {
              yield call(saga);
              break;
            } catch (e) {
              console.log(e);
            }
          }
        })
      )
    );
  };
}
