import { format, formatDistanceToNow } from "date-fns";

// ----------------------------------------------------------------------

export function fDate(date) {
  return format(new Date(date), "dd MMMM yyyy");
}

export function fDateTime(date) {
  return format(new Date(date), "dd MMM yyyy HH:mm");
}

export function fDateTime2(date) {
  return format(new Date(date), "MMMM dd, yyyy, HH:mm");
}

export function fDateTimeSuffix(date) {
  return format(new Date(date), "MMMM dd, yyyy p");
}

export function fToNow(date) {
  return formatDistanceToNow(new Date(date), {
    addSuffix: true,
  });
}
