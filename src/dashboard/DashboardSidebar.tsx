import { Avatar, Box, Drawer, Link, Typography } from "@material-ui/core";
// material
import { styled } from "@material-ui/core/styles";
import { useEffect } from "react";
import { Link as RouterLink, useLocation } from "react-router-dom";
import { useAppSelector } from "../app/hooks";
import { selectCurrentUser } from "../auth/selectors";
import { MHidden } from "../components/MHidden";
// components
//
import { Scrollbar } from "../components/Scrollbar";
import { NavSection } from "./NavSection";
import sidebarConfig from "./SidebarConfig";

const DRAWER_WIDTH = 60;

const MiniDrawer = styled(Drawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  transition: theme.transitions.create(["margin", "width"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),

  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  backgroundColor: theme.palette["background"].paper,
  // [theme.breakpoints.down("lg")]: {
  //   width: open ? DRAWER_WIDTH : 0,
  // },

  // [theme.breakpoints.up("lg")]: {
  //   width: DRAWER_WIDTH,
  // },
  // ...(open
  //   ? {
  //       ...(openedMixin(theme)),
  //       "& .MuiDrawer-paper": openedMixin(theme),
  //     }
  //   : {
  //       ...closedMixin(theme),
  //       "& .MuiDrawer-paper": closedMixin(theme),
  //     }),
}));

// const MiniDrawer = Drawer;

const RootStyle = styled("div")(({ theme }) => ({
  [theme.breakpoints.up("md")]: {
    flexShrink: 0,
    width: DRAWER_WIDTH,
  },
}));

interface DashboardSidebarProps {
  isOpenSidebar: boolean;
  onCloseSidebar: () => void;
}

export default function DashboardSidebar({
  isOpenSidebar,
  onCloseSidebar,
}: DashboardSidebarProps) {
  const { pathname } = useLocation();

  const user = useAppSelector(selectCurrentUser);

  // useEffect(() => {
  //   if (isOpenSidebar) {
  //     onCloseSidebar();
  //   }
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [pathname]);

  const renderContent = (
    <Scrollbar
      sx={{
        height: "100%",
        "& .simplebar-content": {
          height: "100%",
          display: "flex",
          flexDirection: "column",
        },
      }}
    >
      <Box sx={{ px: 0.5, py: 1 }}>
        <Box component={RouterLink} to="/" sx={{ display: "inline-flex" }}>
          <img src="/logo512.png" alt="login" />
        </Box>
      </Box>

      <NavSection navConfig={sidebarConfig} />

      <Box sx={{ flexGrow: 1 }} />
    </Scrollbar>
  );

  return (
    <Box>
      <RootStyle>
        <MHidden width="mdUp">
          <MiniDrawer
            open
            variant="permanent"
            onClose={onCloseSidebar}
            PaperProps={{
              sx: {
                bgcolor: "background.default",
                width: isOpenSidebar ? DRAWER_WIDTH : 0,
              },
            }}
          >
            {renderContent}
          </MiniDrawer>
        </MHidden>

        <MHidden width="mdDown">
          <MiniDrawer
            open={isOpenSidebar}
            variant="permanent"
            PaperProps={{
              sx: {
                width: DRAWER_WIDTH,
                bgcolor: "background.default",
              },
            }}
          >
            {renderContent}
          </MiniDrawer>
        </MHidden>
      </RootStyle>
    </Box>
  );
}
