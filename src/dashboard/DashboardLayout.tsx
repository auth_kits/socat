import { useState } from "react";
// material
import { styled } from "@material-ui/core/styles";
//
import DashboardNavbar from "./DashboardNavbar";
import DashboardSidebar from "./DashboardSidebar";

// ----------------------------------------------------------------------

const APPBAR_MOBILE = 60;
const APPBAR_DESKTOP = 70;

const RootStyle = styled("div")({
  display: "flex",
  minHeight: "100%",
  overflow: "hidden",
});

const MainStyle = styled("div")<{ open: boolean }>(({ theme, open }) => ({
  flexGrow: 1,
  overflow: "auto",
  minHeight: "100%",
  paddingTop: APPBAR_MOBILE,
  paddingBottom: theme.spacing(10),
  [theme.breakpoints.down("lg")]: {
    width: `calc(100% - ${open ? 60 : 0}px)`,
  },
  [theme.breakpoints.up("lg")]: {
    paddingTop: APPBAR_DESKTOP,
    width: "calc(100% - 60px)",
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
}));

// ----------------------------------------------------------------------

export const DashboardLayout: React.FC = ({ children }) => {
  const [open, setOpen] = useState(false);
  return (
    <RootStyle>
      <DashboardNavbar open={open} onOpenSidebar={() => setOpen((o) => !o)} />
      <DashboardSidebar
        isOpenSidebar={open}
        onCloseSidebar={() => setOpen(false)}
      />
      <MainStyle open={open}>{children}</MainStyle>
    </RootStyle>
  );
};
