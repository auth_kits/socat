import { Route, Switch, useRouteMatch } from "react-router-dom";
import { NotificationsPage } from "./NotificationsPage";
import { ProfilePage } from "./ProfilePage";
import { SettingsPage } from "./SettingsPage";

// this file contains the routes
export const AccountPage = () => {
  const { path, url } = useRouteMatch();
  console.log(path, url);
  return (
    <Switch>
      <Route path={`${path}`} exact>
        <ProfilePage />
      </Route>
      <Route path={`${path}/notifications`}>
        <NotificationsPage />
      </Route>
      <Route path={`${path}/settings`} exact>
        <SettingsPage />
      </Route>
    </Switch>
  );
};
