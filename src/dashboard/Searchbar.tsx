import {
  Autocomplete,
  Avatar,
  Button,
  debounce,
  Input,
  InputAdornment,
  ListItemAvatar,
  ListItemButton,
  ListItemText,
  Stack,
} from "@material-ui/core";
// material
import { alpha, styled } from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import { useTheme } from "@material-ui/system";
import { isEmpty } from "lodash";
import { useEffect, useMemo, useRef, useState } from "react";
import { Link as RouterLink, useHistory } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { ELoadingStatus, getSearchResults } from "./influencers/action";
import {
  selectReportSearchStatus,
  selectSearchResults,
} from "./influencers/selector";

// ----------------------------------------------------------------------

const APPBAR_MOBILE = 46;
const APPBAR_DESKTOP = 56;

const SearchbarStyle = styled("div")(({ theme }) => ({
  top: 0,
  left: 0,
  zIndex: 99,
  width: "100%",
  display: "flex",
  position: "absolute",
  alignItems: "center",
  height: APPBAR_MOBILE,
  backdropFilter: "blur(6px)",
  WebkitBackdropFilter: "blur(6px)", // Fix on Mobile
  padding: theme.spacing(0, 3),
  boxShadow: theme["customShadows"].z8,
  backgroundColor: `${alpha(theme.palette.background.default, 0.72)}`,
  [theme.breakpoints.up("md")]: {
    height: APPBAR_DESKTOP,
    padding: theme.spacing(0, 5),
  },
}));

const StyledInput = styled(Input)(({ theme }) => {
  const placeholder = {
    color: theme.palette.mode === "dark" ? "white" : "black",
  };
  return {
    color: "inherit",
    "& input": {
      padding: theme.spacing(0.5),
      paddingLeft: theme.spacing(4),
      transition: theme.transitions.create("width"),
      width: 150,
      "&:focus": {
        width: 300,
      },
      "&::-webkit-input-placeholder": placeholder,
      "&::-moz-placeholder": placeholder, // Firefox 19+
      "&:-ms-input-placeholder": placeholder, // IE11
      "&::-ms-input-placeholder": placeholder, // Edge
    },
  };
});

const SearchDiv = styled("div")(({ theme }) => ({
  width: theme.spacing(4),
  height: "100%",
  // position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  color: theme.palette.grey[700],
}));

const Shortcut = styled("div")(({ theme }) => ({
  fontSize: theme.typography.pxToRem(13),
  fontWeight: 600,
  color:
    theme.palette.mode === "dark"
      ? theme.palette.grey[200]
      : theme.palette.grey[700],
  lineHeight: "21px",
  border: `1px solid ${
    theme.palette.mode === "dark"
      ? theme.palette.primaryDark[400]
      : theme.palette.grey[200]
  }`,
  backgroundColor:
    theme.palette.mode === "dark" ? theme.palette.primaryDark[700] : "#FFF",
  padding: theme.spacing(0, 0.7),
  position: "absolute",
  right: theme.spacing(1),
  height: 23,
  top: "calc(50% - 11px)",
  borderRadius: 5,
  transition: theme.transitions.create("opacity", {
    duration: theme.transitions.duration.shortest,
  }),
  // So that clicks target the input.
  // Makes the text non selectable but neither is the placeholder or adornment.
  pointerEvents: "none",
  "&.Mui-focused": {
    opacity: 0,
  },
}));

const RootDiv = styled("div")(({ theme }) => ({
  display: "none",
  [theme.breakpoints.up("sm")]: {
    display: "flex",
  },
  fontFamily: theme.typography.fontFamily,
  position: "relative",
  backgroundColor:
    theme.palette.mode === "dark"
      ? theme.palette.primaryDark[800]
      : theme.palette.grey[50],
  "&:hover": {
    backgroundColor:
      theme.palette.mode === "dark"
        ? theme.palette.primaryDark[700]
        : theme.palette.grey[100],
  },
  color: theme.palette.mode === "dark" ? "white" : theme.palette.grey[900],
  border: `1px solid ${
    theme.palette.mode === "dark"
      ? theme.palette.primaryDark[600]
      : theme.palette.grey[200]
  }`,
  borderRadius: 10,
}));

// ----------------------------------------------------------------------

export function Searchbar() {
  const theme = useTheme();
  const inputRef = useRef<HTMLInputElement>(null);
  const history = useHistory();

  const [focused, setFocused] = useState(false);

  const dispatch = useAppDispatch();
  const results = useAppSelector(selectSearchResults);
  const [selectedValue, setSelectedValue] = useState<string>();

  const searchStatus = useAppSelector(selectReportSearchStatus);
  // Key search
  useEffect(() => {
    const handleKeyDown = (nativeEvent) => {
      if (nativeEvent.defaultPrevented) {
        return;
      }

      if (
        nativeEvent.key === "Escape" &&
        document.activeElement === inputRef.current
      ) {
        inputRef.current?.blur();
        return;
      }

      const matchMainShortcut =
        (nativeEvent.ctrlKey || nativeEvent.metaKey) && nativeEvent.key === "k";
      const matchNonkeyboardNode =
        ["INPUT", "SELECT", "TEXTAREA"].indexOf(
          document.activeElement?.tagName ?? ""
        ) === -1 && !(document.activeElement ?? {})["isContentEditable"];

      if (matchMainShortcut && matchNonkeyboardNode) {
        nativeEvent.preventDefault();
        inputRef.current?.focus();
      }
    };

    document.addEventListener("keydown", handleKeyDown);
    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    };
  }, []);

  const searchQuery = useMemo(
    () =>
      debounce((query: string) => {
        console.log("send search results");
        dispatch(
          getSearchResults({
            query,
          })
        );
      }, 600),
    [dispatch]
  );

  // Change ctrl to cmd
  const macOS = window.navigator.platform.toUpperCase().indexOf("MAC") >= 0;

  return (
    <Stack direction="row" spacing={2}>
      <RootDiv>
        <Autocomplete
          filterOptions={(x) => x}
          options={results}
          fullWidth
          placeholder="Search…"
          aria-label="Search…"
          noOptionsText="No results found"
          onChange={(event, newValue) => {
            setSelectedValue(newValue?.username);
          }}
          onInputChange={(event, newValue) => {
            /* searchQuery(newValue); */
          }}
          getOptionLabel={(x) => x.displayName}
          loading={searchStatus === ELoadingStatus.LOADING}
          onFocus={() => {
            setFocused(true);
          }}
          onBlur={() => {
            setFocused(false);
          }}
          sx={{ mr: 1, fontWeight: "fontWeightBold" }}
          renderOption={(props: any, option, s) => (
            <ListItemButton
              // component={RouterLink}
              {...props}
              // to={`/reports/${option.username}`}
            >
              <ListItemAvatar>
                <Avatar alt={option.displayName} src={option.picture} />
              </ListItemAvatar>
              <ListItemText
                secondary={option.displayName}
                primary={`@${option.username}`}
              />
            </ListItemButton>
          )}
          renderInput={({ InputLabelProps, InputProps, ...inpProps }) => (
            <StyledInput
              sx={{ backgroundColor: "#FFF", p: 1 }}
              disableUnderline
              ref={InputProps.ref}
              inputRef={inputRef}
              endAdornment={InputProps.endAdornment}
              startAdornment={
                <InputAdornment position="start">
                  <SearchIcon
                    sx={{
                      pl: 1,
                    }}
                  />
                </InputAdornment>
              }
              {...inpProps}
            />
          )}
        />

        <Shortcut className={focused ? "Mui-focused" : undefined}>
          {macOS ? "⌘" : "Ctrl+"}K
        </Shortcut>
        {/* </SearchbarStyle> */}
        {/* </Slide> */}
      </RootDiv>
      {isEmpty(selectedValue) ? null : (
        <Button
          size="small"
          disabled={isEmpty(selectedValue)}
          component={RouterLink}
          to={`/reports/${selectedValue}`}
          variant="contained"
        >
          View
        </Button>
      )}
    </Stack>
  );
}
