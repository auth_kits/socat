// material
import { Button, ButtonGroup, Container, Grid } from "@material-ui/core";
import { useState } from "react";
import {
  Link as RouterLink,
  Redirect,
  Switch,
  useRouteMatch,
} from "react-router-dom";
import { PrivateRoute } from "../auth/PrivateRoute";
import CoockieBanner from "../components/CookieBanner";
import { AudienceSearchPage } from "./influencers/components/AudienceSearchPage";
import { SearchPage } from "./influencers/SearchPage";

// ----------------------------------------------------------------------

export function DashboardHomePage() {
  const [current, setCurrent] = useState("influencer");
  const { path, url } = useRouteMatch();
  const rm = useRouteMatch([`${path}/influencer`, `${path}/audience`]);
  return (
    <Container maxWidth={false}>
      <Grid container>
        <Grid
          item
          xs={12}
          sx={{
            pt: 4,
            textAlign: "center",
          }}
        >
          <ButtonGroup variant="outlined">
            <Button
              variant={rm?.url === `${url}/audience` ? "contained" : "outlined"}
              component={RouterLink}
              to={`${path}/audience`}
            >
              Audience Data
            </Button>
            <Button
              variant={
                rm?.url === `${url}/influencer` ? "contained" : "outlined"
              }
              component={RouterLink}
              to={`${path}/influencer`}
            >
              Influencer Discovery
            </Button>
          </ButtonGroup>
        </Grid>
        <Grid
          item
          xs={12}
          sx={{
            pt: 2,
          }}
        >
          <Switch>
            <PrivateRoute path={`${path}`} exact>
              <Redirect to={`${url}/influencer`} />
            </PrivateRoute>
            <PrivateRoute path={`${path}/audience`}>
              <AudienceSearchPage />
            </PrivateRoute>
            <PrivateRoute path={`${path}/influencer`}>
              <SearchPage />
            </PrivateRoute>
          </Switch>
        </Grid>
      </Grid>
      <CoockieBanner />
    </Container>
  );
}
