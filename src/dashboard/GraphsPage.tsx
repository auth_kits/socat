// material
import { Box, Container, Grid, Typography } from "@material-ui/core";
import GraphExample from "./charts/GraphExample";
import { HBarExample } from "./charts/HBarExample";
import { PiExample } from "./charts/PieExample";

// ----------------------------------------------------------------------

export function GraphsPage() {
  return (
    <Box>
      <Container maxWidth="xl">
        <Box sx={{ pb: 5 }}>
          <Typography variant="h4">Hi, Welcome back</Typography>
        </Box>
        <Grid container spacing={3}>
          <Grid item xs={12} md={6} lg={8}>
            <GraphExample />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <PiExample />
          </Grid>

          <Grid item xs={12} md={6} lg={8}>
            <HBarExample />
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
}
