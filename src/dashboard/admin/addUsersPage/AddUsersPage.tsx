
import { Button, Container, FormControl, Grid, InputLabel, MenuItem, Paper, Select, Stack, TextField, Typography } from "@material-ui/core";
import { useEffect, useState } from "react";
import { useParams, useHistory } from "react-router-dom";

import { Controller, SubmitHandler, useForm } from "react-hook-form";
import '../Admin.scss';

/* eslint-disable camelcase */
interface userObj {
  user_name: string;
  user_email: string;
  password: string;
  user_role_id: string;
  phoneNo: string;
  country: string;
  currency: string;
  status: string;
  user_id?: string;
  user_role?: string;
}

interface UrlKey {
  userId: string;
}

export const AddUsersPage: React.FC = (props) => {
  const history = useHistory();
  const [userDetail, setUserDetail] = useState<userObj>({
    user_name: '',
    user_email: '',
    password: '',
    user_role_id: '',
    phoneNo: '',
    country: '',
    currency: '',
    status: '',
    user_role: '',
  });
  const [pageTitle, setPageTitle] = useState<string>('Add User');
  const params = useParams<UrlKey>();

  const { control, handleSubmit, reset, register,
    formState: { isDirty, isValid, errors },
  } = useForm<userObj>();


  const selectFeild = (e) => {
    const { name, value } = e.target;
    setUserDetail((pre) => ({ ...pre, [name]: value }))
    console.log(name, value, userDetail)
  }
  const userFormSubmit: SubmitHandler<userObj> = async (data) => {
    const formdata = new FormData();
    let crUsr = 'create_user'
    if (userDetail.user_id) {
      formdata.append("editid", userDetail.user_id);
      crUsr = 'edit_user';
    } else {
      formdata.append("password", data.password);
    }

    formdata.append("user_email", data.user_email);
    formdata.append("user_name", data.user_name);
    formdata.append("user_role_id", data.user_role_id);

    formdata.append("phoneNo", data.phoneNo);
    formdata.append("country", userDetail.country);
    formdata.append("currency", userDetail.currency);
    formdata.append("status", userDetail.status);

    const url = `${process.env.REACT_APP_BASE_BACKEND_URL}/admin/${crUsr}`;
    const token = localStorage?.getItem(process.env.REACT_APP_TOKEN_NAME ?? "");
    const myHeaders = new Headers();
    myHeaders.append("Authorization", token || '');
    myHeaders.append("Cookie", "ci_session=3aa57532ad6cd29e77a378a1944002e78f5152e3");
    const res = await fetch(url, {
      headers: myHeaders,
      method: "POST",
      body: formdata,
    })

    if (res.status === 200) {
      history.push({
        pathname: `users`,
      })
      reset({
        user_name: '',
        user_email: '',
        password: '',
        user_role_id: '',
        phoneNo: '',
        country: '',
        currency: '',
        status: '',
      })
    }

  }

  useEffect(() => {
    if (params.userId) {
      setPageTitle('Edit User')
      const user = localStorage.getItem('userData');
      if (user) {
        setUserDetail(JSON.parse(user))
        console.log('JSON.parse(user)', JSON.parse(user))
        reset(JSON.parse(user))
      }
    } else {
      localStorage.removeItem('userData')
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params.userId])

  // console.log('userDetail', userDetail)

  return (
    <>
      <Container>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          mb={5}
        >
          <Typography variant="h4" gutterBottom>
            {pageTitle}
          </Typography>

        </Stack>
        <form onSubmit={handleSubmit(userFormSubmit)}>
          <Grid container spacing={2}>

            <Grid item xs={12} md={5}>

              <Controller
                name="user_name"
                control={control}
                rules={{
                  required: true,
                }}
                render={({ field }) => (
                  <TextField
                    fullWidth
                    label="User Name"
                    autoComplete="off"
                    {...field}
                  />
                )}
              />

              {errors.user_name &&
                <Typography sx={{ color: "#f51700" }}>
                  Enter your user Name.
                </Typography>
              }
            </Grid>
            <Grid item xs={12} md={5}>
              <Controller
                name="phoneNo"
                control={control}
                rules={{
                  required: true,
                }}
                render={({ field }) => (
                  <TextField
                    fullWidth
                    label="Phone No"
                    {...field}
                    onInput={(e: any) => {
                      e.target.value = Math.max(0, parseInt(e.target.value, 10)).toString().slice(0, 10)
                    }}
                  />
                )}
              />
              {errors.phoneNo &&
                <Typography sx={{ color: "#f51700" }}>
                  Enter user phone number.
                </Typography>
              }
            </Grid>
            <Grid item xs={12} md={5}>
              <Controller
                name="user_email"
                control={control}
                rules={{
                  required: true,
                }}
                render={({ field }) => (
                  <TextField
                    fullWidth
                    label="User Email"
                    {...field}
                  />
                )}
              />
              {errors.user_email &&
                <Typography sx={{ color: "#f51700" }}>
                  Enter User Email Id.
                </Typography>
              }
            </Grid>
            {!params.userId &&
              <Grid item xs={12} md={5}>
                <Controller
                  name="password"
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      type="password"
                      fullWidth

                      label="Password"
                      {...field}
                    />
                  )}
                />
                {errors.password &&
                  <Typography sx={{ color: "#f51700" }}>
                    Enter User password.
                  </Typography>
                }
              </Grid>
            }
            <Grid item xs={12} md={5}>
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Select role</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Select role"
                  value={userDetail.user_role}
                  name="user_role_id"
                >
                  <MenuItem value="1">Admin</MenuItem>
                  <MenuItem value="0">Moderator</MenuItem>
                  <MenuItem value="2">Normal User</MenuItem>
                </Select>

              </FormControl>
              {errors.user_role_id &&
                <Typography sx={{ color: "#f51700" }}>
                  Please select role.
                </Typography>
              }
            </Grid>


            <Grid item xs={12} md={5}>
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Country</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Select Country"
                  value={userDetail.country}
                  onChange={selectFeild}
                  name="country"
                >
                  <MenuItem value="india">India</MenuItem>
                  <MenuItem value="indonesia">Indonesia</MenuItem>
                  <MenuItem value="malesia">Malesia</MenuItem>
                  <MenuItem value="singapur">Singapur</MenuItem>
                  <MenuItem value="dubai">Dubai</MenuItem>
                </Select>

              </FormControl>
              {errors.country &&
                <Typography sx={{ color: "#f51700" }}>
                  Please select country.
                </Typography>
              }
            </Grid>

            <Grid item xs={12} md={5}>

              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Currency </InputLabel>
                <Select
                  label="Select Currency"
                  value={userDetail.currency}
                  name="currency"
                  onChange={selectFeild}
                >
                  <MenuItem value="inr">India</MenuItem>
                  <MenuItem value="idr">Indonesia</MenuItem>
                  <MenuItem value="myr">Malesia</MenuItem>
                  <MenuItem value="sgd">Singapur</MenuItem>
                  <MenuItem value="aed">Dubai</MenuItem>
                </Select>

              </FormControl>
              {errors.currency &&
                <Typography sx={{ color: "#f51700" }}>
                  Please select currency.
                </Typography>
              }
            </Grid>
            <Grid item xs={12} md={5}>
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Status</InputLabel>

                <Select
                  defaultValue={userDetail.status}
                  label="Select role"
                  value={userDetail.status}
                  name="status"
                  onChange={selectFeild}
                >
                  <MenuItem value="0">Pending</MenuItem>
                  <MenuItem value="1">Approve</MenuItem>
                  <MenuItem value="2">Disable</MenuItem>
                </Select>
              </FormControl>
              {errors.status &&
                <Typography sx={{ color: "#f51700" }}>
                  Please select user status.
                </Typography>
              }
            </Grid>
            <Grid item xs={12} md={12}>
              <Button type="submit" variant="contained" size="large">Submit</Button>
            </Grid>
          </Grid>
        </form>

      </Container>
    </>
  );
}
