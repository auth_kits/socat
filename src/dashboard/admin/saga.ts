import combineSaga from "../../utils/sagaCreator";
import watchAdminUserSaga from "./usersPage/saga";

export const adminSaga = combineSaga([watchAdminUserSaga]);
