import { combineReducers } from "@reduxjs/toolkit";
import userReducer from "./usersPage/reducer";

const adminReducer = combineReducers({
  user: userReducer,
});

export default adminReducer;
