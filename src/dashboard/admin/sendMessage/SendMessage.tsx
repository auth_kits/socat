
import { Button, Container, Grid, Stack, TextField, Typography } from "@material-ui/core";
import { useState } from "react";
import { useParams } from "react-router-dom";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import '../Admin.scss';

/* eslint-disable camelcase */
interface IFormInput {
  message_title: string;
  message: string;
  user_id?: string;
}
interface UrlKey {
  userId: string;
}

export const SendMessage: React.FC = (props) => {

  const params = useParams<UrlKey>();

  const [state, setSate] = useState()
  const {
    control,
    handleSubmit,
    formState: { isDirty, isValid },
  } = useForm<IFormInput>();

  const senMessage: SubmitHandler<IFormInput> = async (data) => {

    const formdata = new FormData();

    if (params.userId) {
      formdata.append("user_id", params.userId);
    }

    formdata.append("message_title", data.message_title);
    formdata.append("message", data.message);
    const url = `${process.env.REACT_APP_BASE_BACKEND_URL}/admin/add_message`;
    const token = localStorage?.getItem(process.env.REACT_APP_TOKEN_NAME ?? "");
    const myHeaders = new Headers();
    myHeaders.append("Authorization", token || '');
    myHeaders.append("Cookie", "ci_session=3aa57532ad6cd29e77a378a1944002e78f5152e3");
    const res = await fetch(url, {
      headers: myHeaders,
      method: "POST",
      body: formdata,
    })
    console.log(res)
    // if (res.status === 200) {

    //   alert('Message successfully sent!');
    // }
  }


  return (
    <>
      <Container>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          mb={5}
        >
          <Typography variant="h4" gutterBottom>
            Send Message
          </Typography>

        </Stack>
        <form onSubmit={handleSubmit(senMessage)}>
          <Grid container spacing={2}>
            <Grid item xs={12} md={12}>
              <Controller
                name="message_title"
                control={control}
                defaultValue=""
                rules={{
                  required: true,
                }}
                render={({ field }) => (
                  <TextField
                    fullWidth
                    id="outlined-required"
                    label="Title"
                    {...field}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} md={12}>
              <Controller
                name="message"
                control={control}
                defaultValue=""
                rules={{
                  required: true,
                }}
                render={({ field }) => (
                  <TextField
                    fullWidth
                    multiline
                    rows={5}
                    id="outlined-required"
                    label="Message"
                    {...field}
                  />
                )}
              />
            </Grid>

            <Grid item xs={12} md={12}>
              <Button variant="contained" type="submit" size="large">Send</Button>
            </Grid>

          </Grid>
        </form>
      </Container>
    </>
  );
}
