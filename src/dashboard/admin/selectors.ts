import { RootState } from "../../app/store";

export const selectAdminState = ({ admin }: RootState) => admin;
