import { all, call, put, select, takeLatest } from "redux-saga/effects";
import { getApi } from "../../../app/axios";
import * as types from "./action";
import { selectAllUsersParams } from "./selectors";

function* addUserSaga({ payload }: ReturnType<typeof types.addUser>) {
  try {
    const { data } = yield call(getApi().post, "/admin/users/new", {
      ...payload,
    });
    yield put(types.addUserSuccess(data));
  } catch (error) {
    yield put(types.addUserError(error));
  }
}

function* getAllUsersSaga() {
  try {
    const params = yield select(selectAllUsersParams);
    const { data } = yield call(getApi().get, "/admin/users", params);
    yield put(
      types.getAllUsersSuccess({
        users: data,
      })
    );
  } catch (error) {
    console.log(error);
  }
}

function* editUserSaga({
  payload: { id, ...userDetails },
}: ReturnType<typeof types.editUser>) {
  try {
    const { data } = yield call(getApi().put, "/admin/user/edit", {
      id,
      changes: userDetails,
    });
    yield put(
      types.editUserSuccess({
        id: data.userId,
        email: data.email,
        name: data.name,
        roles: data.roles,
        phone: data.phone,
      })
    );
  } catch (error) {
    yield put(
      types.editUserError({
        message: error.message,
      })
    );
  }
}

function* setNewPageSaga(action: ReturnType<typeof types.setNewPage>) {
  // fetch again
  yield put(types.getAllUsers());
}

export default function* watchAdminUserSaga() {
  yield all([
    takeLatest(types.addUser, addUserSaga),
    takeLatest(types.editUser, editUserSaga),
    takeLatest(types.getAllUsers, getAllUsersSaga),
    takeLatest(types.setNewPage, setNewPageSaga),
  ]);
}
