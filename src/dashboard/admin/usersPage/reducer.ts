import {
  createEntityAdapter,
  createReducer,
  EntityState,
} from "@reduxjs/toolkit";
import {
  addUser,
  addUserError,
  addUserSuccess,
  ELoadingStatus,
  resetAdminUser,
  setNewPage,
  setRowsPerPage,
  editUser,
  editUserError,
  editUserSuccess,
  UserDetail,
} from "./action";

export interface AdminUserState extends EntityState<UserDetail> {
  errors: any;
  status: ELoadingStatus;
  itemsPerPage: number;
  startFrom: number;
  totalItems: number;
}

export const adapter = createEntityAdapter<UserDetail>({
  selectId: (user) => user.id,
  sortComparer: (a, b) => a.email.localeCompare(b.email),
});

export default createReducer<AdminUserState>(
  adapter.getInitialState({
    errors: null,
    status: ELoadingStatus.INITIAL,
    itemsPerPage: 20,
    startFrom: 0,
    totalItems: 0,
  }),
  (builder) => {
    builder.addCase(setRowsPerPage, (state, { payload: { noOfRows } }) => {
      state.itemsPerPage = noOfRows;
    });
    builder.addCase(resetAdminUser, (state) => {
      state.errors = null;
      state.status = ELoadingStatus.INITIAL;
    });

    builder.addCase(addUser, (state) => {
      state.status = ELoadingStatus.LOADING;
    });
    builder.addCase(addUserSuccess, (state, { payload }) => {
      adapter.addOne(state, {
        email: payload.email,
        id: payload.id,
        imageUrl: payload.imageUrl,
        name: payload.name,
        phone: payload.phone,
        roles: payload.roles,
      });
      state.status = ELoadingStatus.SUCCESS;
    });
    builder.addCase(addUserError, (state, { payload }) => {
      state.status = ELoadingStatus.ERROR;
      state.errors = payload.message;
    });

    builder.addCase(editUser, (state) => {
      state.status = ELoadingStatus.LOADING;
    });

    builder.addCase(setNewPage, (state, { payload: { pageNo } }) => {
      state.startFrom = pageNo * state.itemsPerPage;
    });

    builder.addCase(editUserSuccess, (state, { payload: { id, roles } }) => {
      adapter.updateOne(state, {
        id,
        changes: {
          roles,
        },
      });
      state.status = ELoadingStatus.SUCCESS;
    });

    builder.addCase(editUserError, (state, { payload: { message } }) => {
      state.errors = message;
    });
  }
);
