import {
  Button,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  InputLabel,
  MenuItem,
  OutlinedInput,
  Select,
  TextField,
} from "@material-ui/core";
import { LoadingButton } from "@material-ui/lab";
import { Box } from "@material-ui/system";
import { isEmpty } from "lodash";
import React, { useState } from "react";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { UserRole } from "../../../auth/roles";
import { editUser, ELoadingStatus, UserDetail } from "./action";
import { selectStatus } from "./selectors";

type IFormInput = UserDetail;

interface EditUserFormProps {
  userId?: string;
  onClose?: () => void;
}

export const EditUserForm: React.FC<EditUserFormProps> = ({
  userId,
  onClose,
}) => {
  const dispatch = useAppDispatch();
  const networkStatus = useAppSelector(selectStatus);

  const {
    control,
    handleSubmit,
    formState: { isDirty, isValid },
    reset,
  } = useForm<IFormInput>();
  const onEditUser: SubmitHandler<IFormInput> = (data) => {
    dispatch(editUser);
  };

  const handleClose = () => {
    if (onClose) {
      onClose();
    }
  };

  return (
    <Dialog open={!isEmpty(userId)}>
      <form onSubmit={handleSubmit(onEditUser)}>
        <DialogTitle>Edit User</DialogTitle>
        <DialogContent>
          <DialogContentText />
          <Controller
            control={control}
            name="roles"
            rules={{
              required: true,
            }}
            defaultValue={[]}
            render={({ field }) => (
              <FormControl fullWidth margin="dense" variant="outlined">
                <InputLabel
                  style={{ background: "white" }}
                  margin="dense"
                  variant="outlined"
                  id="user-roles"
                >
                  Roles
                </InputLabel>
                <Select
                  labelId="user-roles"
                  id="roles-select"
                  multiple
                  variant="outlined"
                  margin="dense"
                  defaultValue={[]}
                  renderValue={(selected: any[]) => (
                    <Box sx={{ display: "flex", flexWrap: "wrap" }}>
                      {selected?.map((value) => (
                        <Chip key={value} label={value} sx={{ m: "2px" }} />
                      ))}
                    </Box>
                  )}
                  input={<OutlinedInput id="selected-roles" margin="dense" />}
                >
                  {Object.keys(UserRole).map((name) => (
                    <MenuItem key={name} value={name}>
                      {name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            )}
          />
        </DialogContent>
        <DialogActions>
          {networkStatus !== ELoadingStatus.LOADING && (
            <Button onClick={handleClose}>Cancel</Button>
          )}
          <LoadingButton
            size="large"
            variant="contained"
            loading={networkStatus === ELoadingStatus.LOADING}
            disabled={!isDirty || !isValid}
            type="submit"
          >
            Add Roles
          </LoadingButton>
        </DialogActions>
      </form>
    </Dialog>
  );
};
