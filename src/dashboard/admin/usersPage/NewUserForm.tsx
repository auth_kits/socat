import {
  Box,
  Button,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  InputLabel,
  MenuItem,
  OutlinedInput,
  Select,
  TextField,
} from "@material-ui/core";
import AddRoundedIcon from "@material-ui/icons/AddRounded";
import { LoadingButton } from "@material-ui/lab";
import React, { useEffect, useState } from "react";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { UserRole } from "../../../auth/roles";
import { addUser, ELoadingStatus, resetAdminUser } from "./action";
import { selectStatus } from "./selectors";

interface IFormInput {
  name: string;
  email: string;
  roles: UserRole[];
  // phone: string;
  // country: string;
  // address: string;
}
export const NewUserForm: React.FC = () => {
  const [open, setOpen] = useState(false);
  const {
    control,
    handleSubmit,
    formState: { isDirty, isValid },
    reset,
  } = useForm<IFormInput>();
  const dispatch = useAppDispatch();

  const networkStatus = useAppSelector(selectStatus);

  useEffect(() => {
    if (open) {
      dispatch(resetAdminUser());
      reset();
    }
  }, [dispatch, open, reset]);

  useEffect(() => {
    if (networkStatus === ELoadingStatus.SUCCESS) {
      handleClose();
    }
  }, [networkStatus]);
  const handleClose = () => setOpen(false);

  const onAddUser: SubmitHandler<IFormInput> = (data) => {
    dispatch(
      addUser({
        roles: data.roles,
        // address: data.address,
        name: data.name,
        email: data.email,
        // phoneNo: data.phone,
      })
    );
  };

  return (
    <>
      <Button
        variant="contained"
        onClick={() => {
          setOpen(true);
        }}
        startIcon={<AddRoundedIcon />}
      >
        New User
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <form onSubmit={handleSubmit(onAddUser)}>
          <DialogTitle>New User</DialogTitle>
          <DialogContent>
            <DialogContentText>
              A new verified user will be created and an email will be sent to
              the user asking for more details.
            </DialogContentText>
            {/* <Controller
              name="name"
              control={control}
              rules={{
                required: true,
              }}
              render={({ field }) => (
                <TextField
                  fullWidth
                  margin="dense"
                  id="name"
                  label="Name"
                  variant="outlined"
                  {...field}
                />
              )}
            /> */}
            <Controller
              name="email"
              control={control}
              rules={{
                required: true,
              }}
              render={({ field }) => (
                <TextField
                  margin="dense"
                  id="name"
                  type="email"
                  fullWidth
                  label="Email"
                  variant="outlined"
                  {...field}
                />
              )}
            />
            <Controller
              name="roles"
              rules={{ required: true }}
              control={control}
              defaultValue={[]}
              render={({ field }) => (
                <FormControl fullWidth margin="dense" variant="outlined">
                  <InputLabel
                    style={{ background: "white" }}
                    margin="dense"
                    variant="outlined"
                    id="user-roles"
                  >
                    Roles
                  </InputLabel>
                  <Select
                    labelId="user-roles"
                    id="roles-select"
                    multiple
                    variant="outlined"
                    margin="dense"
                    input={<OutlinedInput id="selected-roles" margin="dense" />}
                    renderValue={(selected) => (
                      <Box sx={{ display: "flex", flexWrap: "wrap" }}>
                        {selected.map((value) => (
                          <Chip key={value} label={value} sx={{ m: "2px" }} />
                        ))}
                      </Box>
                    )}
                    {...field}
                  >
                    {Object.keys(UserRole).map((name) => (
                      <MenuItem key={name} value={name}>
                        {name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              )}
            />
            {/* <Controller
              name="phone"
              rules={{
                required: true,
              }}
              control={control}
              render={({ field }) => (
                <TextField
                  fullWidth
                  type="phone"
                  margin="dense"
                  id="name"
                  label="Phone no"
                  variant="outlined"
                  {...field}
                />
              )}
            /> */}
            {/* <Controller
              name="country"
              rules={{
                required: true,
              }}
              control={control}
              render={({ field }) => (
                <TextField
                  fullWidth
                  label="Country"
                  variant="outlined"
                  margin="dense"
                  id="name"
                  {...field}
                />
              )}
            />
            <Controller
              name="address"
              rules={{
                required: true,
              }}
              control={control}
              render={({ field }) => (
                <TextField
                  fullWidth
                  label="Address"
                  variant="outlined"
                  margin="dense"
                  id="name"
                  {...field}
                />
              )}
            /> */}
          </DialogContent>
          <DialogActions>
            {networkStatus !== ELoadingStatus.LOADING && (
              <Button onClick={handleClose}>Cancel</Button>
            )}
            <LoadingButton
              size="large"
              variant="contained"
              loading={networkStatus === ELoadingStatus.LOADING}
              disabled={!isDirty || !isValid}
              type="submit"
            >
              Add user
            </LoadingButton>
          </DialogActions>
        </form>
      </Dialog>
    </>
  );
};
