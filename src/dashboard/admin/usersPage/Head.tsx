import {
  Box,
  Checkbox,
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
} from "@material-ui/core";
import { visuallyHidden } from "@material-ui/utils";
import React from "react";

export interface IHeadCell {
  id: string;
  label: string;
  alignRight: boolean;
}

interface HeadProps {
  order: any;
  orderBy: string;
  rowCount: number;
  headLabel: IHeadCell[];
  /* this is mandatory when passing onSelectAllClick */
  numSelected?: number;
  onRequestSort: (event: React.MouseEvent, property: string) => void;
  onSelectAllClick?: (event: any) => void;
}

export const Head: React.FC<HeadProps> = ({
  order,
  orderBy,
  rowCount,
  headLabel,
  numSelected = 0,
  onRequestSort,
  onSelectAllClick,
}) => {
  const createSortHandler =
    (property: string): React.MouseEventHandler<HTMLAnchorElement> =>
    (event) => {
      onRequestSort(event, property);
    };
  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          {onSelectAllClick && (
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          )}
        </TableCell>
        {headLabel.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.alignRight ? "right" : "left"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              hideSortIcon
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box sx={{ ...visuallyHidden }}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};
