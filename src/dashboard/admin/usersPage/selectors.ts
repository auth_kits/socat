import { createSelector } from "@reduxjs/toolkit";
import { selectAdminState } from "../selectors";
import { adapter } from "./reducer";

const selectUserState = createSelector(selectAdminState, ({ user }) => user);

const userSelectors = adapter.getSelectors();

export const selectStatus = createSelector(
  selectUserState,
  ({ status }) => status
);

export const selectAllUsers = createSelector(
  selectUserState,
  userSelectors.selectAll
);

export const selectAllUsersParams = createSelector(
  selectUserState,
  (state) => ({
    offset: state.startFrom,
    itemsPerPage: state.itemsPerPage,
  })
);

export const selectTotalUsers = createSelector(
  selectUserState,
  (state) => state.totalItems ?? 0
);

export const selectTotalItems = createSelector(
  selectUserState,
  (state) => state.totalItems
);

export const selectRowsPerPage = createSelector(
  selectUserState,
  (state) => state.itemsPerPage ?? 15
);

export const selectPage = createSelector(
  selectTotalItems,
  selectRowsPerPage,
  (totalItems, noOfRows) => (totalItems / noOfRows) | 0
);
