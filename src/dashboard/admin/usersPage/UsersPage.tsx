// material
import {
  Avatar,
  Box,
  Button,
  Card,
  Grid,
  Container,
  IconButton,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
  Typography,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "@material-ui/core";
import { useHistory } from "react-router-dom";
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import CreateRoundedIcon from '@material-ui/icons/CreateRounded';
import SendIcon from '@material-ui/icons/Send';
import AddRoundedIcon from "@material-ui/icons/AddRounded";
import SearchIcon from '@material-ui/icons/Search';
import { sentenceCase } from "change-case";
import { filter } from "lodash";
import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { Label } from "../../../components/Label";
import { Scrollbar } from "../../../components/Scrollbar";
import { setNewPage, setRowsPerPage } from "./action";
import { EditUserForm } from "./EditUserForm";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
// components
// import { UserListHead, UserListToolbar, UserMoreMenu } from '../components/_dashboard/user';
//
import { Head, IHeadCell } from "./Head";
import { NewUserForm } from "./NewUserForm";
import {
  selectAllUsers,
  selectPage,
  selectRowsPerPage,
  selectTotalUsers,
} from "./selectors";

// import Slide from '@mui/material/Slide';
// import { TransitionProps } from '@mui/material/transitions';
// ----------------------------------------------------------------------
/* eslint-disable camelcase */
export interface Ulist {
  user_name: string,
  user_email: string,
  user_role: string,
  user_id: string,
  phoneNo: string,
  userName: string,
  country: string,
  currency: string
  status: string
}
export interface creditObj {
  credit: string
}
const TABLE_HEAD: IHeadCell[] = [
  { id: "name", label: "Name", alignRight: false },
  { id: "company", label: "Company", alignRight: false },
  { id: "role", label: "Role", alignRight: false },
  { id: "isVerified", label: "Verified", alignRight: false },
  { id: "status", label: "Status", alignRight: false },
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export function UsersPage() {

  const history = useHistory();
  const dispatch = useAppDispatch();
  const page = useAppSelector(selectPage);

  const [modelType, setModelType] = useState()
  const [order, setOrder] = useState("asc");
  const [selected, setSelected] = useState<string>();
  const [orderBy, setOrderBy] = useState("name");
  const [filterName, setFilterName] = useState("");
  const [userDeleteId, setuserDeleteId] = useState<string>("");
  const rowsPerPage = useAppSelector(selectRowsPerPage);
  // const userList = useAppSelector(selectAllUsers);
  const [userList, setUserList] = useState<Ulist[]>([]);
  const totalItems = useAppSelector(selectTotalUsers);
  const [open, setOpen] = useState(false);
  const handleRequestSort = (_, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const { control, handleSubmit,
    formState: { isDirty, isValid, errors },
  } = useForm<creditObj>();

  const creditFormSubmit: SubmitHandler<creditObj> = async (data) => {
    console.log('creait Data', data)
  }

  const status = "banned";

  const handleClick = (_e, userId) => {
    console.log(userId, selected);
    setSelected((selectedId) => (userId === selectedId ? undefined : userId));
  };

  const handleClickOpen = (mt, usrObj?) => {
    setModelType(mt)
    if (usrObj) {
      setuserDeleteId(usrObj.user_id)
    }
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  // const handleClickOld = (_, name) => {
  //   const selectedIndex = selected.indexOf(name);
  //   let newSelected: any[] = [];
  //   if (selectedIndex === -1) {
  //     newSelected = newSelected.concat(selected, name);
  //   } else if (selectedIndex === 0) {
  //     newSelected = newSelected.concat(selected.slice(1));
  //   } else if (selectedIndex === selected.length - 1) {
  //     newSelected = newSelected.concat(selected.slice(0, -1));
  //   } else if (selectedIndex > 0) {
  //     newSelected = newSelected.concat(
  //       selected.slice(0, selectedIndex),
  //       selected.slice(selectedIndex + 1)
  //     );
  //   }
  //   // setSelected(newSelected);
  // };

  const handleChangePage = (_, newPage) => {
    dispatch(setNewPage(newPage));
  };

  const handleChangeRowsPerPage = (event) => {
    dispatch(
      setRowsPerPage({
        noOfRows: parseInt(event.target.value, 10),
      })
    );
  };

  const deleteHandler = async () => {
    console.log('userDeleteId', userDeleteId)
    const url = `${process.env.REACT_APP_BASE_BACKEND_URL}/admin/remove_user`;
    const token = localStorage?.getItem(process.env.REACT_APP_TOKEN_NAME ?? "");
    const myHeaders = new Headers();
    myHeaders.append("Authorization", token || '');
    myHeaders.append("Cookie", "ci_session=3aa57532ad6cd29e77a378a1944002e78f5152e3");
    const formdata = new FormData();
    formdata.append("user_id", userDeleteId);
    const res = await fetch(url, {
      headers: myHeaders,
      method: "POST",
      body: formdata,
    })

    if (res.status === 200) {
      setOpen(false);
      getUserList();

      // const ii = userList.findIndex((ee) => ee.user_id === userDeleteId);
      // setUserList(pre => {
      //   return { ...pre.filter((img, i) => i !== ii) }
      // });
    }

  }

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - userList.length) : 0;

  // const filteredUsers = applySortFilter(
  //   userList,
  //   getComparator(order, orderBy),
  //   filterName
  // );

  const isUserNotFound = userList?.length === 0;

  const getUserList = async () => {

    const url = `${process.env.REACT_APP_BASE_BACKEND_URL}/admin/all_users`;
    const token = localStorage?.getItem(process.env.REACT_APP_TOKEN_NAME ?? "");
    const res = await fetch(url, {
      headers: {
        Authorization: `${token}`,
        "Content-Type": "application/json",
      },
      method: "GET",
    })
    const userData = res.json()
    userData.then(data => {
      setUserList(data.data)
    })
      .catch(error => {
        console.log(error)
      })
  }

  const editClickHandler = (user, type) => {
    if (type === 'edit') {
      localStorage.setItem('userData', JSON.stringify(user))
      history.push({
        pathname: `edit-user/${user.user_id}`,
      })
    } else if (type === 'msg') {
      history.push({
        pathname: `send-message/${user.user_id}`,
      })
    }
  }

  useEffect(() => {
    getUserList()
    localStorage.removeItem('userData');
  }, [])

  return (
    <>
      <Box>
        <Container maxWidth="xl">
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="space-between"
            mb={5}
          >
            <Typography variant="h4" gutterBottom>
              User
            </Typography>
            <div className="button-actions">
              <Button
                variant="contained"
                startIcon={<AddRoundedIcon />}
                onClick={() =>
                  history.push({
                    pathname: "add-user",
                  })
                }
              >
                Create User
              </Button>
              <Button
                variant="contained"
                startIcon={<SearchIcon />}
                onClick={() => handleClickOpen('countForm')}
              >
                Audiance search
              </Button>
              <Button
                variant="contained"
                startIcon={<SearchIcon />}
                onClick={() => handleClickOpen('countForm')}
              >
                Influencer User
              </Button>
            </div>
            {/* <NewUserForm /> */}
          </Stack>

          <Card>
            <Scrollbar>
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Name</TableCell>
                      <TableCell >Email</TableCell>
                      <TableCell >Phone</TableCell>
                      <TableCell >Country</TableCell>
                      <TableCell >Currency</TableCell>
                      <TableCell >Role</TableCell>
                      <TableCell >Status</TableCell>
                      <TableCell align="center">Action</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {userList.map((user, index) => (
                      <TableRow
                        key={user.user_id}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell component="th" scope="row">
                          {user?.user_name}
                        </TableCell>

                        <TableCell> {user?.user_email} </TableCell>
                        <TableCell>{user?.phoneNo}</TableCell>
                        <TableCell style={{ textTransform: "capitalize" }}>{user?.country}</TableCell>
                        <TableCell style={{ textTransform: "uppercase" }}>{user?.currency}</TableCell>
                        <TableCell>{user?.user_role === '0' && 'Super Admin' || user?.user_role === '1' && 'Admin' || user?.user_role === '2' && 'User'}</TableCell>
                        <TableCell>{user?.status === '0' && 'Pending' || user?.status === '1' && 'Approve' || user?.status === '2' && 'Disable'}</TableCell>
                        <TableCell align="center">

                          <IconButton onClick={() => editClickHandler(user, 'edit')}>
                            <CreateRoundedIcon />
                          </IconButton>

                          <IconButton onClick={() => handleClickOpen('delete', user)}>
                            <DeleteRoundedIcon sx={{ color: '#e41500' }} />
                          </IconButton>

                          <IconButton onClick={() => editClickHandler(user, 'msg')}>
                            <SendIcon sx={{ color: '#007aff' }} />
                          </IconButton>

                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>

                  {/* 
                <Head
                  order={order}
                  orderBy={orderBy}
                  headLabel={TABLE_HEAD}
                  rowCount={userList.length}
                  onRequestSort={handleRequestSort}
                /> 
                <TableBody>
                  {userList?.map((row) => {
                    const { id, name, roles, imageUrl: avatarUrl } = row;
                    const isItemSelected = selected === id;
                    const company = "Company value";
                    const isVerified = true;
                    return (
                      <TableRow
                        hover
                        key={id}
                        tabIndex={-1}
                        role="checkbox"
                        selected={isItemSelected}
                        aria-checked={isItemSelected}
                        onClick={(event) => handleClick(event, id)}
                      >
                        <TableCell padding="checkbox">
                          <Checkbox
                            checked={isItemSelected}
                            onChange={(event) => handleClick(event, name)}
                          />
                        </TableCell>
                        <TableCell component="th" scope="row" padding="none">
                          <Stack
                            direction="row"
                            alignItems="center"
                            spacing={2}
                          >
                            <Avatar alt={name} src={avatarUrl} />
                            <Typography variant="subtitle2" noWrap>
                              {name}
                            </Typography>
                          </Stack>
                        </TableCell>
                        <TableCell align="left">{company}</TableCell>
                        <TableCell align="left">helo</TableCell>
                        <TableCell align="left">
                          {isVerified ? "Yes" : "No"}
                        </TableCell>
                        <TableCell align="left">
                          <Label
                            variant="ghost"
                            color={status === "banned" ? "error" : "success"}
                          >
                            {sentenceCase(status)}
                          </Label>
                        </TableCell>

                        <TableCell align="right">
                          <UserMoreMenu />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                {isUserNotFound && (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                        {/* <SearchNotFound searchQuery={filterName} /> 
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )} */}
                </Table>
              </TableContainer>
            </Scrollbar>

            <TablePagination
              rowsPerPageOptions={[10]}
              component="div"
              count={userList.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Card>
        </Container>
      </Box>
      <Dialog
        open={open}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
      >
        {modelType === 'delete' ?
          <>
            <DialogTitle>Delete?</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-slide-description">
                Are you sure you want to delete this user?
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>No</Button>
              <Button onClick={deleteHandler}>Yes</Button>
            </DialogActions>
          </> :
          <div className="credit-form">

            <form onSubmit={handleSubmit(creditFormSubmit)}>
              <Grid container spacing={2}>

                <Controller
                  name="credit"
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      fullWidth
                      label="Credit"
                      autoComplete="off"
                      {...field}
                    />
                  )}
                />

                <Grid style={{ margin: '10px 0 0 0' }}>
                  <Button type="submit" variant="contained" size="large">Save</Button>
                </Grid>
              </Grid>
            </form>
          </div>
        }



      </Dialog>
    </>
  );
}
