import { createAction } from "@reduxjs/toolkit";
import { UserInfo } from "../../../auth/action";
import { UserRole } from "../../../auth/roles";

const PREFIX = "admin/user";

export enum ELoadingStatus {
  INITIAL,
  LOADING,
  SUCCESS,
  ERROR,
}

export interface UserDetail extends UserInfo {
  id: string;
  phone: string;
}
export const resetAdminUser = createAction(`${PREFIX}/resetAdminUser`);

export const getAllUsers = createAction(`${PREFIX}/getAllUsers`);
interface GetAllUsersSuccessPayload {
  users: UserDetail[];
}
export const getAllUsersSuccess = createAction<GetAllUsersSuccessPayload>(
  `${PREFIX}/getAllUsersSuccess`
);

interface GetAllUsersError {
  message: string;
}
export const getAllUsersError = createAction<GetAllUsersError>(
  `${PREFIX}/getAllUsersError`
);

interface SetRowsPerPagePayload {
  noOfRows: number;
}
export const setRowsPerPage = createAction<SetRowsPerPagePayload>(
  `${PREFIX}/setRowsPerPage`
);

interface SetNewPagePayload {
  pageNo: number;
}
export const setNewPage = createAction<SetNewPagePayload>(
  `${PREFIX}/setNewPage`
);

interface AddUserPayload {
  roles: UserRole[];
  email: string;
  name?: string;
  phoneNo?: string;
  address?: string;
}
export const addUser = createAction<AddUserPayload>(`${PREFIX}/addUser`);

interface AddUserSuccess {
  roles: UserRole[];
  imageUrl: string;
  email: string;
  name: string;
  phone: string;
  address: string;
  id: string;
}
export const addUserSuccess = createAction<AddUserSuccess>(
  `${PREFIX}/adddUserSuccess`
);

interface AddUserError {
  message: string;
}
export const addUserError = createAction<AddUserError>(
  `${PREFIX}/addUserError`
);

interface EditUserPayload {
  id: string;
  changes: Partial<UserDetail>;
}
export const editUser = createAction<EditUserPayload>(`${PREFIX}/editUser`);

type EditUserSuccessPayload = UserDetail
export const editUserSuccess = createAction<EditUserSuccessPayload>(
  `${PREFIX}/editUserSuccess`
);

interface EditUserErrorPayload {
  message: string;
}
export const editUserError = createAction<EditUserErrorPayload>(
  `${PREFIX}/editUserError`
);
