
import { Box, Button, Container, Grid, Paper } from "@material-ui/core";
import { styled } from "@material-ui/styles";
import { useState } from "react";
import GroupsIcon from "@material-ui/icons/Groups";
import AddIcon from '@material-ui/icons/Add';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import ButtonBase from '@material-ui/core/ButtonBase';
import CardContent from '@material-ui/core/CardContent';
import { CenterFocusWeakRounded } from "@material-ui/icons";
import '../Admin.scss';

export const UsersListPage: React.FC = (props) => {
  const [state, setSate] = useState()
  const styles = styled(ButtonBase)(({ theme }) => ({

  }));

  const pageClick = () => {
    console.log('Click Page')
  }
  return (
    <>
      <Container>
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6} md={3}>
                <Card onClick={pageClick} className="text-center">
                  <div className="text-center d-flex box">
                    <GroupsIcon
                      sx={{
                        pr: 1,
                      }}
                      fontSize="large"
                    />
                    View User
                  </div>
                </Card>
              </Grid>

              <Grid item xs={12} sm={6} md={3}>
                <Card>
                  <div className="text-center d-flex box">
                    <BorderColorIcon
                      sx={{
                        pr: 1,
                      }}
                      fontSize="large"
                    />
                    Edit User
                  </div>
                </Card>

              </Grid>

            </Grid>
          </Grid>
          {/* <Grid item xs={6}>
            fsdf
          </Grid> */}

        </Grid>
      </Container>
    </>
  );
}
