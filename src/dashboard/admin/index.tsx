import { Route, Switch, useRouteMatch } from "react-router-dom";
import { PrivateRoute } from "../../auth/PrivateRoute";
import { UserRole } from "../../auth/roles";
import { RolesPage } from "./RolesPage";
import { UsersPage } from "./usersPage";
import { UsersListPage } from "./usersListPage";
import { AddUsersPage } from "./addUsersPage";
import { SendMessage } from "./sendMessage";

// This file contains the routes for admin pages
export const AdminPage = () => {
  const { path } = useRouteMatch();
  console.log('path', path)
  return (

    <Switch>
      <PrivateRoute path={`${path}/users-info`} roles={[UserRole.ADMIN]}>
        <UsersListPage />
      </PrivateRoute>
      <PrivateRoute path={`${path}/users`} roles={[UserRole.ADMIN]}>
        <UsersPage />
      </PrivateRoute>
      <PrivateRoute path={`${path}/add-user`} roles={[UserRole.ADMIN]}>
        <AddUsersPage />
      </PrivateRoute>
      <PrivateRoute path={`${path}/edit-user/:userId`} roles={[UserRole.ADMIN]}>
        <AddUsersPage />
      </PrivateRoute>
      <PrivateRoute path={`${path}/send-message/:userId`} roles={[UserRole.ADMIN]}>
        <SendMessage />
      </PrivateRoute>
      {/* <Route path={`${path}/roles`}>
        <RolesPage />
      </Route> */}
    </Switch>
  );
};
