import {
  AppBar,
  AppBarProps,
  Box,
  IconButton,
  Stack,
  Toolbar,
} from "@material-ui/core";
import { alpha, styled } from "@material-ui/core/styles";
import MenuIcon from "@material-ui/icons/Menu";
import CloseIcon from "@material-ui/icons/Close";
import { useRouteMatch } from "react-router-dom";
import { MHidden } from "../components/MHidden";
import AccountPopover from "./AccountPopover";
import NotificationsPopover from "./NotificationsPopover";

const DRAWER_WIDTH = 60;
const APPBAR_MOBILE = 46;
const APPBAR_DESKTOP = 56;

const RootStyle = styled(AppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})<AppBarProps & { open: boolean }>(({ theme, open }) => ({
  boxShadow: "none",
  backdropFilter: "blur(6px)",
  WebkitBackdropFilter: "blur(6px)", // Fix on Mobile
  backgroundColor: alpha(theme.palette.background.default, 0.72),
  ...(open && {
    width: `calc(100% - ${DRAWER_WIDTH + 1}px)`,
  }),
  [theme.breakpoints.up("lg")]: {
    width: `calc(100% - ${DRAWER_WIDTH + 1}px)`,
  },
}));

const ToolbarStyle = styled(Toolbar)(({ theme }) => ({
  minHeight: `${theme.typography.pxToRem(APPBAR_MOBILE)} !important`,
  [theme.breakpoints.up("lg")]: {
    minHeight: `${theme.typography.pxToRem(APPBAR_DESKTOP)} !important`,
    padding: theme.spacing(0, 5),
  },
  "& .MuiToolbar-root": {
    height: APPBAR_MOBILE,
  },
}));

// ----------------------------------------------------------------------

interface DashboardNavbarProps {
  open: boolean;
  onOpenSidebar: React.MouseEventHandler<HTMLButtonElement>;
}

export default function DashboardNavbar({
  open,
  onOpenSidebar,
}: DashboardNavbarProps) {
  console.log(open);
  return (
    <RootStyle open={open}>
      <ToolbarStyle>
        <MHidden width="lgUp">
          <IconButton
            onClick={onOpenSidebar}
            sx={{ mr: 1, color: "text.primary" }}
          >
            {open ? <CloseIcon /> : <MenuIcon />}
          </IconButton>
        </MHidden>

        <Box sx={{ flexGrow: 1 }} />

        <Stack
          direction="row"
          alignItems="center"
          spacing={{ xs: 0.5, sm: 1.5 }}
        >
          <NotificationsPopover />
          <AccountPopover />
        </Stack>
      </ToolbarStyle>
    </RootStyle>
  );
}
