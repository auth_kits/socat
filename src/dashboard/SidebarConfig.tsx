import AdminPanelSettingsIcon from "@material-ui/icons/AdminPanelSettings";
import AutoGraphIcon from "@material-ui/icons/AutoGraph";
import CampaignIcon from "@material-ui/icons/Campaign";
import HomeIcon from "@material-ui/icons/Home";
import ListAltIcon from "@material-ui/icons/ListAlt";
import { UserRole } from "../auth/roles";
import { INavItem } from "./NavSection";

const iconSize = "2.2rem";

const sidebarConfig: INavItem[] = [
  // {
  //   title: "Audience",
  //   path: "/audience",
  //   icon: <GroupIcon />,
  // },
  {
    title: "Graphs",
    path: "/graphs",
    roles: [UserRole.ADMIN, UserRole.SUPER_ADMIN],
    icon: (
      <AutoGraphIcon
        sx={{
          fontSize: "1.8rem !important",
        }}
      />
    ),
  },
  {
    title: "Home",
    path: "/dashboard",
    icon: (
      <HomeIcon
        sx={{
          fontSize: iconSize,
          color: "#880e4f",
        }}
      />
    ),
  },
  {
    title: "Reports",
    path: "/reports",
    icon: (
      <AutoGraphIcon
        sx={{
          fontSize: iconSize,
          color: "#00ACC1",
        }}
      />
    ),
  },
  {
    title: "Curated lists",
    path: "/curated",
    // disabled: true,
    icon: (
      <ListAltIcon
        sx={{
          fontSize: iconSize,
          color: "#0b420f",
        }}
      />
    ),
  },
  {
    title: "Campaigns",
    disabled: true,
    path: "/wip",
    icon: (
      <CampaignIcon
        sx={{
          fontSize: iconSize,
          color: "#bf360c",
        }}
      />
    ),
  },
  {
    title: "Admin",
    path: "/admin/users",
    icon: (
      <AdminPanelSettingsIcon
        sx={{
          fontSize: iconSize,
          color: "#1565C0",
        }}
      />
    ),
  },
  /* {
    title: "Admin",
    path: "/admin/users",
    icon: (
      <AdminPanelSettingsIcon
        sx={{
          fontSize: iconSize,
          // color: "#ccc"
        }}
      />
    ),
    roles: [UserRole.ADMIN],
    children: [
      // {
      //   title: "Roles",
      //   path: "/admin/roles",
      //   icon: null,
      // },
      {
        title: "Users",
        path: "/admin/users",
        icon: (
          <CampaignIcon
            sx={{
              fontSize: iconSize,
              color: "#bf360c",
            }}
          />
        ),
        roles: [UserRole.ADMIN],
      },
    ],
  }, */
];

export default sidebarConfig;
