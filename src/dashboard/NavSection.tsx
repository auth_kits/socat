import {
  Box,
  Collapse,
  IconButton,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Tooltip,
} from "@material-ui/core";
// material
import { alpha, styled, useTheme } from "@material-ui/core/styles";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import { difference } from "lodash";
import { useState } from "react";
import {
  matchPath,
  NavLink as RouterLink,
  useLocation,
} from "react-router-dom";
import { useAppSelector } from "../app/hooks";
import { Privileged } from "../auth/Privileged";
import { UserRole } from "../auth/roles";
import { selectUserRoles } from "../auth/selectors";

// ----------------------------------------------------------------------

const ListItemStyle = styled((props: any) => (
  <ListItemButton disableGutters {...props} />
))(({ theme }) => ({
  ...theme.typography.body2,
  height: 60,
  position: "relative",
  textTransform: "capitalize",
  paddingLeft: theme.spacing(2.2),
  paddingRight: theme.spacing(2),
  color: theme.palette.text.secondary,
  "&:before": {
    top: 0,
    right: 0, // eslint-disable-next-line
    width: 3,
    bottom: 0,
    content: "''",
    display: "none",
    position: "absolute",
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    backgroundColor: theme.palette.primary.main,
  },
}));

const ListItemIconStyle = styled(ListItemIcon)({
  width: 22,
  height: 22,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

// ----------------------------------------------------------------------

export interface INavItem {
  icon: any;
  title: string;
  path: string;
  roles?: UserRole[];
  info?: string;
  children?: INavItem[];
  disabled?: boolean;
}
interface NavItemProps {
  item: INavItem;
  active: (path: string) => boolean;
}

function NavItem({ item, active }: NavItemProps) {
  const theme = useTheme();
  const isActiveRoot = active(item.path);
  const { title, path, icon, info, children } = item;
  const [open, setOpen] = useState(isActiveRoot);

  const handleOpen = () => {
    setOpen((prev) => !prev);
  };

  const activeRootStyle = {
    color: "primary.main",
    fontWeight: "fontWeightMedium",
    bgcolor: alpha(
      theme.palette.primary.main,
      theme.palette.action.selectedOpacity
    ),
    "&:before": { display: "block" },
  };

  const activeSubStyle = {
    color: "text.primary",
    fontWeight: "fontWeightMedium",
  };

  if (children) {
    return (
      <>
        <Privileged roles={item?.roles ?? []}>
          <ListItemStyle
            onClick={handleOpen}
            sx={{
              ...(isActiveRoot && activeRootStyle),
            }}
          >
            <Tooltip placement="right" title={title}>
              <ListItemIconStyle>{icon && icon}</ListItemIconStyle>
            </Tooltip>
            {/* <ListItemText disableTypography primary={title} /> */}
            {info && info}
            <Box
              component={open ? KeyboardArrowDownIcon : ArrowForwardIosIcon}
              sx={{ width: 16, height: 16, ml: 1 }}
            />
          </ListItemStyle>
        </Privileged>

        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {children.map((it) => {
              const isActiveSub = active(it.path);

              return (
                <Privileged key={it.title} roles={it?.roles ?? []}>
                  <ListItemStyle
                    component={RouterLink}
                    to={it.path}
                    sx={{
                      ...(isActiveSub && activeSubStyle),
                    }}
                  >
                    <Tooltip placement="right" title={it.title}>
                      <ListItemIconStyle>
                        <Box
                          component="span"
                          sx={{
                            width: 4,
                            height: 4,
                            display: "flex",
                            borderRadius: "50%",
                            alignItems: "center",
                            justifyContent: "center",
                            bgcolor: "text.disabled",
                            transition: (t) =>
                              t.transitions.create("transform"),
                            ...(isActiveSub && {
                              transform: "scale(2)",
                              bgcolor: "primary.main",
                            }),
                          }}
                        />
                      </ListItemIconStyle>
                    </Tooltip>
                    {/* <ListItemText disableTypography primary={it.title} /> */}
                  </ListItemStyle>
                </Privileged>
              );
            })}
          </List>
        </Collapse>
      </>
    );
  }

  return (
    <Privileged roles={item?.roles ?? []}>
      <ListItemStyle
        component={item.disabled ? "div" : RouterLink}
        to={path}
        sx={{
          ...(isActiveRoot && activeRootStyle),
        }}
      >
        <Tooltip placement="right" title={title}>
          <ListItemIconStyle>{icon && icon}</ListItemIconStyle>
        </Tooltip>
        {/* <ListItemText disableTypography primary={title} /> */}
        {info && info}
      </ListItemStyle>
    </Privileged>
  );
}

interface NavSectionProps {
  navConfig: INavItem[];
}
export function NavSection({ navConfig, ...other }: NavSectionProps) {
  const { pathname } = useLocation();
  const roles = useAppSelector(selectUserRoles);

  const match = (path) =>
    path ? !!matchPath(pathname, { path, exact: false }) : false;

  return (
    <Box {...other}>
      <List disablePadding>
        {navConfig
          .filter((item) => difference(item.roles, roles ?? []).length === 0)
          .map((item) => (
            <NavItem key={item.title} item={item} active={match} />
          ))}
      </List>
    </Box>
  );
}
