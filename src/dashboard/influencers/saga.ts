import { sentenceCase } from "change-case";
import { omit, truncate } from "lodash";
import { all, call, put, select, takeLatest, delay } from "redux-saga/effects";
import { getApi } from "../../app/axios";
import { ELoadingStatus } from "../../auth/action";
import { fPercent, fShortenNumber } from "../../utils/formatNumber";
import { fDateTimeSuffix } from "../../utils/formatTime";
import * as types from "./action";
import { selectCurrentId, selectFilterData } from "./selector";

const overviewAdapter = (data: any) => {
  const growthData: Record<
    "followers" | "following" | "likes",
    [number, number][]
  > = {
    following: [],
    followers: [],
    likes: [],
  };

  data?.user_profile?.stat_history?.forEach((h) => {
    const date = new Date(h.month).getTime();
    growthData.following.push([date, h.following]);
    growthData.followers.push([date, h.followers]);
    growthData.likes.push([date, h.avg_likes]);
  });

  return {
    comments: data?.user_profile?.avg_comments,
    likes: data?.user_profile?.avg_likes,
    followers: data?.user_profile?.followers,
    views: data?.user_profile?.avg_views,
    keywords: data?.user_profile?.relevant_tags?.map((v) => [
      v.tag,
      parseInt(`${v.freq / v.distance}`, 10),
    ]),
    performance: data?.user_profile?.paid_post_performance * 100,
    engagementRate: data?.user_profile?.engagement_rate * 100,
    growthData,
    affinity: data?.user_profile?.brand_affinity?.map((v) => ({
      label: v.name,
      iconUrl: v.logo,
    })),
    interests: data?.user_profile?.interests?.map((v) => v.name),
    lookalikes: data?.user_profile?.similar_users?.map((v) => ({
      engagements: v.engagements,
      followers: v.followers,
      fullname: v.fullname,
      verified: v.is_verified,
      imageUrl: v.picture,
      score: v.score,
      url: v.url,
      username: v.username,
      locationLabel: [v?.geo?.city?.name, v?.geo?.country?.name]
        .filter((a) => !!a)
        .join(", "),
    })),
  };
};

const detailAdapter = (data: any): types.InfluencerDetail => ({
  verified: data?.user_profile?.is_verified,
  displayName: data?.user_profile?.fullname,
  imageUrl: data?.user_profile?.picture,
  location: {
    label: [
      data?.user_profile?.geo?.city?.name,
      data?.user_profile?.geo?.country?.name,
    ]
      .filter((a) => !!a)
      .join(", "),
    latitude: data?.user_profile?.geo?.city?.coords?.lat,
    longitude: data?.user_profile?.geo?.city?.coords?.lng,
  },
  tagLine: data?.user_profile?.description,
  id: data?.user_profile?.username,
  socialProfiles: [
    {
      profileType: types.ProfileType.INSTAGRAM,
      label: "Instagram",
      count: `${fShortenNumber(
        data?.user_profile?.followers
      ).toLocaleUpperCase()} Followers`,
      url: data?.user_profile?.url,
    },
  ],
  contact:
    data?.user_profile?.contacts?.map((c) => ({
      formatted: c?.formatted_value,
      value: c.value,
      type: c.type,
    })) ?? [],
});

const getRangeLabel = (
  min = -Infinity,
  max = Infinity,
  formatter = (v) => `${v}`
): string => {
  if (max === Infinity) {
    return ` > ${formatter(min)} `;
  }

  if (min === -Infinity) {
    return `< ${formatter(max)}`;
  }

  return `${formatter(min)}-${formatter(max)}`;
};

const engagementAdapter = (data: any): types.InfluencerEngagementData => {
  const newData: types.InfluencerEngagementData = {
    comments: [],
    likes: [],
    rate: [],
    hashtags: data?.user_profile?.top_hashtags?.map((ht) => ({
      tag: ht.tag,
      weight: ht.weight * 100,
    })),
    mentions: data?.user_profile?.top_mentions?.map((tm) => ({
      tag: tm.tag,
      weight: tm.weight * 100,
    })),
  };

  data?.extra?.engagement_rate_histogram
    ?.sort((b, a) => (a.max ?? Infinity) - (b.max ?? Infinity))
    .forEach((er) => {
      const curr = data?.user_profile?.engagement_rate ?? 0;
      newData.rate.push({
        // x: (er.max ?? 0) * 100,
        y: er.total,
        user: curr < er.max && curr > er.min,
        x: getRangeLabel(er.min, er.max, (v) => fPercent(v * 100)),
        description: `${fPercent(er.min * 100)} - ${fPercent(er.max * 100)}`,
        median: er.median,
      });
    });

  data?.user_profile?.recent_posts?.forEach((p) => {
    const date = new Date(p.created).getTime();
    newData.comments.push({
      x: date,
      y: p.stat.comments,
      postUrl: p?.link,
    });
    newData.likes.push({
      x: date,
      y: p.stat.likes,
      postUrl: p?.link,
    });
  });

  return newData;
};

const audienceDataByType = (data: any, audienceType: types.AudienceType) => {
  const newData: types.InfluencerAudienceData = {
    audience: audienceType,
    country:
      data?.audience_geo.countries.map((c) => ({
        x: c.name,
        y: c.weight * 100,
      })) ?? [],
    city:
      data?.audience_geo.cities.map((c) => ({
        x: c.name,
        y: c.weight * 100,
      })) ?? [],
    cityGeo: data?.audience_geo.cities ?? [],
    age: {
      male: [],
      female: [],
    },
    commonAge: [],
    ethnicity:
      data?.audience_ethnicities?.map((c) => ({
        x: c.name,
        y: c.weight * 100,
      })) ?? [],
    language:
      data?.audience_languages?.map((c) => ({
        x: c.name ?? sentenceCase(c.code),
        y: c.weight * 100,
      })) ?? [],
    reachability:
      data?.audience_reachability?.map((c) => {
        const [min, max] = c.code.split("-");
        return {
          x: getRangeLabel(min ?? -Infinity, max ?? Infinity),
          y: c.weight * 100,
        };
      }) ?? [],
    interests:
      data?.audience_interests?.map((c) => ({
        label: c.name,
        weight: c.weight * 100,
        affinity: c.affinity,
      })) ?? [],
    brands:
      data?.audience_brand_affinity?.map((c) => ({
        iconUrl: "",
        label: c.name,
        weight: c.weight * 100,
        affinity: c.affinity,
      })) ?? [],
    notable:
      data?.notable_users?.map((u) => ({
        imageUrl: u.picture,
        influencerId: u.username,
        url: u.url,
        displayName: u.fullname,
        followers: u.followers,
        engagements: u.engagements,
      })) ?? [],
    lookalike:
      data?.audience_lookalikes?.map((u) => ({
        imageUrl: u.picture,
        influencerId: u.username,
        url: u.url,
        displayName: u.fullname,
        followers: u.followers,
        engagements: u.engagements,
        locationLabel: [u?.geo?.city?.name, u?.geo?.country?.name]
          .filter((a) => !!a)
          .join(", "),
      })) ?? [],
    gender: data?.audience_genders ?? [],
  };

  data?.audience_genders_per_age?.forEach((c) => {
    newData.commonAge.push({
      x: c?.code,
      y: (c?.male + c?.female) * 100,
      count: { male: c?.male * 100, female: c?.female * 100 },
    });
    newData.age.male.push({
      x: c?.code,
      y: c?.male * 100,
    });
    newData.age.female.push({
      x: c?.code,
      y: c?.female * 100,
    });
  });
  return newData;
};

const audienceAdapter = (data: any) => ({
  likes: audienceDataByType(
    data?.audience_likers.data,
    types.AudienceType.LIKES
  ),
  followers: audienceDataByType(
    data?.audience_followers.data,
    types.AudienceType.FOLLOWERS
  ),
  extraData: {
    followersCreds: data?.audience_followers?.data?.audience_credibility * 100,
    notableFollowers: data?.audience_followers?.data?.notable_users_ratio * 100,
    likerCreds: data?.audience_likers?.data?.audience_credibility * 100,
    notableLikes: data?.audience_likers?.data?.notable_users_ratio * 100,
    nonFollowerLikes:
      data?.audience_likers?.data?.likes_not_from_followers * 100,
  },
});

const postsAdapter = (data: any): Record<string, types.TopPostsData[]> => ({
  sponsored:
    data?.user_profile?.commercial_posts?.map((p) => ({
      imageUrl: p.thumbnail,
      text: truncate(p.text, {
        length: 100,
        separator: " ",
        omission: "...",
      }),
      created: fDateTimeSuffix(new Date(p.created)),
      likes: p.stat.likes,
      comments: p.stat.comments,
      postUrl: p.link,
    })) ?? [],
  top:
    data?.user_profile?.top_posts?.map((p) => ({
      imageUrl: p.thumbnail,
      text: truncate(p.text, {
        length: 100,
        separator: " ",
        omission: "...",
      }),
      created: fDateTimeSuffix(new Date(p.created)),
      likes: p.stat.likes,
      comments: p.stat.comments,
      postUrl: p.link,
    })) ?? [],
});

// Need to split this in multiple API's, and move the data into individual saga's below
function* getInfluencerProfileSaga() {
  try {
    const influencerId = yield select(selectCurrentId);
    const { data } = yield call(getApi().get, "/user/user_profile", {
      params: {
        url: influencerId,
      },
    });
    yield put(
      types.getInfluencerProfileSuccess({
        detail: detailAdapter(data),
        overview: overviewAdapter(data),
        engagement: engagementAdapter(data),
        audience: audienceAdapter(data),
        posts: postsAdapter(data),
      })
    );
  } catch (error) {
    yield put(
      types.getInfluencerProfileError({
        message: error?.message ?? "There was an error loading profile",
      })
    );
  }
}

function* getInfluencerAudienceSaga(payload) {
  try {
    const { data } = yield call(getApi().get, "/influencer/audience");
    yield put(
      types.getInfluencerAudienceSuccess({
        influencerId: "123",
      })
    );
  } catch (error) {
    yield put(types.getInfluencerAudienceError(error));
  }
}

function* getInfluencerEngagementSaga(payload) {
  try {
    const { data } = yield call(getApi().get, "/influencer/engagement");
    yield put(
      types.getInfluencerEngagementSuccess({
        influencerId: "123",
      })
    );
  } catch (error) {
    yield put(types.getInfluencerEngagementError(error));
  }
}

function* getInfluencerOverviewSaga(payload) {
  try {
    const { data } = yield call(getApi().get, "/influencer/overview");
    // yields put(
    // types.getInfluencerOverviewSuccess({
    //   data: {
    //     affinity: data.affinity,

    //   }
    // })
    // );
  } catch (error) {
    yield put(types.getInfluencerOverviewError(error));
  }
}

function* getInfluencerPostsSaga(payload) {
  try {
    const { data } = yield call(getApi().get, "/influencer/posts");
    yield put(
      types.getInfluencerPostsSuccess({
        influencerId: "123",
      })
    );
  } catch (error) {
    yield put(types.getInfluencerPostsError(error));
  }
}

function* fetchInfluencerSaga({
  payload: { influencerId },
}: ReturnType<typeof types.setCurrentInfluencerId>) {
  yield put(types.getInfluencerProfile());
}

function* reportSearchSaga({
  payload: { query },
}: ReturnType<typeof types.getSearchResults>) {
  const filterData = yield select(selectFilterData);
  if (query?.length < 3) {
    return;
  }

  try {
    const { data } = yield call(getApi().get, "/user/profile_search", {
      params: {
        q: query,
        f: filterData,
      },
    });

    yield put(
      types.getSearchResultsSuccess({
        data: data?.data?.map((p) => ({
          followers: p.followers,
          displayName: p.fullname,
          verified: p.is_verified,
          picture: p.picture,
          username: p.username,
        })),
      })
    );
  } catch (error) {
    console.log(error);
    yield put(
      types.getSearchResultsError({
        message: "No results",
      })
    );
  }
}

const sanitizeFilters = (filterData: types.FilterData) => {
  const newFilters = omit(filterData, [
    "audience_geo.label",
    "geo.label",
    "audience_brand.label",
    "lang.label",
    "audience_lang.label",
    "brand_category.label",
    "audience_brand_category.label",
  ]);

  newFilters.ads_brands = newFilters?.ads_brands?.map((v) => v.id);
  newFilters.brand = newFilters?.brand?.map((v) => v.id);
  newFilters.brand_category = newFilters?.brand_category?.map((v) => v.id);
  newFilters.audience_geo = newFilters?.audience_geo?.map((v) => ({
    id: v.id,
    weight: v.weight,
  }));
  newFilters.geo = newFilters?.geo?.map((v) => ({
    id: v.id,
  })) as any;
  return newFilters;
};

function* influencersSearchSaga({
  payload: { offset },
}: ReturnType<typeof types.getInfluencerSearchResults>) {
  const filterData = yield select(selectFilterData);

  try {
    const { data } = yield call(
      getApi().post,
      `/user/search_influencers?skip=${offset}`,
      {
        ...filterData,
        filter: sanitizeFilters(filterData.filter),
      }
    );

    yield put(
      types.getInfluencerSearchResultsSuccess({
        totalCount: data?.total ?? 0,
        append: offset > 0,
        data: data?.accounts?.map((p) => ({
          followers: p?.account.user_profile?.followers,
          displayName: p?.account.user_profile?.fullname,
          verified: p?.account.user_profile?.is_verified,
          picture: p?.account.user_profile?.picture,
          username: p?.account.user_profile?.username,
          engagements: p?.account.user_profile?.engagements,
          engagementRate: p?.account.user_profile?.engagement_rate,
        })),
      })
    );
  } catch (error) {
    yield put(
      types.getSearchResultsError({
        message: "No results",
      })
    );
  }
}

function* filterLocationsSaga({
  payload: { query },
}: ReturnType<typeof types.getFilterLocations>) {
  if (query?.length < 3) {
    return;
  }

  try {
    const { data } = yield call(getApi().get, "/user/get_location", {
      params: {
        q: query,
      },
    });

    yield put(types.getFilterLocationsSuccess(data));
  } catch (error) {
    console.log(error);
    yield put(
      types.setFilterSearchError({
        message: "No results",
      })
    );
  }
}

function* filterBrandsSaga({
  payload: { query },
}: ReturnType<typeof types.getFilterBrands>) {
  try {
    const { data } = yield call(getApi().get, "/user/get_brands", {
      params: {
        q: query,
      },
    });

    yield put(
      types.getFilterBrandsSuccess({
        data: data?.data?.map((d) => ({
          id: d.id,
          name: d.name,
          count: d.count | 0,
          deprecated: d.depreacted,
        })),
      })
    );
  } catch (error) {
    yield put(
      types.setFilterSearchError({
        message: "No results",
      })
    );
  }
}

function* filterLangsSaga({
  payload: { query },
}: ReturnType<typeof types.getFilterLangs>) {
  try {
    const { data } = yield call(getApi().get, "/user/get_langs", {
      params: {
        q: query,
      },
    });

    yield put(
      types.getFilterLangsSuccess({
        data: data?.data?.map((d) => ({
          code: d.code,
          name: d.name,
          count: d.count | 0,
        })),
      })
    );
  } catch (error) {
    yield put(
      types.setFilterSearchError({
        message: "No results",
      })
    );
  }
}

function* filterInterestsSaga({
  payload: { query },
}: ReturnType<typeof types.getFilterInterests>) {
  try {
    const { data } = yield call(getApi().get, "/user/get_interests", {
      params: {
        q: query,
      },
    });

    yield put(
      types.getFilterInterestSuccess({
        data: data?.data?.map((d) => ({
          id: d.id,
          name: d.name,
          count: d.count | 0,
        })),
      })
    );
  } catch (error) {
    yield put(
      types.setFilterSearchError({
        message: "No results",
      })
    );
  }
}

function* filterLookalikesSaga({
  payload: { query },
}: ReturnType<typeof types.getFilterInterests>) {
  if (!query) {
    yield put(
      types.getFilterLookalikesuccess({
        data: [],
      })
    );
    return;
  }
  try {
    const { data } = yield call(getApi().get, "/user/profile_search", {
      params: {
        q: query,
        type: "lookalike",
      },
    });

    yield put(
      types.getFilterLookalikesuccess({
        data: data?.data?.map((d) => ({
          id: d.id,
          name: d.fullname,
          value: d.username,
        })),
      })
    );
  } catch (error) {
    yield put(
      types.setFilterSearchError({
        message: "No results",
      })
    );
  }
}

export function* tagSearchSaga({
  payload: { query },
}: ReturnType<typeof types.getInfluencerTagResults>) {
  try {
    const { data } = yield call(getApi().get, "/user/search_topics", {
      params: {
        q: query,
      },
    });
    yield put(
      types.getInfluencerTagResultsSuccess({
        data: data?.data?.map((d) => ({
          tag: d.tag,
          value: d.value,
        })),
      })
    );
  } catch (error) {
    yield put(
      types.getInfluencerTagResultsError({
        message: error?.response?.data?.msg ?? "Tags results fetch errors",
      })
    );
  }
}
function* startInfluencersSearch() {
  yield put(
    types.getInfluencerSearchResults({
      query: "",
      offset: 0,
    })
  );
}

function* reportDataSaga({
  payload: { offset },
}: ReturnType<typeof types.getReportsData>) {
  try {
    const { data } = yield call(getApi().get, "/user/report_lists", {
      params: {
        skip: offset,
      },
    });

    yield put(
      types.getReportsSuccess({
        data: data?.results?.map((res) => ({
          id: res.id,
          createAt: res.created_at,
          displayName: res.user_profile.fullname,
          engagementRate: res.user_profile.fullname,
          engagements: res.user_profile.engagements,
          followers: res.user_profile.followers,
          picture: res.user_profile.picture,
          username: res.user_profile.username,
        })),
        append: offset > 0,
      })
    );
  } catch (error) {
    yield put(
      types.getReportsError({
        message:
          error?.response?.data?.msg ?? "There was an error in SOCAT api",
      })
    );
  }
}

function* influencersExportSaga() {
  const filterData = yield select(selectFilterData);

  try {
    const { data } = yield call(getApi().post, `/user/export_report`, {
      ...filterData,
      filter: sanitizeFilters(filterData.filter),
    });

    yield delay(10000);
    yield put(
      types.getInfluencerExportResultsSuccess({
        exportId: data.export_id,
        status: ELoadingStatus.SUCCESS,
      })
    );
    yield delay(2000);
    yield put(
      types.getInfluencerExportResultsSuccess({
        exportId: "",
        status: ELoadingStatus.INITIAL,
      })
    );
  } catch (error) {
    console.log(error);
    // yield put(
    //   types.getSearchResultsError({
    //     message: "No results",
    //   })
    // );
  }
}

export default function* watchInfluencerSaga() {
  yield all([
    takeLatest(types.setCurrentInfluencerId, fetchInfluencerSaga),
    takeLatest(types.getInfluencerProfile, getInfluencerProfileSaga),
    takeLatest(types.getSearchResults, reportSearchSaga),
    takeLatest(types.setFilterData, startInfluencersSearch),
    takeLatest(types.getInfluencerSearchResults, influencersSearchSaga),
    takeLatest(types.getFilterLocations, filterLocationsSaga),
    takeLatest(types.getFilterBrands, filterBrandsSaga),
    takeLatest(types.getFilterLangs, filterLangsSaga),
    takeLatest(types.getFilterInterests, filterInterestsSaga),
    takeLatest(types.getFilterLookalikes, filterLookalikesSaga),
    takeLatest(types.getInfluencerTagResults, tagSearchSaga),
    takeLatest(types.getReportsData, reportDataSaga),
    takeLatest(types.getInfluencerExportResults, influencersExportSaga),
    // This doesnot do anything its for the FUTURE :(
    // takeLatest(types.getInfluencerAudience, getInfluencerAudienceSaga),
    // takeLatest(types.getInfluencerEngagement, getInfluencerEngagementSaga),
    // takeLatest(types.getInfluencerOverview, getInfluencerOverviewSaga),
    // takeLatest(types.getInfluencerPosts, getInfluencerPostsSaga),
  ]);
}
