import {
  CardActions,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  ImageList,
  ImageListItem,
  ImageListItemBar,
} from "@material-ui/core";
import { Box } from "@material-ui/system";
import { useState } from "react";

export interface ViewMoreGraphProps {
  renderGraph: any;
  data: any[];
  title: string;
  noOfItems: number;
  height: number;
}
export const ViewMoreGraph: React.FC<ViewMoreGraphProps> = ({
  renderGraph,
  data,
  noOfItems,
  title,
  height,
}) => {
  const [dialogOpen, setDialogOpen] = useState(false);
  if (data?.length > noOfItems) {
    return (
      <>
        {renderGraph(data.slice(0, noOfItems), height)}
        <CardActions
          sx={{
            bottom: 0,
            position: "absolute",
            width: "100%",
          }}
        >
          <Box style={{ flexGrow: 1 }} />
          <Button
            onClick={() => setDialogOpen(true)}
            size="small"
            color="inherit"
          >
            View More
          </Button>
          <Dialog
            fullWidth
            maxWidth="md"
            open={dialogOpen}
            onClose={() => setDialogOpen(false)}
            aria-labelledby="view-more-modal-title"
            aria-describedby="view-more-modal-description"
          >
            <DialogTitle id="view-more-modal-title">{title}</DialogTitle>
            <DialogContent>
              <DialogContentText id="view-more-modal-description">
                {renderGraph(data)}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={() => setDialogOpen(false)} autoFocus>
                Close
              </Button>
            </DialogActions>
          </Dialog>
        </CardActions>
      </>
    );
  }
  return renderGraph(data, height);
};
