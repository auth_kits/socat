import { FormControl, FormLabel, Slider, Stack } from "@material-ui/core";
import { Box } from "@material-ui/system";
import { Controller } from "react-hook-form";
import { fShortenNumber } from "../../../utils/formatNumber";
import { FilterComponentProps } from "./filterLabels";
import { FilterPoppper } from "./FilterPopper";

const SLIDER_MARKS = [
  { value: 0, realValue: 100 },
  { value: 1, realValue: 200 },
  { value: 2, realValue: 300 },
  { value: 3, realValue: 400 },
  { value: 4, realValue: 500 },
  { value: 5, realValue: 1000 },
  { value: 6, realValue: 5000 },
  { value: 7, realValue: 10000 },
  { value: 8, realValue: 50000 },
  { value: 9, realValue: 100000 },
  { value: 10, realValue: 500000 },
  { value: 11, realValue: 1000000 },
];

const SLIDER_REV_MAP = {
  "100": 0,
  "200": 1,
  "300": 2,
  "400": 3,
  "500": 4,
  "1000": 5,
  "5000": 6,
  "10000": 7,
  "50000": 8,
  "100000": 9,
  "500000": 10,
  "1000000": 11,
};

export const FollowersFilter: React.FC<FilterComponentProps> = ({
  control,
  setValue,
  onSubmit,
  formState: { dirtyFields },
}) => (
  <FilterPoppper label="Followers" isDirty={!!dirtyFields.filter?.followers}>
    <Stack spacing={2}>
      <Controller
        name="filter.followers"
        control={control}
        rules={{
          required: true,
        }}
        render={({ field }) => (
          <FormControl>
            <FormLabel
              sx={{
                pb: 5,
              }}
              htmlFor="influencer-followers"
            >
              Influencer
            </FormLabel>
            <Box sx={{ px: 2, width: 260 }}>
              <Slider
                id="influencer-followers"
                defaultValue={[18, 65]}
                step={null}
                marks={SLIDER_MARKS}
                value={[
                  SLIDER_REV_MAP[field.value.left_number ?? "100"],
                  SLIDER_REV_MAP[field.value.right_number ?? "1000000"],
                ]}
                min={0}
                max={11}
                name={field.name}
                ref={field.ref}
                onChange={(event, v) => {
                  setValue(
                    "filter.followers",
                    {
                      left_number: `${SLIDER_MARKS[v[0]].realValue}`,
                      right_number: `${SLIDER_MARKS[v[1]].realValue}`,
                    },
                    {
                      shouldDirty: true,
                    }
                  );
                  onSubmit(event);
                }}
                scale={(x) => SLIDER_MARKS[x].realValue as number}
                getAriaLabel={() => "Followers"}
                valueLabelDisplay="on"
                valueLabelFormat={(x) => fShortenNumber(x)}
                getAriaValueText={(value) => `${value}`}
              />
            </Box>
          </FormControl>
        )}
      />
    </Stack>
  </FilterPoppper>
);
