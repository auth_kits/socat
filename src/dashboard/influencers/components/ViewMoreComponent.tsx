import {
  Button,
  CardActions,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@material-ui/core";
import { Box } from "@material-ui/system";
import { useState } from "react";

export interface ViewMoreComponentProps {
  moreColor?: any;
  renderTable: any;
  label: any;
  hideMore?: boolean;
}

export const ViewMoreComponent: React.FC<ViewMoreComponentProps> = ({
  moreColor = "primary",
  hideMore = false,
  renderTable,
  label,
}) => {
  const [dialogOpen, setDialogOpen] = useState(false);
  return (
    <>
      {!hideMore && (
        <>
          <Box
            sx={{
              pt: 3,
            }}
          />
          <CardActions
            sx={{
              bottom: 0,
              position: "absolute",
              width: "100%",
            }}
          >
            <Box style={{ flexGrow: 1 }} />
            <Button
              sx={{
                color: moreColor,
              }}
              onClick={() => setDialogOpen(true)}
              size="small"
              color={moreColor ?? "primary"}
            >
              View More
            </Button>
            <Dialog
              fullWidth
              maxWidth="md"
              open={dialogOpen}
              onClose={() => setDialogOpen(false)}
              aria-labelledby="view-more-modal-title"
              aria-describedby="view-more-modal-description"
            >
              <DialogTitle id="view-more-modal-title">{label}</DialogTitle>
              <DialogContent>{renderTable()}</DialogContent>
              <DialogActions>
                <Button onClick={() => setDialogOpen(false)} autoFocus>
                  Close
                </Button>
              </DialogActions>
            </Dialog>
          </CardActions>
        </>
      )}
    </>
  );
};
