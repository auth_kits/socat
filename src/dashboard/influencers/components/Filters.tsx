import { Grid, Paper } from "@material-ui/core";
import { Box } from "@material-ui/system";
import { debounce } from "lodash";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import { useAppDispatch } from "../../../app/hooks";
import { INITIAL_FILTER_VALUE, setFilterData } from "../action";
import { selectFilterData } from "../selector";
import { AgeFilter } from "./AgeFilter";
import { AppliedFilters } from "./AppliedFilters";
import { AudienceFilter } from "./AudienceFilter";
import { BioFilter } from "./BioFilter";
import { BrandsFilter } from "./BrandsFilter";
import { CheckboxFilters } from "./CheckboxFilters";
import { EngagementsFilter } from "./EngagementsFilter";
import { EthnicityFilter } from "./EthnicityFilter";
import { FilterFormState } from "./filterLabels";
import { FollowersFilter } from "./FollowersFilter";
import { GenderFilter } from "./GenderFilter";
import { InterestsFilter } from "./InterestsFilter";
import { LanguageFilter } from "./LanguageFilter";
import { LocationFilter } from "./LocationFilter";
import { LookalikeFilter } from "./LookalikesFilter";
import { PartnershipFilter } from "./PartnershipFilter";
import { ContactsFilter } from "./ContactsFilter";
import { TagFilter } from "./TagsFilter";

export const Filters: React.FC = () => {
  const filterData = useSelector(selectFilterData);
  const dispatch = useAppDispatch();
  const { control, handleSubmit, setValue, formState } =
    useForm<FilterFormState>({
      defaultValues: INITIAL_FILTER_VALUE,
    });
  const { isDirty, isValid } = formState;
  const onApplyFilter = (filters) => {
    dispatch(setFilterData(filters));
  };
  const onSubmit = debounce(handleSubmit(onApplyFilter), 500);
  return (
    <form onChange={onSubmit}>
      <Grid container>
        <Grid
          item
          xs={12}
          sm={8}
          md={6}
          lg={4}
          sx={{
            m: "0 auto",
          }}
        >
          <TagFilter
            control={control}
            onSubmit={onSubmit}
            setValue={setValue}
            formState={formState}
          />
        </Grid>
        <Grid item xs={12} />
        <Grid
          item
          xs={12}
          sm={12}
          md={12}
          lg={10}
          sx={{
            m: "0 auto",
          }}
        >
          <Paper
            elevation={2}
            sx={{
              mt: 1.5,
            }}
          >
            <Grid
              container
              sx={{
                p: 2,
              }}
            >
              <Grid item xs={12} sm={12} md={12} lg={12} sx={{ m: "0 auto" }}>
                <Grid
                  container
                  sx={{
                    p: 2,
                  }}
                >
                  <Grid item xs={6} sm={4} md={2} lg={2} sx={{ p: 1 }}>
                    <LocationFilter
                      control={control}
                      formState={formState}
                      onSubmit={onSubmit}
                      setValue={setValue}
                    />
                  </Grid>
                  <Grid item xs={6} sm={4} md={2} lg={2} sx={{ p: 1 }}>
                    <LanguageFilter
                      control={control}
                      formState={formState}
                      setValue={setValue}
                      onSubmit={onSubmit}
                    />
                  </Grid>
                  <Grid item xs={6} sm={4} md={2} lg={2} sx={{ p: 1 }}>
                    <BrandsFilter
                      control={control}
                      formState={formState}
                      onSubmit={onSubmit}
                      setValue={setValue}
                    />
                  </Grid>
                  <Grid item xs={6} sm={4} md={2} lg={2} sx={{ p: 1 }}>
                    <InterestsFilter
                      control={control}
                      formState={formState}
                      onSubmit={onSubmit}
                      setValue={setValue}
                    />
                  </Grid>
                  <Grid item xs={6} sm={4} md={2} lg={2} sx={{ p: 1 }}>
                    <PartnershipFilter
                      control={control}
                      formState={formState}
                      onSubmit={onSubmit}
                      setValue={setValue}
                    />
                  </Grid>

                  <Grid item xs={6} sm={4} md={2} lg={2} sx={{ p: 1 }}>
                    <GenderFilter
                      control={control}
                      formState={formState}
                      setValue={setValue}
                    />
                  </Grid>
                  <Grid item xs={6} sm={4} md={2} lg={2} sx={{ p: 1 }}>
                    <EthnicityFilter
                      control={control}
                      formState={formState}
                      setValue={setValue}
                    />
                  </Grid>

                  <Grid item xs={6} sm={4} md={2} lg={2} sx={{ p: 1 }}>
                    <AgeFilter
                      control={control}
                      formState={formState}
                      setValue={setValue}
                      onSubmit={onSubmit}
                    />
                  </Grid>

                  <Grid item xs={6} sm={4} md={2} lg={2} sx={{ p: 1 }}>
                    <LookalikeFilter
                      control={control}
                      formState={formState}
                      onSubmit={onSubmit}
                      setValue={setValue}
                    />
                  </Grid>

                  <Grid item xs={6} sm={4} md={2} lg={2} sx={{ p: 1 }}>
                    <BioFilter
                      control={control}
                      formState={formState}
                      setValue={setValue}
                    />
                  </Grid>

                  <Grid item xs={6} sm={4} md={2} lg={2} sx={{ p: 1 }}>
                    <FollowersFilter
                      control={control}
                      formState={formState}
                      onSubmit={onSubmit}
                      setValue={setValue}
                    />
                  </Grid>

                  <Grid item xs={6} sm={4} md={2} lg={2} sx={{ p: 1 }}>
                    <EngagementsFilter
                      control={control}
                      formState={formState}
                      onSubmit={onSubmit}
                      setValue={setValue}
                    />
                  </Grid>

                  {/* 
            <Grid item xs={6} sm={4} md={3} lg={2}>
              <LastPostFilter control={control} />
            </Grid>

            <Grid item xs={6} sm={4} md={3} lg={2}>
              <AccountFilter control={control} />
            </Grid>

            <Grid item xs={6} sm={4} md={3} lg={2}>
              <GrowthFilter control={control} />
            </Grid> */}

                  {/* <Grid item xs={12} /> */}
                  <Grid item xs={6} sm={4} md={2} lg={2} sx={{ p: 1 }}>
                    <AudienceFilter
                      control={control}
                      formState={formState}
                      setValue={setValue}
                    />
                  </Grid>

                  <Grid item xs={6} sm={4} md={2} lg={2} sx={{ p: 1 }}>
                    <ContactsFilter
                      control={control}
                      formState={formState}
                      onSubmit={onSubmit}
                      setValue={setValue}
                    />
                  </Grid>

                  <Grid item xs={6} sm={4} md={2} lg={4} sx={{ p: 1 }}>
                    <Box>
                      <CheckboxFilters
                        control={control}
                        formState={formState}
                        onSubmit={onSubmit}
                        setValue={setValue}
                      />
                    </Box>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <Box sx={{ m: "0 auto", mt: 2 }}>
                  <AppliedFilters
                    dirtyFields={formState.dirtyFields}
                    setValue={setValue}
                    onSubmit={onSubmit}
                  />
                </Box>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </form>
  );
};
