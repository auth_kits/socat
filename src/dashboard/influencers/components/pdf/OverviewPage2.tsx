import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardHeader,
  Chip,
  Grid,
  List,
  ListItemAvatar,
  ListItemButton,
  ListItemText,
  Typography,
} from "@material-ui/core";
import { styled } from "@material-ui/core/styles";
import { ApexOptions } from "apexcharts";
import { isEmpty, merge } from "lodash";
import React, { useLayoutEffect, useRef } from "react";
import * as wc from "wordcloud";
import { useAppSelector } from "../../../../app/hooks";
import { BaseOptionChart } from "../../../../components/charts";
import { fShortenNumber } from "../../../../utils/formatNumber";
import { ELoadingStatus } from "../../action";
import { selectInfluencerOverview, selectStatus } from "../../selector";

const ChipListItem = styled("li")(({ theme }) => ({
  margin: theme.spacing(0.5),
  "& span": {
    paddingBottom: "15px !important",
  },
}));

export const OverviewPage2: React.FC<{ pdf?: boolean }> = ({ pdf: isPdf }) => {
  const graphColor = "#8bc34a";
  const tagRef = useRef(null);
  const data = useAppSelector(selectInfluencerOverview);
  const status = useAppSelector(selectStatus);

  useLayoutEffect(() => {
    if (tagRef.current && data?.keywords) {
      wc(tagRef.current, {
        list: data?.keywords,
        weightFactor: 2,
        shrinkToFit: false,
        shape: "square",
        rotationSteps: 2,
      });
    }
    return () => {
      // stop the renderring
      wc?.stop();
    };
  }, [tagRef, data?.keywords]);

  const chartOptions: ApexOptions = merge(BaseOptionChart(), {
    chart: {
      offsetX: 0,
    },
    grid: {
      padding: {
        top: 5,
        right: 20,
        bottom: 5,
        left: 20,
      },
    },
    xaxis: {
      type: "datetime",
      title: {
        text: "Period",
      },
    },
    yaxis: {
      labels: {
        formatter: (seriesName) =>
          fShortenNumber(seriesName).toLocaleUpperCase(),
      },
      type: "numeric",
      title: {
        text: "Count",
      },
    },
    stroke: {
      width: 7,
    },
    colors: [graphColor],
    fill: {
      opacity: 5,
    },
    tooltip: {
      shared: true,
      intersect: false,
      y: {
        formatter: (y) => {
          if (typeof y !== "undefined") {
            return `${fShortenNumber(y.toFixed(0))}`;
          }
          return y;
        },
      },
    },
  });

  const isLoading = [ELoadingStatus.LOADING, ELoadingStatus.INITIAL].includes(
    status
  );

  return (
    <Grid container spacing={2} style={{ flexGrow: 1 }}>
      <Grid item xs={12}>
        <Box
          sx={{
            pt: 4,
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }} variant="h5">
            Influencer Details
          </Typography>
        </Box>
      </Grid>
      <Grid item xs={6}>
        <Card
          sx={{
            height: "100%",
          }}
        >
          <CardHeader
            sx={{ color: "#26a69a" }}
            title="Influencers Lookalikes"
          />
          <CardContent>
            <List
              sx={{
                width: "100%",
              }}
            >
              {data?.lookalikes?.slice(0, 3).map((n) => (
                <ListItemButton
                  component="a"
                  alignItems="flex-start"
                  key={n.username}
                  href={n.url ?? "#"}
                  target="_blank"
                  rel="nofollow"
                >
                  <ListItemAvatar>
                    <Avatar src={n.imageUrl} />
                  </ListItemAvatar>
                  <ListItemText
                    primary={n.fullname}
                    secondary={`${fShortenNumber(
                      n.followers
                    )} followers, ${fShortenNumber(n.engagements)} engagements`}
                  />
                </ListItemButton>
              ))}
            </List>
          </CardContent>
        </Card>
      </Grid>

      <Grid item xs={6}>
        <Card
          style={{ display: "flex", flexDirection: "column", height: "100%" }}
        >
          <CardHeader sx={{ color: "#26a69a" }} title="Word Cloud" />
          <CardContent style={{ flexGrow: 1 }}>
            <div ref={tagRef} style={{ height: "100%", minHeight: "200px" }} />
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={12} sx={{ p: "0 !important", m: 0 }} />
      <Grid item xs={6}>
        <Card
          sx={{
            height: "100%",
          }}
        >
          <CardHeader
            sx={{ color: "#26a69a" }}
            title="Influencer Brand Affinity"
          />

          <CardContent>
            {isEmpty(data?.affinity) ? (
              <Box>No Data for Influencer Brand Affinity</Box>
            ) : (
              <Grid container>
                {(data?.affinity.slice(0, 9) ?? []).map((item) => (
                  <Grid item xs={4} key={item.label}>
                    {/* <img src={item.iconUrl} alt={item.label} loading="lazy" /> */}
                    <Box sx={{ px: 2.5, py: 1.5 }}>{item.label}</Box>
                  </Grid>
                ))}
              </Grid>
            )}
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={6}>
        <Card sx={{ height: "100%" }}>
          <CardHeader sx={{ color: "#26a69a" }} title="Influencer Interests" />
          <CardContent>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                flexWrap: "wrap",
                listStyle: "none",
                p: 0.5,
                pl: 0,
                m: 0,
              }}
              component="ul"
            >
              {data?.interests?.slice(0, 8)?.map((i) => (
                <ChipListItem key={i}>
                  <Chip variant="outlined" label={i} />{" "}
                </ChipListItem>
              )) ?? <li>No Data for Influencer Interests</li>}
            </Box>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};
