import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import { chunk } from "lodash";
import { useAppSelector } from "../../../../app/hooks";
import { fNumber, fPercent } from "../../../../utils/formatNumber";
import { AudienceType } from "../../action";
import {
  selectAudienceTotalCount,
  selectInfluencerAudience,
} from "../../selector";

export const LocationPage: React.FC = () => {
  const data = useAppSelector(selectInfluencerAudience);
  const totalCount = useAppSelector(selectAudienceTotalCount);
  const countryFD = chunk(data?.followers?.country ?? [], 22);
  const countryLD = chunk(data?.likes?.country ?? [], 22);

  const cityFD = chunk(data?.followers?.city ?? [], 22);
  const cityLD = chunk(data?.likes?.city ?? [], 22);

  const countryPages = new Array(
    Math.max(countryFD.length, countryLD.length)
  ).fill(null);

  const cityPages = new Array(Math.max(cityFD.length, cityLD.length)).fill(
    null
  );

  return (
    <Grid container sx={{ pt: 2 }} spacing={2}>
      {countryPages.map((o, i) => (
        <>
          <Grid item xs={12}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography
                sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }}
                variant="h5"
              >
                Location By Country
              </Typography>
            </Box>
          </Grid>

          <Grid item xs={6}>
            {i < countryFD.length ? (
              <Card>
                <CardContent>
                  <Table size="small">
                    <TableHead>
                      <TableRow>
                        <TableCell component="th">Country</TableCell>
                        <TableCell component="th">Followers</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {countryFD[i]?.map((d) => (
                        <TableRow key={d.x}>
                          <TableCell>{d.x}</TableCell>
                          <TableCell>
                            {fNumber(
                              parseInt(
                                `${
                                  (totalCount[AudienceType.FOLLOWERS] * d.y) /
                                  100
                                }`,
                                10
                              )
                            )}
                            /{fPercent(d.y)}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </CardContent>
              </Card>
            ) : null}
          </Grid>

          <Grid item xs={6}>
            {i < countryLD.length ? (
              <Card>
                <CardContent>
                  <Table size="small">
                    <TableHead>
                      <TableRow>
                        <TableCell component="th">Country</TableCell>
                        <TableCell component="th">Likes</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {countryLD[i]?.map((d) => (
                        <TableRow key={d.x}>
                          <TableCell>{d.x}</TableCell>
                          <TableCell>
                            {fNumber(
                              parseInt(
                                `${
                                  (totalCount[AudienceType.LIKES] * d.y) / 100
                                }`,
                                10
                              )
                            )}
                            /{fPercent(d.y)}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </CardContent>
              </Card>
            ) : null}
          </Grid>
          <Grid
            item
            xs={12}
            sx={{ p: "0 !important", m: "0 !important" }}
            className="breakAfterMe"
          />
        </>
      ))}

      {cityPages.map((o, i, a) => (
        <>
          <Grid item xs={12}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography
                sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }}
                variant="h5"
              >
                Location By City
              </Typography>
            </Box>
          </Grid>

          <Grid item xs={6}>
            {i < cityFD.length ? (
              <Card>
                <CardContent>
                  <Table size="small">
                    <TableHead>
                      <TableRow>
                        <TableCell component="th">City</TableCell>
                        <TableCell component="th">Followers</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {cityFD[i]?.map((d) => (
                        <TableRow key={d.x}>
                          <TableCell>{d.x}</TableCell>
                          <TableCell>
                            {fNumber(
                              parseInt(
                                `${
                                  (totalCount[AudienceType.FOLLOWERS] * d.y) /
                                  100
                                }`,
                                10
                              )
                            )}
                            /{fPercent(d.y)}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </CardContent>
              </Card>
            ) : null}
          </Grid>

          <Grid item xs={6}>
            {i < cityLD.length ? (
              <Card>
                <CardContent>
                  <Table size="small">
                    <TableHead>
                      <TableRow>
                        <TableCell component="th">City</TableCell>
                        <TableCell component="th">Likes</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {cityLD[i]?.map((d) => (
                        <TableRow key={d.x}>
                          <TableCell>{d.x}</TableCell>
                          <TableCell>
                            {fNumber(
                              parseInt(
                                `${
                                  (totalCount[AudienceType.LIKES] * d.y) / 100
                                }`,
                                10
                              )
                            )}
                            /{fPercent(d.y)}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </CardContent>
              </Card>
            ) : null}
          </Grid>
          {a.length - 1 === i ? null : (
            <Grid
              item
              xs={12}
              sx={{ p: "0 !important", m: "0 !important" }}
              className="breakAfterMe"
            />
          )}
        </>
      ))}
    </Grid>
  );
};
