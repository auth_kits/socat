import {
  Box,
  Card,
  CardContent,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import { sentenceCase } from "change-case";
import { useAppSelector } from "../../../../app/hooks";
import { fNumber, fPercent } from "../../../../utils/formatNumber";
import {
  selectAudienceEthnicData,
  selectAudienceGenderData,
  selectAudienceLanugageData,
  selectInfluencerAudience,
} from "../../selector";

export const GenderSplitData: React.FC = () => {
  const data = useAppSelector(selectInfluencerAudience);
  const genderData = useAppSelector(selectAudienceGenderData);

  const ethnicityData = useAppSelector(selectAudienceEthnicData);

  const languageData = useAppSelector(selectAudienceLanugageData);
  // const hashTagData = chunk(data?.hashtags ?? [], 22);
  // const mentionData = chunk(data?.mentions ?? [], 22);

  // const noOfPages = new Array(
  //   Math.max(hashTagData.length, mentionData.length)
  // ).fill(null);

  return (
    <Grid container sx={{ pt: 2 }} spacing={2}>
      <Grid item xs={6}>
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }} variant="h5">
            Gender Split
          </Typography>
        </Box>
      </Grid>

      <Grid item xs={6}>
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }} variant="h5">
            Ethnicity
          </Typography>
        </Box>
      </Grid>

      <Grid item xs={12} />

      <Grid item xs={6}>
        <Card>
          <CardContent>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell component="th">Gender</TableCell>
                  <TableCell component="th">Followers</TableCell>
                  <TableCell component="th">Likes</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {genderData?.map((d) => (
                  <TableRow key={d.code}>
                    <TableCell>{sentenceCase(d.code)}</TableCell>
                    <TableCell>
                      {fNumber(d.followers)}/{fPercent(d?.followersWeight)}
                    </TableCell>
                    <TableCell>
                      {fNumber(d.likes)}/{fPercent(d?.likesWeight)}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      </Grid>

      <Grid item xs={6}>
        <Card>
          <CardContent>
            <Table size="small">
              <TableHead>
                <TableRow>
                  <TableCell component="th">Group</TableCell>
                  <TableCell component="th">Followers</TableCell>
                  <TableCell component="th">Likes</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {ethnicityData?.map((d) => (
                  <TableRow key={d.code}>
                    <TableCell>{sentenceCase(d.code)}</TableCell>
                    <TableCell>
                      {fNumber(d.followers)}/{fPercent(d?.followersWeight)}
                    </TableCell>
                    <TableCell>
                      {fNumber(d.likes)}/{fPercent(d?.likesWeight)}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      </Grid>

      <Grid item xs={12}>
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }} variant="h5">
            Language Of Audience
          </Typography>
        </Box>
      </Grid>
      <Grid item xs={6}>
        <Card>
          <CardContent>
            <Table size="small">
              <TableHead>
                <TableRow>
                  <TableCell component="th">Language</TableCell>
                  <TableCell component="th">Followers</TableCell>
                  <TableCell component="th">Likes</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {languageData[0]?.map((d) => (
                  <TableRow key={d.code}>
                    <TableCell>{sentenceCase(d.code)}</TableCell>
                    <TableCell>
                      {fNumber(d.followers)}/{fPercent(d?.followersWeight)}
                    </TableCell>
                    <TableCell>
                      {fNumber(d.likes)}/{fPercent(d?.likesWeight)}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={6}>
        <Card>
          <CardContent>
            <Table size="small">
              <TableHead>
                <TableRow>
                  <TableCell component="th">Language</TableCell>
                  <TableCell component="th">Followers</TableCell>
                  <TableCell component="th">Likes</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {languageData[1]?.map((d) => (
                  <TableRow key={d.code}>
                    <TableCell>{sentenceCase(d.code)}</TableCell>
                    <TableCell>
                      {fNumber(d.followers)}/{fPercent(d?.followersWeight)}
                    </TableCell>
                    <TableCell>
                      {fNumber(d.likes)}/{fPercent(d?.likesWeight)}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      </Grid>
      <Grid
        item
        xs={12}
        sx={{ p: "0 !important", m: "0 !important" }}
        className="breakAfterMe"
      />
    </Grid>
  );
};
