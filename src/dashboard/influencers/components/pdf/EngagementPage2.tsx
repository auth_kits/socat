import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Chip,
  Grid,
} from "@material-ui/core";
import { styled } from "@material-ui/core/styles";
import AlternateEmailIcon from "@material-ui/icons/AlternateEmail";
import { useAppSelector } from "../../../../app/hooks";
import { selectInfluencerEngagement } from "../../selector";

const ChipListItem = styled("li")(({ theme }) => ({
  margin: theme.spacing(0.5),
  "& span": {
    // marginTop: "-15px",
    paddingBottom: "15px",
  },
}));

// replace this to selectors when backend is ready and integration is pending
export const EngagementPage2: React.FC<{ pdf?: boolean }> = () => {
  const data = useAppSelector(selectInfluencerEngagement);

  return (
    <Grid container sx={{ pt: 2 }} spacing={2}>
      <Grid item xs={6}>
        <Card
          sx={{
            height: "100%",
          }}
        >
          <CardHeader sx={{ color: "#26a69a" }} title="Popular #" />
          <CardContent>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                flexWrap: "wrap",
                listStyle: "none",
                p: 0.5,
                pl: 0,
                m: 0,
              }}
              component="ul"
            >
              {(data?.hashtags?.slice(0, 9) ?? []).map((item) => (
                <ChipListItem key={item.tag}>
                  <Box
                    sx={{
                      p: 0.5,
                    }}
                  >
                    <Chip variant="outlined" label={`#${item.tag}`} />
                  </Box>
                </ChipListItem>
              ))}
            </Box>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={6}>
        <Card
          sx={{
            height: "100%",
          }}
        >
          <CardHeader sx={{ color: "#26a69a" }} title="Popular @" />
          <CardContent>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                flexWrap: "wrap",
                listStyle: "none",
                p: 0.5,
                pl: 0,
                m: 0,
              }}
              component="ul"
            >
              {data?.mentions?.slice(0, 8)?.map((i) => (
                <ChipListItem key={i.tag}>
                  <Chip
                    variant="outlined"
                    icon={<AlternateEmailIcon />}
                    label={i.tag}
                  />{" "}
                </ChipListItem>
              ))}
            </Box>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};
