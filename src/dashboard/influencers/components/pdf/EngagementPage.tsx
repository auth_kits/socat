import { Box, Card, CardHeader, Grid } from "@material-ui/core";
import { styled } from "@material-ui/core/styles";
import { merge } from "lodash";
import ReactApexChart from "react-apexcharts";
import { useAppDispatch, useAppSelector } from "../../../../app/hooks";
import { BaseOptionChart } from "../../../../components/charts";
import { fPercent, fShortenNumber } from "../../../../utils/formatNumber";
import { fDate } from "../../../../utils/formatTime";
import { ELoadingStatus } from "../../action";
import {
  selectengagementRate,
  selectInfluencerDisplayName,
  selectInfluencerEngagement,
  selectStatus,
} from "../../selector";

const ChipListItem = styled("li")(({ theme }) => ({
  margin: theme.spacing(0.5),
}));

// replace this to selectors when backend is ready and integration is pending
export const EngagementPage: React.FC<{ pdf?: boolean }> = () => {
  const erBarColor = "#ffa726";
  const dispatch = useAppDispatch();
  const data = useAppSelector(selectInfluencerEngagement);
  const displayName = useAppSelector(selectInfluencerDisplayName);
  const status = useAppSelector(selectStatus);
  const engagementRate = useAppSelector(selectengagementRate);

  const barOptions = merge(BaseOptionChart(), {
    tooltip: {
      x: {
        show: true,
        formatter: (seriesName, opts) =>
          opts.w.config.series[opts.seriesIndex].data[opts.dataPointIndex]
            .description,
        title: {
          formatter: () => "",
        },
      },
      marker: { show: false },
      y: {
        formatter: (seriesName) => `${fShortenNumber(seriesName)} accounts`,
        title: {
          formatter: () => "",
        },
      },
    },
    dataLabels: {
      enabled: true,
      textAnchor: "start",
      offsetY: -12,
      formatter(value, { seriesIndex, w, dataPointIndex }) {
        if (
          w?.config?.series[seriesIndex]?.data[dataPointIndex]?.user &&
          w?.config?.series[seriesIndex]?.data[dataPointIndex]?.median
        ) {
          return `${displayName}`;
        }

        if (w?.config?.series[seriesIndex]?.data[dataPointIndex]?.user) {
          return displayName;
        }

        if (w?.config?.series[seriesIndex]?.data[dataPointIndex]?.median) {
          return "";
        }
        return "";
      },
      style: {
        colors: ["#000"],
      },
    },
    colors: [
      ({ value, seriesIndex, w, dataPointIndex }) => {
        if (w?.config?.series[seriesIndex]?.data[dataPointIndex]?.user) {
          // return "#f05d56";
          return "#00bcd4";
        }

        if (w?.config?.series[seriesIndex]?.data[dataPointIndex]?.median) {
          return "#ffd54f";
        }
        return "#ffecb3";
      },
    ],
    plotOptions: {
      bar: {
        horizontal: true,
        // vertical: false,
        barHeight: "60%",
        borderRadius: 2,
      },
    },
    yaxis: {
      type: "numeric",
      title: {
        text: "Engagement rate",
        style: {
          color: "#111",
        },
      },
      labels: {
        style: {
          colors: "#111",
        },
      },
    },
    xaxis: {
      type: "numeric",
      labels: {
        formatter: (seriesName) => fShortenNumber(seriesName),
        style: {
          colors: "#111",
        },
      },
      title: {
        text: "No of accounts",
        style: {
          color: "#111",
        },
      },
    },
  });

  const erBarOptions = merge(BaseOptionChart(), {
    tooltip: {
      x: {
        show: true,
        formatter: (seriesName, opts) => fDate(seriesName),
        title: {
          formatter: () => "",
        },
      },
      marker: { show: false },
      y: {
        formatter: (seriesName) => fShortenNumber(seriesName),
        title: {
          formatter: () => "",
        },
      },
    },

    stroke: {
      width: 5,
    },
    colors: [erBarColor],
    plotOptions: {
      bar: { vertical: true, barHeight: "28%", borderRadius: 2 },
    },
    yaxis: {
      type: "numeric",
      title: {
        text: "Count",
      },
      labels: {
        formatter: (val) => fShortenNumber(val).toLocaleUpperCase(),
      },
    },
    xaxis: {
      type: "datetime",
      title: {
        text: "Period",
      },
    },
  });

  const isLoading = [ELoadingStatus.LOADING, ELoadingStatus.INITIAL].includes(
    status
  );

  return (
    <Grid container sx={{ pt: 2 }} spacing={2}>
      <Grid item xs={4}>
        <Card>
          <CardHeader
            title="Engagement Spread Comments"
            sx={{ color: "#26a69a" }}
          />
          <Box sx={{ p: 3, pb: 1 }} dir="ltr">
            <ReactApexChart
              type="bar"
              series={[
                {
                  data: data ? data.comments ?? [] : [],
                },
              ]}
              options={erBarOptions}
              height={364}
            />
          </Box>
        </Card>
      </Grid>

      <Grid item xs={4}>
        <Card>
          <CardHeader
            title="Engagement Spread Likes"
            sx={{ color: "#26a69a" }}
          />
          <Box sx={{ p: 3, pb: 1 }} dir="ltr">
            <ReactApexChart
              type="bar"
              series={[
                {
                  data: data ? data.likes ?? [] : [],
                },
              ]}
              options={erBarOptions}
              height={364}
            />
          </Box>
        </Card>
      </Grid>

      <Grid item xs={4}>
        <Card>
          <CardHeader
            title={`Engagement Rate - ${fPercent(engagementRate)}`}
            sx={{ color: "#26a69a" }}
          />

          <Box sx={{ p: 3, pb: 1 }} dir="ltr">
            <ReactApexChart
              type="bar"
              series={[
                {
                  name: "Engagement rate",
                  data: data?.rate ?? [],
                },
              ]}
              options={barOptions}
              height={364}
            />
          </Box>
        </Card>
      </Grid>

      <Grid item xs={12} sx={{ p: 0, m: 0 }} className="breakAfterMe" />
    </Grid>
  );
};
