import { Box, Card, CardHeader, Grid, Typography } from "@material-ui/core";
import { styled } from "@material-ui/core/styles";
import EqualizerIcon from "@material-ui/icons/Equalizer";
import ForumIcon from "@material-ui/icons/Forum";
import GroupsIcon from "@material-ui/icons/Groups";
import ThumbsUpDownIcon from "@material-ui/icons/ThumbsUpDown";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { ApexOptions } from "apexcharts";
import { sentenceCase } from "change-case";
import { merge } from "lodash";
import React, { useLayoutEffect, useRef, useState } from "react";
import ReactApexChart from "react-apexcharts";
import * as wc from "wordcloud";
import { useAppDispatch, useAppSelector } from "../../../../app/hooks";
import { BaseOptionChart } from "../../../../components/charts";
import { fShortenNumber } from "../../../../utils/formatNumber";
import { ELoadingStatus } from "../../action";
import {
  selectInfluencerLocation,
  selectInfluencerOverview,
  selectInfluencerVerfied,
  selectStatus,
} from "../../selector";
import { NumberTile } from "../NumberTile";

const ChipListItem = styled("li")(({ theme }) => ({
  margin: theme.spacing(0.5),
}));

export const OverviewPage: React.FC<{ pdf?: boolean }> = ({ pdf: isPdf }) => {
  const influencerLocation = useAppSelector(selectInfluencerLocation);
  const influencerVerified = useAppSelector(selectInfluencerVerfied);
  const graphColor = "#8bc34a";
  const tagRef = useRef(null);
  const dispatch = useAppDispatch();
  const data = useAppSelector(selectInfluencerOverview);
  const [growthTab, setGrowthTab] = useState("followers");
  const status = useAppSelector(selectStatus);
  const [interestModal, setInterestModal] = useState(false);
  const [brandModal, setBrandModal] = useState(false);

  useLayoutEffect(() => {
    if (tagRef.current && data?.keywords) {
      wc(tagRef.current, {
        list: data?.keywords,
        weightFactor: 2,
        shrinkToFit: false,
        shape: "square",
        rotationSteps: 2,
      });
    }
    return () => {
      // stop the renderring
      wc?.stop();
    };
  }, [tagRef, data?.keywords]);

  const chartOptions: ApexOptions = merge(BaseOptionChart(), {
    chart: {
      offsetX: 0,
    },
    grid: {
      padding: {
        top: 5,
        right: 20,
        bottom: 5,
        left: 20,
      },
    },
    xaxis: {
      type: "datetime",
      title: {
        text: "Period",
      },
    },
    yaxis: {
      labels: {
        formatter: (seriesName) =>
          fShortenNumber(seriesName).toLocaleUpperCase(),
      },
      type: "numeric",
      title: {
        text: "Count",
      },
    },
    stroke: {
      width: 7,
    },
    colors: [graphColor],
    fill: {
      opacity: 5,
    },
    tooltip: {
      shared: true,
      intersect: false,
      y: {
        formatter: (y) => {
          if (typeof y !== "undefined") {
            return `${fShortenNumber(y.toFixed(0))}`;
          }
          return y;
        },
      },
    },
  });

  const isLoading = [ELoadingStatus.LOADING, ELoadingStatus.INITIAL].includes(
    status
  );

  return (
    <Grid container spacing={2} style={{ flexGrow: 1 }}>
      <Grid item xs={3}>
        <NumberTile
          num="one"
          text="Followers"
          count={data?.followers}
          icon={
            <GroupsIcon
              sx={{
                pr: 1,
              }}
              fontSize="large"
            />
          }
        />
      </Grid>
      <Grid item xs={3}>
        <NumberTile
          num="two"
          text="Average Views"
          count={data?.views}
          icon={
            <VisibilityIcon
              sx={{
                pr: 1,
              }}
              fontSize="large"
            />
          }
        />
      </Grid>
      <Grid item xs={3}>
        <NumberTile
          num="three"
          text="Average Likes"
          count={data?.likes}
          icon={
            <ThumbsUpDownIcon
              fontSize="large"
              sx={{
                pr: 1,
              }}
            />
          }
        />
      </Grid>
      <Grid item xs={3}>
        <NumberTile
          text="Average Comments"
          num="four"
          count={data?.comments}
          icon={
            <ForumIcon
              fontSize="large"
              sx={{
                pr: 1,
              }}
            />
          }
        />
      </Grid>
      <Grid item xs={6}>
        <NumberTile
          num="five"
          text={
            <Box
              flexDirection="row"
              alignItems="center"
              sx={{ display: "flex" }}
              justifyContent="center"
            >
              <span>Paid Post Performance</span>
            </Box>
          }
          percentage
          count={data?.performance ?? 0}
        />
      </Grid>
      <Grid item xs={6}>
        <NumberTile
          num="six"
          text="Engagement Rate"
          percentage
          count={data?.engagementRate ?? 0}
          icon={
            <EqualizerIcon
              sx={{
                pr: 1,
              }}
              fontSize="large"
            />
          }
        />
      </Grid>
      <Grid item xs={12} sx={{ p: 0, m: 0 }} className="breakAfterMe" />
      <Grid item xs={12}>
        <Box
          sx={{
            pt: 4,
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography
            sx={{ whiteSpace: "nowrap", pr: 1, pb: 0, mb: 0 }}
            variant="h5"
          >
            Influencer Details
          </Typography>
        </Box>
      </Grid>
      <Grid item xs={4}>
        <Card>
          <CardHeader title="Followers" sx={{ color: "#26a69a" }} />
          <Box sx={{ p: 3, pb: 1 }} dir="ltr">
            <ReactApexChart
              type="line"
              series={[
                {
                  name: sentenceCase("followers"),
                  data: data?.growthData?.followers ?? [],
                },
              ]}
              options={chartOptions}
              height={220}
            />
          </Box>
        </Card>
      </Grid>
      <Grid item xs={4}>
        <Card>
          <CardHeader title="Following" sx={{ color: "#26a69a" }} />
          <Box sx={{ p: 3, pb: 1 }} dir="ltr">
            <ReactApexChart
              type="line"
              series={[
                {
                  name: sentenceCase("Following"),
                  data: data?.growthData.following ?? [],
                },
              ]}
              options={chartOptions}
              height={220}
            />
          </Box>
        </Card>
      </Grid>
      <Grid item xs={4}>
        <Card>
          <CardHeader title="Likes" sx={{ color: "#26a69a" }} />
          <Box sx={{ p: 3, pb: 1 }} dir="ltr">
            <ReactApexChart
              type="line"
              series={[
                {
                  name: sentenceCase("Likes"),
                  data: data?.growthData.likes ?? [],
                },
              ]}
              options={chartOptions}
              height={220}
            />
          </Box>
        </Card>
      </Grid>
    </Grid>
  );
};
