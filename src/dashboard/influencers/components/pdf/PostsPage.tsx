import {
  Box,
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Grid,
  IconButton,
  Typography,
} from "@material-ui/core";
import CommentBankIcon from "@material-ui/icons/CommentBank";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import { chunk } from "lodash";
import { useAppSelector } from "../../../../app/hooks";
import { fShortenNumber } from "../../../../utils/formatNumber";
import { ELoadingStatus } from "../../action";
import { selectInfluencerPosts, selectStatus } from "../../selector";

export const PostsPage: React.FC<{ pdf?: boolean }> = () => {
  const status = useAppSelector(selectStatus);
  const data = useAppSelector(selectInfluencerPosts);
  const renderPost = (post) => (
    <Grid
      item
      xs={3}
      sx={{
        pageBreakInside: "avoid",
      }}
      className="dontBreakMe"
    >
      <Card
        sx={{
          height: "100%",
          maxHeight: 420,
        }}
        key={post.postUrl}
      >
        <CardActionArea
          sx={{
            height: "100%",
          }}
          href={post.postUrl}
          rel="nofollow"
          target="_blank"
        >
          <CardMedia
            component="img"
            image={post?.imageUrl}
            crossOrigin="anonymous"
          />
          <CardHeader subheader={post.created} />
          <CardContent>
            <Typography
              sx={{
                mb: 6,
              }}
              variant="body2"
              color="text.secondary"
            >
              {post.text}
            </Typography>
          </CardContent>
          <Box sx={{ flexGrow: 1 }} />
          <CardActions
            sx={{
              bottom: 0,
              position: "absolute",
              width: "100%",
              backgroundColor: "#ffffff",
            }}
          >
            <Button disabled fullWidth startIcon={<CommentBankIcon />}>
              {fShortenNumber(post.likes).toLocaleUpperCase()}
            </Button>
            <Button disabled fullWidth startIcon={<ThumbUpIcon />}>
              {fShortenNumber(post.comments).toLocaleUpperCase()}
            </Button>
          </CardActions>
        </CardActionArea>
      </Card>
    </Grid>
  );

  return (
    <Grid container spacing={2}>
      {chunk(data?.sponsored, 8).map((ps) => (
        <>
          <Grid item xs={12}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography
                sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }}
                variant="h5"
              >
                Sponsored Posts
              </Typography>
            </Box>
          </Grid>

          {ps.map(renderPost)}
          <Grid
            item
            xs={12}
            className="breakAfterMe"
            sx={{ p: "0 !important", m: "0 !imporant" }}
          />
        </>
      ))}

      {chunk(data?.top, 8).map((ps, i, a) => (
        <>
          <Grid item xs={12}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography
                sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }}
                variant="h5"
              >
                Top Posts
              </Typography>
            </Box>
          </Grid>

          {ps.map(renderPost)}
          {a.length - 1 === i ? null : (
            <Grid
              item
              xs={12}
              className="breakAfterMe"
              sx={{ p: "0 !important", m: "0 !imporant" }}
            />
          )}
        </>
      ))}
    </Grid>
  );
};
