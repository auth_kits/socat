import { Container, Typography } from "@material-ui/core";
import RoomIcon from "@material-ui/icons/Room";
import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import { Box } from "@material-ui/system";
import { useLayoutEffect } from "react";
import { useParams, useRouteMatch } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../../app/hooks";
import { setCurrentInfluencerId } from "../../action";
import { selectBasicLoading, selectInfluencerDetail } from "../../selector";

const styles = (theme) => ({
  listItemText: {
    fontSize: "0.7em",
  },
});

export const ProfilePage: React.FC<{ pdf?: boolean }> = ({ pdf }) => {
  const { path, url } = useRouteMatch();

  const params = useParams<{ id: string }>();
  const dispatch = useAppDispatch();
  useLayoutEffect(() => {
    if (params?.id) {
      dispatch(
        setCurrentInfluencerId({
          influencerId: params?.id,
        })
      );
    }
  }, [dispatch, params?.id]);

  const networkStatus = useAppSelector(selectBasicLoading);

  const influencer = useAppSelector(selectInfluencerDetail);
  return (
    <Box>
      <Container maxWidth={false}>
        <Box
          sx={{
            height: "100%",
            width: "100% !important",
            display: "flex",
            position: "sticky",
            flexDirection: "column",
            margin: "0 auto",
            textAlign: "center",
            alignItems: "center",
            px: 0,
            py: 3,
          }}
        >
          <div
            style={{
              borderRadius: "50%",
              backgroundImage: `url(${influencer?.imageUrl})`,
              backgroundSize: "cover",
              width: "160px",
              height: "160px",
              borderWidth: 2,
              resize: "both",
            }}
          />
          <Typography
            variant="h5"
            sx={{
              pt: 2,
              alignItems: "center",
              display: "flex",
              whiteSpace: "nowrap",
            }}
          >
            {influencer?.displayName}
            &nbsp;
            {influencer?.verified && <VerifiedUserIcon color="info" />}
          </Typography>
          <Typography
            variant="body1"
            sx={{
              fontSize: {
                md: 14,
              },
              color: "#455a64",
            }}
          >
            {influencer?.tagLine}
          </Typography>
          {influencer?.location?.label && (
            <Typography
              variant="body1"
              sx={{
                fontSize: {
                  md: 14,
                },
                color: "#455a64",
                display: "flex",
                alignItems: "center",
                py: 0,
                px: 0,
              }}
            >
              <RoomIcon color="primary" />
              {influencer?.location?.label}
            </Typography>
          )}
        </Box>

        {/* <Box sx={{ mb: 5, mx: 2.5 }}>
                <Link underline="none" component={RouterLink} to="/account">
                  <AccountStyle>
                    <Box sx={{ ml: 2 }}>
                      <Typography
                        variant="subtitle2"
                        sx={{ color: "text.primary" }}
                      >
                        {user?.name}
                      </Typography>
                      <Typography variant="body2" sx={{ color: 'text.secondary' }}>
                {user?.description}
              </Typography>
              </Box>
                  </AccountStyle>
                </Link>
              </Box>

              <NavSection navConfig={sidebarConfig} /> */}
      </Container>
    </Box>
  );
};
