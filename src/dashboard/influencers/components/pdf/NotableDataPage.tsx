import { Box, Grid, Typography } from "@material-ui/core";
import { chunk } from "lodash";
import { useAppSelector } from "../../../../app/hooks";
import { selectInfluencerAudience, selectStatus } from "../../selector";
import { AudienceTable } from "./AudienceTable";

export const NotableDataPage: React.FC = () => {
  const influencerData = useAppSelector(selectInfluencerAudience);
  const status = useAppSelector(selectStatus);

  return (
    <Grid container sx={{ pt: 2 }} spacing={2}>
      {chunk(influencerData?.followers?.notable ?? [], 30).map((data) => (
        <>
          <Grid item xs={12}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography
                sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }}
                variant="h5"
              >
                Notable Followers
              </Typography>
            </Box>
          </Grid>
          <AudienceTable data={data} />
          <Grid
            item
            xs={12}
            sx={{ p: "0 !important", m: "0 !important" }}
            className="breakAfterMe"
          />
        </>
      ))}

      {chunk(influencerData?.likes?.notable ?? [], 30).map((data) => (
        <>
          <Grid item xs={12}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography
                sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }}
                variant="h5"
              >
                Notable Likes
              </Typography>
            </Box>
          </Grid>
          <AudienceTable data={data} />
          <Grid
            item
            xs={12}
            sx={{ p: "0 !important", m: "0 !important" }}
            className="breakAfterMe"
          />
        </>
      ))}
    </Grid>
  );
};
