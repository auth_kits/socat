import {
  Avatar,
  Box,
  Button,
  ButtonGroup,
  Card,
  CardContent,
  CardHeader,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  List,
  ListItemAvatar,
  ListItemButton,
  ListItemText,
  Stack,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tabs,
  Typography,
} from "@material-ui/core";
import { styled } from "@material-ui/core/styles";
import { ApexOptions } from "apexcharts";
import { sentenceCase } from "change-case";
import { isEmpty, merge } from "lodash";
import { useState } from "react";
import ReactApexChart from "react-apexcharts";
import { useAppDispatch, useAppSelector } from "../../../../app/hooks";
import { BaseOptionChart } from "../../../../components/charts";
import {
  fNumber,
  fPercent,
  fShortenNumber,
} from "../../../../utils/formatNumber";
import { AudienceType, ELoadingStatus, setAudienceType } from "../../action";
import {
  selectAudienceData,
  selectAudienceExtraData,
  selectAudienceTotalCount,
  selectStatus,
} from "../../selector";
import { NumberTile } from "../NumberTile";
import { ViewMoreComponent } from "../ViewMoreComponent";

const ChipListItem = styled("li")(({ theme }) => ({
  margin: theme.spacing(0.5),
}));

const CHART_HEIGHT = 264;
const LEGEND_HEIGHT = 72;

const ChartWrapperStyle = styled(Box)(({ theme }) => ({
  // height: CHART_HEIGHT,
  marginBottom: theme.spacing(5),
  paddingBottom: theme.spacing(5),
  // "& .apexcharts-canvas svg": { height: CHART_HEIGHT - 40 },
  // "& .apexcharts-canvas svg,.apexcharts-canvas foreignObject": {
  //   overflow: "visible",
  // },
  "& .apexcharts-legend": {
    // height: LEGEND_HEIGHT,
    // alignContent: "center",
    // position: "relative !important",
    top: `calc(${CHART_HEIGHT - LEGEND_HEIGHT}px) !important`,
    bottom: 0,
  },
}));

export const AudiencePage2: React.FC<{
  audienceType: AudienceType;
  pdf?: boolean;
}> = ({ audienceType }) => {
  const [interestModal, setInterestModal] = useState(false);
  const [notableModal, setNotableModal] = useState(false);
  const [lookalikeModal, setLookalikeModal] = useState(false);
  const [brandModal, setBrandModal] = useState(false);
  const dispatch = useAppDispatch();
  const data = useAppSelector(selectAudienceData);
  const status = useAppSelector(selectStatus);
  const extraData = useAppSelector(selectAudienceExtraData);

  const totalCount = useAppSelector(selectAudienceTotalCount);
  const locationOptions: ApexOptions = merge(BaseOptionChart(), {
    stroke: {
      width: 5,
    },
    colors: ["#26c6da", "#ffca28"],
    dataLabels: {
      enabled: true,
      textAnchor: "start",
      offsetY: -12,
      formatter(value) {
        return fPercent(value);
      },
      style: {
        colors: ["#f05d56"],
      },
    },
    plotOptions: {
      bar: {
        horizontal: true,
        barHeight: "15%",
        borderRadius: 2,
        padding: 10,

        dataLabels: {
          enabled: true,
          position: "right",
        },
      },
    },
    yaxis: {
      type: "category",
    },
    tooltip: {
      enabled: false,
      marker: { show: false },
      //   x: { show: true },
      //   y: {
      //     title: {
      //       formatter: (ser, opts) => "",
      //     },
      //     formatter: (ser) => fPercent(ser),
      //   },
    },
    xaxis: {
      type: "numeric",
      labels: {
        format: "asd",
      },
      title: {
        text: "Percent",
      },
    },
  } as Partial<ApexOptions>);

  const ageBarOptions = merge(BaseOptionChart(), {
    stroke: {
      width: 5,
    },
    yaxis: {
      type: "numeric",
      labels: {
        formatter: (val) => fPercent(val),
      },
    },
    plotOptions: {
      bar: {
        barHeight: "42%",
        horizontal: false,
        columnWidth: "42%",
        borderRadius: 4,
      },
    },
    xaxis: {
      type: "category",
    },
  } as Partial<ApexOptions>);

  const pieChartOptions = merge(BaseOptionChart(), {
    labels: data?.gender.map((v) => sentenceCase(v.code)),
    stroke: { colors: ["#fff"] },
    legend: {
      floating: true,
      horizontalAlign: "right",
      position: "bottom",
      labels: {
        colors: ["#3f51b5"],
        useSeriesColors: false,
      },
      offsetY: 20,
    },
    dataLabels: { enabled: true, dropShadow: { enabled: true } },
    tooltip: {
      fillSeriesColor: false,
      y: {
        formatter: (seriesName) => fPercent(seriesName),
      },
    },
    plotOptions: {
      pie: { customScale: 0.8, donut: { labels: { show: false } } },
    },
  });

  const rechabilityChartOptions = merge(BaseOptionChart(), {
    colors: ["#afb42b"],
    dataLabels: {
      enabled: true,
      textAnchor: "middle",
      formatter(value) {
        return fPercent(value);
      },
      style: {
        colors: ["#f05d56"],
      },
      offsetY: -25,
    },
    yaxis: {
      type: "numeric",
      labels: {
        formatter: (val) => fPercent(val),
      },
    },
    tooltip: {
      enabled: false,
      marker: { show: false },
      y: {
        title: {
          formatter: (ser) => ``,
        },
        formatter: (ser) => fPercent(ser),
      },
    },
    plotOptions: {
      bar: {
        barHeight: "28%",
        horizontal: false,
        columnWidth: "28%",
        borderRadius: 4,
        dataLabels: {
          enabled: true,
          position: "top",
        },
      },
    },
    xaxis: {
      type: "category",
      title: {
        text: "Followings",
      },
    },
  } as Partial<ApexOptions>);

  const isLoading = [ELoadingStatus.INITIAL, ELoadingStatus.LOADING].includes(
    status
  );

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Box
          sx={{
            pt: 4,
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }} variant="h5">
            Audience Details By{" "}
            {audienceType === AudienceType.FOLLOWERS ? "Followers" : "Likes"}
          </Typography>
        </Box>
      </Grid>

      {audienceType === AudienceType.FOLLOWERS && (
        <Grid item xs={12} sm={12} md={6}>
          <NumberTile
            num="four"
            text={
              <Box
                flexDirection="row"
                alignItems="center"
                sx={{ display: "flex" }}
                justifyContent="center"
              >
                <span>Follower&apos;s Credibility</span>
              </Box>
            }
            percentage
            count={extraData?.followersCreds}
          />
        </Grid>
      )}
      {audienceType === AudienceType.FOLLOWERS && (
        <Grid item xs={12} sm={12} md={6}>
          <NumberTile
            num="five"
            text={
              <Box
                flexDirection="row"
                alignItems="center"
                sx={{ display: "flex" }}
                justifyContent="center"
              >
                <span>Notable Followers</span>
              </Box>
            }
            percentage
            count={extraData?.notableFollowers}
          />
        </Grid>
      )}
      {audienceType === AudienceType.LIKES && (
        <>
          <Grid item xs={12} sm={12} md={4}>
            <NumberTile
              num="four"
              text={
                <Box
                  flexDirection="row"
                  alignItems="center"
                  sx={{ display: "flex" }}
                  justifyContent="center"
                >
                  <span>Liker Credibility</span>
                </Box>
              }
              percentage
              count={extraData?.likerCreds}
            />
          </Grid>
          <Grid item xs={12} sm={12} md={4}>
            <NumberTile
              num="five"
              text={
                <Box
                  flexDirection="row"
                  alignItems="center"
                  sx={{ display: "flex" }}
                  justifyContent="center"
                >
                  <span>Notable Likes</span>
                </Box>
              }
              percentage
              count={extraData?.notableLikes}
            />
          </Grid>
          <Grid item xs={12} sm={12} md={4}>
            <NumberTile
              num="six"
              text={
                <Box
                  flexDirection="row"
                  alignItems="center"
                  sx={{ display: "flex" }}
                  justifyContent="center"
                >
                  <span>Likes not from Followers</span>
                </Box>
              }
              percentage
              count={extraData?.nonFollowerLikes}
            />
          </Grid>
        </>
      )}

      <Grid item xs={4}>
        <Card>
          <CardHeader sx={{ color: "#26a69a" }} title="Audience Reachability" />
          <CardContent>
            <ReactApexChart
              type="bar"
              series={[
                {
                  data: data?.reachability,
                },
              ]}
              options={rechabilityChartOptions}
              height={240}
            />
          </CardContent>
        </Card>
      </Grid>

      {["country", "city"].map((locationType) => (
        <Grid key={locationType} item xs={4}>
          <Card>
            <CardHeader
              sx={{ color: "#26a69a" }}
              title={`Location By ${sentenceCase(locationType)}`}
            />
            <Box sx={{ p: 3, pt: 0, pb: 1 }} dir="ltr">
              <ReactApexChart
                type="bar"
                height={CHART_HEIGHT}
                series={[
                  {
                    data: (data && data[locationType].slice(0, 5)) ?? [],
                  },
                ]}
                options={locationOptions}
              />
            </Box>
            <ViewMoreComponent
              hideMore
              label={`Location By ${sentenceCase(locationType)}`}
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Location</TableCell>
                      <TableCell component="th">
                        {audienceType === AudienceType.FOLLOWERS && "Followers"}
                        {audienceType === AudienceType.LIKES && "Likes"}
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {((data && data[locationType]) ?? [])?.map((d) => (
                      <TableRow key={d.x}>
                        <TableCell>{d.x}</TableCell>
                        <TableCell>
                          {fNumber(
                            parseInt(
                              `${
                                (totalCount[
                                  audienceType ?? AudienceType.FOLLOWERS
                                ] *
                                  d.y) /
                                100
                              }`,
                              10
                            )
                          )}
                          /{fPercent(d.y)}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
            />
          </Card>
        </Grid>
      ))}
      <Grid item xs={12} sx={{ p: 0, m: 0 }} />
      <Grid item xs={4}>
        <Card>
          <CardHeader
            sx={{ color: "#26a69a" }}
            title="Age &amp; Gender Split"
          />
          <CardContent>
            <Box sx={{ p: 3, pt: 0, pb: 1 }} dir="ltr">
              <ReactApexChart
                key="age-bar"
                type="bar"
                series={[
                  {
                    name: "Male",
                    data: data?.age?.male ?? [],
                  },
                  {
                    name: "Female",
                    data: data?.age?.female ?? [],
                  },
                ]}
                options={ageBarOptions}
                height={CHART_HEIGHT}
              />
            </Box>
            <ViewMoreComponent
              hideMore
              label="Age Split"
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Age</TableCell>
                      <TableCell component="th">
                        {audienceType === AudienceType.FOLLOWERS && "Followers"}
                        {audienceType === AudienceType.LIKES && "Likes"}
                      </TableCell>
                      <TableCell>Male</TableCell>
                      <TableCell>Female</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.commonAge?.map((d) => (
                      <TableRow key={d.x}>
                        <TableCell>{d.x}</TableCell>
                        <TableCell>
                          {fNumber(
                            parseInt(
                              `${(totalCount[audienceType] * d.y) / 100}`,
                              10
                            )
                          )}
                        </TableCell>
                        <TableCell>
                          {fNumber(
                            parseInt(
                              `${
                                (totalCount[audienceType] * d?.count?.male) /
                                100
                              }`,
                              10
                            )
                          )}
                          /{fPercent(d?.count?.male)}
                        </TableCell>
                        <TableCell>
                          {fNumber(
                            parseInt(
                              `${
                                (totalCount[audienceType] * d?.count?.female) /
                                100
                              }`,
                              10
                            )
                          )}
                          /{fPercent(d?.count?.female)}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
            />
          </CardContent>
        </Card>
      </Grid>
      {["ethnicity", "language"].map((basicOtherTabs) => (
        <Grid item xs={4} key={basicOtherTabs}>
          <Card>
            <CardHeader
              sx={{ color: "#26a69a" }}
              title={sentenceCase(basicOtherTabs)}
            />
            <Box sx={{ p: 3, pt: 0, pb: 1 }} dir="ltr">
              <ReactApexChart
                height={CHART_HEIGHT + 30}
                key="ethnic-bar"
                type="bar"
                series={[
                  {
                    name: sentenceCase(basicOtherTabs),
                    data: (data && data[basicOtherTabs])?.slice(0, 5),
                  },
                ]}
                options={locationOptions}
              />
            </Box>
            <ViewMoreComponent
              hideMore
              // hideMore={((data && data[basicOtherTabs]) ?? []).length <= 5}
              label={sentenceCase(basicOtherTabs)}
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Name</TableCell>
                      <TableCell component="th">
                        {audienceType === AudienceType.FOLLOWERS && "Followers"}
                        {audienceType === AudienceType.LIKES && "Likes"}
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {((data && data[basicOtherTabs]) ?? [])?.map((d) => (
                      <TableRow key={d.x}>
                        <TableCell>{d.x}</TableCell>
                        <TableCell>
                          {fNumber(
                            parseInt(
                              `${
                                (totalCount[
                                  audienceType ?? AudienceType.FOLLOWERS
                                ] *
                                  d?.y) /
                                100
                              }`,
                              10
                            )
                          )}{" "}
                          / {fPercent(d?.y)}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
            />
          </Card>
        </Grid>
      ))}
      <Grid item xs={12} sx={{ p: 0, m: 0 }} className="breakAfterMe" />

      <Grid item xs={12} />
      {isEmpty(data?.lookalike) ? null : (
        <Grid item xs={12} sm={12} md={6}>
          <Box
            sx={{
              pt: 3,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
              Audience Lookalike
            </Typography>
          </Box>
        </Grid>
      )}
      <Grid item xs={12} sm={12} md={isEmpty(data?.lookalike) ? 12 : 6}>
        <Box
          sx={{
            pt: 3,
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
            Notable Followers
          </Typography>
        </Box>
      </Grid>
      {isEmpty(data?.lookalike) ? null : (
        <Grid item xs={12} sm={12} md={6}>
          <Dialog
            open={lookalikeModal}
            onClose={() => setLookalikeModal(false)}
            aria-labelledby="notable-modal-title"
            aria-describedby="notable-modal-description"
          >
            <DialogTitle id="notable-modal-title">
              Audience Lookalike
            </DialogTitle>
            <DialogContent>
              <List
                sx={{
                  width: "100%",
                }}
              >
                {data?.lookalike?.map((n) => (
                  <ListItemButton
                    component="a"
                    alignItems="flex-start"
                    key={n.influencerId}
                    href={n.url ?? "#"}
                    target="_blank"
                    rel="nofollow"
                  >
                    <ListItemAvatar>
                      <Avatar src={n.imageUrl} />
                    </ListItemAvatar>
                    <ListItemText
                      primary={n.displayName}
                      secondary={`${fShortenNumber(
                        n.followers
                      )} followers, ${fShortenNumber(
                        n.engagements
                      )} engagements`}
                    />
                  </ListItemButton>
                ))}
              </List>
            </DialogContent>
            <DialogActions>
              <Button onClick={() => setLookalikeModal(false)} autoFocus>
                Close
              </Button>
            </DialogActions>
          </Dialog>
          <Card
            sx={{
              height: "100%",
            }}
          >
            <CardContent>
              <List
                sx={{
                  width: "100%",
                }}
              >
                {data?.lookalike?.slice(0, 3).map((n) => (
                  <ListItemButton
                    component="a"
                    alignItems="flex-start"
                    key={n.influencerId}
                    href={n.url ?? "#"}
                    target="_blank"
                    rel="nofollow"
                  >
                    <ListItemAvatar>
                      <Avatar src={n.imageUrl} />
                    </ListItemAvatar>
                    <ListItemText
                      sx={{
                        "& .MuiListItemText-secondary": {
                          color: "#004d40",
                        },
                      }}
                      primary={n.displayName}
                      secondary={`${fShortenNumber(
                        n.followers
                      )} followers, ${fShortenNumber(
                        n.engagements
                      )} engagements`}
                    />
                  </ListItemButton>
                ))}
              </List>
            </CardContent>
            <ViewMoreComponent
              label="Audience Lookalike"
              hideMore
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">User</TableCell>
                      <TableCell component="th">Engagements</TableCell>
                      <TableCell component="th">Followers</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.lookalike?.map((d) => (
                      <TableRow key={d.influencerId}>
                        <TableCell>
                          <Stack
                            direction="row"
                            alignItems="center"
                            spacing={2}
                          >
                            <Avatar alt={d.displayName} src={d.imageUrl} />
                            <Box
                              flexDirection="column"
                              sx={{ display: "flex" }}
                            >
                              <Typography variant="h6" noWrap>
                                {d.displayName}
                              </Typography>
                              <Typography variant="subtitle2" noWrap>
                                {d.locationLabel}
                              </Typography>
                            </Box>
                          </Stack>
                        </TableCell>
                        <TableCell>{fShortenNumber(d.engagements)}</TableCell>
                        <TableCell>{fShortenNumber(d.followers)}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
            />
          </Card>
        </Grid>
      )}
      <Grid item xs={12} sm={12} md={6}>
        <Card
          sx={{
            height: "100%",
          }}
        >
          <CardContent>
            <List
              sx={{
                width: "100%",
              }}
            >
              {data?.notable?.slice(0, 3).map((n) => (
                <ListItemButton
                  component="a"
                  alignItems="flex-start"
                  key={n.influencerId}
                  href={n.url ?? "#"}
                  target="_blank"
                  rel="nofollow"
                >
                  <ListItemAvatar>
                    <Avatar src={n.imageUrl} />
                  </ListItemAvatar>
                  <ListItemText
                    sx={{
                      "& .MuiListItemText-secondary": {
                        color: "#004d40",
                      },
                    }}
                    primary={n.displayName}
                    secondary={`${fShortenNumber(
                      n.followers
                    )} followers, ${fShortenNumber(n.engagements)} engagements`}
                  />
                </ListItemButton>
              ))}
            </List>
          </CardContent>
          <ViewMoreComponent
            label="Notable Followers"
            hideMore
            renderTable={() => (
              <Table stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell component="th">User</TableCell>
                    <TableCell component="th">Engagements</TableCell>
                    <TableCell component="th">Followers</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {data?.notable?.map((d) => (
                    <TableRow key={d.influencerId}>
                      <TableCell>
                        <Stack direction="row" alignItems="center" spacing={2}>
                          <Avatar alt={d.displayName} src={d.imageUrl} />
                          <Box flexDirection="column" sx={{ display: "flex" }}>
                            <Typography variant="h6" noWrap>
                              {d.displayName}
                            </Typography>
                            <Typography variant="subtitle2" noWrap>
                              {d.locationLabel}
                            </Typography>
                          </Box>
                        </Stack>
                      </TableCell>
                      <TableCell>{fShortenNumber(d.engagements)}</TableCell>
                      <TableCell>{fShortenNumber(d.followers)}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            )}
          />
        </Card>
      </Grid>

      <Grid item xs={12} />
      <Grid item xs={12} sm={12} md={6}>
        <Box
          sx={{
            pt: 3,
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
            Audience Brand Affinity
          </Typography>
        </Box>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <Box
          sx={{
            pt: 3,
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
            Audience Interests
          </Typography>
        </Box>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <Card
          sx={{
            height: "100%",
          }}
        >
          <CardContent>
            <Grid container>
              {(data?.brands.slice(0, 9) ?? []).map((item) => (
                <Grid item xs={4} key={item.label}>
                  {/* <img src={item.iconUrl} alt={item.label} loading="lazy" /> */}
                  <Box sx={{ px: 2.5, py: 1.5 }}>{item.label}</Box>
                </Grid>
              ))}
            </Grid>
          </CardContent>
          <ViewMoreComponent
            moreColor="primary"
            hideMore
            label="Audience Brand Affinity"
            renderTable={() => (
              <Table stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell component="th">Brand</TableCell>
                    <TableCell component="th">
                      {audienceType === AudienceType.FOLLOWERS && "Followers"}
                      {audienceType === AudienceType.LIKES && "Likes"}
                    </TableCell>
                    <TableCell component="th">Affinity</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {data?.brands?.map((item) => (
                    <TableRow key={item.label}>
                      <TableCell>{item.label}</TableCell>
                      <TableCell>
                        {fNumber(
                          parseInt(
                            `${(totalCount[audienceType] * item.weight) / 100}`,
                            10
                          )
                        )}
                        /{fPercent(item.weight)}
                      </TableCell>
                      <TableCell>{item.affinity?.toFixed(2)}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            )}
          />
        </Card>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <Card
          sx={{
            height: "100%",
          }}
        >
          <CardContent>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                flexWrap: "wrap",
                listStyle: "none",
                p: 0.5,
                pl: 0,
                m: 0,
              }}
              component="ul"
            >
              {data?.interests?.slice(0, 8)?.map((i) => (
                <ChipListItem key={i.label}>
                  <Chip variant="outlined" label={i.label} />{" "}
                </ChipListItem>
              ))}
            </Box>
          </CardContent>
          <ViewMoreComponent
            moreColor="primary"
            hideMore
            label="Audience Interests"
            renderTable={() => (
              <Table stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell component="th">Interests</TableCell>
                    <TableCell component="th">
                      {audienceType === AudienceType.FOLLOWERS && "Followers"}
                      {audienceType === AudienceType.LIKES && "Likes"}
                    </TableCell>
                    <TableCell component="th">Affinity</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {data?.interests?.map((item) => (
                    <TableRow key={item.label}>
                      <TableCell>{item.label}</TableCell>
                      <TableCell>
                        {fNumber(
                          parseInt(
                            `${(totalCount[audienceType] * item.weight) / 100}`,
                            10
                          )
                        )}
                        /{fPercent(item.weight)}
                      </TableCell>
                      <TableCell>{item.affinity?.toFixed(2)}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            )}
          />
        </Card>
      </Grid>
    </Grid>
  );
};
