import {
  Box,
  Card,
  CardContent,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import { chunk } from "lodash";
import { useAppSelector } from "../../../../app/hooks";
import { selectInfluencerOverview } from "../../selector";

export const InfluencerBrandAffinityPage: React.FC = () => {
  const data = useAppSelector(selectInfluencerOverview);

  const brandData = chunk(data?.affinity ?? [], 22);
  const interestData = chunk(data?.interests ?? [], 22);

  const noOfPages = new Array(
    Math.max(brandData.length, interestData.length)
  ).fill(null);

  return (
    <Grid container sx={{ pt: 2 }} spacing={2}>
      {noOfPages.map((o, i, a) => (
        <>
          <Grid item xs={12}>
            {i < brandData.length ? (
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Typography
                  sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }}
                  variant="h5"
                >
                  Influencer Brand Affinity
                </Typography>
              </Box>
            ) : null}
          </Grid>
          <Grid item xs={6}>
            {i < brandData.length ? (
              <Card>
                <CardContent>
                  <Table size="small">
                    <TableHead>
                      <TableRow>
                        <TableCell component="th">Brand</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {brandData[i]?.map((item) => (
                        <TableRow key={item.label}>
                          <TableCell>{item.label}</TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </CardContent>
              </Card>
            ) : null}
          </Grid>

          <Grid item xs={6}>
            {i < interestData.length ? (
              <Card>
                <CardContent>
                  <Table size="small">
                    <TableHead>
                      <TableRow>
                        <TableCell component="th">Interest</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {interestData[i]?.map((item) => (
                        <TableRow key={item}>
                          <TableCell>{item}</TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </CardContent>
              </Card>
            ) : null}
          </Grid>
          {a.length - 1 === i ? null : (
            <Grid
              item
              xs={12}
              sx={{ p: "0 !important", m: "0 !important" }}
              className="breakAfterMe"
            />
          )}
        </>
      ))}
    </Grid>
  );
};
