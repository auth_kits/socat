import {
  Avatar,
  Box,
  Button,
  ButtonGroup,
  Card,
  CardContent,
  CardHeader,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  List,
  ListItemAvatar,
  ListItemButton,
  ListItemText,
  Stack,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tabs,
  Typography,
} from "@material-ui/core";
import { styled } from "@material-ui/core/styles";
import { ApexOptions } from "apexcharts";
import { sentenceCase } from "change-case";
import { isEmpty, merge } from "lodash";
import { useState } from "react";
import ReactApexChart from "react-apexcharts";
import { useAppDispatch, useAppSelector } from "../../../../app/hooks";
import { BaseOptionChart } from "../../../../components/charts";
import {
  fNumber,
  fPercent,
  fShortenNumber,
} from "../../../../utils/formatNumber";
import { AudienceType, ELoadingStatus, setAudienceType } from "../../action";
import {
  selectAudienceData,
  selectAudienceExtraData,
  selectAudienceTotalCount,
  selectStatus,
} from "../../selector";
import { NumberTile } from "../NumberTile";
import { ViewMoreComponent } from "../ViewMoreComponent";

const ChipListItem = styled("li")(({ theme }) => ({
  margin: theme.spacing(0.5),
}));

const CHART_HEIGHT = 264;
const LEGEND_HEIGHT = 72;

const ChartWrapperStyle = styled(Box)(({ theme }) => ({
  // height: CHART_HEIGHT,
  marginBottom: theme.spacing(5),
  paddingBottom: theme.spacing(5),
  // "& .apexcharts-canvas svg": { height: CHART_HEIGHT - 40 },
  // "& .apexcharts-canvas svg,.apexcharts-canvas foreignObject": {
  //   overflow: "visible",
  // },
  "& .apexcharts-legend": {
    // height: LEGEND_HEIGHT,
    // alignContent: "center",
    // position: "relative !important",
    top: `calc(${CHART_HEIGHT - LEGEND_HEIGHT}px) !important`,
    bottom: 0,
  },
}));

export const NotableFollowersPage: React.FC = () => {
  const [interestModal, setInterestModal] = useState(false);
  const [notableModal, setNotableModal] = useState(false);
  const [lookalikeModal, setLookalikeModal] = useState(false);
  const [brandModal, setBrandModal] = useState(false);
  const dispatch = useAppDispatch();
  const data = useAppSelector(selectAudienceData);
  const status = useAppSelector(selectStatus);
  const extraData = useAppSelector(selectAudienceExtraData);

  const totalCount = useAppSelector(selectAudienceTotalCount);

  const isLoading = [ELoadingStatus.INITIAL, ELoadingStatus.LOADING].includes(
    status
  );

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Box
          sx={{
            pt: 4,
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }} variant="h5">
            Notable Followers
          </Typography>
        </Box>
      </Grid>

      <Grid item xs={6}>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              <TableCell component="th">User</TableCell>
              <TableCell component="th">Engagements</TableCell>
              <TableCell component="th">Followers</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data?.notable?.map((d) => (
              <TableRow key={d.influencerId}>
                <TableCell>
                  <Stack direction="row" alignItems="center" spacing={2}>
                    <Avatar alt={d.displayName} src={d.imageUrl} />
                    <Box flexDirection="column" sx={{ display: "flex" }}>
                      <Typography variant="h6" noWrap>
                        {d.displayName}
                      </Typography>
                      <Typography variant="subtitle2" noWrap>
                        {d.locationLabel}
                      </Typography>
                    </Box>
                  </Stack>
                </TableCell>
                <TableCell>{fShortenNumber(d.engagements)}</TableCell>
                <TableCell>{fShortenNumber(d.followers)}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Grid>

      {isEmpty(data?.lookalike) ? null : (
        <Grid item xs={12} sm={12} md={6}>
          <Dialog
            open={lookalikeModal}
            onClose={() => setLookalikeModal(false)}
            aria-labelledby="notable-modal-title"
            aria-describedby="notable-modal-description"
          >
            <DialogTitle id="notable-modal-title">
              Audience Lookalike
            </DialogTitle>
            <DialogContent>
              <List
                sx={{
                  width: "100%",
                }}
              >
                {data?.lookalike?.map((n) => (
                  <ListItemButton
                    component="a"
                    alignItems="flex-start"
                    key={n.influencerId}
                    href={n.url ?? "#"}
                    target="_blank"
                    rel="nofollow"
                  >
                    <ListItemAvatar>
                      <Avatar src={n.imageUrl} />
                    </ListItemAvatar>
                    <ListItemText
                      primary={n.displayName}
                      secondary={`${fShortenNumber(
                        n.followers
                      )} followers, ${fShortenNumber(
                        n.engagements
                      )} engagements`}
                    />
                  </ListItemButton>
                ))}
              </List>
            </DialogContent>
            <DialogActions>
              <Button onClick={() => setLookalikeModal(false)} autoFocus>
                Close
              </Button>
            </DialogActions>
          </Dialog>
          <Card
            sx={{
              height: "100%",
            }}
          >
            <CardContent>
              <List
                sx={{
                  width: "100%",
                }}
              >
                {data?.lookalike?.slice(0, 3).map((n) => (
                  <ListItemButton
                    component="a"
                    alignItems="flex-start"
                    key={n.influencerId}
                    href={n.url ?? "#"}
                    target="_blank"
                    rel="nofollow"
                  >
                    <ListItemAvatar>
                      <Avatar src={n.imageUrl} />
                    </ListItemAvatar>
                    <ListItemText
                      sx={{
                        "& .MuiListItemText-secondary": {
                          color: "#004d40",
                        },
                      }}
                      primary={n.displayName}
                      secondary={`${fShortenNumber(
                        n.followers
                      )} followers, ${fShortenNumber(
                        n.engagements
                      )} engagements`}
                    />
                  </ListItemButton>
                ))}
              </List>
            </CardContent>
            <ViewMoreComponent
              label="Audience Lookalike"
              hideMore
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">User</TableCell>
                      <TableCell component="th">Engagements</TableCell>
                      <TableCell component="th">Followers</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.lookalike?.map((d) => (
                      <TableRow key={d.influencerId}>
                        <TableCell>
                          <Stack
                            direction="row"
                            alignItems="center"
                            spacing={2}
                          >
                            <Avatar alt={d.displayName} src={d.imageUrl} />
                            <Box
                              flexDirection="column"
                              sx={{ display: "flex" }}
                            >
                              <Typography variant="h6" noWrap>
                                {d.displayName}
                              </Typography>
                              <Typography variant="subtitle2" noWrap>
                                {d.locationLabel}
                              </Typography>
                            </Box>
                          </Stack>
                        </TableCell>
                        <TableCell>{fShortenNumber(d.engagements)}</TableCell>
                        <TableCell>{fShortenNumber(d.followers)}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
            />
          </Card>
        </Grid>
      )}

      <Grid item xs={12} />
    </Grid>
  );
};
