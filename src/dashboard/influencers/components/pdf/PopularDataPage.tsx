import {
  Box,
  Card,
  CardContent,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import { chunk } from "lodash";
import { useAppSelector } from "../../../../app/hooks";
import { fPercent } from "../../../../utils/formatNumber";
import { selectInfluencerEngagement, selectStatus } from "../../selector";

export const PopularDataPage: React.FC = () => {
  const data = useAppSelector(selectInfluencerEngagement);
  const status = useAppSelector(selectStatus);
  const hashTagData = chunk(data?.hashtags ?? [], 22);
  const mentionData = chunk(data?.mentions ?? [], 22);

  const noOfPages = new Array(
    Math.max(hashTagData.length, mentionData.length)
  ).fill(null);

  return (
    <Grid container sx={{ pt: 2 }} spacing={2}>
      {noOfPages.map((o, i) => (
        <>
          <Grid item xs={6}>
            {i < hashTagData.length ? (
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Typography
                  sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }}
                  variant="h5"
                >
                  Popular # Hashtags
                </Typography>
              </Box>
            ) : null}
          </Grid>
          <Grid item xs={6}>
            {i < mentionData.length ? (
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Typography
                  sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }}
                  variant="h5"
                >
                  Popular @ Mentions
                </Typography>
              </Box>
            ) : null}
          </Grid>
          <Grid item xs={12} sx={{ p: "0 !important", m: "0 !important" }} />
          <Grid item xs={6}>
            {i < hashTagData.length ? (
              <Card>
                <CardContent>
                  <Table size="small">
                    <TableHead>
                      <TableRow>
                        <TableCell component="th">Hashtag</TableCell>
                        <TableCell component="th">Percent</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {hashTagData[i]?.map((ht) => (
                        <TableRow key={ht.tag}>
                          <TableCell>#{ht.tag}</TableCell>
                          <TableCell>{fPercent(ht.weight)}</TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </CardContent>
              </Card>
            ) : null}
          </Grid>

          <Grid item xs={6}>
            {i < mentionData.length ? (
              <Card>
                <CardContent>
                  <Table size="small">
                    <TableHead>
                      <TableRow>
                        <TableCell component="th">Mention</TableCell>
                        <TableCell component="th">Percent</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {mentionData[i]?.map((ht) => (
                        <TableRow key={ht.tag}>
                          <TableCell>@{ht.tag}</TableCell>
                          <TableCell>{fPercent(ht.weight)}</TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </CardContent>
              </Card>
            ) : null}
          </Grid>
          <Grid
            item
            xs={12}
            sx={{ p: "0 !important", m: "0 !important" }}
            className="breakAfterMe"
          />
        </>
      ))}
    </Grid>
  );
};
