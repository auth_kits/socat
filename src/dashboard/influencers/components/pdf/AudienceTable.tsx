import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Link,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import { fShortenNumber } from "../../../../utils/formatNumber";
import { AudienceProfile } from "../../action";

export const AudienceTable: React.FC<{ data: AudienceProfile[] }> = ({
  data = [],
}) => {
  const midWay = (data.length / 2) | 0;
  const data1 = data.slice(0, midWay);
  const data2 = data.slice(midWay);

  return (
    <>
      <Grid item xs={6}>
        <Card>
          <CardContent>
            <Table size="small">
              <TableHead>
                <TableRow>
                  <TableCell component="th">Influencer</TableCell>
                  <TableCell component="th">Likes</TableCell>
                  <TableCell component="th">Followers</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data1?.map((d) => (
                  <TableRow key={d.influencerId}>
                    <TableCell>
                      <Link
                        underline="none"
                        href={d.url ?? "#"}
                        target="_blank"
                        rel="nofollow"
                      >
                        <Stack direction="row" alignItems="center" spacing={2}>
                          <Avatar alt={d.displayName} src={d.imageUrl} />
                          <Box
                            flexDirection="column"
                            sx={{
                              display: "flex",
                              paddingBottom: "15px !important",
                            }}
                          >
                            <Typography variant="h6" sx={{ m: 0, p: 0 }} noWrap>
                              {d.displayName}
                            </Typography>
                            <Typography variant="subtitle2" noWrap>
                              {d.locationLabel}
                            </Typography>
                          </Box>
                        </Stack>
                      </Link>
                    </TableCell>
                    <TableCell>{fShortenNumber(d.engagements)}</TableCell>
                    <TableCell>{fShortenNumber(d.followers)}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={6}>
        <Card>
          <CardContent>
            <Table size="small">
              <TableHead>
                <TableRow>
                  <TableCell component="th">Influencer</TableCell>
                  <TableCell component="th">Likes</TableCell>
                  <TableCell component="th">Followers</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data2?.map((d) => (
                  <TableRow key={d.influencerId}>
                    <TableCell>
                      <Link
                        underline="none"
                        href={d.url ?? "#"}
                        target="_blank"
                        rel="nofollow"
                      >
                        <Stack direction="row" alignItems="center" spacing={2}>
                          <Avatar alt={d.displayName} src={d.imageUrl} />
                          <Box
                            flexDirection="column"
                            sx={{
                              display: "flex",
                              paddingBottom: "15px !important",
                            }}
                          >
                            <Typography variant="h6" sx={{ m: 0, p: 0 }} noWrap>
                              {d.displayName}
                            </Typography>
                            <Typography variant="subtitle2" noWrap>
                              {d.locationLabel}
                            </Typography>
                          </Box>
                        </Stack>
                      </Link>
                    </TableCell>
                    <TableCell>{fShortenNumber(d.engagements)}</TableCell>
                    <TableCell>{fShortenNumber(d.followers)}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      </Grid>
    </>
  );
};
