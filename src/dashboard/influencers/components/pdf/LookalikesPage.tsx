import { Box, Grid, Typography } from "@material-ui/core";
import { chunk } from "lodash";
import { useAppSelector } from "../../../../app/hooks";
import {
  selectInfluencerAudience,
  selectInfluencerOverview,
  selectStatus,
} from "../../selector";
import { AudienceTable } from "./AudienceTable";

export const LookalikeDataPage: React.FC = () => {
  const influencerData = useAppSelector(selectInfluencerAudience);
  const overviewData = useAppSelector(selectInfluencerOverview);

  const status = useAppSelector(selectStatus);

  return (
    <Grid container sx={{ pt: 2 }} spacing={2}>
      {chunk(
        overviewData?.lookalikes?.map((d) => ({
          imageUrl: d.imageUrl,
          influencerId: d.username,
          displayName: d.fullname,
          url: d.url,
          followers: d.followers,
          engagements: d.engagements,
          locationLabel: d.locationLabel,
        })) ?? [],
        22
      ).map((data) => (
        <>
          <Grid item xs={12}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography
                sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }}
                variant="h5"
              >
                Lookalikes (Similar Topics)
              </Typography>
            </Box>
          </Grid>
          <AudienceTable data={data} />
          <Grid
            item
            xs={12}
            sx={{ p: "0 !important", m: "0 !important" }}
            className="breakAfterMe"
          />
        </>
      ))}

      {chunk(influencerData?.followers?.lookalike ?? [], 22).map(
        (data, i, a) => (
          <>
            <Grid item xs={12}>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Typography
                  sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }}
                  variant="h5"
                >
                  Audience Lookalikes
                </Typography>
              </Box>
            </Grid>
            <AudienceTable data={data} />
            {a.length - 1 === i ? null : (
              <Grid
                item
                xs={12}
                sx={{ p: "0 !important", m: "0 !important" }}
                className="breakAfterMe"
              />
            )}
          </>
        )
      )}
    </Grid>
  );
};
