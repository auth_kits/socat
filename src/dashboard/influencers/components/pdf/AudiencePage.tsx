import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Grid,
  Typography,
} from "@material-ui/core";
import { styled } from "@material-ui/core/styles";
import { ApexOptions } from "apexcharts";
import { sentenceCase } from "change-case";
import { merge } from "lodash";
import { useState } from "react";
import ReactApexChart from "react-apexcharts";
import { useAppDispatch, useAppSelector } from "../../../../app/hooks";
import { BaseOptionChart } from "../../../../components/charts";
import { fPercent } from "../../../../utils/formatNumber";
import { AudienceType, ELoadingStatus } from "../../action";
import {
  selectAudienceData,
  selectAudienceExtraData,
  selectAudienceTotalCount,
  selectStatus,
} from "../../selector";
import { NumberTile } from "../NumberTile";

const ChipListItem = styled("li")(({ theme }) => ({
  margin: theme.spacing(0.5),
}));

const CHART_HEIGHT = 264;
const LEGEND_HEIGHT = 72;

const ChartWrapperStyle = styled(Box)(({ theme }) => ({
  // height: CHART_HEIGHT,
  marginBottom: theme.spacing(5),
  paddingBottom: theme.spacing(5),
  // "& .apexcharts-canvas svg": { height: CHART_HEIGHT - 40 },
  // "& .apexcharts-canvas svg,.apexcharts-canvas foreignObject": {
  //   overflow: "visible",
  // },
  "& .apexcharts-legend": {
    // height: LEGEND_HEIGHT,
    // alignContent: "center",
    // position: "relative !important",
    top: `calc(${CHART_HEIGHT - LEGEND_HEIGHT}px) !important`,
    bottom: 0,
  },
}));

export const AudiencePage: React.FC<{
  audienceType: AudienceType;
  pdf?: boolean;
}> = ({ audienceType }) => {
  const [interestModal, setInterestModal] = useState(false);
  const [notableModal, setNotableModal] = useState(false);
  const [lookalikeModal, setLookalikeModal] = useState(false);
  const [brandModal, setBrandModal] = useState(false);
  const dispatch = useAppDispatch();
  const data = useAppSelector(selectAudienceData);
  const status = useAppSelector(selectStatus);
  const extraData = useAppSelector(selectAudienceExtraData);

  const totalCount = useAppSelector(selectAudienceTotalCount);
  const locationOptions: ApexOptions = merge(BaseOptionChart(), {
    stroke: {
      width: 5,
    },
    colors: ["#26c6da", "#ffca28"],
    dataLabels: {
      enabled: true,
      textAnchor: "start",
      offsetY: -12,
      formatter(value) {
        return fPercent(value);
      },
      style: {
        colors: ["#f05d56"],
      },
    },
    plotOptions: {
      bar: {
        horizontal: true,
        barHeight: "15%",
        borderRadius: 2,
        padding: 10,

        dataLabels: {
          enabled: true,
          position: "right",
        },
      },
    },
    yaxis: {
      type: "category",
    },
    tooltip: {
      enabled: false,
      marker: { show: false },
      //   x: { show: true },
      //   y: {
      //     title: {
      //       formatter: (ser, opts) => "",
      //     },
      //     formatter: (ser) => fPercent(ser),
      //   },
    },
    xaxis: {
      type: "numeric",
      labels: {
        format: "asd",
      },
      title: {
        text: "Percent",
      },
    },
  } as Partial<ApexOptions>);

  const ageBarOptions = merge(BaseOptionChart(), {
    stroke: {
      width: 5,
    },
    yaxis: {
      type: "numeric",
      labels: {
        formatter: (val) => fPercent(val),
      },
    },
    plotOptions: {
      bar: {
        barHeight: "42%",
        horizontal: false,
        columnWidth: "42%",
        borderRadius: 4,
      },
    },
    xaxis: {
      type: "category",
    },
  } as Partial<ApexOptions>);

  const pieChartOptions = merge(BaseOptionChart(), {
    labels: data?.gender.map((v) => sentenceCase(v.code)),
    stroke: { colors: ["#fff"] },
    legend: {
      floating: true,
      horizontalAlign: "right",
      position: "bottom",
      labels: {
        colors: ["#3f51b5"],
        useSeriesColors: false,
      },
      offsetY: 20,
    },
    dataLabels: { enabled: true, dropShadow: { enabled: true } },
    tooltip: {
      fillSeriesColor: false,
      y: {
        formatter: (seriesName) => fPercent(seriesName),
      },
    },
    plotOptions: {
      pie: { customScale: 0.8, donut: { labels: { show: false } } },
    },
  });

  const rechabilityChartOptions = merge(BaseOptionChart(), {
    colors: ["#afb42b"],
    dataLabels: {
      enabled: true,
      textAnchor: "middle",
      formatter(value) {
        return fPercent(value);
      },
      style: {
        colors: ["#f05d56"],
      },
      offsetY: -25,
    },
    yaxis: {
      type: "numeric",
      labels: {
        formatter: (val) => fPercent(val),
      },
    },
    tooltip: {
      enabled: false,
      marker: { show: false },
      y: {
        title: {
          formatter: (ser) => ``,
        },
        formatter: (ser) => fPercent(ser),
      },
    },
    plotOptions: {
      bar: {
        barHeight: "28%",
        horizontal: false,
        columnWidth: "28%",
        borderRadius: 4,
        dataLabels: {
          enabled: true,
          position: "top",
        },
      },
    },
    xaxis: {
      type: "category",
      title: {
        text: "Followings",
      },
    },
  } as Partial<ApexOptions>);

  const isLoading = [ELoadingStatus.INITIAL, ELoadingStatus.LOADING].includes(
    status
  );

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Box
          sx={{
            pt: 2,
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }} variant="h5">
            Audience Details By{" "}
            {audienceType === AudienceType.FOLLOWERS ? "Followers" : "Likes"}
          </Typography>
        </Box>
      </Grid>

      {audienceType === AudienceType.FOLLOWERS && (
        <Grid item xs={6}>
          <NumberTile
            num="four"
            text={
              <Box
                flexDirection="row"
                alignItems="center"
                sx={{ display: "flex" }}
                justifyContent="center"
              >
                <span>Follower&apos;s Credibility</span>
              </Box>
            }
            percentage
            count={extraData?.followersCreds}
          />
        </Grid>
      )}
      {audienceType === AudienceType.FOLLOWERS && (
        <Grid item xs={6}>
          <NumberTile
            num="five"
            text={
              <Box
                flexDirection="row"
                alignItems="center"
                sx={{ display: "flex" }}
                justifyContent="center"
              >
                <span>Notable Followers</span>
              </Box>
            }
            percentage
            count={extraData?.notableFollowers}
          />
        </Grid>
      )}
      {audienceType === AudienceType.LIKES && (
        <>
          <Grid item xs={4}>
            <NumberTile
              num="four"
              text={
                <Box
                  flexDirection="row"
                  alignItems="center"
                  sx={{ display: "flex" }}
                  justifyContent="center"
                >
                  <span>Liker Credibility</span>
                </Box>
              }
              percentage
              count={extraData?.likerCreds}
            />
          </Grid>
          <Grid item xs={4}>
            <NumberTile
              num="five"
              text={
                <Box
                  flexDirection="row"
                  alignItems="center"
                  sx={{ display: "flex" }}
                  justifyContent="center"
                >
                  <span>Notable Likes</span>
                </Box>
              }
              percentage
              count={extraData?.notableLikes}
            />
          </Grid>
          <Grid item xs={4}>
            <NumberTile
              num="six"
              text={
                <Box
                  flexDirection="row"
                  alignItems="center"
                  sx={{ display: "flex" }}
                  justifyContent="center"
                >
                  <span>Likes not from Followers</span>
                </Box>
              }
              percentage
              count={extraData?.nonFollowerLikes}
            />
          </Grid>
        </>
      )}

      <Grid item xs={4}>
        <Card>
          <CardHeader sx={{ color: "#26a69a" }} title="Audience Reachability" />
          <CardContent>
            <ReactApexChart
              type="bar"
              series={[
                {
                  data: data?.reachability,
                },
              ]}
              options={rechabilityChartOptions}
              height={240}
            />
          </CardContent>
        </Card>
      </Grid>

      {["country", "city"].map((locationType) => (
        <Grid key={locationType} item xs={4}>
          <Card>
            <CardHeader
              sx={{ color: "#26a69a" }}
              title={`Location By ${sentenceCase(locationType)}`}
            />
            <Box sx={{ p: 3, pt: 0, pb: 1 }} dir="ltr">
              <ReactApexChart
                type="bar"
                height={CHART_HEIGHT}
                series={[
                  {
                    data: (data && data[locationType].slice(0, 5)) ?? [],
                  },
                ]}
                options={locationOptions}
              />
            </Box>
          </Card>
        </Grid>
      ))}
      <Grid item xs={12} sx={{ p: 0, m: 0 }} />
      <Grid item xs={4}>
        <Card>
          <CardHeader
            sx={{ color: "#26a69a" }}
            title="Age &amp; Gender Split"
          />
          <CardContent>
            <Box sx={{ p: 3, pt: 0, pb: 1 }} dir="ltr">
              <ReactApexChart
                key="age-bar"
                type="bar"
                series={[
                  {
                    name: "Male",
                    data: data?.age?.male ?? [],
                  },
                  {
                    name: "Female",
                    data: data?.age?.female ?? [],
                  },
                ]}
                options={ageBarOptions}
                height={CHART_HEIGHT}
              />
            </Box>
          </CardContent>
        </Card>
      </Grid>
      {["ethnicity", "language"].map((basicOtherTabs) => (
        <Grid item xs={4} key={basicOtherTabs}>
          <Card>
            <CardHeader
              sx={{ color: "#26a69a" }}
              title={sentenceCase(basicOtherTabs)}
            />
            <Box sx={{ p: 3, pt: 0, pb: 1 }} dir="ltr">
              <ReactApexChart
                height={CHART_HEIGHT + 30}
                key="ethnic-bar"
                type="bar"
                series={[
                  {
                    name: sentenceCase(basicOtherTabs),
                    data: (data && data[basicOtherTabs])?.slice(0, 5),
                  },
                ]}
                options={locationOptions}
              />
            </Box>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};
