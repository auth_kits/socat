import {
  Box,
  Card,
  CardContent,
  Grid,
  List,
  ListItemAvatar,
  ListItemButton,
  ListItemText,
  Typography,
} from "@material-ui/core";
import { styled } from "@material-ui/core/styles";
import ConnectWithoutContactIcon from "@material-ui/icons/ConnectWithoutContact";
import ContactPhoneIcon from "@material-ui/icons/ContactPhone";
import EmailIcon from "@material-ui/icons/Email";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import YoutubeIcon from "@material-ui/icons/YouTube";
import { useAppSelector } from "../../../../app/hooks";
import { selectInfluencerDetail } from "../../selector";

const ChipListItem = styled("li")(({ theme }) => ({
  margin: theme.spacing(0.5),
}));

export const ContactsPage: React.FC = () => {
  const data = useAppSelector(selectInfluencerDetail);

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Box
          sx={{
            pt: 4,
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }} variant="h5">
            Contacts
          </Typography>
        </Box>
      </Grid>

      <Card>
        <CardContent>
          <List
            sx={{
              width: "100%",
            }}
          >
            {data?.contact?.map((c) => (
              <ListItemButton
                sx={{
                  px: {
                    md: 1,
                  },
                }}
                component="a"
                alignItems="flex-start"
                key={c.value}
                href={c.formatted ?? "#"}
                target="_blank"
                rel="nofollow"
              >
                <ListItemAvatar>
                  {c.type === "facebook" && <FacebookIcon />}
                  {c.type === "instagram" && <InstagramIcon />}
                  {c.type === "email" && <EmailIcon />}
                  {c.type === "youtube" && <YoutubeIcon />}
                  {c.type === "phone" && <ContactPhoneIcon />}
                  {![
                    "facebook",
                    "instagram",
                    "email",
                    "phone",
                    "youtube",
                  ].includes(c.type) && <ConnectWithoutContactIcon />}
                </ListItemAvatar>
                <ListItemText primary={c.value} secondary={c.formatted} />
              </ListItemButton>
            ))}
          </List>
        </CardContent>
      </Card>

      <Grid item xs={12} />
    </Grid>
  );
};
