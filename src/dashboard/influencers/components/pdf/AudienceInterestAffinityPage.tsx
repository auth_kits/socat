import {
  Box,
  Card,
  CardContent,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import { chunk } from "lodash";
import { useAppSelector } from "../../../../app/hooks";
import { fNumber, fPercent } from "../../../../utils/formatNumber";
import {
  selectAudienceBrandData,
  selectAudienceInterestData,
} from "../../selector";

export const AudienceInterestAffinityPage: React.FC = () => {
  const interestData = useAppSelector(selectAudienceInterestData);
  const brandData = useAppSelector(selectAudienceBrandData);

  // const hashTagData = chunk(data?.hashtags ?? [], 22);
  // const mentionData = chunk(data?.mentions ?? [], 22);

  // const noOfPages = new Array(
  //   Math.max(hashTagData.length, mentionData.length)
  // ).fill(null);

  return (
    <Grid container sx={{ pt: 2 }} spacing={2}>
      {chunk(interestData ?? [], 22).map((data) => (
        <>
          <Grid item xs={12}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography
                sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }}
                variant="h5"
              >
                Audience Interest Affinity
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Card>
              <CardContent>
                <Table size="small">
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Interests</TableCell>
                      <TableCell component="th">Followers</TableCell>
                      <TableCell component="th">
                        Follower&apos;s Affinity
                      </TableCell>
                      <TableCell component="th">Likes</TableCell>
                      <TableCell component="th">
                        Liker&apos;s Affinity
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.map((item) => (
                      <TableRow key={item.label}>
                        <TableCell>{item.label}</TableCell>
                        <TableCell>
                          {fNumber(item.followers)}/
                          {fPercent(item.followersWeight)}
                        </TableCell>
                        <TableCell>{item.followerAffinity}</TableCell>
                        <TableCell>
                          {fNumber(item.likers)}/{fPercent(item.likersWeight)}
                        </TableCell>
                        <TableCell>{item.likersAffinity}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </CardContent>
            </Card>
          </Grid>

          <Grid
            item
            xs={12}
            sx={{ p: "0 !important", m: "0 !important" }}
            className="breakAfterMe"
          />
        </>
      ))}

      {chunk(brandData ?? [], 22).map((data, i, a) => (
        <>
          <Grid item xs={12}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography
                sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }}
                variant="h5"
              >
                Audience Brand Affinity
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Card>
              <CardContent>
                <Table size="small">
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Brand</TableCell>
                      <TableCell component="th">Followers</TableCell>
                      <TableCell component="th">
                        Follower&apos;s Affinity
                      </TableCell>
                      <TableCell component="th">Likes</TableCell>
                      <TableCell component="th">
                        Liker&apos;s Affinity
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.map((item) => (
                      <TableRow key={item.label}>
                        <TableCell>{item.label}</TableCell>
                        <TableCell>
                          {fNumber(item.followers)}/
                          {fPercent(item.followersWeight)}
                        </TableCell>
                        <TableCell>{item.followerAffinity}</TableCell>
                        <TableCell>
                          {fNumber(item.likers)}/{fPercent(item.likersWeight)}
                        </TableCell>
                        <TableCell>{item.likersAffinity}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </CardContent>
            </Card>
          </Grid>

          {a.length - 1 === i ? null : (
            <Grid
              item
              xs={12}
              sx={{ p: "0 !important", m: "0 !important" }}
              className="breakAfterMe"
            />
          )}
        </>
      ))}
    </Grid>
  );
};
