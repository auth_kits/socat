import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import { useAppSelector } from "../../../../app/hooks";
import { fNumber, fPercent } from "../../../../utils/formatNumber";
import { AudienceType } from "../../action";
import {
  selectAudienceTotalCount,
  selectInfluencerAudience,
} from "../../selector";

export const AgeSplitPage: React.FC = () => {
  const data = useAppSelector(selectInfluencerAudience);
  const totalCount = useAppSelector(selectAudienceTotalCount);
  // const hashTagData = chunk(data?.hashtags ?? [], 22);
  // const mentionData = chunk(data?.mentions ?? [], 22);

  // const noOfPages = new Array(
  //   Math.max(hashTagData.length, mentionData.length)
  // ).fill(null);

  return (
    <Grid container sx={{ pt: 2 }} spacing={2}>
      <Grid item xs={12}>
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography sx={{ whiteSpace: "nowrap", p: 0, mb: 0 }} variant="h5">
            Age Split
          </Typography>
        </Box>
      </Grid>

      <Grid item xs={12}>
        <Card>
          <CardHeader title="Followers" sx={{ color: "#26a69a" }} />
          <CardContent>
            <Table size="small">
              <TableHead>
                <TableRow>
                  <TableCell component="th">Age</TableCell>
                  <TableCell component="th">Followers</TableCell>
                  <TableCell component="th">Male</TableCell>
                  <TableCell component="th">Female</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data?.followers?.commonAge?.map((d) => (
                  <TableRow key={d.x}>
                    <TableCell>{d.x}</TableCell>
                    <TableCell>
                      {fNumber(
                        parseInt(
                          `${(totalCount[AudienceType.FOLLOWERS] * d.y) / 100}`,
                          10
                        )
                      )}
                    </TableCell>
                    <TableCell>
                      {fNumber(
                        parseInt(
                          `${
                            (totalCount[AudienceType.FOLLOWERS] *
                              d?.count?.male) /
                            100
                          }`,
                          10
                        )
                      )}
                      /{fPercent(d?.count?.male)}
                    </TableCell>
                    <TableCell>
                      {fNumber(
                        parseInt(
                          `${
                            (totalCount[AudienceType.FOLLOWERS] *
                              d?.count?.female) /
                            100
                          }`,
                          10
                        )
                      )}
                      /{fPercent(d?.count?.female)}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      </Grid>

      <Grid item xs={12}>
        <Card>
          <CardHeader title="Likes" sx={{ color: "#26a69a" }} />
          <CardContent>
            <Table size="small">
              <TableHead>
                <TableRow>
                  <TableCell component="th">Age</TableCell>
                  <TableCell component="th">Likes</TableCell>
                  <TableCell component="th">Male</TableCell>
                  <TableCell component="th">Female</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data?.likes?.commonAge?.map((d) => (
                  <TableRow key={d.x}>
                    <TableCell>{d.x}</TableCell>
                    <TableCell>
                      {fNumber(
                        parseInt(
                          `${(totalCount[AudienceType.LIKES] * d.y) / 100}`,
                          10
                        )
                      )}
                    </TableCell>
                    <TableCell>
                      {fNumber(
                        parseInt(
                          `${
                            (totalCount[AudienceType.LIKES] * d?.count?.male) /
                            100
                          }`,
                          10
                        )
                      )}
                      /{fPercent(d?.count?.male)}
                    </TableCell>
                    <TableCell>
                      {fNumber(
                        parseInt(
                          `${
                            (totalCount[AudienceType.LIKES] *
                              d?.count?.female) /
                            100
                          }`,
                          10
                        )
                      )}
                      /{fPercent(d?.count?.female)}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};
