import {
  Autocomplete,
  Avatar,
  Box,
  Button,
  Grid,
  InputAdornment,
  ListItemAvatar,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Select,
  Stack,
} from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import InstagramIcon from "@material-ui/icons/Instagram";
import MusicNoteIcon from "@material-ui/icons/MusicNote";
import SubscriptionsIcon from "@material-ui/icons/Subscriptions";
import ForwardIcon from "@material-ui/icons/Forward";
import { debounce, isEmpty } from "lodash";
import { useMemo, useState } from "react";
import { Link as RouterLink, useHistory } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { MHidden } from "../../../components/MHidden";
import {
  ELoadingStatus,
  getSearchResults,
  InfluencerReportData,
  ProfileType,
} from "../action";
import { selectReportSearchStatus, selectSearchResults } from "../selector";
import { ProfilePage } from "../ProfilePage";
import { fShortenNumber } from "../../../utils/formatNumber";
// const StyledInput = styled(Input)(({ theme }) => {
//   const placeholder = {
//     color: theme.palette.mode === "dark" ? "white" : "black",
//   };
//   return {
//     color: "inherit",
//     "& input": {
//       padding: theme.spacing(0.5),
//       paddingLeft: theme.spacing(4),
//       transition: theme.transitions.create("width"),
//       width: 150,
//       "&:focus": {
//         width: 170,
//       },
//       "&::-webkit-input-placeholder": placeholder,
//       "&::-moz-placeholder": placeholder, // Firefox 19+
//       "&:-ms-input-placeholder": placeholder, // IE11
//       "&::-ms-input-placeholder": placeholder, // Edge
//     },
//   };
// });

export const AudienceSearchPage = () => {
  const dispatch = useAppDispatch();
  const history = useHistory();
  const searchStatus = useAppSelector(selectReportSearchStatus);
  const results = useAppSelector(selectSearchResults);
  const theme = useTheme();
  const [selectedValue, setSelectedValue] = useState<string>();
  const [showProfile, setShowProfile] = useState<string>();
  const searchQuery = useMemo(
    () =>
      debounce((query: string) => {
        dispatch(
          getSearchResults({
            query,
          })
        );
      }, 600),
    [dispatch]
  );
  return (
    <Grid container>
      <Grid
        item
        xs={12}
        sm={8}
        md={6}
        lg={4}
        sx={{
          m: "0 auto",
        }}
      >
        <Stack direction="row" spacing={1}>
          <Autocomplete
            filterOptions={(x) => x}
            options={results}
            fullWidth
            placeholder="Search…"
            aria-label="Search…"
            noOptionsText="No results found"
            onInputChange={(event, newValue) => {
              searchQuery(newValue);
            }}
            onChange={(e, v) => {
              setSelectedValue(v?.username);
            }}
            getOptionLabel={(x) => x.displayName}
            loading={searchStatus === ELoadingStatus.LOADING}
            sx={{ mr: 1, fontWeight: "fontWeightBold" }}
            renderOption={(props: any, option, s) => (
              <ListItemButton
                // component={RouterLink}
                {...props}
              >
                <ListItemAvatar>
                  <Avatar alt={option.displayName} src={option.picture} />
                </ListItemAvatar>
                <ListItemText
                  secondary={`${option.displayName} (${fShortenNumber(
                    option.followers
                  )})`}
                  primary={`@${option.username}`}
                />
              </ListItemButton>
            )}
            renderInput={({ InputLabelProps, InputProps, ...inpProps }) => (
              <OutlinedInput
                sx={{ backgroundColor: "#FFF" }}
                ref={InputProps.ref}
                endAdornment={InputProps.endAdornment}
                startAdornment={
                  <InputAdornment position="start">
                    <Select
                      autoWidth
                      disableUnderline
                      variant="standard"
                      defaultValue={ProfileType.INSTAGRAM}
                      fullWidth
                      sx={{
                        whiteSpace: "nowrap",
                      }}
                      renderValue={(option) => (
                        <Stack
                          spacing={1}
                          alignItems="center"
                          flexDirection="row"
                        >
                          {
                            {
                              [ProfileType.INSTAGRAM]: <InstagramIcon />,
                              [ProfileType.YOUTUBE]: <SubscriptionsIcon />,
                              [ProfileType.TIKTOK]: <MusicNoteIcon />,
                            }[option]
                          }

                          <Box
                            sx={{
                              fontSize: theme.typography.pxToRem(20),
                              pl: 1,
                              mt: "0 !important",
                            }}
                          >
                            {
                              {
                                [ProfileType.INSTAGRAM]: "Instagram",
                                [ProfileType.YOUTUBE]: "YouTube",
                                [ProfileType.TIKTOK]: "Tik Tok",
                              }[option]
                            }
                          </Box>
                        </Stack>
                      )}
                      // onChange={handleChange}
                    >
                      <MenuItem selected value={ProfileType.INSTAGRAM}>
                        <ListItemIcon
                          sx={{
                            mr: 0,
                          }}
                        >
                          <InstagramIcon />
                        </ListItemIcon>
                        <ListItemText>Instagram</ListItemText>
                      </MenuItem>
                      <MenuItem value={ProfileType.YOUTUBE} disabled>
                        <ListItemIcon
                          sx={{
                            mr: 0,
                          }}
                        >
                          <SubscriptionsIcon />
                        </ListItemIcon>
                        <ListItemText
                          sx={{
                            fontSize: theme.typography.pxToRem(20),
                          }}
                        >
                          YouTube
                        </ListItemText>
                      </MenuItem>
                      <MenuItem value={ProfileType.TIKTOK} disabled>
                        <ListItemIcon
                          sx={{
                            mr: 0,
                          }}
                        >
                          <MusicNoteIcon />
                        </ListItemIcon>
                        <ListItemText
                          sx={{
                            fontSize: theme.typography.pxToRem(20),
                          }}
                        >
                          Tik Tok
                        </ListItemText>
                      </MenuItem>
                    </Select>
                  </InputAdornment>
                }
                {...inpProps}
              />
            )}
          />
          <Button
            disabled={isEmpty(selectedValue)}
            variant="contained"
            onClick={() => setShowProfile(selectedValue)}
          >
            <MHidden width="mdDown">Analyze</MHidden>
            <MHidden width="mdUp">
              <ForwardIcon />
            </MHidden>
          </Button>
        </Stack>
      </Grid>
      <Grid item xs={12} />

      {showProfile && (
        <Grid item xs={12} sx={{ pt: 4 }}>
          <ProfilePage profileId={showProfile} />{" "}
        </Grid>
      )}
    </Grid>
  );
};
