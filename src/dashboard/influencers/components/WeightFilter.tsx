import { FormControl, FormLabel, Slider } from "@material-ui/core";
import { Box } from "@material-ui/system";

const SLIDER_MARKS = [
  {
    value: 0.01,
  },
  {
    value: 0.05,
  },
  {
    value: 0.1,
  },
  {
    value: 0.15,
  },
  {
    value: 0.2,
  },
  {
    value: 0.25,
  },
  {
    value: 0.3,
  },
  {
    value: 0.35,
  },
  {
    value: 0.4,
  },
  {
    value: 0.45,
  },
  {
    value: 0.5,
  },
  {
    value: 0.55,
  },
  {
    value: 0.6,
  },
  {
    value: 0.65,
  },
  {
    value: 0.7,
  },
  {
    value: 0.75,
  },
  {
    value: 0.8,
  },
  {
    value: 0.85,
  },
  {
    value: 0.9,
  },
  {
    value: 0.95,
  },
];

export interface WeightFilterProps {
  label?: any;
  value: string;
  id: string;
  field?: any;
  onChange: (a: any) => void;
}
export const WeightFilter: React.FC<WeightFilterProps> = ({
  id,
  label,
  value,
  field,
  onChange,
}) => (
  <FormControl fullWidth>
    {label ? (
      <FormLabel
        sx={{
          pb: 5,
        }}
        htmlFor={id}
      >
        {label}
      </FormLabel>
    ) : (
      <Box sx={{ p: 1 }} />
    )}

    <Box sx={{ px: 2 }}>
      <Slider
        id={id}
        step={null}
        marks={SLIDER_MARKS}
        min={0}
        max={0.95}
        defaultValue={parseFloat(value ?? "0.1")}
        name={field?.name}
        ref={field?.ref}
        track="inverted"
        onChange={(event, v) => {
          // const val: any = [];
          // let firstValue = `${v[0] ?? 13}`;
          // SLIDER_MARKS.forEach((m, index) => {
          //   if (m.value > v[0] && v[1] > m.value && index > 0) {
          //     val.push({
          //       code: `${firstValue}-${m.value - 1}`,
          //       weight: "0.1",
          //     });
          //     firstValue = `${m.value}`;
          //   }
          // });
          // val.push({
          //   code: `${firstValue}-${v[1] === 75 ? "" : v[1]}`,
          //   weight: "0.1",
          // });
          onChange(`${v}`);
        }}
        getAriaLabel={() => "Audience's Age"}
        valueLabelDisplay="on"
        valueLabelFormat={(x) => `>${(x * 100) | 0}%`}
        getAriaValueText={(v) => `${v}`}
      />
    </Box>
  </FormControl>
);
// };
