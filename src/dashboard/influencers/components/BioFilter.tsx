import { Stack, TextField } from "@material-ui/core";
import { isEmpty } from "lodash";
import { useEffect } from "react";
import { Controller } from "react-hook-form";
import { useAppSelector } from "../../../app/hooks";
import { selectBioFilter } from "../selector";
import { FilterComponentProps } from "./filterLabels";
import { FilterPoppper } from "./FilterPopper";

export const BioFilter: React.FC<FilterComponentProps> = ({
  control,
  setValue,
  formState: { dirtyFields },
}) => {
  const values = useAppSelector(selectBioFilter);

  useEffect(() => {
    setValue("filter.text", values, {
      shouldDirty: true,
    });
  }, [values, setValue]);

  return (
    <FilterPoppper label="Bio" isDirty={!isEmpty(dirtyFields.filter?.text)}>
      <Stack spacing={2}>
        <Controller
          name="filter.text"
          control={control}
          render={({ field }) => (
            <TextField
              size="small"
              variant="outlined"
              defaultValue={values}
              {...field}
            />
          )}
        />
      </Stack>
    </FilterPoppper>
  );
};
