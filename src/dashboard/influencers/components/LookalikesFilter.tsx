import {
  Autocomplete,
  // Button,
  debounce,
  // Divider,
  Grid,
  // Stack,
  TextField,
  Typography,
} from "@material-ui/core";
// import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
// import { Box } from "@material-ui/system";
// import { isEmpty } from "lodash";
import { useEffect, useMemo } from "react";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import {
  ELoadingStatus,
  getFilterLookalikes,
  INITIAL_FILTER_VALUE,
} from "../action";
import {
  selectFilterLookalikes,
  selectLookalikeFilter,
  selectRemoteFilterStatus,
} from "../selector";
import { FilterComponentProps } from "./filterLabels";
import { FilterPoppper } from "./FilterPopper";

export const LookalikeFilter: React.FC<FilterComponentProps> = ({
  control,
  setValue,
  onSubmit,
  formState: { dirtyFields },
}) => {
  const values = useAppSelector(selectLookalikeFilter);
  // console.log("selectLookalikeFilter", values);
  const lookalikes = useAppSelector(selectFilterLookalikes);
  const status = useAppSelector(selectRemoteFilterStatus);
  const dispatch = useAppDispatch();

  const searchLookalikes = useMemo(
    () =>
      debounce((query: string) => {
        dispatch(
          getFilterLookalikes({
            query,
          })
        );
      }, 600),
    [dispatch]
  );

  useEffect(() => {
    // setValue("filter.relevance", values.la, {
    //   shouldDirty: true,
    // });
    // setValue("filter.audience_relevance", values.audLa, {
    //   shouldDirty: true,
    // });
  }, [values, setValue]);

  return (
    <FilterPoppper
      label="Lookalikes"
      isDirty={
        !!(
          dirtyFields.filter?.audience_relevance ||
          (dirtyFields.filter?.relevance && values.la?.value.includes("@"))
        )
      }
    >
      <Grid
        container
        columnSpacing={2}
        sx={{
          width: 420,
        }}
      >
        <Grid item xs={6}>
          <Typography variant="caption">Audience</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography variant="caption">Influencer</Typography>
        </Grid>
        <Grid item xs={12} />
        <Grid item xs={6}>
          <Autocomplete
            clearOnBlur
            filterOptions={(x) => x}
            size="small"
            options={!lookalikes ? [] : lookalikes}
            autoHighlight
            aria-label="Audience lookalikes…"
            noOptionsText="No results found"
            loading={status === ELoadingStatus.LOADING}
            fullWidth
            onInputChange={(event, newValue, reason) => {
              searchLookalikes(newValue);
            }}
            onChange={(e, val) => {
              setValue(
                "filter.audience_relevance",
                {
                  value: val?.value ?? "",
                },
                {
                  shouldDirty: true,
                }
              );
              onSubmit(e);
              // e.currentTarget["value"] = "";
            }}
            getOptionLabel={(option) => option.name}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Lookalikes"
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password", // disable autocomplete and autofill
                }}
              />
            )}
          />
          {/* <Box>
            {values.audLa?.value && (
              <Stack spacing={2}>
                <Divider />
                <Box
                  sx={{
                    alignItems: "center",
                    flexDirection: "row",
                    display: "flex",
                  }}
                >
                  <span>{values.audLa?.value}</span>
                  <Button
                    variant="text"
                    size="small"
                    color="error"
                    sx={{
                      m: 0,
                    }}
                    onClick={(e) => {
                      setValue(
                        "filter.audience_relevance",
                        INITIAL_FILTER_VALUE.filter.audience_relevance,
                        {
                          shouldDirty: true,
                        }
                      );
                      // onSubmit(e);
                    }}
                  >
                    <DeleteForeverIcon />
                  </Button>
                </Box>
              </Stack>
            )}
          </Box> */}
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            filterOptions={(x) => x}
            size="small"
            options={!lookalikes ? [] : lookalikes}
            autoHighlight
            aria-label="Influencer lookalikes"
            noOptionsText="No results found"
            loading={status === ELoadingStatus.LOADING}
            fullWidth
            onInputChange={(event, newValue) => {
              // event.preventDefault();
              searchLookalikes(newValue);
            }}
            onChange={(e, val) => {
              if (val) {
                const newVal = [values.la?.value, `@${val.value}` ?? ""].join(
                  " "
                );

                setValue(
                  "filter.relevance",
                  {
                    value: newVal ?? "",
                    weight: values.la?.weight ?? "0.5",
                  },
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }
            }}
            getOptionLabel={(option) => option.name}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Lookalikes"
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password", // disable autocomplete and autofill
                }}
              />
            )}
          />
          {/* <Box>
            {values.la?.value
              ?.split(" ")
              .filter((a) => a.startsWith("@"))
              .map((f, idx) => (
                <Stack spacing={2} key={f}>
                  <Divider />
                  <Box
                    sx={{
                      alignItems: "center",
                      flexDirection: "row",
                      display: "flex",
                    }}
                  >
                    <span>{f}</span>
                    <Button
                      variant="text"
                      size="small"
                      color="error"
                      sx={{
                        m: 0,
                      }}
                      onClick={(e) => {
                        const val: string = (values.la?.value ?? "")
                          .split(" ")
                          .filter((v) => v !== f)
                          .join(" ")
                          .trim();
                        console.log(val, val.trim(), isEmpty(val));
                        if (isEmpty(val)) {
                          setValue("filter.relevance", undefined, {
                            shouldDirty: true,
                          });
                        } else {
                          setValue(
                            "filter.relevance",
                            {
                              value: val ?? "",
                              weight: values.la?.weight ?? "0.5",
                            },
                            {
                              shouldDirty: true,
                            }
                          );
                        }
                        onSubmit(e);
                      }}
                    >
                      <DeleteForeverIcon />
                    </Button>
                  </Box>
                </Stack>
              ))}
          </Box> */}
        </Grid>
      </Grid>
    </FilterPoppper>
  );
};
