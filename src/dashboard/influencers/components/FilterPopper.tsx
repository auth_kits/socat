import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  ClickAwayListener,
  Fade,
  Paper,
  Popper,
  Typography,
} from "@material-ui/core";
import { useState } from "react";
import DownArrow from "@material-ui/icons/KeyboardArrowDown";
import UpArrow from "@material-ui/icons/KeyboardArrowUp";

interface FilterPopper {
  label: string;
  isDirty?: boolean;
}
export const FilterPoppper: React.FC<FilterPopper> = ({
  label,
  isDirty,
  children,
}) => {
  const [openFilter, setOpenFilter] = useState(false);
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
    setOpenFilter((of) => !of);
  };
  return (
    <>
      <Button
        fullWidth
        color="secondary"
        // eslint-disable-next-line no-nested-ternary
        variant={isDirty ? "contained" : openFilter ? "outlined" : "text"}
        onClick={handleClick}
        sx={{ justifyContent: "flex-start" }}
      >
        {label}
        {openFilter ? <UpArrow /> : <DownArrow />}
      </Button>
      <Popper
        open={openFilter}
        anchorEl={anchorEl}
        placement="bottom"
        transition
      >
        {({ TransitionProps }) => (
          <ClickAwayListener onClickAway={() => setOpenFilter(false)}>
            <Fade {...TransitionProps} timeout={350}>
              <Paper
                elevation={3}
                sx={{
                  p: 1,
                  px: 2,
                }}
              >
                {/* <Typography
                  sx={{
                    fontWeight: "bold",
                  }}
                  variant="body2"
                >
                  {label}
                </Typography> */}
                {/* <Box> */}
                {children}
                {/* </Box> */}
              </Paper>
            </Fade>
          </ClickAwayListener>
        )}
      </Popper>
    </>
  );
};
