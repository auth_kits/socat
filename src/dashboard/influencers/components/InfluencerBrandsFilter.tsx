import {
  Autocomplete,
  InputAdornment,
  OutlinedInput,
  Stack,
  TextField,
} from "@material-ui/core";
import GroupsIcon from "@material-ui/icons/Groups";
import { Control } from "react-hook-form";
import { EAudienceSource, FilterData } from "../action";
import { FilterPoppper } from "./FilterPopper";

export const InfluencerBrandsFilter: React.FC<{
  control: Control<{
    filter: FilterData;
    /* eslint-disable camelcase */
    audience_source: EAudienceSource;
    /* eslint-enable camelcase */
  }>;
}> = () => (
  <FilterPoppper label="Brands">
    <Stack spacing={2}>
      <Autocomplete
        options={[]}
        autoHighlight
        noOptionsText="Brand not found"
        getOptionLabel={(option: any) => option.label}
        renderInput={({ InputLabelProps, InputProps, ...inpProps }) => (
          <OutlinedInput
            startAdornment={
              <InputAdornment position="start">
                <GroupsIcon />
              </InputAdornment>
            }
            {...InputProps}
            {...inpProps}
            label="Audience"
            inputProps={{
              ...inpProps.inputProps,
              autoComplete: "new-password", // disable autocomplete and autofill
            }}
          />
        )}
      />
      <Autocomplete
        options={[]}
        autoHighlight
        getOptionLabel={(option: any) => option.label}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Influencer"
            inputProps={{
              ...params.inputProps,
              autoComplete: "new-password", // disable autocomplete and autofill
            }}
          />
        )}
      />
    </Stack>
  </FilterPoppper>
);
