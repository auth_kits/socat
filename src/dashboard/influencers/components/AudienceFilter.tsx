import {
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
  Stack,
} from "@material-ui/core";
import { useEffect } from "react";
import { Controller } from "react-hook-form";
import { useAppSelector } from "../../../app/hooks";
import { EAudienceSource } from "../action";
import { selectAudienceSource } from "../selector";
import { FilterComponentProps, FILTER_LABELS } from "./filterLabels";
import { FilterPoppper } from "./FilterPopper";

export const AudienceFilter: React.FC<FilterComponentProps> = ({
  control,
  formState: { dirtyFields },
  setValue,
}) => {
  const values = useAppSelector(selectAudienceSource);
  useEffect(() => {
    setValue("audience_source", values ?? EAudienceSource.ANY, {
      shouldDirty: true,
    });
  }, [values, setValue]);
  return (
    <FilterPoppper isDirty={dirtyFields?.audience_source} label="Audience Type">
      <Stack spacing={2}>
        <Controller
          name="audience_source"
          control={control}
          rules={{
            required: true,
          }}
          render={({ field }) => (
            <FormControl>
              <RadioGroup aria-label="audienceSource" {...field}>
                <FormControlLabel
                  value={EAudienceSource.ANY}
                  control={<Radio />}
                  label={FILTER_LABELS.get(EAudienceSource.ANY)}
                />
                <FormControlLabel
                  value={EAudienceSource.FOLLOWES}
                  control={<Radio />}
                  label={FILTER_LABELS.get(EAudienceSource.FOLLOWES)}
                />
                <FormControlLabel
                  value={EAudienceSource.LIKES}
                  control={<Radio />}
                  label={FILTER_LABELS.get(EAudienceSource.LIKES)}
                />
              </RadioGroup>
            </FormControl>
          )}
        />
      </Stack>
    </FilterPoppper>
  );
};
