import { Control, FormState, UseFormSetValue } from "react-hook-form";
import {
  AccountTypeFilter,
  ContactTypeFilter,
  EAudienceSource,
  EGenderFilter,
  ERaceFilter,
  FilterData,
  IntervalFilter,
  LastPostedFilter,
} from "../action";

export interface FilterFormState {
  filter: FilterData;
  /* eslint-disable camelcase */
  audience_source: EAudienceSource;
  /* eslint-enable camelcase */
}

export interface FilterComponentProps {
  formState: FormState<FilterFormState>;
  control: Control<FilterFormState>;
  setValue: UseFormSetValue<FilterFormState>;
  onSubmit?: any;
}
export const FILTER_LABELS = new Map<any, string>();
FILTER_LABELS.set(EAudienceSource.ANY, "Any");
FILTER_LABELS.set(EAudienceSource.LIKES, "Likers");
FILTER_LABELS.set(EAudienceSource.FOLLOWES, "Followers");
FILTER_LABELS.set(EGenderFilter.ANY, "Any");
FILTER_LABELS.set(EGenderFilter.FEMALE, "Female");
FILTER_LABELS.set(EGenderFilter.MALE, "Male");
FILTER_LABELS.set(EGenderFilter.KNOWN, "Male or Female");
FILTER_LABELS.set(EGenderFilter.UNKNOWN, "Gender Neutral");
FILTER_LABELS.set(ERaceFilter.ANY, "Any");
FILTER_LABELS.set(ERaceFilter.BLACK, "African Descent");
FILTER_LABELS.set(ERaceFilter.ASIAN, "Asian");
FILTER_LABELS.set(ERaceFilter.WHITE, "White/Caucasian");
FILTER_LABELS.set(ERaceFilter.HISPANIC, "Hispanic American");
FILTER_LABELS.set(LastPostedFilter.ANY, "Any");
FILTER_LABELS.set(LastPostedFilter.ONE_MONTH, "1 Month");
FILTER_LABELS.set(LastPostedFilter.THREE_MONTH, "3 Months");
FILTER_LABELS.set(LastPostedFilter.SIX_MONTH, "6 Months");
FILTER_LABELS.set(AccountTypeFilter.REGULAR, "Regular");
FILTER_LABELS.set(AccountTypeFilter.CREATOR, "Business");
FILTER_LABELS.set(AccountTypeFilter.BUSINESS, "Creator");
FILTER_LABELS.set(IntervalFilter.ONE_MONTH, "1 Month");
FILTER_LABELS.set(IntervalFilter.TWO_MONTHS, "2 Months");
FILTER_LABELS.set(IntervalFilter.THREE_MONTHS, "3 Months");
FILTER_LABELS.set(IntervalFilter.FOUR_MONTHS, "4 Months");
FILTER_LABELS.set(IntervalFilter.FIVE_MONTHS, "5 Months");
FILTER_LABELS.set(IntervalFilter.SIX_MONTHS, "6 Months");
FILTER_LABELS.set(ContactTypeFilter.EMAIL, "Has Email");
FILTER_LABELS.set(ContactTypeFilter.PHONE, "Has Phone");
FILTER_LABELS.set(ContactTypeFilter.SNAPCHAT, "Has Snapchat");
FILTER_LABELS.set(ContactTypeFilter.FACEBOOK, "Has Facebook");
FILTER_LABELS.set(ContactTypeFilter.YOUTUBE, "Has Youtube");
FILTER_LABELS.set(ContactTypeFilter.TIKTOK, "Has Tiktok");
FILTER_LABELS.set(ContactTypeFilter.TWITTER, "Has Twitter");
FILTER_LABELS.set(ContactTypeFilter.TELEGRAM, "Has Telegram");
FILTER_LABELS.set(ContactTypeFilter.WHATSAPP, "Has Whatsapp");
FILTER_LABELS.set(ContactTypeFilter.LINKEDIN, "Has LinkedIn");
FILTER_LABELS.set(ContactTypeFilter.VK, "Has VK");
FILTER_LABELS.set(ContactTypeFilter.BBM, "Has BBM");
FILTER_LABELS.set(ContactTypeFilter.KIK, "Has Kik");
FILTER_LABELS.set(ContactTypeFilter.WECHAT, "Has WeChat");
FILTER_LABELS.set(ContactTypeFilter.VIBER, "Has Viber");
FILTER_LABELS.set(ContactTypeFilter.SKYPE, "Has Skype");
FILTER_LABELS.set(ContactTypeFilter.TUMBLR, "Has Tumblr");
FILTER_LABELS.set(ContactTypeFilter.TWITCHTV, "Has Twitch");
FILTER_LABELS.set(ContactTypeFilter.KAKAO, "Has Kakao");
FILTER_LABELS.set(ContactTypeFilter.PINTEREST, "Has Pinterest");
FILTER_LABELS.set(ContactTypeFilter.LINKTREE, "Has Linktree");
FILTER_LABELS.set(ContactTypeFilter.SARAHAH, "Has Sarahah");
FILTER_LABELS.set(ContactTypeFilter.SAYAT, "Has Sayat.me");
FILTER_LABELS.set(ContactTypeFilter.ITUNES, "Has Apple Music Account");
FILTER_LABELS.set(ContactTypeFilter.WEIBO, "Has Weibo");
// FILTER_LABELS.set(BrandCategoryFilter.TV_FILM, "Television & Film");
// FILTER_LABELS.set(BrandCategoryFilter.MUSIC, "Music");
// FILTER_LABELS.set(BrandCategoryFilter.SHOPPING, "Shopping & Retail");
// FILTER_LABELS.set(BrandCategoryFilter.BEVERAGES, "Coffee, Tea & Beverages");
// FILTER_LABELS.set(BrandCategoryFilter.PHOTOGRAPHY, "Camera & Photography");
// FILTER_LABELS.set(
//   BrandCategoryFilter.CLOTHES,
//   "Clothes, Shoes, Handbags & Accessories"
// );
// FILTER_LABELS.set(BrandCategoryFilter.SPIRITS, "Beer, Wine & Spirits");
// FILTER_LABELS.set(BrandCategoryFilter.SPORTS, "Sports");
// FILTER_LABELS.set(BrandCategoryFilter.COPUTERS, "Electronics & Computers");
// FILTER_LABELS.set(BrandCategoryFilter.GAMING, "Gaming");
// FILTER_LABELS.set(BrandCategoryFilter.ACTIVEWEAR, "Activewear");
// FILTER_LABELS.set(BrandCategoryFilter.ART, "Art & Design");
// FILTER_LABELS.set(BrandCategoryFilter.TRAVEL, "Travel, Tourism & Aviation");
// FILTER_LABELS.set(BrandCategoryFilter.BUSINESS, "Business & Careers");
// FILTER_LABELS.set(BrandCategoryFilter.BEAUTY, "Beauty & Costmetics");
// FILTER_LABELS.set(BrandCategoryFilter.HEALTHCARE, "Healthcare & Medicine");
// FILTER_LABELS.set(BrandCategoryFilter.JEWELLERY, "Jewellery & Watches");
// FILTER_LABELS.set(BrandCategoryFilter.FOOD, "Restaurants, Food & Grocery");
// FILTER_LABELS.set(BrandCategoryFilter.TOYS, "Toys, Children & Baby");
// FILTER_LABELS.set(BrandCategoryFilter.FITNESS, "Fitness & Yoga");
// FILTER_LABELS.set(BrandCategoryFilter.WEDDING, "Wedding");
// FILTER_LABELS.set(BrandCategoryFilter.SMOKE, "Tobacco & Smoking");
// FILTER_LABELS.set(BrandCategoryFilter.PET, "Pets");
// FILTER_LABELS.set(BrandCategoryFilter.LIFESTYLE, "Healthy Lifestyle");
// FILTER_LABELS.set(BrandCategoryFilter.LUXURY, "Luxury Goods");
// FILTER_LABELS.set(BrandCategoryFilter.DECOR, "Home Decor, Furniture & Garden");
// FILTER_LABELS.set(
//   BrandCategoryFilter.SOCIAL,
//   "Friends, Family & Relationships"
// );
// FILTER_LABELS.set(BrandCategoryFilter.AUTOMOBILE, "Cars & Motorbikes");
