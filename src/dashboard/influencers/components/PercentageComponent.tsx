import { Card, CardContent, CardHeader, Typography } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import LinearProgress, {
  linearProgressClasses,
} from "@material-ui/core/LinearProgress";
import { styled } from "@material-ui/core/styles";
import * as React from "react";
import { fPercent } from "../../../utils/formatNumber";

const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
  height: 15,
  borderRadius: 5,
  [`&.${linearProgressClasses.colorPrimary}`]: {
    backgroundColor:
      theme.palette.primary[theme.palette.mode === "light" ? 200 : 800],
  },
  [`& .${linearProgressClasses.bar}`]: {
    borderRadius: 5,
  },
}));

export interface PercentageComponentProps {
  percentage: number;
  label: string;
}

export const PercentageComponent: React.FC<PercentageComponentProps> = ({
  percentage,
  label,
}) => (
  <Card
    sx={{
      height: "100%",
    }}
  >
    <CardHeader title={label} color="#eee" />
    <CardContent>
      <Box
        sx={{ display: "flex", flexDirection: "column", alignItems: "right" }}
      >
        <Box
          sx={{
            py: 1,
          }}
        >
          <Typography variant="body1" color="text.secondary">{`${fPercent(
            percentage
          )}`}</Typography>
        </Box>
        <Box sx={{ width: "100%", mr: 1 }}>
          <BorderLinearProgress
            color="warning"
            variant="determinate"
            value={percentage}
          />
        </Box>
      </Box>
    </CardContent>
  </Card>
);
