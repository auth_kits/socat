import {
  Autocomplete,
  Box,
  Button,
  debounce,
  Divider,
  Grid,
  Stack,
  TextField,
  Typography,
} from "@material-ui/core";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import { isEmpty } from "lodash";
import { useEffect, useMemo } from "react";
import { Controller, useFieldArray } from "react-hook-form";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { ELoadingStatus, getFilterBrands } from "../action";
import {
  selectBrandsFilter,
  selectFilterBrands,
  selectRemoteFilterStatus,
} from "../selector";
import { FilterComponentProps } from "./filterLabels";
import { FilterPoppper } from "./FilterPopper";
import { WeightFilter } from "./WeightFilter";

export const BrandsFilter: React.FC<FilterComponentProps> = ({
  control,
  setValue,
  onSubmit,
  formState: { dirtyFields },
}) => {
  const values = useAppSelector(selectBrandsFilter);
  const brands = useAppSelector(selectFilterBrands);
  const status = useAppSelector(selectRemoteFilterStatus);
  const dispatch = useAppDispatch();
  const audArr = useFieldArray({
    control, // control props comes from useForm (optional: if you are using FormContext)
    name: "filter.audience_brand", // unique name for your Field Array
    keyName: "id", // default to "id", you can change the key name
  });

  const infArr = useFieldArray({
    control,
    name: "filter.brand",
  });

  const searchBrands = useMemo(
    () =>
      debounce((query: string) => {
        dispatch(
          getFilterBrands({
            query,
          })
        );
      }, 600),
    [dispatch]
  );

  useEffect(() => {
    const emptyQuery = "";
    searchBrands(emptyQuery);
  }, [searchBrands]);

  useEffect(() => {
    setValue("filter.audience_brand", values.audBrands, {
      shouldDirty: true,
    });
    setValue("filter.brand", values.brands, {
      shouldDirty: true,
    });
  }, [values, setValue]);

  return (
    <FilterPoppper
      label="Brand"
      isDirty={
        !isEmpty(dirtyFields.filter?.audience_brand) ||
        !isEmpty(dirtyFields.filter?.brand)
      }
    >
      <Grid
        container
        columnSpacing={2}
        sx={{
          width: 420,
        }}
      >
        <Grid item xs={6}>
          <Typography variant="caption">Audience</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography variant="caption">Influencer</Typography>
        </Grid>
        <Grid item xs={12} />
        <Grid item xs={6}>
          <Autocomplete
            clearOnBlur
            filterOptions={(x) => x}
            size="small"
            options={!brands ? [] : brands}
            autoHighlight
            aria-label="Brands…"
            noOptionsText="No results found"
            loading={status === ELoadingStatus.LOADING}
            fullWidth
            onInputChange={(event, newValue, reason) => {
              searchBrands(newValue);
            }}
            onChange={(e, val) => {
              if (val?.id) {
                audArr.append({
                  id: val.id,
                  weight: "0.05",
                  // label: val.name,
                });
              }
              onSubmit(e);
            }}
            getOptionLabel={(option) => option.name}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Brands"
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password", // disable autocomplete and autofill
                }}
              />
            )}
          />
          <Box>
            {audArr.fields?.map((f, idx) => {
              const filterName: any = `filter.audience_brand.${idx}.weight`;
              return (
                <Controller
                  key={f.id}
                  name={filterName}
                  control={control}
                  render={({ field }) => (
                    <Stack spacing={2}>
                      <Divider />
                      <WeightFilter
                        label={
                          <Box
                            sx={{
                              alignItems: "center",
                              flexDirection: "row",
                              display: "flex",
                            }}
                          >
                            <span>{f?.label}</span>
                            <Button
                              variant="text"
                              size="small"
                              color="error"
                              sx={{
                                m: 0,
                              }}
                              onClick={() => audArr.remove(idx)}
                            >
                              <DeleteForeverIcon />
                            </Button>
                          </Box>
                        }
                        id={filterName}
                        field={field}
                        value={field.value}
                        onChange={(v) => {
                          setValue(filterName, v, {
                            shouldDirty: true,
                          });

                          onSubmit();
                        }}
                      />
                    </Stack>
                  )}
                />
              );
            })}
          </Box>
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            filterOptions={(x) => x}
            size="small"
            options={!brands ? [] : brands}
            autoHighlight
            aria-label="Brand…"
            noOptionsText="No results found"
            loading={status === ELoadingStatus.LOADING}
            fullWidth
            onInputChange={(event, newValue) => {
              searchBrands(newValue);
            }}
            onChange={(e, val) => {
              if (!isEmpty(val)) {
                infArr.append({
                  id: val?.id,
                  label: val?.name,
                });
                onSubmit(e);
              }
            }}
            getOptionLabel={(option) => option?.name ?? ""}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Brands"
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password", // disable autocomplete and autofill
                }}
              />
            )}
          />
          <Box>
            {infArr.fields?.map((f, idx) => {
              const filterName: any = `filter.brand.${idx}.label`;
              return (
                <Stack spacing={2} key={`${f.id}-label`}>
                  <Divider />
                  <Box
                    sx={{
                      flexDirection: "row",
                      alignItems: "center",
                      display: "flex",
                    }}
                  >
                    <span>{f["label"]}</span>
                    <Button
                      variant="text"
                      size="small"
                      color="error"
                      sx={{
                        m: 0,
                      }}
                      onClick={() => infArr.remove(idx)}
                    >
                      <DeleteForeverIcon />
                    </Button>
                  </Box>
                </Stack>
              );
            })}
          </Box>
        </Grid>
      </Grid>
    </FilterPoppper>
  );
};
