import {
  Autocomplete,
  Box,
  Button,
  debounce,
  Divider,
  Grid,
  Stack,
  TextField,
  Typography,
} from "@material-ui/core";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import { isEmpty } from "lodash";
import { useEffect, useMemo } from "react";
import { Controller } from "react-hook-form";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import {
  ELoadingStatus,
  getFilterLangs,
  INITIAL_FILTER_VALUE,
} from "../action";
import {
  selectFilterLangs,
  selectLanguageFilter,
  selectRemoteFilterStatus,
} from "../selector";
import { FilterComponentProps } from "./filterLabels";
import { FilterPoppper } from "./FilterPopper";
import { WeightFilter } from "./WeightFilter";

export const LanguageFilter: React.FC<FilterComponentProps> = ({
  control,
  setValue,
  onSubmit,
  formState: { dirtyFields },
}) => {
  const values = useAppSelector(selectLanguageFilter);
  const langs = useAppSelector(selectFilterLangs);
  const status = useAppSelector(selectRemoteFilterStatus);
  const dispatch = useAppDispatch();

  const searchLangs = useMemo(
    () =>
      debounce((query: string) => {
        dispatch(
          getFilterLangs({
            query,
          })
        );
      }, 600),
    [dispatch]
  );

  useEffect(() => {
    setValue("filter.audience_lang", values.audLangs, {
      shouldDirty: true,
    });
    setValue("filter.lang", values.langs, {
      shouldDirty: true,
    });
  }, [values, setValue]);

  const isDirty = !!(
    dirtyFields.filter?.audience_lang || dirtyFields.filter?.lang
  );
  return (
    <FilterPoppper label="Language" isDirty={isDirty}>
      <Grid
        container
        columnSpacing={2}
        sx={{
          width: 420,
        }}
      >
        <Grid item xs={6}>
          <Typography variant="caption">Audience</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography variant="caption">Influencer</Typography>
        </Grid>
        <Grid item xs={12} />
        <Grid item xs={6}>
          <Autocomplete
            clearOnBlur
            filterSelectedOptions
            filterOptions={(x) => x}
            size="small"
            options={!langs ? [] : langs}
            autoHighlight
            aria-label="Languages…"
            noOptionsText="No results found"
            loading={status === ELoadingStatus.LOADING}
            fullWidth
            onInputChange={(event, newValue, reason) => {
              searchLangs(newValue);
            }}
            onChange={(e, val) => {
              if (val?.code) {
                setValue(
                  "filter.audience_lang",
                  {
                    code: val?.code,
                    weight: "0.05",
                    label: val.name,
                  } as never,
                  {
                    shouldDirty: true,
                    shouldValidate: true,
                  }
                );
                onSubmit(e);
              }
            }}
            getOptionLabel={(option) => option.name}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Languages"
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password", // disable autocomplete and autofill
                }}
              />
            )}
          />
          <Box>
            {dirtyFields.filter?.audience_lang && (
              <Controller
                name="filter.audience_lang"
                control={control}
                render={({ field }) => (
                  <Stack spacing={2}>
                    <Divider />
                    <WeightFilter
                      label={
                        <Box
                          sx={{
                            alignItems: "center",
                            flexDirection: "row",
                            display: "flex",
                          }}
                        >
                          <span>{values?.audLangs?.label}</span>
                          <Button
                            variant="text"
                            size="small"
                            color="error"
                            sx={{
                              m: 0,
                            }}
                            onClick={() => {
                              setValue(
                                "filter.audience_lang",
                                INITIAL_FILTER_VALUE.filter?.audience_lang,
                                {
                                  shouldDirty: true,
                                }
                              );
                            }}
                          >
                            <DeleteForeverIcon />
                          </Button>
                        </Box>
                      }
                      id="filter.audience_lang"
                      field={field}
                      value={field.value?.weight ?? "0.05"}
                      onChange={(v) => {
                        setValue(
                          "filter.audience_lang",
                          {
                            code: field.value?.code ?? "",
                            weight: v,
                            label: field.value?.label,
                          },
                          {
                            shouldDirty: true,
                          }
                        );
                        onSubmit();
                      }}
                    />
                  </Stack>
                )}
              />
            )}
          </Box>
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            filterOptions={(x) => x}
            filterSelectedOptions
            size="small"
            options={!langs ? [] : langs}
            autoHighlight
            aria-label="Languages…"
            noOptionsText="No results found"
            loading={status === ELoadingStatus.LOADING}
            fullWidth
            onInputChange={(event, newValue) => {
              searchLangs(newValue);
            }}
            onChange={(e, val) => {
              if (!isEmpty(val?.code)) {
                setValue(
                  "filter.lang",
                  {
                    code: val?.code ?? "",
                    label: val?.name,
                  },
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }
            }}
            getOptionLabel={(option) => option?.name ?? ""}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Languages"
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password", // disable autocomplete and autofill
                }}
              />
            )}
          />
          <Box>
            {dirtyFields.filter?.lang && (
              <Stack spacing={2}>
                <Divider />
                <Box
                  sx={{
                    flexDirection: "row",
                    alignItems: "center",
                    display: "flex",
                  }}
                >
                  <span>{values?.langs?.label}</span>
                  <Button
                    variant="text"
                    size="small"
                    color="error"
                    sx={{
                      m: 0,
                    }}
                    onClick={() =>
                      setValue(
                        "filter.lang",
                        INITIAL_FILTER_VALUE.filter?.lang,
                        {
                          shouldDirty: true,
                        }
                      )
                    }
                  >
                    <DeleteForeverIcon />
                  </Button>
                </Box>
              </Stack>
            )}
          </Box>
        </Grid>
      </Grid>
    </FilterPoppper>
  );
};
