// material
import { Box, Card, Stack, Typography } from "@material-ui/core";
import { alpha, styled } from "@material-ui/core/styles";
// utils
import { fPercent, fShortenNumber } from "../../../utils/formatNumber";

// ----------------------------------------------------------------------

const RootStyle = styled(Card)(({ theme }) => ({
  // boxShadow: "none",
  height: "100%",
  display: "flex",
  flexDirection: "column",
  boxShadow:
    "0 0 2px 0 rgb(239 228 228), 9px 1px 32px 0px rgb(145 158 171 / 20%)",
  textAlign: "center",
  padding: theme.spacing(3, 0),
  "&.one": {
    color: theme.palette.teal["main"],
    // background: theme.palette.gradients["teal"],
    // backgroundImage: "url(/graph_images/bgTeal1.jpg)",
  },
  "&.two": {
    color: theme.palette.yellow["darker"],
    // background: theme.palette.gradients["purple"],
    // backgroundImage: "url(/tile_images/tileRed.jpg)",
  },
  "&.three": {
    color: theme.palette.lightblue["dark"],
    // background: theme.palette.gradients["pink"],
    // backgroundImage: "url(/graph_images/bgGreen.jpg)",
  },
  "&.four": {
    color: theme.palette.green["dark"],
    // background: theme.palette.gradients["deeporange"],
    // backgroundImage: "url(/graph_images/bgBlue.jpg)",
  },
  "&.five": {
    color: theme.palette.primary["dark"],
    // color: theme.palette.white["main"],
    // background: theme.palette.gradients["orange"],
    // backgroundImage: "url(/graph_images/bgOrange.jpg)",
  },
  "&.six": {
    // color: theme.palette.green["darker"],
    color: theme.palette.lime["darker"],
    // background: theme.palette.gradients["lightlime"],
    // backgroundImage: "url(/graph_images/bgYellow.jpg)",
  },
  "&.eight": {
    // color: theme.palette.teal["darker"],
    color: theme.palette.black["main"],
    background: theme.palette.gradients["green"],
    // backgroundImage: "url(/tile_images/tileGreenLg.jpg)",
  },
  "&.seven": {
    // color: theme.palette.bluegrey["darker"],
    color: theme.palette.black["main"],
    // background: theme.palette.gradients["bluegrey"],
    backgroundImage: "url(/tile_images/tileBlue.jpg)",
  },
  "&.on": {
    color: theme.palette.white["main"],
    background: theme.palette.gradients["teal"],
  },
  "&.tw": {
    color: theme.palette.white["main"],
    background: theme.palette.gradients["deeporange"],
  },
  "&.notLiker": {
    color: theme.palette.white["main"],
    background: theme.palette.gradients["red"],
  },
  "&.fou": {
    color: theme.palette.white["main"],
    background: theme.palette.gradients["cyan"],
  },
}));

// ----------------------------------------------------------------------

interface NumberTileProps {
  count?: number;
  icon?: any;
  text: any;
  num: string;
  percentage?: boolean;
  customTop?: any;
  hideTop?: boolean;
}
export const NumberTile: React.FC<NumberTileProps> = ({
  count = 0,
  icon,
  text,
  num,
  percentage,
  customTop,
  hideTop = false,
}) => (
  <RootStyle className={num}>
    {customTop && (
      <Typography
        sx={{
          flexGrow: 1,
        }}
        variant="h5"
      >
        {customTop}
      </Typography>
    )}
    {!hideTop && (
      <Typography
        sx={{
          flexGrow: 1,
        }}
        variant="h3"
      >
        {percentage
          ? fPercent(count)
          : fShortenNumber(count).toLocaleUpperCase()}
      </Typography>
    )}
    <Typography variant="h6" sx={{ fontSize: { md: "0.9rem" }, opacity: 0.72 }}>
      <Stack
        flexDirection="row"
        alignItems="center"
        justifyContent="center"
        whiteSpace="nowrap"
      >
        {icon}
        <div>{text}</div>
      </Stack>
    </Typography>
  </RootStyle>
);
