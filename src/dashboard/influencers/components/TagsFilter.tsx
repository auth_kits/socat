import {
  Autocomplete,
  Box,
  Card,
  CardContent,
  debounce,
  // FormControl,
  FormLabel,
  Grid,
  InputAdornment,
  ListItemIcon,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Select,
  Slider,
  Stack,
} from "@material-ui/core";
import { styled, useTheme } from "@material-ui/core/styles";
import InstagramIcon from "@material-ui/icons/Instagram";
import MusicNoteIcon from "@material-ui/icons/MusicNote";
import SubscriptionsIcon from "@material-ui/icons/Subscriptions";
// import { id } from "date-fns/locale";
import isEmpty from "lodash/isEmpty";
import { useEffect, useMemo, useRef, useState } from "react";
import * as wc from "wordcloud";
// import { Controller } from "react-hook-form";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { SkeletonLoader } from "../../../components/SkeletonLoader";
import {
  ELoadingStatus,
  getInfluencerTagResults,
  ProfileType,
} from "../action";
import {
  selectInfluencerTagResults,
  selectInfluencerTagStatus,
  seletRelevanceFilter,
} from "../selector";
import { FilterComponentProps } from "./filterLabels";

const SearchContainer = styled(Box)(({ theme }) => ({
  // position: "fixed",
  // top: "50%",
  // left: "50%",
  // transform: "translate(-50%, -50%)",
  display: "flex",
  // transition: theme["transitions"].create("top"),
  "&.filters-open": {
    // top: "20%",
  },
  "& .Mui-Focussed .tag-end-adornment": {
    position: "relative",
  },
  // "& .Mui-Focussed .tag-end-adornment .MuiChip-root": {
  //   position: "absolute",
  //   padding: 2,
  // },
}));
const SLIDER_MARKS = [
  {
    value: 0.01,
  },
  {
    value: 0.05,
  },
  {
    value: 0.1,
  },
  {
    value: 0.15,
  },
  {
    value: 0.2,
  },
  {
    value: 0.25,
  },
  {
    value: 0.3,
  },
  {
    value: 0.35,
  },
  {
    value: 0.4,
  },
  {
    value: 0.45,
  },
  {
    value: 0.5,
  },
  {
    value: 0.55,
  },
  {
    value: 0.6,
  },
  {
    value: 0.65,
  },
  {
    value: 0.7,
  },
  {
    value: 0.75,
  },
  {
    value: 0.8,
  },
  {
    value: 0.85,
  },
  {
    value: 0.9,
  },
  {
    value: 0.95,
  },
];

export const TagFilter: React.FC<FilterComponentProps> = ({
  control,
  setValue,
  onSubmit,
  formState: { dirtyFields },
}) => {
  const tagSearchStatus = useAppSelector(selectInfluencerTagStatus);
  const tagSearchResults = useAppSelector(selectInfluencerTagResults);
  const values = useAppSelector(seletRelevanceFilter);

  const dispatch = useAppDispatch();
  const theme = useTheme();

  const tagRef = useRef(null);
  const [wordCloudLoading, setWordCloudLoading] = useState(false);
  const [shouldDisplayWordCloud, setShouldDisplayWordCloud] = useState(false);

  useEffect(() => {
    const tagList = values?.value?.split("#") || [];
    if (tagList.length) {
      setShouldDisplayWordCloud(true);
      setWordCloudLoading(true);
      const res = tagList.map((tag) =>
        fetch(
          `https://139-99-120-139.cprapid.com/~socatinfu/user/relevant_tags?q=${tag}`
        ).then((result) => result.json())
      );

      Promise.all(res)
        .then((results) => {
          const keywords = results
            .reduce(
              (prev, result) => [
                ...prev,
                ...result.data.map((item) => [
                  item.tag,
                  parseInt(`${item.freq / item.distance}`, 10),
                ]),
              ],
              []
            )
            .filter((i) => i[1]);

          setWordCloudLoading(false);
          if (keywords) {
            wc(tagRef.current, {
              list: keywords,
              weightFactor: 2,
              shrinkToFit: true,
              shape: "square",
              rotationSteps: 2,
            });
          } else {
            setShouldDisplayWordCloud(false);
          }
        })
        .catch(() => setWordCloudLoading(false));
    } else if (shouldDisplayWordCloud) {
      setShouldDisplayWordCloud(false);
    }
    return wc?.stop();
  }, [values, shouldDisplayWordCloud]);

  const searchQuery = useMemo(
    () =>
      debounce((query: string) => {
        dispatch(
          getInfluencerTagResults({
            query,
          })
        );
      }, 600),
    [dispatch]
  );

  return (
    <Grid container>
      <Grid item xs={12}>
        <SearchContainer>
          <Autocomplete
            multiple
            limitTags={1}
            getLimitTagsText={(more) => `+${more} tags`}
            filterOptions={(x) => x}
            filterSelectedOptions
            options={tagSearchResults ?? []}
            fullWidth
            placeholder="Search tags"
            aria-label="Search tags"
            noOptionsText="No tags found"
            onChange={(event, newValue) => {
              const initValue = values?.value ?? "";
              const calculatedValue = initValue
                .split(" ")
                .filter((a) => !a.startsWith("#"))
                .concat(newValue.map((v) => `#${v.tag}`))
                .join(" ");

              setValue(
                "filter.relevance",
                calculatedValue.length > 0
                  ? {
                      value: calculatedValue,
                      weight: values?.weight ?? "0.5",
                    }
                  : undefined
              );
              onSubmit(event);
            }}
            onInputChange={(event, newValue) => {
              searchQuery(newValue);
            }}
            getOptionLabel={(x) => x.tag}
            loading={tagSearchStatus === ELoadingStatus.LOADING}
            sx={{ mr: 1, fontWeight: "fontWeightBold" }}
            renderInput={({ InputLabelProps, InputProps, ...inpProps }) => (
              <OutlinedInput
                sx={{ backgroundColor: "#FFF" }}
                autoFocus
                ref={InputProps.ref}
                endAdornment={InputProps.endAdornment}
                startAdornment={
                  <InputAdornment position="start">
                    <Select
                      autoWidth
                      disableUnderline
                      variant="standard"
                      defaultValue={ProfileType.INSTAGRAM}
                      fullWidth
                      sx={{
                        whiteSpace: "nowrap",
                      }}
                      renderValue={(option) => (
                        <Stack
                          spacing={1}
                          alignItems="center"
                          flexDirection="row"
                        >
                          {
                            {
                              [ProfileType.INSTAGRAM]: <InstagramIcon />,
                              [ProfileType.YOUTUBE]: <SubscriptionsIcon />,
                              [ProfileType.TIKTOK]: <MusicNoteIcon />,
                            }[option]
                          }

                          <Box
                            sx={{
                              fontSize: theme["typography"].pxToRem(20),
                              pl: 1,
                              mt: "0 !important",
                            }}
                          >
                            {
                              {
                                [ProfileType.INSTAGRAM]: "Instagram",
                                [ProfileType.YOUTUBE]: "YouTube",
                                [ProfileType.TIKTOK]: "Tik Tok",
                              }[option]
                            }
                          </Box>
                        </Stack>
                      )}
                      // onChange={handleChange}
                    >
                      <MenuItem selected value={ProfileType.INSTAGRAM}>
                        <ListItemIcon
                          sx={{
                            mr: 0,
                          }}
                        >
                          <InstagramIcon />
                        </ListItemIcon>
                        <ListItemText>Instagram</ListItemText>
                      </MenuItem>
                      <MenuItem value={ProfileType.YOUTUBE} disabled>
                        <ListItemIcon
                          sx={{
                            mr: 0,
                          }}
                        >
                          <SubscriptionsIcon />
                        </ListItemIcon>
                        <ListItemText
                          sx={{
                            fontSize: theme.typography.pxToRem(20),
                          }}
                        >
                          YouTube
                        </ListItemText>
                      </MenuItem>
                      <MenuItem value={ProfileType.TIKTOK} disabled>
                        <ListItemIcon
                          sx={{
                            mr: 0,
                          }}
                        >
                          <MusicNoteIcon />
                        </ListItemIcon>
                        <ListItemText
                          sx={{
                            fontSize: theme.typography.pxToRem(20),
                          }}
                        >
                          Tik Tok
                        </ListItemText>
                      </MenuItem>
                    </Select>
                    <Box className="tag-end-adornment" sx={{}}>
                      {InputProps.startAdornment}
                    </Box>
                  </InputAdornment>
                }
                {...inpProps}
              />
            )}
          />
        </SearchContainer>
      </Grid>

      {shouldDisplayWordCloud && (
        <Grid item xs={12} sm={12} md={12} style={{ marginTop: "20px" }}>
          <SkeletonLoader loading={wordCloudLoading} variant="rectangular">
            <Card
              style={{
                display: "flex",
                flexDirection: "column",
                height: "100%",
              }}
            >
              <CardContent style={{ flexGrow: 1 }}>
                <div
                  ref={tagRef}
                  style={{ height: "100%", minHeight: "200px" }}
                />
              </CardContent>
            </Card>
          </SkeletonLoader>
        </Grid>
      )}
      {dirtyFields.filter?.relevance && !isEmpty(values?.value) ? (
        <Grid
          item
          xs={12}
          sx={{
            pt: 2,
          }}
        >
          <Box>
            <FormLabel
              sx={{
                pb: 5,
              }}
            >
              Search Priority
            </FormLabel>
          </Box>
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Box
              sx={{
                pr: 2,
              }}
            >
              Relevance
            </Box>
            <Slider
              step={null}
              marks={SLIDER_MARKS}
              min={0}
              max={0.95}
              defaultValue={parseFloat(values?.weight ?? "0.5")}
              track="normal"
              onChange={(event, v) => {
                const initValue = values?.value ?? "";
                setValue("filter.relevance", {
                  weight: `${v}`,
                  value: initValue,
                });
                onSubmit(event);
              }}
              getAriaLabel={() => "Search Relevance"}
              valueLabelDisplay="off"
            />
            <Box
              sx={{
                pl: 2,
              }}
            >
              Account Size
            </Box>
          </Box>
        </Grid>
      ) : null}
    </Grid>
  );
};
