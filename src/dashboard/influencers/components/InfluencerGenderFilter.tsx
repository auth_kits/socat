import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  Stack,
} from "@material-ui/core";
import { useEffect } from "react";
import { Controller } from "react-hook-form";
import { useAppSelector } from "../../../app/hooks";
import { EGenderFilter, INITIAL_FILTER_VALUE } from "../action";
import { selectGenderFilter } from "../selector";
import { FilterComponentProps, FILTER_LABELS } from "./filterLabels";
import { FilterPoppper } from "./FilterPopper";

export const InfluencerGenderFilter: React.FC<FilterComponentProps> = ({
  control,
  setValue,
  formState: { dirtyFields },
}) => {
  const values = useAppSelector(selectGenderFilter);
  useEffect(() => {
    setValue(
      "filter.gender.code",
      values?.influencerGender?.code ??
        INITIAL_FILTER_VALUE.filter.gender?.code,
      {
        shouldDirty: true,
      }
    );
    setValue(
      "filter.gender.weight",
      values?.influencerGender?.weight ??
        INITIAL_FILTER_VALUE.filter.gender?.weight,
      {
        shouldDirty: true,
      }
    );
    setValue(
      "filter.audience_gender.code",
      values?.audienceGender?.code ??
        INITIAL_FILTER_VALUE.filter.audience_gender?.code,
      {
        shouldDirty: true,
      }
    );
    setValue(
      "filter.audience_gender.weight",
      values?.audienceGender?.weight ??
        INITIAL_FILTER_VALUE.filter.audience_gender?.weight,
      {
        shouldDirty: true,
      }
    );
  }, [values, setValue]);

  return (
    <FilterPoppper
      label="Gender"
      isDirty={
        !!(dirtyFields.filter?.gender || dirtyFields.filter?.audience_gender)
      }
    >
      <Stack spacing={2}>
        <Controller
          name="filter.audience_gender.code"
          control={control}
          render={({ field }) => (
            <FormControl>
              <FormLabel component="legend">Audience Gender</FormLabel>
              <RadioGroup
                row
                aria-label="filter.audience_gender.code"
                {...field}
              >
                <FormControlLabel
                  value={EGenderFilter.ANY}
                  control={<Radio />}
                  label={FILTER_LABELS.get(EGenderFilter.ANY)}
                />
                <FormControlLabel
                  value={EGenderFilter.MALE}
                  control={<Radio />}
                  label={FILTER_LABELS.get(EGenderFilter.MALE)}
                />
                <FormControlLabel
                  value={EGenderFilter.FEMALE}
                  control={<Radio />}
                  label={FILTER_LABELS.get(EGenderFilter.FEMALE)}
                />
              </RadioGroup>
            </FormControl>
          )}
        />
        <Controller
          name="filter.gender.code"
          control={control}
          render={({ field }) => (
            <FormControl>
              <FormLabel component="legend">Influencer Gender</FormLabel>
              <RadioGroup aria-label="filter.gender.code" {...field}>
                <FormControlLabel
                  value={EGenderFilter.ANY}
                  control={<Radio />}
                  label={FILTER_LABELS.get(EGenderFilter.ANY)}
                />
                <FormControlLabel
                  value={EGenderFilter.MALE}
                  control={<Radio />}
                  label={FILTER_LABELS.get(EGenderFilter.MALE)}
                />
                <FormControlLabel
                  value={EGenderFilter.FEMALE}
                  control={<Radio />}
                  label={FILTER_LABELS.get(EGenderFilter.FEMALE)}
                />

                <FormControlLabel
                  value={EGenderFilter.KNOWN}
                  control={<Radio />}
                  label={FILTER_LABELS.get(EGenderFilter.KNOWN)}
                />
                <FormControlLabel
                  value={EGenderFilter.UNKNOWN}
                  control={<Radio />}
                  label={FILTER_LABELS.get(EGenderFilter.UNKNOWN)}
                />
              </RadioGroup>
            </FormControl>
          )}
        />
      </Stack>
    </FilterPoppper>
  );
};
