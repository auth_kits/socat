import { useState } from "react";
import { Checkbox, FormControlLabel, Grid, Button } from "@material-ui/core";
import { Controller } from "react-hook-form";
import { FilterComponentProps } from "./filterLabels";

export const CheckboxFilters: React.FC<FilterComponentProps> = ({
  control,
  setValue,
  onSubmit,
}) => {
  const [showVerified, setShowVerified] = useState(false);

  const handleClick = (value) => {
    setValue("filter.is_verified", value ? true : undefined, {
      shouldDirty: true,
    });
    setShowVerified(value);
    onSubmit();
  };
  return (
    <Grid container>
      <Grid item>
        <Controller
          name="filter.is_verified"
          control={control}
          render={({ field }) => (
            // <FormControlLabel
            //   control={
            //     <Checkbox
            //       size="small"
            //       value
            //       ref={field.ref}
            //       checked={field?.value === true}
            //       onChange={(e, checked) => {
            // setValue("filter.is_verified", checked ? true : undefined, {
            //   shouldDirty: true,
            // });
            //         onSubmit(e);
            //       }}
            //     />
            //   }
            //   label="Only Verified Accounts"
            // />
            <Button
              variant={showVerified ? "contained" : "outlined"}
              color="secondary"
              onClick={() => handleClick(!showVerified)}
            >
              Only Verified Accounts
            </Button>
          )}
        />
      </Grid>
      <Grid item className="hideMeAlways">
        <Controller
          name="filter.only"
          control={control}
          render={({ field }) => (
            <FormControlLabel
              control={
                <Checkbox
                  size="small"
                  value="known"
                  ref={field.ref}
                  checked={field?.value === "known"}
                  onChange={(e, checked) => {
                    setValue("filter.only", checked ? "known" : undefined);
                    onSubmit(e);
                  }}
                />
              }
              label="Only Previously Exported"
            />
          )}
        />
      </Grid>
      <Grid item className="hideMeAlways">
        <Controller
          name="filter.audience_credibility_class"
          control={control}
          render={({ field }) => (
            <FormControlLabel
              control={
                <Checkbox
                  size="small"
                  value={["low", "normal", "high", "best"]}
                  ref={field.ref}
                  checked={(field?.value ?? []).length > 0}
                  onChange={(e, checked) => {
                    setValue(
                      "filter.audience_credibility_class",
                      checked ? ["low", "normal", "high", "best"] : undefined
                    );
                    onSubmit(e);
                  }}
                />
              }
              label="Only Credible Accounts"
            />
          )}
        />
      </Grid>
      <Grid item className="hideMeAlways">
        <Controller
          name="filter.is_hidden"
          control={control}
          render={({ field }) => (
            <FormControlLabel
              control={
                <Checkbox
                  size="small"
                  value={false}
                  ref={field.ref}
                  checked={field.value === false}
                  onChange={(e, checked) => {
                    setValue("filter.is_hidden", checked ? false : undefined);
                    onSubmit(e);
                  }}
                />
              }
              label="Exclude Private Accounts"
            />
          )}
        />
      </Grid>
      <Grid item className="hideMeAlways">
        <Controller
          name="filter.has_audience_data"
          control={control}
          render={({ field }) => (
            <FormControlLabel
              control={
                <Checkbox
                  size="small"
                  value
                  ref={field.ref}
                  checked={field.value === true}
                  onChange={(e, checked) => {
                    console.log("changing value");
                    setValue(
                      "filter.has_audience_data",
                      checked ? true : undefined
                    );
                    onSubmit(e);
                  }}
                />
              }
              label="Has Audience Data"
            />
          )}
        />
      </Grid>
    </Grid>
  );
};
