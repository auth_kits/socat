import { FormControl, FormLabel, Slider, Stack } from "@material-ui/core";
import { Box } from "@material-ui/system";
import { isEmpty } from "lodash";
import { useEffect, useState } from "react";
import { Controller } from "react-hook-form";
import { useAppSelector } from "../../../app/hooks";
import { selectAgeFilter } from "../selector";
import { FilterComponentProps } from "./filterLabels";
import { FilterPoppper } from "./FilterPopper";
import { WeightFilter } from "./WeightFilter";

// !!IMPORTANT
// PLEASE KEEP THIS ARRAY SORTED BY 'value', IF YOU WANT TO CHANGE
// THE ORDER PLEASE GO THROUGH THE AUDIENCE_AGE CODE
const SLIDER_MARKS = [
  {
    value: 13,
  },
  {
    value: 18,
  },
  {
    value: 25,
  },
  {
    value: 35,
  },
  {
    value: 45,
  },
  {
    value: 65,
  },
  {
    value: 75,
  },
];

export const getAudienceAgeValue = (field) => {
  const values: [number, number] = [Infinity, -Infinity];
  field.value?.forEach((v) => {
    const ages = v.code.split("-");
    const startAge = parseInt(ages[0], 10);
    if (startAge < values[0]) {
      values[0] = startAge;
    }

    if (ages[1] === "") {
      values[1] = 75;
    }

    const endAge = parseInt(ages[1], 10);
    if (endAge > values[1]) {
      values[1] = endAge;
    }
  });
  if (isEmpty(values)) {
    return undefined;
  }
  return values;
};
export const InfluencerAgeFilter: React.FC<FilterComponentProps> = ({
  control,
  formState: { dirtyFields },
  setValue,
}) => {
  const values = useAppSelector(selectAgeFilter);
  const [weight, setWeight] = useState("0.1");
  useEffect(() => {
    setValue(
      "filter.age",
      values.influencerAge ?? {
        left_number: "",
        right_number: "",
      },
      {
        shouldDirty: true,
      }
    );
    setValue("filter.audience_age", values.audienceAge ?? [], {
      shouldDirty: true,
    });
  }, [values, setValue]);

  const handleAudienceAgeChange = (v) => {
    const val: any = [];
    let firstValue = `${v[0] ?? 13}`;
    SLIDER_MARKS.forEach((m, index) => {
      if (m.value > v[0] && v[1] > m.value && index > 0) {
        val.push({
          code: `${firstValue}-${m.value - 1}`,
          weight,
        });
        firstValue = `${m.value}`;
      }
    });
    val.push({
      code: `${firstValue}-${v[1] === 75 ? "" : v[1]}`,
      weight,
    });
    setValue("filter.audience_age", val, {
      shouldDirty: true,
      shouldTouch: true,
    });
  };
  return (
    <FilterPoppper
      label="Age"
      isDirty={!!(dirtyFields.filter?.age || dirtyFields.filter?.audience_age)}
    >
      <Stack
        spacing={2}
        sx={{
          width: 200,
        }}
      >
        <Controller
          name="filter.audience_age"
          control={control}
          rules={{
            required: true,
          }}
          render={({ field }) => (
            <FormControl>
              <FormLabel
                sx={{
                  pb: 5,
                }}
                htmlFor="audience-age"
              >
                Audience&apos;s Age
              </FormLabel>
              <Box sx={{ px: 2 }}>
                <Slider
                  id="audience-age"
                  defaultValue={[13, 65]}
                  step={null}
                  marks={SLIDER_MARKS}
                  min={13}
                  max={75}
                  value={getAudienceAgeValue(field)}
                  name={field.name}
                  ref={field.ref}
                  onChange={(event, v) => {
                    handleAudienceAgeChange(v);
                  }}
                  getAriaLabel={() => "Audience's Age"}
                  valueLabelDisplay="on"
                  valueLabelFormat={(x) => (x > 65 ? "65+" : `${x}`)}
                  getAriaValueText={(value) => `${value}`}
                />
              </Box>
            </FormControl>
          )}
        />
        <WeightFilter
          value={weight}
          id="audience-weight"
          onChange={(w) => {
            setWeight(w);
            handleAudienceAgeChange(
              getAudienceAgeValue({
                value: values.audienceAge,
              })
            );
          }}
        />

        <Controller
          name="filter.age"
          control={control}
          rules={{
            required: true,
          }}
          render={({ field }) => (
            <FormControl>
              <FormLabel
                sx={{
                  pb: 5,
                }}
                htmlFor="influencer-age"
              >
                Influencer&apos;s Age
              </FormLabel>
              <Box sx={{ px: 2 }}>
                <Slider
                  id="influencer-age"
                  defaultValue={[18, 65]}
                  step={null}
                  marks={SLIDER_MARKS}
                  min={18}
                  max={75}
                  value={[
                    field.value.left_number === ""
                      ? 18
                      : parseInt(field.value.left_number, 10),
                    field.value.right_number === ""
                      ? 75
                      : parseInt(field.value.right_number, 10) + 1,
                  ]}
                  name={field.name}
                  ref={field.ref}
                  onChange={(event, v) => {
                    setValue(
                      "filter.age",
                      {
                        left_number: `${v[0]}`,
                        right_number: `${v[1] > 65 ? "" : v[1] - 1}`,
                      },
                      {
                        shouldDirty: true,
                        shouldTouch: true,
                      }
                    );
                  }}
                  getAriaLabel={() => "Influencer's Age"}
                  valueLabelDisplay="on"
                  valueLabelFormat={(x) => (x > 65 ? "65+" : `${x}`)}
                  getAriaValueText={(value) => `${value}`}
                />
              </Box>
            </FormControl>
          )}
        />
      </Stack>
    </FilterPoppper>
  );
};
