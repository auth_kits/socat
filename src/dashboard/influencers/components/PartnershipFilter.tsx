import {
  Autocomplete,
  Box,
  Button,
  debounce,
  Divider,
  Stack,
  TextField,
  Typography,
} from "@material-ui/core";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import isEmpty from "lodash/isEmpty";
import { useEffect, useMemo } from "react";
import { useFieldArray } from "react-hook-form";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { ELoadingStatus, getFilterBrands } from "../action";
import {
  selectFilterBrands,
  selectPartnershipFilter,
  selectRemoteFilterStatus,
} from "../selector";
import { FilterComponentProps } from "./filterLabels";
import { FilterPoppper } from "./FilterPopper";

export const PartnershipFilter: React.FC<FilterComponentProps> = ({
  control,
  setValue,
  onSubmit,
  formState: { dirtyFields },
}) => {
  const values = useAppSelector(selectPartnershipFilter);
  const brands = useAppSelector(selectFilterBrands);
  const status = useAppSelector(selectRemoteFilterStatus);
  const dispatch = useAppDispatch();

  const infArr = useFieldArray({
    control,
    name: "filter.ads_brands",
  });

  const searchBrands = useMemo(
    () =>
      debounce((query: string) => {
        dispatch(
          getFilterBrands({
            query,
          })
        );
      }, 600),
    [dispatch]
  );

  useEffect(() => {
    setValue("filter.ads_brands", values ?? [], {
      shouldDirty: true,
    });
  }, [values, setValue]);

  return (
    <FilterPoppper
      label="Partnership"
      isDirty={!isEmpty(dirtyFields.filter?.ads_brands)}
    >
      <Stack
        spacing={2}
        sx={{
          width: 220,
        }}
      >
        <Typography variant="caption">Influencer</Typography>
        <Autocomplete
          filterOptions={(x) => x}
          size="small"
          options={!brands ? [] : brands}
          autoHighlight
          aria-label="Brand…"
          noOptionsText="No results found"
          loading={status === ELoadingStatus.LOADING}
          fullWidth
          onInputChange={(event, newValue) => {
            searchBrands(newValue);
          }}
          onChange={(e, val) => {
            if (!isEmpty(val)) {
              infArr.append({
                id: val?.id ?? "",
                label: val?.name,
              } as never);
              onSubmit(e);
            }
          }}
          getOptionLabel={(option) => option?.name ?? ""}
          renderInput={(params) => (
            <TextField
              {...params}
              label="Brands"
              inputProps={{
                ...params.inputProps,
                autoComplete: "new-password", // disable autocomplete and autofill
              }}
            />
          )}
        />
        <Box>
          {infArr.fields?.map((f: { id: string; label?: string }, idx) => {
            const filterName: any = `filter.ads_brand.${idx}.label`;
            return (
              <Stack spacing={2} key={`${f.id}-label`}>
                <Divider />
                <Box
                  sx={{
                    flexDirection: "row",
                    alignItems: "center",
                    display: "flex",
                  }}
                >
                  <span>{f.label}</span>
                  <Button
                    variant="text"
                    size="small"
                    color="error"
                    sx={{
                      m: 0,
                    }}
                    onClick={() => infArr.remove(idx)}
                  >
                    <DeleteForeverIcon />
                  </Button>
                </Box>
              </Stack>
            );
          })}
        </Box>
      </Stack>
    </FilterPoppper>
  );
};
