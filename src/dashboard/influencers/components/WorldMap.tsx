import React from "react";
import {
  ComposableMap,
  Geographies,
  Geography,
  Marker,
  ZoomableGroup,
} from "react-simple-maps";
import { topojson } from "../../../utils/worldMapTopojson";

function WorldMap({ cities }) {
  const markers =
    cities?.map((city) => ({
      name: city.name,
      coordinates: [city.coords.lon, city.coords.lat],
    })) || [];

  return (
    <ComposableMap
      projection="geoEqualEarth"
      projectionConfig={{
        scale: 200,
      }}
    >
      <ZoomableGroup zoom={1}>
        <Geographies geography={topojson}>
          {({ geographies }) =>
            geographies.map((geo) => (
              <Geography
                key={geo.rsmKey}
                geography={geo}
                fill="#EAEAEC"
                stroke="#D6D6DA"
              />
            ))
          }
        </Geographies>
        {markers.map(({ name, coordinates }) => (
          <Marker key={name} coordinates={coordinates}>
            <g
              fill="none"
              stroke="#FF5533"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
              transform="translate(-12, -24)"
            >
              <circle cx="12" cy="10" r="3" />
              <path
                style={{ outlineWidth: 0 }}
                d="M12 21.7C17.3 17 20 13 20 10a8 8 0 1 0-16 0c0 3 2.7 6.9 8 11.7z"
              />
            </g>
          </Marker>
        ))}
      </ZoomableGroup>
    </ComposableMap>
  );
}

export default WorldMap;
