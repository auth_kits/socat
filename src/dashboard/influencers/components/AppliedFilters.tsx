import { Chip, Divider } from "@material-ui/core";
import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/system";
import { isEmpty } from "lodash";
import { FieldNamesMarkedBoolean, UseFormSetValue } from "react-hook-form";
import { useAppSelector } from "../../../app/hooks";
import { fPercent, fShortenNumber } from "../../../utils/formatNumber";
import { INITIAL_FILTER_VALUE } from "../action";
import { selectFilterData } from "../selector";
import { getAudienceAgeValue } from "./AgeFilter";
import { FilterFormState, FILTER_LABELS } from "./filterLabels";

const ChipListItem = styled("li")(({ theme }) => ({
  margin: theme.spacing(0.5),
}));

const FilterValue = ({ label, value }) => (
  <Box>
    {label} : {value}
  </Box>
);

interface AppliedFiltersProps {
  dirtyFields: FieldNamesMarkedBoolean<FilterFormState>;
  setValue: UseFormSetValue<FilterFormState>;
  onSubmit: any;
}
export const AppliedFilters: React.FC<AppliedFiltersProps> = ({
  dirtyFields,
  setValue,
  onSubmit,
}) => {
  const filterData = useAppSelector(selectFilterData);
  // console.log("filterData", filterData);
  const audienceAge = getAudienceAgeValue({
    value: filterData.filter?.audience_age,
  }) ?? [0, 0];

  return (
    <>
      {isEmpty(dirtyFields) ? null : <Divider />}
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          flexWrap: "wrap",
          listStyle: "none",
          p: 0.5,
          pl: 0,
          m: 0,
        }}
        component="ul"
      >
        {dirtyFields.audience_source && (
          <ChipListItem key="audience_source">
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Audience Type"
                  value={FILTER_LABELS.get(filterData.audience_source)}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "audience_source",
                  INITIAL_FILTER_VALUE.audience_source,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        )}
        {dirtyFields.filter?.age && (
          <ChipListItem key="filter.age">
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Influencer Age"
                  value={`${filterData.filter?.age?.left_number}-${filterData.filter?.age?.right_number} y.o.`}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue("filter.age", INITIAL_FILTER_VALUE.filter.age, {
                  shouldDirty: true,
                });
                onSubmit(e);
              }}
            />
          </ChipListItem>
        )}
        {dirtyFields.filter?.audience_age && (
          <ChipListItem key="filter.audience_age">
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Audience Age"
                  value={`${audienceAge[0]}-${audienceAge[1]} y.o. >${
                    parseFloat(filterData.filter?.audience_age[0].weight) * 100
                  }%`}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.audience_age",
                  INITIAL_FILTER_VALUE.filter.audience_age,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        )}
        {dirtyFields.filter?.gender && (
          <ChipListItem key="filter.gender">
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Influencer Gender"
                  value={FILTER_LABELS.get(filterData.filter?.gender?.code)}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue("filter.gender", INITIAL_FILTER_VALUE.filter.gender, {
                  shouldDirty: true,
                });
                onSubmit(e);
              }}
            />
          </ChipListItem>
        )}
        {dirtyFields.filter?.audience_gender && (
          <ChipListItem key="filter.audience_gender">
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Audience Gender"
                  value={FILTER_LABELS.get(
                    filterData.filter?.audience_gender.code
                  )}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.audience_gender",
                  INITIAL_FILTER_VALUE.filter.audience_gender,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        )}

        {dirtyFields.filter?.audience_race && (
          <ChipListItem key="filter.audience_race">
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Audience Ethnicity"
                  value={FILTER_LABELS.get(
                    filterData.filter?.audience_race.code
                  )}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.audience_race",
                  INITIAL_FILTER_VALUE.filter.audience_race,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        )}

        {filterData.filter?.audience_geo?.map((f, idx) => (
          <ChipListItem key={`filter.audience_geo.${f.id}`}>
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Audience Location"
                  value={`${f.label} | ${fPercent(
                    parseFloat(f.weight) * 100
                  )} `}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.audience_geo",
                  INITIAL_FILTER_VALUE.filter.audience_geo,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        ))}

        {filterData.filter?.geo?.map((f, idx) => (
          <ChipListItem key={`filter.geo.${f.id}`}>
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Influencer Location"
                  value={`${f.label} | ${fPercent(
                    parseFloat(f.weight) * 100
                  )} `}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue("filter.geo", INITIAL_FILTER_VALUE.filter.geo, {
                  shouldDirty: true,
                });
                onSubmit(e);
              }}
            />
          </ChipListItem>
        ))}

        {dirtyFields.filter?.text && (
          <ChipListItem key="filter.text">
            <Chip
              size="small"
              label={
                <FilterValue label="Bio" value={filterData.filter?.text} />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue("filter.text", INITIAL_FILTER_VALUE.filter.text, {
                  shouldDirty: true,
                });
                onSubmit(e);
              }}
            />
          </ChipListItem>
        )}

        {filterData.filter?.audience_brand?.map((f, idx) => (
          <ChipListItem key={`filter.audience_brand.${f.id}`}>
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Audience Brand"
                  value={`${f.label} | ${fPercent(
                    parseFloat(f.weight) * 100
                  )} `}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.audience_brand",
                  INITIAL_FILTER_VALUE.filter.audience_geo,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        ))}

        {filterData.filter?.brand?.map((f, idx) => (
          <ChipListItem key={`filter.brand.${f.id}`}>
            <Chip
              size="small"
              label={<FilterValue label="Influencer Brand" value={f.label} />}
              variant="outlined"
              onDelete={(e) => {
                setValue("filter.brand", INITIAL_FILTER_VALUE.filter.brand, {
                  shouldDirty: true,
                });
                onSubmit(e);
              }}
            />
          </ChipListItem>
        ))}

        {dirtyFields.filter?.lang && (
          <ChipListItem key="filter.lang">
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Influencer"
                  value={filterData.filter?.lang?.label}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue("filter.lang", INITIAL_FILTER_VALUE.filter.lang, {
                  shouldDirty: true,
                });
                onSubmit(e);
              }}
            />
          </ChipListItem>
        )}
        {dirtyFields.filter?.audience_lang && (
          <ChipListItem key="filter.audience_lang">
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Audience"
                  value={`${
                    filterData.filter?.audience_lang?.label
                  } | ${fPercent(
                    parseFloat(
                      filterData.filter?.audience_lang?.weight ?? "0.05"
                    ) * 100
                  )} `}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.audience_lang",
                  INITIAL_FILTER_VALUE.filter.audience_lang,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        )}

        {filterData.filter?.audience_brand_category?.map((f, idx) => (
          <ChipListItem key={`filter.audience_brand_category.${f.id}`}>
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Audience Interests"
                  value={`${f.label} | ${fPercent(
                    parseFloat(f.weight) * 100
                  )} `}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.audience_brand_category",
                  INITIAL_FILTER_VALUE.filter.audience_brand_category,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        ))}

        {filterData.filter?.brand_category?.map((f, idx) => (
          <ChipListItem key={`filter.brand_category.${f.id}`}>
            <Chip
              size="small"
              label={
                <FilterValue label="Influencer Interests" value={f.label} />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.brand_category",
                  INITIAL_FILTER_VALUE.filter.brand_category,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        ))}

        {filterData.filter?.ads_brands?.map((f, idx) => (
          <ChipListItem key={`filter.brand.${f.id}`}>
            <Chip
              size="small"
              label={<FilterValue label="Partnership" value={f.label} />}
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.ads_brands",
                  INITIAL_FILTER_VALUE.filter.ads_brands,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        ))}

        {/* Engagements */}
        {dirtyFields.filter?.followers && (
          <ChipListItem key="filter.followers">
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Followers"
                  value={`${fShortenNumber(
                    filterData.filter?.followers?.left_number ?? 0
                  )}-${fShortenNumber(
                    filterData.filter?.followers?.right_number ?? 0
                  )}`}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.followers",
                  INITIAL_FILTER_VALUE.filter.followers,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        )}

        {/* Engagements */}
        {dirtyFields.filter?.engagements && (
          <ChipListItem key="filter.engagements">
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Engagements"
                  value={`${fShortenNumber(
                    filterData.filter?.engagements?.left_number ?? 0
                  )}-${fShortenNumber(
                    filterData.filter?.engagements?.right_number ?? 0
                  )}`}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.engagements",
                  INITIAL_FILTER_VALUE.filter.engagements,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        )}
        {dirtyFields.filter?.engagement_rate && (
          <ChipListItem key="filter.engagement_rate">
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Engagement Rate"
                  value={`≥ ${fPercent(
                    filterData.filter.engagement_rate.value
                      ? filterData.filter.engagement_rate.value * 100
                      : 0
                  )}`}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.engagement_rate",
                  INITIAL_FILTER_VALUE.filter.engagement_rate,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        )}

        {dirtyFields.filter?.relevance && (
          <ChipListItem key="filter.relevance">
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Influencer Lookalike"
                  value={filterData.filter.relevance?.value}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.relevance",
                  INITIAL_FILTER_VALUE.filter.relevance,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        )}

        {dirtyFields.filter?.audience_relevance && (
          <ChipListItem key="filter.audience_relevance">
            <Chip
              size="small"
              label={
                <FilterValue
                  label="Audience Lookalike"
                  value={filterData.filter.audience_relevance?.value}
                />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.audience_relevance",
                  INITIAL_FILTER_VALUE.filter.audience_relevance,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        )}

        {dirtyFields.filter?.is_verified && (
          <ChipListItem key="filter.is_verified">
            <Chip
              size="small"
              label={
                <FilterValue label="Only Verified Accounts" value="true" />
              }
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.is_verified",
                  INITIAL_FILTER_VALUE.filter.is_verified,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        )}

        {filterData.filter?.with_contact?.map((f, idx) => (
          <ChipListItem key={`filter.with_contact.${f.type}`}>
            <Chip
              size="small"
              label={<FilterValue label="Contact" value={f.type} />}
              variant="outlined"
              onDelete={(e) => {
                setValue(
                  "filter.with_contact",
                  INITIAL_FILTER_VALUE.filter.with_contact,
                  {
                    shouldDirty: true,
                  }
                );
                onSubmit(e);
              }}
            />
          </ChipListItem>
        ))}
      </Box>
    </>
  );
};
