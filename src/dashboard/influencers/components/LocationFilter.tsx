import {
  Autocomplete,
  Button,
  debounce,
  Divider,
  Grid,
  Stack,
  TextField,
  Typography,
} from "@material-ui/core";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import { Box } from "@material-ui/system";
import { isEmpty } from "lodash";
import { useEffect, useMemo } from "react";
import { Controller, useFieldArray } from "react-hook-form";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { ELoadingStatus, getFilterLocations } from "../action";
import {
  selectAudienceLocation,
  selectFilterLocations,
  selectInfluencerGeo,
  selectInfluencerLocation,
  selectRemoteFilterStatus,
} from "../selector";
import { FilterComponentProps } from "./filterLabels";
import { FilterPoppper } from "./FilterPopper";
import { WeightFilter } from "./WeightFilter";

export const LocationFilter: React.FC<FilterComponentProps> = ({
  control,
  setValue,
  onSubmit,
  formState: { dirtyFields },
}) => {
  const aValues = useAppSelector(selectAudienceLocation);
  const iValues = useAppSelector(selectInfluencerGeo);
  const locations = useAppSelector(selectFilterLocations);
  const status = useAppSelector(selectRemoteFilterStatus);
  const dispatch = useAppDispatch();
  const audArr = useFieldArray({
    control, // control props comes from useForm (optional: if you are using FormContext)
    name: "filter.audience_geo", // unique name for your Field Array
    keyName: "id", // default to "id", you can change the key name
  });

  const infArr = useFieldArray({
    control,
    name: "filter.geo",
  });
  const searchLocations = useMemo(
    () =>
      debounce((query: string) => {
        dispatch(
          getFilterLocations({
            query,
          })
        );
      }, 600),
    [dispatch]
  );

  useEffect(() => {
    setValue("filter.audience_geo", aValues, {
      shouldDirty: true,
    });
    setValue("filter.geo", iValues, {
      shouldDirty: true,
    });
  }, [iValues, aValues, setValue]);

  return (
    <FilterPoppper
      label="Location"
      isDirty={
        !isEmpty(dirtyFields.filter?.audience_geo) ||
        !isEmpty(dirtyFields.filter?.geo)
      }
    >
      <Grid
        container
        columnSpacing={2}
        sx={{
          width: 260,
        }}
      >
        <Grid item xs={12}>
          <Typography variant="caption">Audience</Typography>
        </Grid>
        <Grid item xs={12}>
          <Autocomplete
            clearOnBlur
            filterOptions={(x) => x}
            size="small"
            options={!locations ? [] : locations}
            autoHighlight
            aria-label="City/Country…"
            noOptionsText="No results found"
            loading={status === ELoadingStatus.LOADING}
            fullWidth
            onInputChange={(event, newValue, reason) => {
              searchLocations(newValue);
            }}
            onChange={(e, val) => {
              if (val?.id) {
                audArr.append({
                  id: val.id,
                  weight: "0.05",
                  label: val.name,
                });
                onSubmit(e);
              }
              e.currentTarget["value"] = "";
            }}
            getOptionLabel={(option) => option.name}
            renderOption={(props, option) => (
              <Box
                key={option.id}
                component="li"
                sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                {...props}
              >
                {option.type && (
                  <img
                    loading="lazy"
                    width="20"
                    src={`https://flagcdn.com/w20/${option.code.toLowerCase()}.png`}
                    srcSet={`https://flagcdn.com/w40/${option.code.toLowerCase()}.png 2x`}
                    alt=""
                  />
                )}
                {option.name} ({option.code})
              </Box>
            )}
            renderInput={(params) => (
              <TextField
                {...params}
                label="City or Country"
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password", // disable autocomplete and autofill
                }}
              />
            )}
          />
          <Box>
            {audArr.fields?.map((f, idx) => {
              const filterName: any = `filter.audience_geo.${idx}.weight`;
              return (
                <Controller
                  key={f.id}
                  name={filterName}
                  control={control}
                  render={({ field }) => (
                    <Stack spacing={2}>
                      <Divider />
                      <WeightFilter
                        label={
                          <Box
                            sx={{
                              alignItems: "center",
                              flexDirection: "row",
                              display: "flex",
                            }}
                          >
                            <span>{f?.label}</span>
                            <Button
                              variant="text"
                              size="small"
                              color="error"
                              sx={{
                                m: 0,
                              }}
                              onClick={() => audArr.remove(idx)}
                            >
                              <DeleteForeverIcon />
                            </Button>
                          </Box>
                        }
                        id={filterName}
                        field={field}
                        value={field.value}
                        onChange={(v) => {
                          setValue(filterName, v, {
                            shouldDirty: true,
                          });
                          onSubmit();
                        }}
                      />
                    </Stack>
                  )}
                />
              );
            })}
          </Box>
        </Grid>
        <Grid item xs={12} sx={{ py: 1 }} />
        <Grid item xs={12}>
          <Typography variant="caption">Influencer</Typography>
        </Grid>
        <Grid item xs={12}>
          <Autocomplete
            filterOptions={(x) => x}
            size="small"
            options={!locations ? [] : locations}
            autoHighlight
            aria-label="City/Country…"
            noOptionsText="No results found"
            loading={status === ELoadingStatus.LOADING}
            fullWidth
            onInputChange={(event, newValue) => {
              event.preventDefault();
              searchLocations(newValue);
            }}
            onChange={(e, val) => {
              if (val?.id) {
                infArr.append({
                  id: val.id,
                  weight: "0.05",
                  label: val.name,
                });
                onSubmit(e);
              }
            }}
            getOptionLabel={(option) => option.name}
            renderOption={(props, option) => (
              <Box
                key={option.id}
                component="li"
                sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                {...props}
              >
                {option.type && (
                  <img
                    loading="lazy"
                    width="20"
                    src={`https://flagcdn.com/w20/${option.code.toLowerCase()}.png`}
                    srcSet={`https://flagcdn.com/w40/${option.code.toLowerCase()}.png 2x`}
                    alt=""
                  />
                )}
                {option.name} ({option.code})
              </Box>
            )}
            renderInput={(params) => (
              <TextField
                {...params}
                label="City or Country"
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password", // disable autocomplete and autofill
                }}
              />
            )}
          />
          <Box>
            {infArr.fields?.map((f, idx) => {
              const filterName: any = `filter.geo.${idx}.weight`;
              return (
                <Controller
                  key={f.id}
                  name={filterName}
                  control={control}
                  render={({ field }) => (
                    <Stack spacing={2}>
                      <Divider />
                      {/* <WeightFilter
                        label={}
                        id={filterName}
                        field={field}
                        value={field.value}
                        onChange={(v) => {
                          setValue(filterName, v, {
                            shouldDirty: true,
                          });
                          onSubmit();
                        }}
                      /> */}
                      <Box sx={{ flexDirection: "row", display: "flex" }}>
                        <span>{f?.label}</span>
                        <Button
                          variant="text"
                          size="small"
                          color="error"
                          sx={{
                            m: 0,
                          }}
                          onClick={() => {
                            infArr.remove(idx);
                            onSubmit();
                          }}
                        >
                          <DeleteForeverIcon />
                        </Button>
                      </Box>
                    </Stack>
                  )}
                />
              );
            })}
          </Box>
        </Grid>
      </Grid>
    </FilterPoppper>
  );
};
