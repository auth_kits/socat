import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  Stack,
} from "@material-ui/core";
import { useEffect } from "react";
import { Controller } from "react-hook-form";
import { useAppSelector } from "../../../app/hooks";
import { ERaceFilter, INITIAL_FILTER_VALUE } from "../action";
import { selectEthnicityFilter } from "../selector";
import { FilterComponentProps, FILTER_LABELS } from "./filterLabels";
import { FilterPoppper } from "./FilterPopper";

export const EthnicityFilter: React.FC<FilterComponentProps> = ({
  control,
  setValue,
  formState: { dirtyFields },
}) => {
  const values = useAppSelector(selectEthnicityFilter);
  useEffect(() => {
    setValue(
      "filter.audience_race.code",
      values?.code ?? INITIAL_FILTER_VALUE.filter.audience_race?.code,
      {
        shouldDirty: true,
      }
    );
    setValue(
      "filter.audience_race.weight",
      values?.weight ?? INITIAL_FILTER_VALUE.filter.audience_race?.weight,
      {
        shouldDirty: true,
      }
    );
  }, [values, setValue]);

  return (
    <FilterPoppper
      label="Ethnicity"
      isDirty={!!dirtyFields.filter?.audience_race}
    >
      <Stack spacing={2}>
        <Controller
          name="filter.audience_race.code"
          control={control}
          render={({ field }) => (
            <FormControl>
              <FormLabel component="legend">Audience Ethnicity</FormLabel>
              <RadioGroup aria-label="filter.audience_race.code" {...field}>
                <FormControlLabel
                  value={ERaceFilter.ANY}
                  control={<Radio />}
                  label={FILTER_LABELS.get(ERaceFilter.ANY)}
                />
                <FormControlLabel
                  value={ERaceFilter.BLACK}
                  control={<Radio />}
                  label={FILTER_LABELS.get(ERaceFilter.BLACK)}
                />
                <FormControlLabel
                  value={ERaceFilter.ASIAN}
                  control={<Radio />}
                  label={FILTER_LABELS.get(ERaceFilter.ASIAN)}
                />
                <FormControlLabel
                  value={ERaceFilter.HISPANIC}
                  control={<Radio />}
                  label={FILTER_LABELS.get(ERaceFilter.HISPANIC)}
                />
                <FormControlLabel
                  value={ERaceFilter.WHITE}
                  control={<Radio />}
                  label={FILTER_LABELS.get(ERaceFilter.WHITE)}
                />
              </RadioGroup>
            </FormControl>
          )}
        />
      </Stack>
    </FilterPoppper>
  );
};
