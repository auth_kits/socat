import {
  Autocomplete,
  Button,
  debounce,
  Divider,
  Grid,
  Stack,
  TextField,
  Typography,
} from "@material-ui/core";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import { Box } from "@material-ui/system";
import { isEmpty } from "lodash";
import { useEffect, useMemo } from "react";
import { Controller, useFieldArray } from "react-hook-form";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { ELoadingStatus, getFilterInterests } from "../action";
import {
  selectFilterIntrests,
  selectIntrestsFilter,
  selectRemoteFilterStatus,
} from "../selector";
import { FilterComponentProps } from "./filterLabels";
import { FilterPoppper } from "./FilterPopper";
import { WeightFilter } from "./WeightFilter";

export const InterestsFilter: React.FC<FilterComponentProps> = ({
  control,
  setValue,
  onSubmit,
  formState: { dirtyFields },
}) => {
  const values = useAppSelector(selectIntrestsFilter);
  const interests = useAppSelector(selectFilterIntrests);
  const status = useAppSelector(selectRemoteFilterStatus);
  const dispatch = useAppDispatch();
  const audArr = useFieldArray({
    control, // control props comes from useForm (optional: if you are using FormContext)
    name: "filter.audience_brand_category", // unique name for your Field Array
    keyName: "id", // default to "id", you can change the key name
  });

  const infArr = useFieldArray({
    control,
    name: "filter.brand_category",
  });
  const searchIntrests = useMemo(
    () =>
      debounce((query: string) => {
        dispatch(
          getFilterInterests({
            query,
          })
        );
      }, 600),
    [dispatch]
  );

  useEffect(() => {
    setValue("filter.audience_brand_category", values.audCategory, {
      shouldDirty: true,
    });
    setValue("filter.brand_category", values.category, {
      shouldDirty: true,
    });
  }, [values, setValue]);

  return (
    <FilterPoppper
      label="Interest"
      isDirty={
        !isEmpty(dirtyFields.filter?.audience_brand_category) ||
        !isEmpty(dirtyFields.filter?.brand_category)
      }
    >
      <Grid
        container
        columnSpacing={2}
        sx={{
          width: 420,
        }}
      >
        <Grid item xs={6}>
          <Typography variant="caption">Audience</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography variant="caption">Influencer</Typography>
        </Grid>
        <Grid item xs={12} />
        <Grid item xs={6}>
          <Autocomplete
            clearOnBlur
            filterOptions={(x) => x}
            size="small"
            options={!interests ? [] : interests}
            autoHighlight
            aria-label="Interest…"
            noOptionsText="No results found"
            loading={status === ELoadingStatus.LOADING}
            fullWidth
            onInputChange={(event, newValue, reason) => {
              searchIntrests(newValue);
            }}
            onChange={(e, val) => {
              if (val?.id) {
                audArr.append({
                  id: val.id,
                  weight: "0.05",
                  label: val.name,
                });
                onSubmit(e);
              }
              e.currentTarget["value"] = "";
            }}
            getOptionLabel={(option) => option.name}
            renderOption={(props, option) => (
              <Box
                key={option.id}
                component="li"
                sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                {...props}
              >
                {option.name}
              </Box>
            )}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Interests"
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password", // disable autocomplete and autofill
                }}
              />
            )}
          />
          <Box>
            {audArr.fields?.map((f, idx) => {
              const filterName: any = `filter.audience_brand_category.${idx}.weight`;
              return (
                <Controller
                  key={f.id}
                  name={filterName}
                  control={control}
                  render={({ field }) => (
                    <Stack spacing={2}>
                      <Divider />
                      <WeightFilter
                        label={
                          <Box
                            sx={{
                              alignItems: "center",
                              flexDirection: "row",
                              display: "flex",
                            }}
                          >
                            <span>{f?.label}</span>
                            <Button
                              variant="text"
                              size="small"
                              color="error"
                              sx={{
                                m: 0,
                              }}
                              onClick={(e) => {
                                audArr.remove(idx);
                                onSubmit(e);
                              }}
                            >
                              <DeleteForeverIcon />
                            </Button>
                          </Box>
                        }
                        id={filterName}
                        field={field}
                        value={field.value}
                        onChange={(v) => {
                          setValue(filterName, v, {
                            shouldDirty: true,
                          });
                          onSubmit();
                        }}
                      />
                    </Stack>
                  )}
                />
              );
            })}
          </Box>
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            filterOptions={(x) => x}
            size="small"
            options={!interests ? [] : interests}
            autoHighlight
            aria-label="Interest…"
            noOptionsText="No results found"
            loading={status === ELoadingStatus.LOADING}
            fullWidth
            onInputChange={(event, newValue) => {
              event.preventDefault();
              searchIntrests(newValue);
            }}
            onChange={(e, val) => {
              if (val?.id) {
                infArr.append({
                  id: val.id,
                  label: val.name,
                });
                onSubmit(e);
              }
            }}
            getOptionLabel={(option) => option.name}
            renderOption={(props, option) => (
              <Box
                key={option.id}
                component="li"
                sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                {...props}
              >
                {option.name}
              </Box>
            )}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Interests"
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password", // disable autocomplete and autofill
                }}
              />
            )}
          />
          <Box>
            {infArr.fields?.map((f, idx) => {
              const filterName: any = `filter.brand_category.${idx}.weight`;
              return (
                <Controller
                  key={f.id}
                  name={filterName}
                  control={control}
                  render={({ field }) => (
                    <Stack spacing={2}>
                      <Divider />
                      <Box sx={{ flexDirection: "row", display: "flex" }}>
                        <span>{f["label"]}</span>
                        <Button
                          variant="text"
                          size="small"
                          color="error"
                          sx={{
                            m: 0,
                          }}
                          onClick={(e) => {
                            infArr.remove(idx);
                            onSubmit(e);
                          }}
                        >
                          <DeleteForeverIcon />
                        </Button>
                      </Box>
                    </Stack>
                  )}
                />
              );
            })}
          </Box>
        </Grid>
      </Grid>
    </FilterPoppper>
  );
};
