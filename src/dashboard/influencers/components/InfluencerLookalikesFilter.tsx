import {
  Stack,
  Autocomplete,
  TextField,
  Select,
  MenuItem,
} from "@material-ui/core";
import { Box } from "@material-ui/system";
import { Control } from "react-hook-form";
import { EAudienceSource, FilterData } from "../action";
import { FilterPoppper } from "./FilterPopper";

export const InfluencerLookalikesFilter: React.FC<{
  control: Control<{
    filter: FilterData;
    /* eslint-disable camelcase */
    audience_source: EAudienceSource;
    /* eslint-enable camelcase */
  }>;
}> = () => (
  <FilterPoppper label="Lookalike">
    <Stack spacing={2}>
      <Autocomplete
        size="small"
        options={[]}
        autoHighlight
        getOptionLabel={(option: any) => option.label}
        renderOption={(props, option: any) => (
          <Box
            component="li"
            sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
            {...props}
          >
            <img
              loading="lazy"
              width="20"
              src={`https://flagcdn.com/w20/${option.code.toLowerCase()}.png`}
              srcSet={`https://flagcdn.com/w40/${option.code.toLowerCase()}.png 2x`}
              alt=""
            />
            {option.label} ({option.code}) +{option.phone}
          </Box>
        )}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Country"
            inputProps={{
              ...params.inputProps,
              autoComplete: "new-password", // disable autocomplete and autofill
            }}
          />
        )}
      />
      <Select size="small" label="City">
        <MenuItem>City</MenuItem>
      </Select>
    </Stack>
  </FilterPoppper>
);
