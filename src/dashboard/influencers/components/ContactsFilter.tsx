import { useEffect } from "react";
import {
  Stack,
  Autocomplete,
  TextField,
  Divider,
  Button,
  Typography,
} from "@material-ui/core";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import { Box } from "@material-ui/system";
import isEmpty from "lodash/isEmpty";
import { useFieldArray } from "react-hook-form";
import { useAppSelector } from "../../../app/hooks";
import { seletWithContactsFilter } from "../selector";
import { FilterPoppper } from "./FilterPopper";
import { FilterComponentProps } from "./filterLabels";

export const ContactsFilter: React.FC<FilterComponentProps> = ({
  control,
  setValue,
  formState: { dirtyFields },
  onSubmit,
}) => {
  const values = useAppSelector(seletWithContactsFilter);

  const contactsArr = useFieldArray({
    control,
    name: "filter.with_contact",
  });

  useEffect(() => {
    setValue("filter.with_contact", values ?? [], {
      shouldDirty: true,
    });
  }, [values, setValue]);

  const filterData = [
    {
      name: "Has Email",
      value: "email",
    },
    {
      name: "Has Phone",
      value: "phone",
    },
    {
      name: "Has Snapchat",
      value: "snapchat",
    },
    {
      name: "Has Facebook",
      value: "facebook",
    },
    {
      name: "Has YouTube",
      value: "youtube",
    },
    {
      name: "Has TikTok",
      value: "tiktok",
    },
    {
      name: "Has Twitter",
      value: "twitter",
    },
    {
      name: "Has WhtsApp",
      value: "whatsapp",
    },
    {
      name: "Has Lineid",
      value: "lineid",
    },
    {
      name: "Has VK",
      value: "vk",
    },
    {
      name: "Has BBM",
      value: "bbm",
    },
    {
      name: "Has Kik",
      value: "kik",
    },
    {
      name: "Has WeChat",
      value: "wechat",
    },
    {
      name: "Has Viber",
      value: "viber",
    },
    {
      name: "Has Skype",
      value: "skype",
    },
    {
      name: "Has Tumblr",
      value: "tumblr",
    },
    {
      name: "Has TwitchTV",
      value: "twitchtv",
    },
    {
      name: "Has Kakao",
      value: "kakao",
    },
    {
      name: "Has Pinterest",
      value: "pinterest",
    },
    {
      name: "Has LinkTree",
      value: "linktree",
    },
    {
      name: "Has Sarahah",
      value: "sarahah",
    },
    {
      name: "Has Sayat",
      value: "sayat",
    },
    {
      name: "Has iTunes",
      value: "itunes",
    },
    {
      name: "Has Weibo",
      value: "weibo",
    },
  ];

  return (
    <FilterPoppper label="Contacts">
      <Stack
        spacing={2}
        sx={{
          width: 220,
        }}
      >
        <Typography variant="caption">Contacts</Typography>
        <Autocomplete
          fullWidth
          size="small"
          filterOptions={(x) => x}
          options={!filterData ? [] : filterData}
          autoHighlight
          getOptionLabel={(option: any) => option.name}
          onChange={(e, val) => {
            if (!isEmpty(val)) {
              contactsArr.append({
                type: val?.value ?? "",
                action: "should",
              } as never);
              onSubmit(e);
            }
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              label="Contacts"
              inputProps={{
                ...params.inputProps,
                autoComplete: "new-password", // disable autocomplete and autofill
              }}
            />
          )}
        />
        <Box>
          {contactsArr.fields?.map(
            (f: { type: string; action?: string }, idx) => {
              const filterName: any = `filter.with_contact.${idx}.type`;
              return (
                <Stack spacing={2} key={`${f.type}-label`}>
                  <Divider />
                  <Box
                    sx={{
                      flexDirection: "row",
                      alignItems: "center",
                      display: "flex",
                    }}
                  >
                    <span style={{ textTransform: "capitalize" }}>
                      {f.type}
                    </span>
                    <Button
                      variant="text"
                      size="small"
                      color="error"
                      sx={{
                        m: 0,
                      }}
                      onClick={() => contactsArr.remove(idx)}
                    >
                      <DeleteForeverIcon />
                    </Button>
                  </Box>
                </Stack>
              );
            }
          )}
        </Box>
        {/* <Select size="small" label="City">
        <MenuItem>City</MenuItem>
      </Select> */}
      </Stack>
    </FilterPoppper>
  );
};
