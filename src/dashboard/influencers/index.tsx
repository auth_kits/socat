import { Redirect, Switch, useRouteMatch } from "react-router-dom";
import { PrivateRoute } from "../../auth/PrivateRoute";
import { UserRole } from "../../auth/roles";
import { PdfView } from "./PdfView";
import { ProfilePage } from "./ProfilePage";
import { ReportPage } from "./ReportPage";
import { SearchPage } from "./SearchPage";

export const InfluencersPage = () => {
  const { path } = useRouteMatch();
  return (
    <Switch>
      <PrivateRoute path={`${path}`} exact>
        <ReportPage />
      </PrivateRoute>
      {/* always open overview page */}
      <PrivateRoute
        path={`${path}/:id`}
        exact
        render={({ match: { url } }) => <Redirect to={`${url}/overview`} />}
      />
      <PrivateRoute path={`${path}/:id/pdf`}>
        <PdfView />
      </PrivateRoute>
      <PrivateRoute path={`${path}/:id`}>
        <ProfilePage />
      </PrivateRoute>
    </Switch>
  );
};
