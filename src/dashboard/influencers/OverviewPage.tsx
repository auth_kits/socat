import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Grid,
  Link,
  List,
  ListItemAvatar,
  ListItemButton,
  ListItemText,
  Stack,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tabs,
  Tooltip,
  Typography,
} from "@material-ui/core";
import { styled } from "@material-ui/core/styles";
import EqualizerIcon from "@material-ui/icons/Equalizer";
import ForumIcon from "@material-ui/icons/Forum";
import GroupsIcon from "@material-ui/icons/Groups";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import PinDropIcon from "@material-ui/icons/PinDrop";
import ThumbsUpDownIcon from "@material-ui/icons/ThumbsUpDown";
import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { ApexOptions } from "apexcharts";
import { sentenceCase } from "change-case";
import { isEmpty, merge } from "lodash";
import React, { useLayoutEffect, useRef, useState } from "react";
import ReactApexChart from "react-apexcharts";
import * as wc from "wordcloud";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { BaseOptionChart } from "../../components/charts";
import { MHidden } from "../../components/MHidden";
import { SkeletonLoader } from "../../components/SkeletonLoader";
import { fShortenNumber } from "../../utils/formatNumber";
import { ELoadingStatus } from "./action";
import { NumberTile } from "./components/NumberTile";
import { ViewMoreComponent } from "./components/ViewMoreComponent";
import {
  selectInfluencerLocation,
  selectInfluencerOverview,
  selectInfluencerVerfied,
  selectStatus,
} from "./selector";

const ChipListItem = styled("li")(({ theme }) => ({
  margin: theme.spacing(0.5),
}));

export const OverviewPage: React.FC<{ pdf?: boolean }> = ({ pdf: isPdf }) => {
  const influencerLocation = useAppSelector(selectInfluencerLocation);
  const influencerVerified = useAppSelector(selectInfluencerVerfied);
  const graphColor = "#8bc34a";
  const tagRef = useRef(null);
  const dispatch = useAppDispatch();
  const data = useAppSelector(selectInfluencerOverview);
  const [growthTab, setGrowthTab] = useState("followers");
  const status = useAppSelector(selectStatus);
  const [interestModal, setInterestModal] = useState(false);
  const [brandModal, setBrandModal] = useState(false);
  const tooltipCloud = "Influencer relevant keywords cloud.";
  const tooltipAffinity =
    "We determine brand affinities by analyzing posts for caption texts, @mentions, #hashtags and location tags.";
  const tooltipInterests =
    "We determine interests by analyzing posts for caption texts, @mentions, #hashtags and location tags.";
  const tooltipNotable = "Top relevant influencers writing on similar topics.";
  const ttPaidPost =
    "Shows how disclosed sponsored posts perform compared to organic posts in terms of the average number of engagements they receive. For example, 10% means that you can overpay by 10 times for a post if you base your pricing on average engagements as, for example, instead of usual average 10,000 engagements you might get just 1,000 on your sponsored post. Please note that we take into account disclosure by Paid Partnerships feature and by 38 sponsored posts’ hashtags including #ad, #sponsored, #paid as well as others.";
  // const getGrowthData = useAppSelector(selectGrowthData);
  // [timestamp, noOfFollowers/noOfFollowing/likes]

  useLayoutEffect(() => {
    if (tagRef.current && data?.keywords) {
      wc(tagRef.current, {
        list: data?.keywords,
        weightFactor: 2,
        shrinkToFit: false,
        shape: "square",
        rotationSteps: 2,
      });
    }
    return () => {
      // stop the renderring
      wc?.stop();
    };
  }, [tagRef, data?.keywords]);

  const chartOptions: ApexOptions = merge(BaseOptionChart(), {
    chart: {
      offsetX: 0,
    },
    grid: {
      padding: {
        top: 5,
        right: 20,
        bottom: 5,
        left: 20,
      },
    },
    xaxis: {
      type: "datetime",
      title: {
        text: "Period",
      },
    },
    yaxis: {
      labels: {
        formatter: (seriesName) =>
          fShortenNumber(seriesName).toLocaleUpperCase(),
      },
      type: "numeric",
      title: {
        text: "Count",
      },
    },
    stroke: {
      width: 7,
    },
    colors: [graphColor],
    fill: {
      opacity: 5,
    },
    tooltip: {
      shared: true,
      intersect: false,
      y: {
        formatter: (y) => {
          if (typeof y !== "undefined") {
            return `${fShortenNumber(y.toFixed(0))}`;
          }
          return y;
        },
      },
    },
  });

  const isLoading = [ELoadingStatus.LOADING, ELoadingStatus.INITIAL].includes(
    status
  );

  return (
    <Grid container spacing={2} style={{ flexGrow: 1 }}>
      <Grid item xs={12} sm={6} md={3}>
        <SkeletonLoader variant="rectangular" loading={isLoading}>
          <NumberTile
            num="one"
            text="Followers"
            count={data?.followers}
            icon={
              <GroupsIcon
                sx={{
                  pr: 1,
                }}
                fontSize="large"
              />
            }
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={6} md={3}>
        <SkeletonLoader variant="rectangular" loading={isLoading}>
          <NumberTile
            num="two"
            text="Average Views"
            count={data?.views}
            icon={
              <VisibilityIcon
                sx={{
                  pr: 1,
                }}
                fontSize="large"
              />
            }
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={6} md={3}>
        <SkeletonLoader variant="rectangular" loading={isLoading}>
          <NumberTile
            num="three"
            text="Average Likes"
            count={data?.likes}
            icon={
              <ThumbsUpDownIcon
                fontSize="large"
                sx={{
                  pr: 1,
                }}
              />
            }
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={6} md={3}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <NumberTile
            text="Average Comments"
            num="four"
            count={data?.comments}
            icon={
              <ForumIcon
                fontSize="large"
                sx={{
                  pr: 1,
                }}
              />
            }
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={6} md={6}>
        <SkeletonLoader variant="rectangular" loading={isLoading}>
          <NumberTile
            num="five"
            text={
              <Box
                flexDirection="row"
                alignItems="center"
                sx={{ display: "flex" }}
                justifyContent="center"
              >
                <span>Paid Post Performance</span>
                <Tooltip placement="right" title={ttPaidPost}>
                  <HelpOutlineIcon />
                </Tooltip>
              </Box>
            }
            percentage
            count={data?.performance ?? 0}
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={6} md={6} className="breakAfterMe">
        <SkeletonLoader variant="rectangular" loading={isLoading}>
          <NumberTile
            num="six"
            text="Engagement Rate"
            percentage
            count={data?.engagementRate ?? 0}
            icon={
              <EqualizerIcon
                sx={{
                  pr: 1,
                }}
                fontSize="large"
              />
            }
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} />
      <Grid className="breakAfterMe" item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Box
            sx={{
              pt: 4,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
              Audience Growth
            </Typography>
          </Box>
        </SkeletonLoader>
      </Grid>
      <MHidden width="mdDown">
        <Grid item xs={12} sm={12} md={6}>
          <SkeletonLoader loading={isLoading} variant="text">
            <Box
              sx={{
                pt: 4,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
                Word Cloud
              </Typography>
              <Tooltip placement="right" title={tooltipCloud}>
                <HelpOutlineIcon />
              </Tooltip>
            </Box>
          </SkeletonLoader>
        </Grid>
      </MHidden>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card>
            <CardHeader
              title={
                <Tabs
                  sx={{
                    "& .MuiTab-root": {
                      color: "#78909c",
                    },
                    "& .Mui-selected": {
                      color: "#009688  !important",
                    },
                    "& .MuiTabs-indicator": {
                      backgroundColor: "#009688  !important",
                    },
                  }}
                  value={growthTab}
                  onChange={(event, newTab) => setGrowthTab(newTab)}
                  aria-label="Growth data"
                >
                  <Tab label="Followers" value="followers" />
                  <Tab label="Following" value="following" />
                  <Tab label="Likes" value="likes" />
                </Tabs>
              }
            />
            <Box sx={{ p: 3, pb: 1 }} dir="ltr">
              <ReactApexChart
                type="line"
                series={[
                  {
                    name: sentenceCase(growthTab),
                    data: data?.growthData[growthTab] ?? [],
                  },
                ]}
                options={chartOptions}
                height={220}
              />
            </Box>
          </Card>
        </SkeletonLoader>
      </Grid>
      <MHidden width="mdUp">
        <Grid item xs={12} sm={12} md={6}>
          <SkeletonLoader loading={isLoading} variant="text">
            <Box
              sx={{
                pt: 4,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
                Word Cloud
              </Typography>
              <Tooltip placement="right" title={tooltipCloud}>
                <HelpOutlineIcon />
              </Tooltip>
            </Box>
          </SkeletonLoader>
        </Grid>
      </MHidden>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card
            style={{ display: "flex", flexDirection: "column", height: "100%" }}
          >
            <CardContent style={{ flexGrow: 1 }}>
              {isEmpty(data?.keywords) ? (
                <Box>No Data for Influencer Keywords</Box>
              ) : (
                <div
                  ref={tagRef}
                  style={{ height: "100%", minHeight: "200px" }}
                />
              )}
            </CardContent>
          </Card>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12}>
        <SkeletonLoader loading={isLoading} variant="rectangular" />
      </Grid>
      <Grid item xs={12} sm={12}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Box
            sx={{
              pt: 4,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
              Influencers Lookalikes
            </Typography>
            <Tooltip placement="right" title={tooltipNotable}>
              <HelpOutlineIcon />
            </Tooltip>
          </Box>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Card
            sx={{
              height: "100%",
            }}
          >
            <CardContent>
              {isEmpty(data?.lookalikes) ? (
                <Box>No Data for Influencer Lookalikes</Box>
              ) : (
                <List
                  sx={{
                    width: "100%",
                  }}
                >
                  {data?.lookalikes?.slice(0, 3).map((n) => (
                    <ListItemButton
                      component="a"
                      alignItems="flex-start"
                      key={n.username}
                      href={n.url ?? "#"}
                      target="_blank"
                      rel="nofollow"
                    >
                      <ListItemAvatar>
                        <Avatar src={n.imageUrl} />
                      </ListItemAvatar>
                      <ListItemText
                        primary={n.fullname}
                        secondary={`${fShortenNumber(
                          n.followers
                        )} followers, ${fShortenNumber(
                          n.engagements
                        )} engagements`}
                      />
                    </ListItemButton>
                  ))}
                </List>
              )}
            </CardContent>
            <ViewMoreComponent
              label="Influencers Lookalikes"
              hideMore={(data?.lookalikes?.length ?? 0) <= 3}
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">User</TableCell>
                      <TableCell component="th">Engagements</TableCell>
                      <TableCell component="th">Followers</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.lookalikes?.map((d) => (
                      <TableRow key={d.username}>
                        <TableCell>
                          <Link
                            underline="none"
                            href={d.url ?? "#"}
                            target="_blank"
                            rel="nofollow"
                          >
                            <Stack
                              direction="row"
                              alignItems="center"
                              spacing={2}
                            >
                              <Avatar alt={d.fullname} src={d.imageUrl} />
                              <Box
                                flexDirection="column"
                                sx={{ display: "flex" }}
                              >
                                <Typography variant="h6" noWrap>
                                  {d.username}
                                </Typography>
                                <Typography variant="subtitle2" noWrap>
                                  {d.locationLabel}
                                </Typography>
                              </Box>
                            </Stack>
                          </Link>
                        </TableCell>
                        <TableCell>{fShortenNumber(d.engagements)}</TableCell>
                        <TableCell>{fShortenNumber(d.followers)}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
            />
          </Card>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12}>
        <SkeletonLoader loading={isLoading} variant="rectangular" />
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Box
            sx={{
              pt: 4,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
              Influencer Brand Affinity
            </Typography>
            <Tooltip placement="right" title={tooltipAffinity}>
              <HelpOutlineIcon />
            </Tooltip>
          </Box>
        </SkeletonLoader>
      </Grid>
      <MHidden width="mdDown">
        <Grid item xs={12} sm={12} md={6}>
          <SkeletonLoader loading={isLoading} variant="text">
            <Box
              sx={{
                pt: 4,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
                Influencer Interests
              </Typography>
              <Tooltip placement="right" title={tooltipInterests}>
                <HelpOutlineIcon />
              </Tooltip>
            </Box>
          </SkeletonLoader>
        </Grid>
      </MHidden>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card
            sx={{
              height: "100%",
            }}
          >
            <CardContent>
              {isEmpty(data?.affinity) ? (
                <Box>No Data for Influencer Brand Affinity</Box>
              ) : (
                <>
                  {/* <Grid container>
                    {(data?.affinity.slice(0, 9) ?? []).map((item) => (
                      <Grid item xs={4} key={item.label}>
                        <Box sx={{ px: 2.5, py: 1.5 }}>
                          <Grid container>
                            <Grid
                              item
                              sx={{
                                marginRight: "10px",
                                maxHeight: "30px",
                                maxWidth: "50px",
                                height: "100%",
                              }}
                            >
                              <img
                                src={item.iconUrl}
                                alt={item.label}
                                loading="lazy"
                                style={{ height: "100%" }}
                              />
                            </Grid>
                            <Grid item>
                              <Typography variant="body2">
                                {item.label}
                              </Typography>
                            </Grid>
                          </Grid>
                        </Box>
                      </Grid>
                    ))}
                  </Grid> */}

                  <ul style={{ display: "block" }}>
                    {(data?.affinity.slice(0, 9) ?? []).map((item) => (
                      <Box sx={{ px: 1, py: 0.5 }}>
                        <li
                          style={{
                            display: "flex",
                            alignItems: "center",
                            fontSize: 0,
                            whiteSpace: "nowrap",
                            position: "relative",
                            minHeight: "24px",
                            padding: "12px 0 0",
                          }}
                        >
                          <div
                            style={{
                              position: "relative",
                              display: "flex",
                              justifyContent: "center",
                              width: "37px",
                              height: "16px",
                              lineHeight: "24px",
                              verticalAlign: "middle",
                              marginRight: "14px",
                            }}
                          >
                            <img
                              style={{ maxWidth: "37px", maxHeight: "16px" }}
                              alt={item.label}
                              loading="lazy"
                              src={item.iconUrl}
                            />
                          </div>
                          <div
                            style={{
                              position: "relative",
                              display: "flex",
                              paddingRight: "8px",
                              width: "calc(100%-51px)",
                              whiteSpace: "nowrap",
                              textOverflow: "ellipsis",
                              overflow: "hidden",
                              fontSize: "16px",
                              lineHeight: "24px",
                              verticalAlign: "middle",
                            }}
                          >
                            <span> {item.label} </span>
                          </div>
                        </li>
                      </Box>
                    ))}
                  </ul>
                </>
              )}
            </CardContent>
            <ViewMoreComponent
              moreColor="primary"
              hideMore={(data?.affinity?.length ?? 0) <= 9}
              label="Influencer Brand Affinity"
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Brand</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.affinity?.map((item) => (
                      <TableRow key={item.label}>
                        <TableCell>
                          <img
                            src={item.iconUrl}
                            alt={item.label}
                            loading="lazy"
                          />
                        </TableCell>
                        <TableCell>{item.label}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
            />
          </Card>
        </SkeletonLoader>
      </Grid>
      <MHidden width="mdUp">
        <Grid item xs={12} sm={12}>
          <SkeletonLoader loading={isLoading} variant="text">
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                pt: 4,
                alignItems: "center",
              }}
            >
              <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
                Influencer Interests
              </Typography>
              <Tooltip placement="right" title="Influencer Interests">
                <HelpOutlineIcon />
              </Tooltip>
            </Box>
          </SkeletonLoader>
        </Grid>
      </MHidden>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Dialog
            open={interestModal}
            onClose={() => setInterestModal(false)}
            aria-labelledby="interest-modal-title"
            aria-describedby="interest-modal-description"
          >
            <DialogTitle id="interest-modal-title">Interests</DialogTitle>
            <DialogContent>
              <DialogContentText id="interest-modal-description">
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    flexWrap: "wrap",
                    listStyle: "none",
                    p: 0.5,
                    pl: 0,
                    m: 0,
                  }}
                  component="ul"
                >
                  {data?.interests?.map((i) => (
                    <ChipListItem key={i}>
                      <Chip variant="outlined" label={i} />{" "}
                    </ChipListItem>
                  ))}
                </Box>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={() => setInterestModal(false)} autoFocus>
                Close
              </Button>
            </DialogActions>
          </Dialog>
          <Card sx={{ height: "100%" }}>
            <CardContent>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  flexWrap: "wrap",
                  listStyle: "none",
                  p: 0.5,
                  pl: 0,
                  m: 0,
                }}
                component="ul"
              >
                {data?.interests?.slice(0, 8)?.map((i) => (
                  <ChipListItem key={i}>
                    <Chip variant="outlined" label={i} />{" "}
                  </ChipListItem>
                )) ?? <li>No Data for Influencer Interests</li>}
              </Box>
            </CardContent>
            <ViewMoreComponent
              moreColor="primary"
              hideMore={(data?.interests?.length ?? 0) <= 8}
              label="Influencer Brand Affinity"
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Interest</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.interests?.map((item) => (
                      <TableRow key={item}>
                        <TableCell>{item}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
            />
          </Card>
        </SkeletonLoader>
      </Grid>
    </Grid>
  );
};
