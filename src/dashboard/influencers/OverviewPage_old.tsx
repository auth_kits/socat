import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Grid,
  ImageList,
  ImageListItem,
  ImageListItemBar,
  Tab,
  Tabs,
  Tooltip,
  Typography,
} from "@material-ui/core";
import { styled } from "@material-ui/core/styles";
import ForumIcon from "@material-ui/icons/Forum";
import GroupsIcon from "@material-ui/icons/Groups";
import ThumbsUpDownIcon from "@material-ui/icons/ThumbsUpDown";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { ApexOptions } from "apexcharts";
import { merge } from "lodash";
import React, { useLayoutEffect, useRef, useState } from "react";
import ReactApexChart from "react-apexcharts";
import * as wc from "wordcloud";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { BaseOptionChart } from "../../components/charts";
import { SkeletonLoader } from "../../components/SkeletonLoader";
import { fShortenNumber } from "../../utils/formatNumber";
import { ELoadingStatus, getInfluencerOverview } from "./action";
import { NumberTile } from "./components/NumberTile";
import { PercentageComponent } from "./components/PercentageComponent";
import {
  selectInfluencerOverview,
  selectStatus,
  selectInfluencerDetail,
} from "./selector";

const ChipListItem = styled("li")(({ theme }) => ({
  margin: theme.spacing(0.5),
}));

export const OverviewPage: React.FC = () => {
  const influencer = useAppSelector(selectInfluencerDetail);
  const graphColor = "#009688";
  const tagRef = useRef(null);
  const dispatch = useAppDispatch();
  const data = useAppSelector(selectInfluencerOverview);
  const [growthTab, setGrowthTab] = useState("followers");
  const status = useAppSelector(selectStatus);
  const [interestModal, setInterestModal] = useState(false);
  const [brandModal, setBrandModal] = useState(false);
  // const getGrowthData = useAppSelector(selectGrowthData);
  // [timestamp, noOfFollowers/noOfFollowing/likes]

  useLayoutEffect(() => {
    if (tagRef.current && data?.keywords) {
      wc(tagRef.current, {
        list: data?.keywords,
        weightFactor: 2,
        shrinkToFit: true,
        shape: "square",
        rotationSteps: 2,
      });
    }
    return () => {
      // stop the renderring
      wc?.stop();
    };
  }, [tagRef, data?.keywords]);

  const chartOptions: ApexOptions = merge(BaseOptionChart(), {
    chart: {
      // background: '#1a237e',
      offsetX: 0,
    },
    theme: {
      mode: "dark",
      palette: "palette1",
      monochrome: {
        enabled: true,
        color: "#255aee",
        shadeTo: "light",
        shadeIntensity: 0.65,
      },
    },
    xaxis: {
      type: "datetime",
      title: {
        text: "Period",
      },
    },
    yaxis: {
      labels: {
        formatter: (seriesName) =>
          fShortenNumber(seriesName).toLocaleUpperCase(),
      },
      type: "numeric",
      title: {
        text: "Count",
      },
    },
    stroke: {
      width: 5,
    },
    colors: [graphColor],
    fill: {
      opacity: 5,
    },
    tooltip: {
      shared: true,
      intersect: false,
      y: {
        formatter: (y) => {
          if (typeof y !== "undefined") {
            return `${fShortenNumber(y.toFixed(0))}`;
          }
          return y;
        },
      },
    },
  });

  const isLoading = [ELoadingStatus.LOADING, ELoadingStatus.INITIAL].includes(
    status
  );

  return (
    <Grid container spacing={2} style={{ flexGrow: 1 }}>
      <Grid item xs={12} sm={6} md={3}>
        <SkeletonLoader variant="rectangular" loading={isLoading}>
          <NumberTile
            num="one"
            text="Followers"
            count={data?.followers}
            icon={
              <GroupsIcon
                sx={{
                  pr: 1,
                }}
                fontSize="large"
              />
            }
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={6} md={3}>
        <SkeletonLoader variant="rectangular" loading={isLoading}>
          <NumberTile
            num="two"
            text="Average Views"
            count={data?.views}
            icon={
              <VisibilityIcon
                sx={{
                  pr: 1,
                }}
                fontSize="large"
              />
            }
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={6} md={3}>
        <SkeletonLoader variant="rectangular" loading={isLoading}>
          <NumberTile
            num="three"
            text="Average Likes"
            count={data?.likes}
            icon={
              <ThumbsUpDownIcon
                fontSize="large"
                sx={{
                  pr: 1,
                }}
              />
            }
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={6} md={3}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <NumberTile
            text="Average Comments"
            num="four"
            count={data?.comments}
            icon={
              <ForumIcon
                fontSize="large"
                sx={{
                  pr: 1,
                }}
              />
            }
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={6} md={3}>
        <SkeletonLoader variant="rectangular" loading={isLoading}>
          <NumberTile
            num="five"
            text="Paid Post Performance"
            count={data?.performance ?? 0}
            icon={
              <GroupsIcon
                sx={{
                  pr: 1,
                }}
                fontSize="large"
              />
            }
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={6} md={3}>
        <SkeletonLoader variant="rectangular" loading={isLoading}>
          <NumberTile
            num="six"
            text="Engagement Rate"
            count={data?.engagementRate ?? 0}
            icon={
              <VisibilityIcon
                sx={{
                  pr: 1,
                }}
                fontSize="large"
              />
            }
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={6} md={3}>
        <SkeletonLoader variant="rectangular" loading={isLoading}>
          <NumberTile
            num="seven"
            text="Location"
            icon={
              <ThumbsUpDownIcon
                fontSize="large"
                sx={{
                  pr: 1,
                }}
              />
            }
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={6} md={3}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <NumberTile
            text="Verified"
            num="eight"
            icon={
              <ForumIcon
                fontSize="large"
                sx={{
                  pr: 1,
                }}
              />
            }
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Typography sx={{ pt: 2, pb: 0, mb: 0 }} variant="h5">
            Affinity
          </Typography>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Dialog
            open={brandModal}
            onClose={() => setBrandModal(false)}
            aria-labelledby="brand-modal-title"
            aria-describedby="brand-modal-description"
          >
            <DialogTitle id="brand-modal-title">Brands</DialogTitle>
            <DialogContent>
              <DialogContentText id="brand-modal-description">
                <ImageList cols={3} rowHeight={80} variant="standard">
                  {(data?.affinity ?? []).map((item) => (
                    <ImageListItem key={item.label}>
                      <img src={item.iconUrl} alt={item.label} loading="lazy" />
                      <ImageListItemBar position="below" title={item.label} />
                    </ImageListItem>
                  ))}
                </ImageList>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={() => setBrandModal(false)} autoFocus>
                Close
              </Button>
            </DialogActions>
          </Dialog>
          <Card
            sx={{
              height: "100%",
            }}
          >
            <CardHeader title="Brands" />
            <CardContent>
              <ImageList cols={3} rowHeight={80} variant="standard">
                {(data?.affinity ?? []).slice(0, 6).map((item) => (
                  <ImageListItem key={item.label}>
                    <img src={item.iconUrl} alt={item.label} loading="lazy" />
                    <ImageListItemBar position="below" title={item.label} />
                  </ImageListItem>
                ))}
              </ImageList>
            </CardContent>
            {(data?.affinity?.length ?? 0) > 6 ? (
              <CardActions
                sx={{
                  bottom: 0,
                  position: "absolute",
                  width: "100%",
                }}
              >
                <Box style={{ flexGrow: 1 }} />
                <Button
                  onClick={() => setBrandModal(true)}
                  size="small"
                  color="primary"
                >
                  View More
                </Button>
              </CardActions>
            ) : null}
          </Card>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Dialog
            open={interestModal}
            onClose={() => setInterestModal(false)}
            aria-labelledby="interest-modal-title"
            aria-describedby="interest-modal-description"
          >
            <DialogTitle id="interest-modal-title">Interests</DialogTitle>
            <DialogContent>
              <DialogContentText id="interest-modal-description">
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    flexWrap: "wrap",
                    listStyle: "none",
                    p: 0.5,
                    pl: 0,
                    m: 0,
                  }}
                  component="ul"
                >
                  {data?.interests?.map((i) => (
                    <ChipListItem key={i}>
                      <Chip variant="outlined" label={i} />{" "}
                    </ChipListItem>
                  ))}
                </Box>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={() => setInterestModal(false)} autoFocus>
                Close
              </Button>
            </DialogActions>
          </Dialog>
          <Card sx={{ height: "100%" }}>
            <CardHeader title="Interests" />
            <CardContent>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  flexWrap: "wrap",
                  listStyle: "none",
                  p: 0.5,
                  pl: 0,
                  m: 0,
                }}
                component="ul"
              >
                {data?.interests?.slice(0, 8)?.map((i) => (
                  <ChipListItem key={i}>
                    <Chip variant="outlined" label={i} />{" "}
                  </ChipListItem>
                ))}
              </Box>
            </CardContent>
            {(data?.interests?.length ?? 0) > 8 ? (
              <CardActions>
                <Box style={{ flexGrow: 1 }} />
                <Button
                  onClick={() => setInterestModal(true)}
                  size="small"
                  color="primary"
                >
                  View More
                </Button>
              </CardActions>
            ) : null}
          </Card>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Typography sx={{ pt: 2, pb: 0, mb: 0 }} variant="h5">
            Growth
          </Typography>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card>
            <CardHeader
              title={
                <Tabs
                  value={growthTab}
                  onChange={(event, newTab) => setGrowthTab(newTab)}
                  aria-label="Growth data"
                >
                  <Tab label="Followers" value="followers" />
                  <Tab label="Following" value="following" />
                  <Tab label="Likes" value="likes" />
                </Tabs>
              }
            />
            <Box sx={{ p: 3, pb: 1 }} dir="ltr">
              <ReactApexChart
                type="line"
                series={[
                  {
                    name: "Followers",
                    data: data?.growthData[growthTab],
                  },
                ]}
                options={chartOptions}
                height={220}
              />
            </Box>
          </Card>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card
            style={{ display: "flex", flexDirection: "column", height: "100%" }}
          >
            <CardHeader title="Word Cloud" />
            <CardContent style={{ flexGrow: 1 }}>
              <div ref={tagRef} style={{ height: "100%" }} />
            </CardContent>
          </Card>
        </SkeletonLoader>
      </Grid>
    </Grid>
  );
};
