import {
  Avatar,
  Button,
  Card,
  CardContent,
  CardHeader,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Link,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  CircularProgress,
} from "@material-ui/core";
import { styled } from "@material-ui/styles";
import { Box } from "@material-ui/system";
import { useCallback, useState, useEffect } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { Link as RouterLink, useHistory } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
// import { SkeletonLoader } from "../../components/SkeletonLoader";
import { fNumber, fShortenNumber } from "../../utils/formatNumber";
import {
  ELoadingStatus,
  getInfluencerSearchResults,
  getInfluencerExportResults,
} from "./action";
import { Filters } from "./components/Filters";
import {
  selectInfluencerSearchResults,
  selectInfluencerSearchStatus,
  selectInfluencerSearchTotal,
  selectInfluencerExportStatus,
} from "./selector";

const SearchResults = styled(Card)(({ theme }) => ({
  overflowX: "auto",
  "& .blurResult": {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    marginRight: -6,
    marginLeft: -6,
    backdropFilter: "blur(6px)",
    WebkitBackdropFilter: "blur(6px)", // Fix on Mobile
  },
  // backgroundColor: `${alpha(theme.palette.background.default, 0.72)}`,
}));

interface ProceedAction {
  actionType: "unlockSingle" | "unlockMultiple";
  actionData: any;
}
export const SearchPage: React.FC = (props) => {
  const searchResults = useAppSelector(selectInfluencerSearchResults);
  const searchStatus = useAppSelector(selectInfluencerSearchStatus);
  const totalCount = useAppSelector(selectInfluencerSearchTotal);
  const dispatch = useAppDispatch();
  const [credDialog, setCredDialog] = useState(false);
  const [proceedAction, setProceedAction] = useState<ProceedAction | null>(
    null
  );
  const [cancelled, setCancelled] = useState(false);
  const [isLoading, setLoading] = useState(false);

  const exportStatus = useAppSelector(selectInfluencerExportStatus);

  const history = useHistory();

  const proceedCallback = useCallback(() => {
    setCredDialog(false);
    switch (proceedAction?.actionType) {
      case "unlockSingle":
        if (proceedAction.actionData?.url) {
          history.push(proceedAction.actionData.url);
        }
        break;
      case "unlockMultiple":
        dispatch(
          getInfluencerSearchResults({
            query: "",
            offset: proceedAction.actionData?.offset,
          })
        );
        break;
      default:
        break;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [proceedAction, setCredDialog]);

  const handleDownloadReport = () => {
    setLoading(true);
    dispatch(getInfluencerExportResults());
  };

  useEffect(() => {
    if (
      exportStatus.status === ELoadingStatus.SUCCESS &&
      exportStatus.exportId !== ""
    ) {
      window
        .open(
          `${process.env.REACT_APP_BASE_BACKEND_URL}/export.php?export_id=${exportStatus.exportId}`,
          "_blank"
        )
        ?.focus();
      setLoading(false);
    }
  }, [exportStatus]);

  return (
    <Stack spacing={2}>
      <Filters />
      <Dialog open={credDialog} onClose={() => setCredDialog(false)}>
        <DialogTitle>Confirmation</DialogTitle>
        <DialogContent>
          <Grid container>
            <Grid item xs={12}>
              <Typography variant="body1">To See this report</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="body1" fontWeight="bold">
                Service Cost
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="body2">
                {proceedAction?.actionData?.credits} credits
              </Typography>
            </Grid>
            <Grid item xs={12} />
            <Grid item xs={6}>
              <Typography variant="body1" fontWeight="bold">
                Your Balance
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="body2">8555 credits</Typography>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              setCancelled(true);
              setCredDialog(false);
            }}
          >
            Cancel
          </Button>
          <Button onClick={() => proceedCallback()} variant="contained">
            Proceed
          </Button>
        </DialogActions>
      </Dialog>
      {searchStatus !== ELoadingStatus.INITIAL && (
        <SearchResults>
          {totalCount > 0 && (
            <CardHeader
              title={
                <Box sx={{ display: "flex" }} flexDirection="row">
                  <Box sx={{ flexGrow: 1 }}>
                    {fNumber(totalCount)} results found
                  </Box>
                  <Button
                    onClick={() => handleDownloadReport()}
                    disabled={isLoading}
                  >
                    {isLoading ? (
                      <>
                        Generating file. &nbsp;
                        <CircularProgress size={25} />
                      </>
                    ) : (
                      "Download Export Result"
                    )}
                  </Button>
                </Box>
              }
            />
          )}
          <CardContent>
            <InfiniteScroll
              scrollThreshold={0.9}
              dataLength={searchResults?.length ?? 0}
              next={(...args) => {
                setCancelled(false);
                setCredDialog(true);
                setProceedAction({
                  actionType: "unlockMultiple",
                  actionData: {
                    credits: "0.4",
                    offset: searchResults?.length ?? 0,
                  },
                });
              }}
              hasMore
              loader={<h4>{cancelled ? null : "Loading..."}</h4>}
              endMessage={
                <p style={{ textAlign: "center" }}>
                  <b>No more results</b>
                </p>
              }
            >
              <Table stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell component="th">&nbsp;</TableCell>
                    <TableCell component="th">&nbsp;</TableCell>
                    <TableCell component="th">Followers</TableCell>
                    <TableCell component="th">Engagements</TableCell>
                    <TableCell component="th">Engagement Rate</TableCell>

                    <TableCell component="th">
                      {/* View/Analyze Reports */}
                      &nbsp;
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {totalCount > 0
                    ? searchResults?.map((res, index) => (
                        <TableRow key={res.picture}>
                          <TableCell>{index + 1}</TableCell>
                          <TableCell>
                            <Link
                              underline="none"
                              href={
                                `https://www.instagram.com/${res.username}/` ??
                                "#"
                              }
                              target="_blank"
                              rel="nofollow"
                            >
                              <Stack
                                direction="row"
                                alignItems="center"
                                spacing={2}
                              >
                                <Avatar
                                  alt={res.displayName}
                                  src={res.picture}
                                />
                                <Box
                                  flexDirection="column"
                                  sx={{ display: "flex", position: "relative" }}
                                  className=""
                                >
                                  {!res.displayName && (
                                    <Box className="blurResult" />
                                  )}
                                  <Typography variant="h6" noWrap>
                                    @{res.username ?? "Socat User"}
                                  </Typography>
                                  <Typography variant="subtitle2" noWrap>
                                    {res.displayName ?? "check me out"}
                                  </Typography>
                                </Box>
                              </Stack>
                            </Link>
                          </TableCell>
                          <TableCell>{fShortenNumber(res.followers)}</TableCell>
                          <TableCell>
                            {fShortenNumber(res.engagements)}
                          </TableCell>
                          <TableCell>
                            {fShortenNumber(res.engagementRate * 100)}
                          </TableCell>
                          <TableCell>
                            {res.displayName ? (
                              <Button
                                // disabled={isEmpty(notAnalyzed)}
                                component={RouterLink}
                                to={`/reports/${res.username}`}
                                variant="contained"
                              >
                                Analyze
                              </Button>
                            ) : (
                              <Button
                                onClick={() => {
                                  setCredDialog(true);
                                  setProceedAction({
                                    actionType: "unlockSingle",
                                    actionData: {
                                      credits: "0.08",
                                      url: `/reports/${res.username}`,
                                    },
                                  });
                                }}
                                variant="outlined"
                              >
                                Unlock
                              </Button>
                            )}
                          </TableCell>
                        </TableRow>
                      ))
                    : searchStatus === ELoadingStatus.LOADING && (
                        <TableRow>
                          <TableCell rowSpan={5}>No results found</TableCell>
                        </TableRow>
                      )}
                </TableBody>
              </Table>
            </InfiniteScroll>
          </CardContent>
        </SearchResults>
      )}
    </Stack>
  );
};
