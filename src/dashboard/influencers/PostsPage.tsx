import {
  Box,
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Grid,
  IconButton,
  Typography,
} from "@material-ui/core";
import CommentBankIcon from "@material-ui/icons/CommentBank";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import AliceCarousel from "react-alice-carousel";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { useAppSelector } from "../../app/hooks";
import { SkeletonLoader } from "../../components/SkeletonLoader";
import { fShortenNumber } from "../../utils/formatNumber";
import { ELoadingStatus } from "./action";
import { selectInfluencerPosts, selectStatus } from "./selector";

const responsive = {
  0: {
    items: 1,
  },
  600: {
    items: 2,
  },
  900: {
    items: 3,
  },
  1200: {
    items: 4,
  },
};

export const PostsPage: React.FC<{ pdf?: boolean }> = () => {
  const status = useAppSelector(selectStatus);
  const data = useAppSelector(selectInfluencerPosts);
  const isLoading = [ELoadingStatus.LOADING, ELoadingStatus.INITIAL].includes(
    status
  );

  const renderPost = (post) => (
    <Box
      sx={{
        px: 1,
        pb: 3,
      }}
    >
      <Card
        sx={{
          height: "100%",
        }}
        key={post.postUrl}
      >
        <CardActionArea
          sx={{
            height: "100%",
          }}
          href={post.postUrl}
          rel="nofollow"
          target="_blank"
        >
          <CardMedia
            component="img"
            image={post.imageUrl}
            crossOrigin="anonymous"
          />
          <CardHeader subheader={post.created} />
          <CardContent>
            <Typography
              sx={{
                mb: 6,
              }}
              variant="body2"
              color="text.secondary"
            >
              {post.text}
            </Typography>
          </CardContent>
          <Box sx={{ flexGrow: 1 }} />
          <CardActions
            sx={{
              bottom: 0,
              position: "absolute",
              width: "100%",
            }}
          >
            <Button disabled fullWidth startIcon={<ThumbUpIcon />}>
              {fShortenNumber(post.likes).toLocaleUpperCase()}
            </Button>
            <Button disabled fullWidth startIcon={<CommentBankIcon />}>
              {fShortenNumber(post.comments).toLocaleUpperCase()}
            </Button>
          </CardActions>
        </CardActionArea>
      </Card>
    </Box>
  );

  const topPosts = data?.top?.map(renderPost);
  const sponsoredPosts = data?.sponsored?.map(renderPost);
  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Typography sx={{ pt: 2, pb: 0, mb: 0 }} variant="h5">
            Top Posts
          </Typography>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <AliceCarousel
            mouseTracking
            items={topPosts}
            disableDotsControls
            responsive={responsive}
            renderPrevButton={({ isDisabled }) =>
              isDisabled ? null : (
                <IconButton size="large" color="primary">
                  <ChevronLeftIcon />
                </IconButton>
              )
            }
            renderNextButton={({ isDisabled }) =>
              isDisabled ? null : (
                <IconButton size="large" color="primary">
                  <ChevronRightIcon />
                </IconButton>
              )
            }
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Typography sx={{ pt: 2, pb: 0, mb: 0 }} variant="h5">
            Sponsored Posts
          </Typography>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <AliceCarousel
            mouseTracking
            items={sponsoredPosts}
            responsive={responsive}
            disableDotsControls
            renderPrevButton={({ isDisabled }) =>
              isDisabled ? null : (
                <IconButton size="large" color="primary">
                  <ChevronLeftIcon />
                </IconButton>
              )
            }
            renderNextButton={({ isDisabled }) =>
              isDisabled ? null : (
                <IconButton size="large" color="primary">
                  <ChevronRightIcon />
                </IconButton>
              )
            }
          />
        </SkeletonLoader>
      </Grid>
    </Grid>
  );
};
