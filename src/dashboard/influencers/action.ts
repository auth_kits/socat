import { createAction } from "@reduxjs/toolkit";
import { FilterFormState } from "./components/filterLabels";

const PREFIX = "influcers";

interface InfluencerError {
  message: string;
}
export enum ELoadingStatus {
  INITIAL,
  LOADING,
  SUCCESS,
  ERROR,
}

export enum ProfileType {
  INSTAGRAM = "instagram",
  YOUTUBE = "youtube",
  TIKTOK = "tiktok",
}

export interface SocialProfileInfo {
  profileType: ProfileType;
  label: string;
  count: string;
  url: string;
}

export interface InfluencerDetail {
  id: string;
  verified: boolean;
  displayName: string;
  imageUrl: string;
  tagLine: string;
  location: {
    label: string;
    latitude?: number;
    longitude?: number;
  };
  socialProfiles: SocialProfileInfo[];
  contact: { formatted: string; type: string; value: string }[];
}

interface InfluencerPayload {
  influencerId: string;
}

export const setCurrentInfluencerId = createAction<InfluencerPayload>(
  `${PREFIX}/setCurrentInfluencerId`
);

export const getInfluencerProfile = createAction(
  `${PREFIX}/getInfluencerProfile`
);

export interface ProductBrand {
  label: string;
  iconUrl: string;
}

export interface OverviewData {
  followers: number;
  views: number;
  likes: number;
  comments: number;
  performance: number;
  engagementRate: number;
  affinity: ProductBrand[];
  interests: string[];
  growthData: {
    // [date, count]
    followers: [number, number][];
    // [date, count]
    following: [number, number][];
    // [date, count]
    likes: [number, number][];
  };
  // [keyword, freq]
  keywords: [string, number][];
  lookalikes: {
    engagements: number;
    followers: number;
    fullname: string;
    verified: boolean;
    imageUrl: string;
    score: number;
    url: string;
    username: string;
    locationLabel: string;
  }[];
}

export interface InfluencerEngagementData {
  // [min, max, total]
  rate: {
    x: string;
    y: number;
    user?: boolean;
    median?: boolean;
    description: string;
  }[];
  // [date, count]
  comments: {
    // date
    x: number;
    // count
    y: number;
    postUrl: string;
  }[];
  // [date, count]
  likes: {
    // date
    x: number;
    // count
    y: number;
    postUrl: string;
  }[];
  hashtags: { tag: string; weight: number }[];
  mentions: { tag: string; weight: number }[];
}

export interface AudienceData {
  x: string;
  y: number;
  count?: any;
}

export enum AudienceType {
  LIKES,
  FOLLOWERS,
}

export interface AudienceProfile {
  imageUrl: string;
  influencerId: string;
  displayName: string;
  url?: string;
  followers?: number;
  engagements: number;
  locationLabel?: string;
}

export interface InfluencerAudienceData {
  audience: AudienceType;
  country: AudienceData[];
  city: AudienceData[];
  cityGeo: AudienceData[];
  age: Record<string, AudienceData[]>;
  commonAge: AudienceData[];
  gender: {
    code: string;
    weight: number;
  }[];
  ethnicity: AudienceData[];
  language: AudienceData[];
  reachability: AudienceData[];
  interests: {
    label: string;
    count: number;
    weight: number;
    affinity: number;
  }[];
  brands: {
    label: string;
    iconUrl: string;
    weight: number;
    affinity: number;
  }[];
  lookalike: AudienceProfile[];
  notable: AudienceProfile[];
}

export interface InfluencerAudienceExtraData {
  followersCreds: number;
  notableFollowers: number;
  likerCreds: number;
  notableLikes: number;
  nonFollowerLikes: number;
}

export interface TopPostsData {
  imageUrl: string;
  text: string;
  created: string;
  likes: number;
  comments: number;
  postUrl: string;
}

export interface InfluencerProfileSuccessPayload {
  detail?: InfluencerDetail;
  overview?: OverviewData;
  engagement?: InfluencerEngagementData;
  audience?: Record<string, InfluencerAudienceData | any>;
  posts?: Record<string, TopPostsData[]>;
}

export const setAudienceType = createAction<{ audience: AudienceType }>(
  `${PREFIX}/setAudienceType`
);

export const getInfluencerProfileSuccess =
  createAction<InfluencerProfileSuccessPayload>(
    `${PREFIX}/getInfluencerProfileSuccess`
  );
export const getInfluencerProfileError = createAction<InfluencerError>(
  `${PREFIX}/getInfluencerProfileError`
);

// overview
export const getInfluencerOverview = createAction(
  `${PREFIX}/getInfluencerOverview`
);
interface OverviewSuccessPayload {
  data: OverviewData;
  influencerId: string;
}
export const getInfluencerOverviewSuccess =
  createAction<OverviewSuccessPayload>(
    `${PREFIX}/getInfluencerOverviewSuccess`
  );
export const getInfluencerOverviewError = createAction<InfluencerError>(
  `${PREFIX}/getInfluencerOverviewError`
);

// audience
export const getInfluencerAudience = createAction<InfluencerPayload>(
  `${PREFIX}/getInfluencerAudience`
);
export const getInfluencerAudienceSuccess = createAction<InfluencerPayload>(
  `${PREFIX}/getInfluencerAudienceSuccess`
);
export const getInfluencerAudienceError = createAction<InfluencerError>(
  `${PREFIX}/getInfluencerAudienceError`
);

// engagement
export const getInfluencerEngagement = createAction<InfluencerPayload>(
  `${PREFIX}/getInfluencerEngagement`
);
export const getInfluencerEngagementSuccess = createAction<InfluencerPayload>(
  `${PREFIX}/getInfluencerEngagementSuccess`
);
export const getInfluencerEngagementError = createAction<InfluencerError>(
  `${PREFIX}/getInfluencerEngagementError`
);

// posts
export const getInfluencerPosts = createAction<InfluencerPayload>(
  `${PREFIX}/getInfluencerPosts`
);
export const getInfluencerPostsSuccess = createAction<InfluencerPayload>(
  `${PREFIX}/getInfluencerPostsSuccess`
);
export const getInfluencerPostsError = createAction<InfluencerError>(
  `${PREFIX}/getInfluencerPostsError`
);

export enum EAudienceSource {
  ANY = "any",
  LIKES = "likers",
  FOLLOWES = "followers ",
}

export interface CodeFilter<T = string> {
  code: T;
  weight: string;
  label?: string;
}

export interface IdFilter<T = string> {
  id: T;
  weight: string;
  label?: string;
}

/* eslint-disable camelcase */
export interface BetweenFilter {
  left_number: string | "";
  right_number: string | "";
}

/* eslint-enable camelcase  */

export enum EGenderFilter {
  ANY = "",
  MALE = "MALE",
  FEMALE = "FEMALE",
  KNOWN = "KNOWN",
  UNKNOWN = "UNKNOWN",
}

export enum ERaceFilter {
  ANY = "",
  BLACK = "Black",
  ASIAN = "Asian",
  WHITE = "White",
  HISPANIC = "Hispanic",
}

export enum AccountTypeFilter {
  REGULAR = 1,
  BUSINESS = 2,
  CREATOR = 3,
}

export enum LastPostedFilter {
  ANY = "",
  ONE_MONTH = "30",
  THREE_MONTH = "90",
  SIX_MONTH = "180",
}

export enum IntervalFilter {
  ONE_MONTH = "i1month",
  TWO_MONTHS = "i2months",
  THREE_MONTHS = "i3months",
  FOUR_MONTHS = "i4months",
  FIVE_MONTHS = "i5months",
  SIX_MONTHS = "i6months",
}

export enum ContactTypeFilter {
  EMAIL = "email",
  PHONE = "phone",
  SNAPCHAT = "snapchat",
  FACEBOOK = "facebook",
  YOUTUBE = "youtube",
  TIKTOK = "tiktok",
  TWITTER = "twitter",
  TELEGRAM = "telegram",
  WHATSAPP = "whatsapp",
  LINKEDIN = "lineid",
  VK = "vk",
  BBM = "bbm",
  KIK = "kik",
  WECHAT = "wechat",
  VIBER = "viber",
  SKYPE = "skype",
  TUMBLR = "tumblr",
  TWITCHTV = "twitchtv",
  KAKAO = "kakao",
  PINTEREST = "pinterest",
  LINKTREE = "linktree",
  SARAHAH = "sarahah",
  SAYAT = "sayat",
  ITUNES = "itunes",
  WEIBO = "weibo",
}

// export enum BrandCategoryFilter {
//   TV_FILM = "1",
//   MUSIC = "3",
//   SHOPPING = "7",
//   BEVERAGES = "9",
//   PHOTOGRAPHY = "11",
//   CLOTHES = "13",
//   SPIRITS = "19",
//   SPORTS = "21",
//   COPUTERS = "25",
//   GAMING = "30",
//   ACTIVEWEAR = "33",
//   ART = "36",
//   TRAVEL = "43",
//   BUSINESS = "58",
//   BEAUTY = "80",
//   HEALTHCARE = "100",
//   JEWELLERY = "130",
//   FOOD = "139",
//   TOYS = "190",
//   FITNESS = "196",
//   WEDDING = "291",
//   SMOKE = "405",
//   PET = "673",
//   LIFESTYLE = "798",
//   LUXURY = "1500",
//   DECOR = "1560",
//   SOCIAL = "1708",
//   AUTOMOBILE = "1826",
// }

// delete below paths before sending to server
// audience_geo.label,geo.label, audience_brand.label,
// lang.label, audience_lang.label, brand_category.label,
// audience_brand_category.label,
// convert ads_brand to string[]
// convert brand to string[]
// convert brand_category to string[]
//
/* eslint-disable camelcase */
export interface FilterData {
  geo: IdFilter[];
  audience_geo: IdFilter[];
  lang?: { code: string; label?: string };
  audience_lang?: CodeFilter;
  brand: { id: string; label?: string }[] | string[];
  ads_brands?: { id: string; label?: string }[] | string[];
  // this is combination of topics and instagram
  relevance?: { value: string; weight: string };
  audience_relevance?: { value: string };
  audience_brand: IdFilter[];
  audience_age: CodeFilter[];
  age: BetweenFilter;
  audience_gender: CodeFilter<"" | "MALE" | "FEMALE">;
  gender: {
    code: EGenderFilter;
    weight: "";
  };
  audience_race: CodeFilter<ERaceFilter>;
  followers: BetweenFilter;
  engagements: BetweenFilter;

  engagement_rate: {
    value: number | null;
    operator: "gte";
  };
  last_posted: LastPostedFilter;
  acount_type?: AccountTypeFilter[];
  followers_growth?: {
    interval: IntervalFilter;
    value: number;
    operator: "gte" | "lte";
  };
  is_verified?: true;
  only?: "known";
  audience_credibility_class?: ["low", "normal", "high", "best"];
  is_hidden?: false;
  has_audience_data?: true;
  // Bio filter by text
  text?: string;
  with_contact?: { type: string; action: "should" }[];
  brand_category: { id: string; label?: string }[] | string[];
  audience_brand_category: IdFilter<string>[];
}

export const INITIAL_FILTER_VALUE: {
  audience_source: EAudienceSource;
  filter: FilterData;
} = {
  audience_source: EAudienceSource.ANY,
  filter: {
    audience_geo: [],
    geo: [],
    brand: [],
    audience_brand: [],
    audience_gender: {
      code: "",
      weight: "",
    },
    gender: {
      code: EGenderFilter.ANY,
      weight: "",
    },
    audience_age: [],
    age: {
      left_number: "",
      right_number: "",
    },
    followers: {
      left_number: "",
      right_number: "",
    },
    engagements: {
      left_number: "",
      right_number: "",
    },
    engagement_rate: {
      value: null,
      operator: "gte",
    },
    last_posted: LastPostedFilter.ANY,
    // relevance: { value: "", weight: "" },
    audience_relevance: { value: "" },
    // audience_brand: [],
    // brand: [],
    audience_brand_category: [],
    brand_category: [],
    audience_race: {
      code: ERaceFilter.ANY,
      weight: "",
    },
    with_contact: [],
  },
};

/* eslint-enable camelcase */

// This is for top search bar, not the search page actions, they are mentioned wih
// influencers like getInfluencerSearch
// this payload contains all the required filters for searching
interface GetSearchResultsPayload {
  query: string;
}
export const getSearchResults = createAction<GetSearchResultsPayload>(
  `${PREFIX}/getSearchResults`
);

export interface InfluencerReportData {
  followers: number;
  displayName: string;
  verified: boolean;
  picture: string;
  username: string;
}

interface GetSearchResulstsSuccessPayload {
  data: InfluencerReportData[];
}
export const getSearchResultsSuccess =
  createAction<GetSearchResulstsSuccessPayload>(
    `${PREFIX}/getSearchResultsSuccess`
  );

interface GetSearchErrorPayload {
  message: string;
}
export const getSearchResultsError = createAction<GetSearchErrorPayload>(
  `${PREFIX}/getsearchResultsError`
);

interface GetInfluencerSearchResultsPayload {
  query: string;
  offset: number;
}
export const getInfluencerSearchResults =
  createAction<GetInfluencerSearchResultsPayload>(
    `${PREFIX}/getInfluencerSearchResults`
  );

export const getInfluencerExportResults = createAction(
  `${PREFIX}/getInfluencerExportResults`
);

interface GetInfluencerExportResulstsSuccessPayload {
  exportId: string;
  status: number;
}

export const getInfluencerExportResultsSuccess =
  createAction<GetInfluencerExportResulstsSuccessPayload>(
    `${PREFIX}/getInfluencerExportResultsSuccess`
  );

export interface InfluencerSearchData {
  followers: number;
  displayName?: string;
  verified: boolean;
  picture: string;
  username?: string;
  engagements: string;
  accountType?: number;
  engagementRate: number;
  url: string;
}

interface GetInfluencerSearchResulstsSuccessPayload {
  data: InfluencerSearchData[];
  totalCount: number;
  append?: boolean;
}
export const getInfluencerSearchResultsSuccess =
  createAction<GetInfluencerSearchResulstsSuccessPayload>(
    `${PREFIX}/getInfluencerSearchResultsSuccess`
  );

export const getInfluencerSearchResultsError =
  createAction<GetSearchErrorPayload>(
    `${PREFIX}/getInfluencerSearchResultsError`
  );

interface GetInfluencerTagResultsPayload {
  query: string;
}
export const getInfluencerTagResults =
  createAction<GetInfluencerTagResultsPayload>(
    `${PREFIX}/getInfluencerTagResults`
  );

export interface TagSearchData {
  value: string;
  tag: string;
}
interface GetInfluencerTagResultsSuccessPayload {
  data: TagSearchData[];
}
export const getInfluencerTagResultsSuccess =
  createAction<GetInfluencerTagResultsSuccessPayload>(
    `${PREFIX}/getInfluencerTagResultsSuccess`
  );

export const getInfluencerTagResultsError = createAction<GetSearchErrorPayload>(
  `${PREFIX}/getInfluencerTagResultsError`
);
// filter section
export const setFilterData = createAction<FilterFormState>(
  `${PREFIX}/setFilterData`
);

export interface LocationsPayload {
  query: string;
}
export const getFilterLocations = createAction<LocationsPayload>(
  `${PREFIX}/getFilterLocations`
);

enum ELocationType {
  CITY = "city",
  COUNTRY = "country",
}
export interface LocationFilter {
  id: string;
  type: ELocationType;
  name: string;
  code: string;
  // parentId: string;
}

interface LocationsSuccessPayload {
  data: LocationFilter[];
}
export const getFilterLocationsSuccess = createAction<LocationsSuccessPayload>(
  `${PREFIX}/getFilterLocationsSuccess`
);

interface FilterSearchError {
  message: string;
}
export const setFilterSearchError = createAction<FilterSearchError>(
  `${PREFIX}/setFilterSearchError`
);

export interface FilterBrandsPayload {
  query: string;
}
export const getFilterBrands = createAction<FilterBrandsPayload>(
  `${PREFIX}/getFilterBrands`
);

export interface BrandFilter {
  id: string;
  name: string;
  count: number;
  deprecated: boolean;
  // parentId: string;
}

interface FilterBrandsSuccessPayload {
  data: BrandFilter[];
}
export const getFilterBrandsSuccess = createAction<FilterBrandsSuccessPayload>(
  `${PREFIX}/getFilterBrandSuccess`
);

export interface FilterLangsPayload {
  query: string;
}
export const getFilterLangs = createAction<FilterLangsPayload>(
  `${PREFIX}/getFilterLangs`
);

export interface LangFilter {
  code: string;
  name: string;
  count: number;
}
interface FilterLangsSuccessPayload {
  data: LangFilter[];
}
export const getFilterLangsSuccess = createAction<FilterLangsSuccessPayload>(
  `${PREFIX}/getFilterLangsSuccess`
);

export interface FilterInterestsPayload {
  query: string;
}
export const getFilterInterests = createAction<FilterInterestsPayload>(
  `${PREFIX}/getFilterInterests`
);

export interface IntrestFilter {
  id: string;
  name: string;
  count: number;
}
interface FilterInterestSuccessPayload {
  data: IntrestFilter[];
}
export const getFilterInterestSuccess =
  createAction<FilterInterestSuccessPayload>(
    `${PREFIX}/getFilterInterestSuccess`
  );

export interface FilterLookalikesPayload {
  query: string;
}
export const getFilterLookalikes = createAction<FilterLookalikesPayload>(
  `${PREFIX}/getFilterLookalikes`
);

export interface LookalikeFilter {
  id: string;
  name: string;
  value: string;
}
interface FilterLookalikesSuccessPayload {
  data: LookalikeFilter[];
}
export const getFilterLookalikesuccess =
  createAction<FilterLookalikesSuccessPayload>(
    `${PREFIX}/getFilterLookalikesuccess`
  );

interface GetReportsPayload {
  offset: number;
}
export const getReportsData = createAction<GetReportsPayload>(
  `${PREFIX}/getReportsData`
);

export interface ReportResultData {
  id: string;
  displayName: string;
  picture: string;
  username: string;
  followers: number;
  engagements: number;
  createAt: Date;
}

interface GetReportsSuccessPayload {
  data: ReportResultData[];
  append: boolean;
}
export const getReportsSuccess = createAction<GetReportsSuccessPayload>(
  `${PREFIX}/getReportsSuccess`
);

interface GetReportErrorPayload {
  message: string;
}
export const getReportsError = createAction<GetReportErrorPayload>(
  `${PREFIX}/getReportsError`
);
