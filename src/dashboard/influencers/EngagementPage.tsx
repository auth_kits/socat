import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Chip,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
} from "@material-ui/core";
import { styled } from "@material-ui/core/styles";
import AlternateEmailIcon from "@material-ui/icons/AlternateEmail";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { merge } from "lodash";
import ReactApexChart from "react-apexcharts";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { BaseOptionChart } from "../../components/charts";
import { MHidden } from "../../components/MHidden";
import { SkeletonLoader } from "../../components/SkeletonLoader";
import { fPercent, fShortenNumber } from "../../utils/formatNumber";
import { fDate, fDateTime2 } from "../../utils/formatTime";
import { ELoadingStatus } from "./action";
import { NumberTile } from "./components/NumberTile";
import { ViewMoreComponent } from "./components/ViewMoreComponent";
import {
  selectengagementRate,
  selectInfluencerDisplayName,
  selectInfluencerEngagement,
  selectStatus,
} from "./selector";

const ChipListItem = styled("li")(({ theme }) => ({
  margin: theme.spacing(0.5),
}));

// replace this to selectors when backend is ready and integration is pending
export const EngagementPage: React.FC<{ pdf?: boolean }> = () => {
  const erBarColor = "#ffa726";
  const dispatch = useAppDispatch();
  const data = useAppSelector(selectInfluencerEngagement);
  const displayName = useAppSelector(selectInfluencerDisplayName);
  const status = useAppSelector(selectStatus);
  const engagementRate = useAppSelector(selectengagementRate);

  const tooltipRate = "Average likes divided by Followers";
  const tooltipHash = "Most frequently used #hashtags on last 30 posts.";
  const tooltipAt = "Most frequently used #mentions on last 30 posts.";
  const tooltipSpread =
    "Shows number of likes and comments account got on their posts. Useful to see growth dynamics and variation of audience engagements with respect to time.";

  const barOptions = merge(BaseOptionChart(), {
    tooltip: {
      x: {
        show: true,
        formatter: (seriesName, opts) =>
          opts.w.config.series[opts.seriesIndex].data[opts.dataPointIndex]
            .description,
        title: {
          formatter: () => "",
        },
      },
      marker: { show: false },
      y: {
        formatter: (seriesName) => `${fShortenNumber(seriesName)} accounts`,
        title: {
          formatter: () => "",
        },
      },
    },
    dataLabels: {
      enabled: true,
      textAnchor: "start",
      offsetY: -12,
      formatter(value, { seriesIndex, w, dataPointIndex }) {
        if (
          w?.config?.series[seriesIndex]?.data[dataPointIndex]?.user &&
          w?.config?.series[seriesIndex]?.data[dataPointIndex]?.median
        ) {
          return `${displayName}`;
        }

        if (w?.config?.series[seriesIndex]?.data[dataPointIndex]?.user) {
          return displayName;
        }

        if (w?.config?.series[seriesIndex]?.data[dataPointIndex]?.median) {
          return "";
        }
        return "";
      },
      style: {
        colors: ["#000"],
      },
    },
    colors: [
      ({ value, seriesIndex, w, dataPointIndex }) => {
        if (w?.config?.series[seriesIndex]?.data[dataPointIndex]?.user) {
          // return "#f05d56";
          return "#00bcd4";
        }

        if (w?.config?.series[seriesIndex]?.data[dataPointIndex]?.median) {
          return "#ffd54f";
        }
        return "#ffecb3";
      },
    ],
    plotOptions: {
      bar: {
        horizontal: true,
        // vertical: false,
        barHeight: "60%",
        borderRadius: 2,
      },
    },
    yaxis: {
      type: "numeric",
      title: {
        text: "Engagement rate",
        style: {
          color: "#111",
        },
      },
      labels: {
        style: {
          colors: "#111",
        },
      },
    },
    xaxis: {
      type: "numeric",
      labels: {
        formatter: (seriesName) => fShortenNumber(seriesName),
        style: {
          colors: "#111",
        },
      },
      title: {
        text: "No of accounts",
        style: {
          color: "#111",
        },
      },
    },
  });

  const erBarOptions = merge(BaseOptionChart(), {
    tooltip: {
      x: {
        show: true,
        formatter: (seriesName, opts) => fDate(seriesName),
        title: {
          formatter: () => "",
        },
      },
      marker: { show: false },
      y: {
        formatter: (seriesName) => fShortenNumber(seriesName),
        title: {
          formatter: () => "",
        },
      },
    },

    stroke: {
      width: 5,
    },
    colors: [erBarColor],
    plotOptions: {
      bar: { vertical: true, barHeight: "28%", borderRadius: 2 },
    },
    yaxis: {
      type: "numeric",
      title: {
        text: "Count",
      },
      labels: {
        formatter: (val) => fShortenNumber(val).toLocaleUpperCase(),
      },
    },
    xaxis: {
      type: "datetime",
      title: {
        text: "Period",
      },
    },
  });

  const isLoading = [ELoadingStatus.LOADING, ELoadingStatus.INITIAL].includes(
    status
  );

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
              Engagement Rate
            </Typography>
            <Tooltip placement="right" title={tooltipRate}>
              <HelpOutlineIcon />
            </Tooltip>
          </Box>
        </SkeletonLoader>
      </Grid>

      <Grid item xs={12} sm={6} md={6}>
        <SkeletonLoader variant="rectangular" loading={isLoading}>
          <NumberTile
            num="six"
            text="Engagement Rate"
            percentage
            count={engagementRate ?? 0}
            icon={
              <VisibilityIcon
                sx={{
                  pr: 1,
                }}
                fontSize="large"
              />
            }
          />
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={6} md={12}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card>
            <Box sx={{ p: 3, pb: 1 }} dir="ltr">
              <ReactApexChart
                type="bar"
                series={[
                  {
                    name: "Engagement rate",
                    data: data?.rate ?? [],
                  },
                ]}
                options={barOptions}
                height={364}
              />
            </Box>
            <ViewMoreComponent
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Engagement Rate</TableCell>
                      <TableCell component="th">Influencers</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.rate?.map((d) => (
                      <TableRow key={d.description}>
                        <TableCell>{d.description}</TableCell>
                        <TableCell>{d.y}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
              label="Engagement"
            />
          </Card>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Box
            sx={{
              pt: 3,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
              Engagement Spread
            </Typography>
            <Tooltip placement="right" title={tooltipSpread}>
              <HelpOutlineIcon />
            </Tooltip>
          </Box>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card>
            <CardHeader title="Comments" sx={{ color: "#26a69a" }} />
            <Box sx={{ p: 3, pb: 1 }} dir="ltr">
              <ReactApexChart
                type="bar"
                series={[
                  {
                    data: data ? data.comments ?? [] : [],
                  },
                ]}
                options={erBarOptions}
                height={220}
              />
            </Box>
            <ViewMoreComponent
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Post Date</TableCell>
                      <TableCell component="th">Comments</TableCell>
                      <TableCell component="th">Post</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.comments?.map((d) => (
                      <TableRow key={d.x}>
                        <TableCell>{fDateTime2(d.x)}</TableCell>
                        <TableCell>{fShortenNumber(d.y)}</TableCell>
                        <TableCell>
                          <Button
                            size="small"
                            rel="nofollow"
                            target="_blank"
                            href={d.postUrl}
                            variant="text"
                            color="inherit"
                          >
                            View
                          </Button>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
              label="Engagement"
            />
          </Card>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card>
            <CardHeader title="Likes" sx={{ color: "#26a69a" }} />
            <Box sx={{ p: 3, pb: 1 }} dir="ltr">
              <ReactApexChart
                type="bar"
                series={[
                  {
                    data: data ? data.likes ?? [] : [],
                  },
                ]}
                options={erBarOptions}
                height={220}
              />
            </Box>
            <ViewMoreComponent
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Post Date</TableCell>
                      <TableCell component="th">Likes</TableCell>
                      <TableCell component="th">Post</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.comments?.map((d) => (
                      <TableRow key={d.x}>
                        <TableCell>{fDateTime2(d.x)}</TableCell>
                        <TableCell>{fShortenNumber(d.y)}</TableCell>
                        <TableCell>
                          <Button
                            size="small"
                            rel="nofollow"
                            target="_blank"
                            href={d.postUrl}
                            variant="text"
                            color="inherit"
                          >
                            View
                          </Button>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
              label="Engagement"
            />
          </Card>
        </SkeletonLoader>
      </Grid>

      <Grid item xs={12}>
        <SkeletonLoader loading={isLoading} variant="rectangular" />
      </Grid>

      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Box
            sx={{
              pt: 3,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
              Popular #
            </Typography>
            <Tooltip placement="right" title={tooltipHash}>
              <HelpOutlineIcon />
            </Tooltip>
          </Box>
        </SkeletonLoader>
      </Grid>
      <MHidden width="mdDown">
        <Grid item xs={12} sm={12} md={6}>
          <SkeletonLoader loading={isLoading} variant="text">
            <Box
              sx={{
                pt: 3,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
                Popular @
              </Typography>
              <Tooltip placement="right" title={tooltipAt}>
                <HelpOutlineIcon />
              </Tooltip>
            </Box>
          </SkeletonLoader>
        </Grid>
      </MHidden>

      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card
            sx={{
              height: "100%",
            }}
          >
            <CardContent>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  flexWrap: "wrap",
                  listStyle: "none",
                  p: 0.5,
                  pl: 0,
                  m: 0,
                }}
                component="ul"
              >
                {(data?.hashtags?.slice(0, 9) ?? []).map((item) => (
                  <ChipListItem key={item.tag}>
                    <Box
                      sx={{
                        m: 0.5,
                      }}
                    >
                      <Chip variant="outlined" label={`#${item.tag}`} />{" "}
                    </Box>
                  </ChipListItem>
                ))}
              </Box>
            </CardContent>
            <ViewMoreComponent
              label="Popular #"
              hideMore={(data?.hashtags?.length ?? 0) <= 9}
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Tag</TableCell>
                      <TableCell component="th">Weight</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.hashtags?.map((ht) => (
                      <TableRow key={ht.tag}>
                        <TableCell>#{ht.tag}</TableCell>
                        <TableCell>{fPercent(ht.weight)}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
            />
          </Card>
        </SkeletonLoader>
      </Grid>
      <MHidden width="mdUp">
        <Grid item xs={12} sm={12} md={6}>
          <SkeletonLoader loading={isLoading} variant="text">
            <Box
              sx={{
                pt: 3,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
                Popular @
              </Typography>
              <Tooltip placement="right" title={tooltipAt}>
                <HelpOutlineIcon />
              </Tooltip>
            </Box>
          </SkeletonLoader>
        </Grid>
      </MHidden>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card
            sx={{
              height: "100%",
            }}
          >
            <CardContent>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  flexWrap: "wrap",
                  listStyle: "none",
                  p: 0.5,
                  pl: 0,
                  m: 0,
                }}
                component="ul"
              >
                {data?.mentions?.slice(0, 8)?.map((i) => (
                  <ChipListItem key={i.tag}>
                    <Chip
                      variant="outlined"
                      icon={<AlternateEmailIcon />}
                      label={i.tag}
                    />{" "}
                  </ChipListItem>
                ))}
              </Box>
            </CardContent>
            <ViewMoreComponent
              label="Popular @"
              hideMore={(data?.mentions?.length ?? 0) <= 8}
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Tag</TableCell>
                      <TableCell component="th">Weight</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.mentions?.map((ht) => (
                      <TableRow key={ht.tag}>
                        <TableCell>@{ht.tag}</TableCell>
                        <TableCell>{fPercent(ht.weight)}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
            />
          </Card>
        </SkeletonLoader>
      </Grid>
    </Grid>
  );
};
