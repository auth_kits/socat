import {
  Avatar,
  Button,
  Card,
  CardContent,
  CardHeader,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import { Box } from "@material-ui/system";
import { set } from "date-fns";
import { Link as RouterLink } from "react-router-dom";
import { fShortenNumber } from "../../utils/formatNumber";
import { fDate } from "../../utils/formatTime";

export const CuratedPage: React.FC = () => {
  const data = [
    {
      displayName: "Some User name",
      picture: "https://www.google.com",
      username: "virat.kohli",
      followers: 12314132,
      engagements: 32132135,
      createdAt: set(new Date(), { hours: 10, minutes: 30 }),
      engagementRate: 1.7,
    },
  ];
  const totalCount = 1;
  return (
    <Card>
      <CardHeader title="Curated lists" />
      <CardContent>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              <TableCell component="th">&nbsp;</TableCell>
              <TableCell component="th">&nbsp;</TableCell>
              <TableCell component="th">Followers</TableCell>
              <TableCell component="th">Engagements</TableCell>
              <TableCell component="th">Engagement Rate</TableCell>
              <TableCell component="th">Created Date</TableCell>
              <TableCell component="th">
                {/* View/Analyze Reports */}
                &nbsp;
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {totalCount > 0 ? (
              data?.map((res, index) => (
                <TableRow key={res.picture}>
                  <TableCell>{index + 1}</TableCell>
                  <TableCell>
                    <Stack direction="row" alignItems="center" spacing={2}>
                      <Avatar alt={res.displayName} src={res.picture} />
                      <Box
                        flexDirection="column"
                        sx={{ display: "flex", position: "relative" }}
                        className=""
                      >
                        {!res.displayName && <Box className="blurResult" />}
                        <Typography variant="h6" noWrap>
                          {res.username ?? "Socat User"}
                        </Typography>
                        <Typography variant="subtitle2" noWrap>
                          {res.displayName ?? "check me out"}
                        </Typography>
                      </Box>
                    </Stack>
                  </TableCell>
                  <TableCell>{fShortenNumber(res.followers)}</TableCell>
                  <TableCell>{fShortenNumber(res.engagements)}</TableCell>
                  <TableCell>{fShortenNumber(res.engagementRate)}</TableCell>
                  <TableCell>{fDate(res.createdAt)}</TableCell>
                  <TableCell>
                    {res.displayName && (
                      <Button
                        // disabled={isEmpty(notAnalyzed)}
                        component={RouterLink}
                        to={`/reports/${res.username}`}
                        variant="contained"
                      >
                        View Report
                      </Button>
                    )}
                  </TableCell>
                </TableRow>
              ))
            ) : (
              <TableRow>
                <TableCell rowSpan={5}>No results found</TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
};
