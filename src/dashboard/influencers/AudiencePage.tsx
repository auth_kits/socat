import {
  Avatar,
  Box,
  Button,
  ButtonGroup,
  Card,
  CardContent,
  CardHeader,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Link,
  List,
  ListItemAvatar,
  ListItemButton,
  ListItemText,
  Stack,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tabs,
  Tooltip,
  Typography,
} from "@material-ui/core";
import { styled } from "@material-ui/core/styles";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import { ApexOptions } from "apexcharts";
import { sentenceCase } from "change-case";
import { isEmpty, merge } from "lodash";
import { useState } from "react";
import ReactApexChart from "react-apexcharts";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { BaseOptionChart } from "../../components/charts";
import { MHidden } from "../../components/MHidden";
import { SkeletonLoader } from "../../components/SkeletonLoader";
import { fNumber, fPercent, fShortenNumber } from "../../utils/formatNumber";
import { AudienceType, ELoadingStatus, setAudienceType } from "./action";
import { NumberTile } from "./components/NumberTile";
import { ViewMoreComponent } from "./components/ViewMoreComponent";
import WorldMap from "./components/WorldMap";
import {
  selectAudienceData,
  selectAudienceExtraData,
  selectAudienceTotalCount,
  selectStatus,
} from "./selector";

const ChipListItem = styled("li")(({ theme }) => ({
  margin: theme.spacing(0.5),
}));

const CHART_HEIGHT = 264;
const LEGEND_HEIGHT = 72;

const ChartWrapperStyle = styled(Box)(({ theme }) => ({
  // height: CHART_HEIGHT,
  marginBottom: theme.spacing(5),
  paddingBottom: theme.spacing(5),
  // "& .apexcharts-canvas svg": { height: CHART_HEIGHT - 40 },
  // "& .apexcharts-canvas svg,.apexcharts-canvas foreignObject": {
  //   overflow: "visible",
  // },
  "& .apexcharts-legend": {
    // height: LEGEND_HEIGHT,
    // alignContent: "center",
    // position: "relative !important",
    top: `calc(${CHART_HEIGHT - LEGEND_HEIGHT}px) !important`,
    bottom: 0,
  },
}));

export const AudiencePage: React.FC<{ pdf?: boolean }> = () => {
  const [locationType, setLocationType] = useState("country");
  const [genderTabs, setGenderTabs] = useState("split");
  const [basicOtherTabs, setBasicOtherTabs] = useState("ethnicity");
  const [interestModal, setInterestModal] = useState(false);
  const [notableModal, setNotableModal] = useState(false);
  const [lookalikeModal, setLookalikeModal] = useState(false);
  const [brandModal, setBrandModal] = useState(false);
  const dispatch = useAppDispatch();
  const data = useAppSelector(selectAudienceData);
  const status = useAppSelector(selectStatus);
  const extraData = useAppSelector(selectAudienceExtraData);

  const totalCount = useAppSelector(selectAudienceTotalCount);
  const ttFcredibility =
    "To establish the followers credibility score for historical followers we take into account factors such as an account’s avatar and bio description, number of posts, number of accounts followed vs following ratio. Influencers with a genuine audience will attain scores of 80 or above.";
  const ttFreachability =
    "Shows percentage of followers that follow more than 1500 accounts, between 1000-1500, 500-1000 and below 500. Accounts following more than 1.5k accounts will most likely not see the sponsored posts.";
  const ttFnotable =
    "Shows what percentage of followers are influencers. Notable Followers for a 0.99 quantile can largely differ from 1% to 20%.";
  const ttFgender =
    "We determine historical followers gender by analyzing the profile picture, name, text in the profile description (bio) and selfies in recent posts.";
  const ttFage =
    "We determine historical followers age and gender by analyzing the profile picture, text in the profile description (bio) and selfied in recent posts. Only available for the audience as an aggregated percentage.";
  const ttFlocation =
    "We determine historical followers' location (country, state, city) by analyzing location tags, language and caption of the recent posts and text in profile description (bio).";
  const ttFethnicity =
    "We determine historical followers ethnicity by analyzing the profile picture, text in the profile description (bio) and selfies in recent posts. Only available for the audience as an aggregated percentage. And, we determine historical followers language by analyzing caption of the recent posts.";
  const ttFlanguage =
    "We determine historical followers language by analyzing caption of the recent posts.";
  const ttFbrand =
    "We determine brand affinities by analyzing posts for caption texts, @mentions, #hashtags and location tags. ";
  const ttFinterests =
    "We determine interests by analyzing posts for caption texts, @mentions, #hashtags and location tags.";
  const ttFlookalikes =
    "Audience lookalikes help you find influencers that have similar audiences.";
  const ttFnotableFollowers =
    "Top followers of this user with more than 1,000 followers.";
  const ttLcredibility =
    "To establish the audience credibility score for active audience we take into account factors such as an account’s avatar and bio description, number of posts, number of accounts followed vs following ratio.Please note that the audience credibility is based on active engaged audience (likes) i.e. a high credibility score does not necessarily indicate “real followers” - the account may still have bought fake followers, the engagement rate could be very low but those few engagements are real and therefore the credibility score could still be high. Influencers with a genuine audience will attain scores of 80 or above.";
  const ttLnotable =
    "Shows what percentage of likers are influencers. Notable Likers for a 0.99 quantile can largely differ from 1% to 20%.";
  const locationOptions: ApexOptions = merge(BaseOptionChart(), {
    stroke: {
      width: 5,
    },
    colors: ["#26c6da", "#ffca28"],
    dataLabels: {
      enabled: true,
      textAnchor: "start",
      offsetY: -12,
      formatter(value) {
        return fPercent(value);
      },
      style: {
        colors: ["#f05d56"],
      },
    },
    plotOptions: {
      bar: {
        horizontal: true,
        barHeight: "15%",
        borderRadius: 2,
        padding: 10,

        dataLabels: {
          enabled: true,
          position: "right",
        },
      },
    },
    yaxis: {
      type: "category",
    },
    tooltip: {
      enabled: false,
      marker: { show: false },
      //   x: { show: true },
      //   y: {
      //     title: {
      //       formatter: (ser, opts) => "",
      //     },
      //     formatter: (ser) => fPercent(ser),
      //   },
    },
    xaxis: {
      type: "numeric",
      labels: {
        format: "asd",
      },
      title: {
        text: "Percent",
      },
    },
  } as Partial<ApexOptions>);

  const ageBarOptions = merge(BaseOptionChart(), {
    stroke: {
      width: 5,
    },
    yaxis: {
      type: "numeric",
      labels: {
        formatter: (val) => fPercent(val),
      },
    },
    plotOptions: {
      bar: {
        barHeight: "42%",
        horizontal: false,
        columnWidth: "42%",
        borderRadius: 4,
      },
    },
    xaxis: {
      type: "category",
    },
  } as Partial<ApexOptions>);

  const pieChartOptions = merge(BaseOptionChart(), {
    labels: data?.gender.map((v) => sentenceCase(v.code)),
    stroke: { colors: ["#fff"] },
    legend: {
      floating: true,
      horizontalAlign: "right",
      position: "bottom",
      labels: {
        colors: ["#3f51b5"],
        useSeriesColors: false,
      },
      offsetY: 20,
    },
    dataLabels: { enabled: true, dropShadow: { enabled: true } },
    tooltip: {
      fillSeriesColor: false,
      y: {
        formatter: (seriesName) => fPercent(seriesName),
      },
    },
    plotOptions: {
      pie: { customScale: 0.8, donut: { labels: { show: false } } },
    },
  });

  const rechabilityChartOptions = merge(BaseOptionChart(), {
    colors: ["#afb42b"],
    dataLabels: {
      enabled: true,
      textAnchor: "middle",
      formatter(value) {
        return fPercent(value);
      },
      style: {
        colors: ["#f05d56"],
      },
      offsetY: -25,
    },
    yaxis: {
      type: "numeric",
      labels: {
        formatter: (val) => fPercent(val),
      },
    },
    tooltip: {
      enabled: false,
      marker: { show: false },
      y: {
        title: {
          formatter: (ser) => ``,
        },
        formatter: (ser) => fPercent(ser),
      },
    },
    plotOptions: {
      bar: {
        barHeight: "28%",
        horizontal: false,
        columnWidth: "28%",
        borderRadius: 4,
        dataLabels: {
          enabled: true,
          position: "top",
        },
      },
    },
    xaxis: {
      type: "category",
      title: {
        text: "Followings",
      },
    },
  } as Partial<ApexOptions>);

  const isLoading = [ELoadingStatus.INITIAL, ELoadingStatus.LOADING].includes(
    status
  );

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
            }}
          >
            <Box sx={{ flexGrow: 1 }} />
            <ButtonGroup
              variant="outlined"
              size="large"
              aria-label="outlined button group"
            >
              <Button
                sx={{
                  px: 5,
                }}
                variant={
                  data?.audience === AudienceType.FOLLOWERS
                    ? "contained"
                    : "outlined"
                }
                onClick={() =>
                  dispatch(
                    setAudienceType({ audience: AudienceType.FOLLOWERS })
                  )
                }
              >
                Followers
              </Button>
              <Button
                sx={{
                  px: 5,
                }}
                variant={
                  data?.audience === AudienceType.LIKES
                    ? "contained"
                    : "outlined"
                }
                onClick={() =>
                  dispatch(setAudienceType({ audience: AudienceType.LIKES }))
                }
              >
                Likes
              </Button>
            </ButtonGroup>
            <Box sx={{ flexGrow: 1 }} />
          </Box>
        </SkeletonLoader>
      </Grid>
      {data?.audience === AudienceType.FOLLOWERS && (
        <Grid item xs={12} sm={12} md={6}>
          <SkeletonLoader variant="rectangular" loading={isLoading}>
            <NumberTile
              num="four"
              text={
                <Box
                  flexDirection="row"
                  alignItems="center"
                  sx={{ display: "flex" }}
                  justifyContent="center"
                >
                  <span>Follower&apos;s Credibility</span>
                  <Tooltip placement="right" title={ttFcredibility}>
                    <HelpOutlineIcon />
                  </Tooltip>
                </Box>
              }
              percentage
              count={extraData?.followersCreds}
            />
          </SkeletonLoader>
        </Grid>
      )}
      {data?.audience === AudienceType.FOLLOWERS && (
        <Grid item xs={12} sm={12} md={6}>
          <SkeletonLoader variant="rectangular" loading={isLoading}>
            <NumberTile
              num="five"
              text={
                <Box
                  flexDirection="row"
                  alignItems="center"
                  sx={{ display: "flex" }}
                  justifyContent="center"
                >
                  <span>Notable Followers</span>
                  <Tooltip placement="right" title={ttFnotable}>
                    <HelpOutlineIcon />
                  </Tooltip>
                </Box>
              }
              percentage
              count={extraData?.notableFollowers}
            />
          </SkeletonLoader>
        </Grid>
      )}
      {data?.audience === AudienceType.LIKES && (
        <>
          <Grid item xs={12} sm={12} md={4}>
            <SkeletonLoader variant="rectangular" loading={isLoading}>
              <NumberTile
                num="four"
                text={
                  <Box
                    flexDirection="row"
                    alignItems="center"
                    sx={{ display: "flex" }}
                    justifyContent="center"
                  >
                    <span>Liker Credibility</span>
                    <Tooltip placement="right" title={ttLcredibility}>
                      <HelpOutlineIcon />
                    </Tooltip>
                  </Box>
                }
                percentage
                count={extraData?.likerCreds}
              />
            </SkeletonLoader>
          </Grid>
          <Grid item xs={12} sm={12} md={4}>
            <SkeletonLoader variant="rectangular" loading={isLoading}>
              <NumberTile
                num="five"
                text={
                  <Box
                    flexDirection="row"
                    alignItems="center"
                    sx={{ display: "flex" }}
                    justifyContent="center"
                  >
                    <span>Notable Likes</span>
                    <Tooltip placement="right" title={ttLnotable}>
                      <HelpOutlineIcon />
                    </Tooltip>
                  </Box>
                }
                percentage
                count={extraData?.notableLikes}
              />
            </SkeletonLoader>
          </Grid>
          <Grid item xs={12} sm={12} md={4}>
            <SkeletonLoader variant="rectangular" loading={isLoading}>
              <NumberTile
                num="six"
                text={
                  <Box
                    flexDirection="row"
                    alignItems="center"
                    sx={{ display: "flex" }}
                    justifyContent="center"
                  >
                    <span>Likes not from Followers</span>
                  </Box>
                }
                percentage
                count={extraData?.nonFollowerLikes}
              />
            </SkeletonLoader>
          </Grid>
        </>
      )}
      <Grid item xs={12} sm={12} md={12}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
              Location
            </Typography>
            <Tooltip placement="right" title={ttFlocation}>
              <HelpOutlineIcon />
            </Tooltip>
          </Box>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card>
            <CardHeader
              title={
                <Tabs
                  sx={{
                    "& .MuiTab-root": {
                      color: "#78909c",
                    },
                    "& .Mui-selected": {
                      color: "#009688 !important",
                    },
                    "& .MuiTabs-indicator": {
                      backgroundColor: "#009688  !important",
                    },
                  }}
                  value={locationType}
                  onChange={(event, newTab) => setLocationType(newTab)}
                >
                  <Tab label="Country" value="country" />
                  <Tab label="City" value="city" />
                </Tabs>
              }
            />
            <Box sx={{ p: 3, pt: 0, pb: 1 }} dir="ltr">
              <ReactApexChart
                type="bar"
                height={CHART_HEIGHT}
                series={[
                  {
                    data: (data && data[locationType].slice(0, 5)) ?? [],
                  },
                ]}
                options={locationOptions}
              />
            </Box>
            <ViewMoreComponent
              label={`Location By ${sentenceCase(locationType)}`}
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Location</TableCell>
                      <TableCell component="th">
                        {data?.audience === AudienceType.FOLLOWERS &&
                          "Followers"}
                        {data?.audience === AudienceType.LIKES && "Likes"}
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {((data && data[locationType]) ?? [])?.map((d) => (
                      <TableRow key={d.x}>
                        <TableCell>{d.x}</TableCell>
                        <TableCell>
                          {fNumber(
                            parseInt(
                              `${
                                (totalCount[
                                  data?.audience ?? AudienceType.FOLLOWERS
                                ] *
                                  d.y) /
                                100
                              }`,
                              10
                            )
                          )}
                          /{fPercent(d.y)}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
              hideMore={!(data && data[locationType].length > 5)}
            />
          </Card>
        </SkeletonLoader>
      </Grid>
      {/* START */}
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card>
            <CardHeader title="Location by City" />
            <Box sx={{ p: 4.5, pt: 0, pb: 1 }} dir="ltr">
              <WorldMap cities={data && data["cityGeo"]} />
            </Box>
            <ViewMoreComponent
              label={`Location By ${sentenceCase("city")}`}
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Location</TableCell>
                      <TableCell component="th">
                        {data?.audience === AudienceType.FOLLOWERS &&
                          "Followers"}
                        {data?.audience === AudienceType.LIKES && "Likes"}
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {((data && data["city"]) ?? [])?.map((d) => (
                      <TableRow key={d.x}>
                        <TableCell>{d.x}</TableCell>
                        <TableCell>
                          {fNumber(
                            parseInt(
                              `${
                                (totalCount[
                                  data?.audience ?? AudienceType.FOLLOWERS
                                ] *
                                  d.y) /
                                100
                              }`,
                              10
                            )
                          )}
                          /{fPercent(d.y)}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
              hideMore={!(data && data["city"].length > 5)}
            />
          </Card>
        </SkeletonLoader>
      </Grid>
      {/* END */}
      <Grid item xs={12}>
        <SkeletonLoader loading={isLoading} variant="text" />
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Box
            sx={{
              py: 2,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
              Age &amp; Gender Split
            </Typography>
            <Tooltip placement="right" title={ttFage}>
              <HelpOutlineIcon />
            </Tooltip>
          </Box>
        </SkeletonLoader>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card>
            <CardHeader
              title={
                <Tabs
                  sx={{
                    "& .MuiTab-root": {
                      color: "#78909c",
                    },
                    "& .Mui-selected": {
                      color: "#009688  !important",
                    },
                    "& .MuiTabs-indicator": {
                      backgroundColor: "#009688  !important",
                    },
                  }}
                  value={genderTabs}
                  onChange={(event, newTab) => setGenderTabs(newTab)}
                  aria-label="Growth data"
                >
                  <Tab label="Gender Split" value="split" />
                  <Tab label="Age &amp; Gender" value="age" />
                </Tabs>
              }
            />
            {genderTabs === "age" ? (
              <>
                <Box sx={{ p: 3, pt: 0, pb: 1 }} dir="ltr">
                  <ReactApexChart
                    key="age-bar"
                    type="bar"
                    series={[
                      {
                        name: "Male",
                        data: data?.age?.male ?? [],
                      },
                      {
                        name: "Female",
                        data: data?.age?.female ?? [],
                      },
                    ]}
                    options={ageBarOptions}
                    height={CHART_HEIGHT}
                  />
                </Box>
                <ViewMoreComponent
                  label="Age Split"
                  renderTable={() => (
                    <Table stickyHeader>
                      <TableHead>
                        <TableRow>
                          <TableCell component="th">Age</TableCell>
                          <TableCell component="th">
                            {data?.audience === AudienceType.FOLLOWERS &&
                              "Followers"}
                            {data?.audience === AudienceType.LIKES && "Likes"}
                          </TableCell>
                          <TableCell>Male</TableCell>
                          <TableCell>Female</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {data?.commonAge?.map((d) => (
                          <TableRow key={d.x}>
                            <TableCell>{d.x}</TableCell>
                            <TableCell>
                              {fNumber(
                                parseInt(
                                  `${(totalCount[data?.audience] * d.y) / 100}`,
                                  10
                                )
                              )}
                            </TableCell>
                            <TableCell>
                              {fNumber(
                                parseInt(
                                  `${
                                    (totalCount[data?.audience] *
                                      d?.count?.male) /
                                    100
                                  }`,
                                  10
                                )
                              )}
                              /{fPercent(d?.count?.male)}
                            </TableCell>
                            <TableCell>
                              {fNumber(
                                parseInt(
                                  `${
                                    (totalCount[data?.audience] *
                                      d?.count?.female) /
                                    100
                                  }`,
                                  10
                                )
                              )}
                              /{fPercent(d?.count?.female)}
                            </TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  )}
                />
              </>
            ) : (
              <>
                <ChartWrapperStyle sx={{ p: 3, pt: 0, pb: 1 }} dir="ltr">
                  <ReactApexChart
                    type="donut"
                    series={data?.gender.map((v) => v?.weight * 100) ?? []}
                    options={pieChartOptions}
                    height={CHART_HEIGHT}
                  />
                </ChartWrapperStyle>
                <ViewMoreComponent
                  label="Gender Split"
                  renderTable={() => (
                    <Table stickyHeader>
                      <TableHead>
                        <TableRow>
                          <TableCell component="th">Gender</TableCell>
                          <TableCell component="th">
                            {data?.audience === AudienceType.FOLLOWERS &&
                              "Followers"}
                            {data?.audience === AudienceType.LIKES && "Likes"}
                          </TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {data?.gender?.map((d) => (
                          <TableRow key={d.code}>
                            <TableCell>{sentenceCase(d.code)}</TableCell>
                            <TableCell>
                              {fNumber(
                                parseInt(
                                  `${totalCount[data?.audience] * d?.weight}`,
                                  10
                                )
                              )}
                              /{fPercent(d?.weight)}
                            </TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  )}
                />
              </>
            )}
          </Card>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Box
            sx={{
              py: 2,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
              Ethnicity &amp; Language
            </Typography>
            <Tooltip placement="right" title={ttFethnicity}>
              <HelpOutlineIcon />
            </Tooltip>
          </Box>
        </SkeletonLoader>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card>
            <CardHeader
              title={
                <Tabs
                  sx={{
                    "& .MuiTab-root": {
                      color: "#78909c",
                    },
                    "& .Mui-selected": {
                      color: "#009688  !important",
                    },
                    "& .MuiTabs-indicator": {
                      backgroundColor: "#009688  !important",
                    },
                  }}
                  value={basicOtherTabs}
                  onChange={(event, newTab) => setBasicOtherTabs(newTab)}
                  aria-label="Growth data"
                >
                  <Tab label="Ethnicity" value="ethnicity" />
                  <Tab label="Language" value="language" />
                </Tabs>
              }
            />
            <Box sx={{ p: 3, pt: 0, pb: 1 }} dir="ltr">
              <ReactApexChart
                height={CHART_HEIGHT}
                key="ethnic-bar"
                type="bar"
                series={[
                  {
                    name: sentenceCase(basicOtherTabs),
                    data: (data && data[basicOtherTabs])?.slice(0, 5),
                  },
                ]}
                options={locationOptions}
              />
            </Box>
            <ViewMoreComponent
              // hideMore={((data && data[basicOtherTabs]) ?? []).length <= 5}
              label={sentenceCase(basicOtherTabs)}
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Name</TableCell>
                      <TableCell component="th">
                        {data?.audience === AudienceType.FOLLOWERS &&
                          "Followers"}
                        {data?.audience === AudienceType.LIKES && "Likes"}
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {((data && data[basicOtherTabs]) ?? [])?.map((d) => (
                      <TableRow key={d.x}>
                        <TableCell>{d.x}</TableCell>
                        <TableCell>
                          {fNumber(
                            parseInt(
                              `${
                                (totalCount[
                                  data?.audience ?? AudienceType.FOLLOWERS
                                ] *
                                  d?.y) /
                                100
                              }`,
                              10
                            )
                          )}{" "}
                          / {fPercent(d?.y)}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
            />
          </Card>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={12} md={12}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Box
            sx={{
              pt: 3,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
              Audience Reachability
            </Typography>
            <Tooltip placement="right" title={ttFreachability}>
              <HelpOutlineIcon />
            </Tooltip>
          </Box>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card>
            <Box sx={{ p: 3, pb: 1 }} dir="ltr">
              <ReactApexChart
                type="bar"
                series={[
                  {
                    data: data?.reachability,
                  },
                ]}
                options={rechabilityChartOptions}
                height={220}
              />
            </Box>
          </Card>
        </SkeletonLoader>
      </Grid>
      <Grid item xs={12}>
        <SkeletonLoader loading={isLoading} variant="text" />
      </Grid>
      {isEmpty(data?.lookalike) ? null : (
        <Grid item xs={12} sm={12} md={6}>
          <SkeletonLoader loading={isLoading} variant="text">
            <Box
              sx={{
                pt: 3,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
                Audience Lookalike
              </Typography>
              <Tooltip placement="right" title={ttFlookalikes}>
                <HelpOutlineIcon />
              </Tooltip>
            </Box>
          </SkeletonLoader>
        </Grid>
      )}
      <MHidden width="mdDown">
        <Grid item xs={12} sm={12} md={isEmpty(data?.lookalike) ? 12 : 6}>
          <SkeletonLoader loading={isLoading} variant="text">
            <Box
              sx={{
                pt: 3,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
                Notable{" "}
                {data?.audience === AudienceType.LIKES ? "Likers" : "Followers"}
              </Typography>
              <Tooltip placement="right" title={ttFnotableFollowers}>
                <HelpOutlineIcon />
              </Tooltip>
            </Box>
          </SkeletonLoader>
        </Grid>
      </MHidden>
      {isEmpty(data?.lookalike) ? null : (
        <Grid item xs={12} sm={12} md={6}>
          <SkeletonLoader loading={isLoading} variant="rectangular">
            <Dialog
              open={lookalikeModal}
              onClose={() => setLookalikeModal(false)}
              aria-labelledby="notable-modal-title"
              aria-describedby="notable-modal-description"
            >
              <DialogTitle id="notable-modal-title">
                Audience Lookalike
              </DialogTitle>
              <DialogContent>
                <List
                  sx={{
                    width: "100%",
                  }}
                >
                  {data?.lookalike?.map((n) => (
                    <ListItemButton
                      component="a"
                      alignItems="flex-start"
                      key={n.influencerId}
                      href={n.url ?? "#"}
                      target="_blank"
                      rel="nofollow"
                    >
                      <ListItemAvatar>
                        <Avatar src={n.imageUrl} />
                      </ListItemAvatar>
                      <ListItemText
                        primary={n.displayName}
                        secondary={`${fShortenNumber(
                          n.followers
                        )} followers, ${fShortenNumber(
                          n.engagements
                        )} engagements`}
                      />
                    </ListItemButton>
                  ))}
                </List>
              </DialogContent>
              <DialogActions>
                <Button onClick={() => setLookalikeModal(false)} autoFocus>
                  Close
                </Button>
              </DialogActions>
            </Dialog>
            <Card
              sx={{
                height: "100%",
              }}
            >
              <CardContent>
                <List
                  sx={{
                    width: "100%",
                  }}
                >
                  {data?.lookalike?.slice(0, 3).map((n) => (
                    <ListItemButton
                      component="a"
                      alignItems="flex-start"
                      key={n.influencerId}
                      href={n.url ?? "#"}
                      target="_blank"
                      rel="nofollow"
                    >
                      <ListItemAvatar>
                        <Avatar src={n.imageUrl} />
                      </ListItemAvatar>
                      <ListItemText
                        sx={{
                          "& .MuiListItemText-secondary": {
                            color: "#004d40",
                          },
                        }}
                        primary={n.displayName}
                        secondary={`${fShortenNumber(
                          n.followers
                        )} followers, ${fShortenNumber(
                          n.engagements
                        )} engagements`}
                      />
                    </ListItemButton>
                  ))}
                </List>
              </CardContent>
              <ViewMoreComponent
                label="Audience Lookalike"
                hideMore={(data?.lookalike?.length ?? 0) <= 3}
                renderTable={() => (
                  <Table stickyHeader>
                    <TableHead>
                      <TableRow>
                        <TableCell component="th">User</TableCell>
                        <TableCell component="th">Engagements</TableCell>
                        <TableCell component="th">Followers</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {data?.lookalike?.map((d) => (
                        <TableRow key={d.influencerId}>
                          <TableCell>
                            <Link
                              underline="none"
                              href={d.url ?? "#"}
                              target="_blank"
                              rel="nofollow"
                            >
                              <Stack
                                direction="row"
                                alignItems="center"
                                spacing={2}
                              >
                                <Avatar alt={d.displayName} src={d.imageUrl} />
                                <Box
                                  flexDirection="column"
                                  sx={{ display: "flex" }}
                                >
                                  <Typography variant="h6" noWrap>
                                    {d.displayName}
                                  </Typography>
                                  <Typography variant="subtitle2" noWrap>
                                    {d.locationLabel}
                                  </Typography>
                                </Box>
                              </Stack>
                            </Link>
                          </TableCell>
                          <TableCell>{fShortenNumber(d.engagements)}</TableCell>
                          <TableCell>{fShortenNumber(d.followers)}</TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                )}
              />
            </Card>
          </SkeletonLoader>
        </Grid>
      )}
      <MHidden width="mdUp">
        <Grid item xs={12} sm={12} md={isEmpty(data?.lookalike) ? 12 : 6}>
          <SkeletonLoader loading={isLoading} variant="text">
            <Box
              sx={{
                pt: 3,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
                Notable{" "}
                {data?.audience === AudienceType.LIKES ? "Likers" : "Followers"}
              </Typography>
              <Tooltip placement="right" title={ttFnotableFollowers}>
                <HelpOutlineIcon />
              </Tooltip>
            </Box>
          </SkeletonLoader>
        </Grid>
      </MHidden>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card
            sx={{
              height: "100%",
            }}
          >
            <CardContent>
              <List
                sx={{
                  width: "100%",
                }}
              >
                {data?.notable?.slice(0, 3).map((n) => (
                  <ListItemButton
                    component="a"
                    alignItems="flex-start"
                    key={n.influencerId}
                    href={n.url ?? "#"}
                    target="_blank"
                    rel="nofollow"
                  >
                    <ListItemAvatar>
                      <Avatar src={n.imageUrl} />
                    </ListItemAvatar>
                    <ListItemText
                      sx={{
                        "& .MuiListItemText-secondary": {
                          color: "#004d40",
                        },
                      }}
                      primary={n.displayName}
                      secondary={`${fShortenNumber(
                        n.followers
                      )} followers, ${fShortenNumber(
                        n.engagements
                      )} engagements`}
                    />
                  </ListItemButton>
                ))}
              </List>
            </CardContent>
            <ViewMoreComponent
              label={`Notable ${
                data?.audience === AudienceType.LIKES ? "Likers" : "Followers"
              }`}
              hideMore={(data?.notable?.length ?? 0) <= 3}
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">User</TableCell>
                      <TableCell component="th">Engagements</TableCell>
                      <TableCell component="th">Followers</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.notable?.map((d) => (
                      <TableRow key={d.influencerId}>
                        <TableCell>
                          <Link
                            underline="none"
                            href={d.url ?? "#"}
                            target="_blank"
                            rel="nofollow"
                          >
                            <Stack
                              direction="row"
                              alignItems="center"
                              spacing={2}
                            >
                              <Avatar alt={d.displayName} src={d.imageUrl} />
                              <Box
                                flexDirection="column"
                                sx={{ display: "flex" }}
                              >
                                <Typography variant="h6" noWrap>
                                  {d.displayName}
                                </Typography>
                                <Typography variant="subtitle2" noWrap>
                                  {d.locationLabel}
                                </Typography>
                              </Box>
                            </Stack>
                          </Link>
                        </TableCell>
                        <TableCell>{fShortenNumber(d.engagements)}</TableCell>
                        <TableCell>{fShortenNumber(d.followers)}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
            />
          </Card>
        </SkeletonLoader>
      </Grid>

      <Grid item xs={12}>
        <SkeletonLoader loading={isLoading} variant="text" />
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="text">
          <Box
            sx={{
              pt: 3,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
              Audience Brand Affinity
            </Typography>
            <Tooltip placement="right" title={ttFbrand}>
              <HelpOutlineIcon />
            </Tooltip>
          </Box>
        </SkeletonLoader>
      </Grid>
      <MHidden width="mdDown">
        <Grid item xs={12} sm={12} md={6}>
          <SkeletonLoader loading={isLoading} variant="text">
            <Box
              sx={{
                pt: 3,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
                Audience Interests
              </Typography>
              <Tooltip placement="right" title={ttFinterests}>
                <HelpOutlineIcon />
              </Tooltip>
            </Box>
          </SkeletonLoader>
        </Grid>
      </MHidden>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card
            sx={{
              height: "100%",
            }}
          >
            <CardContent>
              <Grid container>
                {(data?.brands.slice(0, 9) ?? []).map((item) => (
                  <Grid item xs={4} key={item.label}>
                    {/* <img src={item.iconUrl} alt={item.label} loading="lazy" /> */}
                    <Box sx={{ px: 2.5, py: 1.5 }}>{item.label}</Box>
                  </Grid>
                ))}
              </Grid>
            </CardContent>
            <ViewMoreComponent
              moreColor="primary"
              hideMore={(data?.brands?.length ?? 0) <= 9}
              label="Audience Brand Affinity"
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Brand</TableCell>
                      <TableCell component="th">
                        {data?.audience === AudienceType.FOLLOWERS &&
                          "Followers"}
                        {data?.audience === AudienceType.LIKES && "Likes"}
                      </TableCell>
                      <TableCell component="th">Affinity</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.brands?.map((item) => (
                      <TableRow key={item.label}>
                        <TableCell>{item.label}</TableCell>
                        <TableCell>
                          {fNumber(
                            parseInt(
                              `${
                                (totalCount[data?.audience] * item.weight) / 100
                              }`,
                              10
                            )
                          )}
                          /{fPercent(item.weight)}
                        </TableCell>
                        <TableCell>{item.affinity?.toFixed(2)}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
            />
          </Card>
        </SkeletonLoader>
      </Grid>
      <MHidden width="mdUp">
        <Grid item xs={12} sm={12} md={6}>
          <SkeletonLoader loading={isLoading} variant="text">
            <Box
              sx={{
                pt: 3,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography sx={{ pr: 1, pb: 0, mb: 0 }} variant="h5">
                Audience Interests
              </Typography>
              <Tooltip placement="right" title={ttFinterests}>
                <HelpOutlineIcon />
              </Tooltip>
            </Box>
          </SkeletonLoader>
        </Grid>
      </MHidden>
      <Grid item xs={12} sm={12} md={6}>
        <SkeletonLoader loading={isLoading} variant="rectangular">
          <Card
            sx={{
              height: "100%",
            }}
          >
            <CardContent>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  flexWrap: "wrap",
                  listStyle: "none",
                  p: 0.5,
                  pl: 0,
                  m: 0,
                }}
                component="ul"
              >
                {data?.interests?.slice(0, 8)?.map((i) => (
                  <ChipListItem key={i.label}>
                    <Chip variant="outlined" label={i.label} />{" "}
                  </ChipListItem>
                ))}
              </Box>
            </CardContent>
            <ViewMoreComponent
              moreColor="primary"
              hideMore={(data?.interests?.length ?? 0) <= 8}
              label="Audience Interests"
              renderTable={() => (
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell component="th">Interests</TableCell>
                      <TableCell component="th">
                        {data?.audience === AudienceType.FOLLOWERS &&
                          "Followers"}
                        {data?.audience === AudienceType.LIKES && "Likes"}
                      </TableCell>
                      <TableCell component="th">Affinity</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data?.interests?.map((item) => (
                      <TableRow key={item.label}>
                        <TableCell>{item.label}</TableCell>
                        <TableCell>
                          {fNumber(
                            parseInt(
                              `${
                                (totalCount[data?.audience] * item.weight) / 100
                              }`,
                              10
                            )
                          )}
                          /{fPercent(item.weight)}
                        </TableCell>
                        <TableCell>{item.affinity?.toFixed(2)}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              )}
            />
          </Card>
        </SkeletonLoader>
      </Grid>
    </Grid>
  );
};
