import { createSelector } from "@reduxjs/toolkit";
import { keyBy, merge, values } from "lodash";
import { RootState } from "../../app/store";
import { AudienceType, ELoadingStatus } from "./action";
import { adapter } from "./reducer";

const selectInfluencerState = ({ influencers }: RootState) => influencers;

export const selectStatus = createSelector(
  selectInfluencerState,
  (state) => state.status
);
const selectors = adapter.getSelectors();

export const selectCurrentId = createSelector(
  selectInfluencerState,
  (state) => state.currentId ?? ""
);

// common
export const selectInfluencerBasic = createSelector(
  selectInfluencerState,
  selectCurrentId,
  selectors.selectById
);

export const selectInfluencerDetail = createSelector(
  selectInfluencerState,
  ({ detail }) => detail
);

export const selectInfluencerDisplayName = createSelector(
  selectInfluencerDetail,
  (state) => state?.displayName
);

export const selectInfluencerLocation = createSelector(
  selectInfluencerDetail,
  (state) => state?.location?.label ?? "Not Mentioned"
);

export const selectInfluencerVerfied = createSelector(
  selectInfluencerDetail,
  (state) => state?.verified
);

export const selectBasicLoading = createSelector(
  selectInfluencerState,
  (state) => state?.status
);

// Overview
export const selectInfluencerOverview = createSelector(
  selectInfluencerState,
  (state) => state?.overview
);
export const selectOverviewStatus = createSelector(
  // selectInfluencerOverview,
  selectInfluencerState,
  (state) => state?.status
);

// Engagement
export const selectInfluencerEngagement = createSelector(
  selectInfluencerState,
  (state) => state?.engagement
);
export const selectEngagementStatus = createSelector(
  // selectInfluencerEngagement,
  selectInfluencerState,
  (state) => state?.status
);

// Audience
export const selectInfluencerAudience = createSelector(
  selectInfluencerState,
  (state) => state?.audience
);
export const selectAutdienceStatus = createSelector(
  // selectInfluencerAudience,
  selectInfluencerState,
  (state) => state?.status
);

// Posts
export const selectInfluencerPosts = createSelector(
  selectInfluencerState,
  (state) => state?.posts
);
export const selectPostsStatus = createSelector(
  // selectInfluencerPosts,
  selectInfluencerState,
  (state) => state?.status
);

// export const select
export const selectAudienceData = createSelector(
  selectInfluencerAudience,
  (state) =>
    state?.selected === AudienceType.LIKES ? state?.likes : state?.followers
);

export const selectAudienceExtraData = createSelector(
  selectInfluencerAudience,
  (state) => state?.extraData
);

export const selectAudienceTotalCount = createSelector(
  selectInfluencerOverview,
  (state) => ({
    [AudienceType.FOLLOWERS]: state?.followers ?? 0,
    [AudienceType.LIKES]: state?.likes ?? 0,
  })
);

export const selectAudienceGenderData = createSelector(
  selectInfluencerAudience,
  selectAudienceTotalCount,
  (state, totalCount) => {
    const followerData = keyBy(
      state?.followers?.gender.map((c) => ({
        code: c.code,
        followersWeight: c.weight,
        followers: ((c.weight * totalCount[AudienceType.FOLLOWERS]) / 100) | 0,
      })),
      "code"
    );

    const likersData = keyBy(
      state?.likes?.gender.map((c) => ({
        code: c.code,
        likes: ((c.weight * totalCount[AudienceType.LIKES]) / 100) | 0,
        likesWeight: c.weight,
      })),
      "code"
    );

    return values(merge(followerData, likersData));
  }
);

export const selectAudienceEthnicData = createSelector(
  selectInfluencerAudience,
  selectAudienceTotalCount,
  (state, totalCount) => {
    const followerData = keyBy(
      state?.followers?.ethnicity.map((c) => ({
        code: c.x,
        followersWeight: c.y,
        followers: ((c.y * totalCount[AudienceType.FOLLOWERS]) / 100) | 0,
      })),
      "code"
    );

    const likersData = keyBy(
      state?.likes?.ethnicity.map((c) => ({
        code: c.x,
        likes: ((c.y * totalCount[AudienceType.LIKES]) / 100) | 0,
        likesWeight: c.y,
      })),
      "code"
    );

    return values(merge(followerData, likersData));
  }
);

export const selectAudienceLanugageData = createSelector(
  selectInfluencerAudience,
  selectAudienceTotalCount,
  (state, totalCount) => {
    const followerData = keyBy(
      state?.followers?.language?.map((c) => ({
        code: c.x,
        followersWeight: c.y,
        followers: ((c.y * totalCount[AudienceType.FOLLOWERS]) / 100) | 0,
      })),
      "code"
    );

    const likersData = keyBy(
      state?.likes?.language?.map((c) => ({
        code: c.x,
        likes: ((c.y * totalCount[AudienceType.LIKES]) / 100) | 0,
        likesWeight: c.y,
      })),
      "code"
    );

    const data = values(merge(followerData, likersData));

    const midWay = (data.length / 2) | 0;
    const data1 = data.slice(0, midWay);
    const data2 = data.slice(midWay);
    return [data1, data2];
  }
);

export const selectAudienceBrandData = createSelector(
  selectInfluencerAudience,
  selectAudienceTotalCount,
  (state, totalCount) => {
    const followerData = keyBy(
      state?.followers?.brands?.map((c) => ({
        label: c.label,
        followersWeight: c.weight,
        followers: ((c.weight * totalCount[AudienceType.FOLLOWERS]) / 100) | 0,
        followerAffinity: c.affinity?.toFixed(2),
      })),
      "label"
    );

    const likersData = keyBy(
      state?.likes?.brands?.map((c) => ({
        label: c.label,
        likersWeight: c.weight,
        likers: ((c.weight * totalCount[AudienceType.LIKES]) / 100) | 0,
        likersAffinity: c.affinity?.toFixed(2),
      })),
      "label"
    );

    return values(merge(followerData, likersData));
  }
);

export const selectAudienceInterestData = createSelector(
  selectInfluencerAudience,
  selectAudienceTotalCount,
  (state, totalCount) => {
    const followerData = keyBy(
      state?.followers?.interests?.map((c) => ({
        label: c.label,
        followersWeight: c.weight,
        followers: ((c.weight * totalCount[AudienceType.FOLLOWERS]) / 100) | 0,
        followerAffinity: c.affinity?.toFixed(2),
      })),
      "label"
    );

    const likersData = keyBy(
      state?.likes?.interests?.map((c) => ({
        label: c.label,
        likersWeight: c.weight,
        likers: ((c.weight * totalCount[AudienceType.LIKES]) / 100) | 0,
        likersAffinity: c.affinity?.toFixed(2),
      })),
      "label"
    );

    return values(merge(followerData, likersData));
  }
);

export const selectengagementRate = createSelector(
  selectInfluencerOverview,
  (state) => state?.engagementRate ?? 0
);

export const selectAudienceSearchData = createSelector(
  selectInfluencerState,
  (state) => state.audienceSearch
);

export const selectInfluencerSearchData = createSelector(
  selectInfluencerState,
  (state) => state.influencerSearch
);

export const selectInfluencerTagData = createSelector(
  selectInfluencerState,
  (state) => state.tagSearch
);

export const selectReportSearchStatus = createSelector(
  selectAudienceSearchData,
  (state) => state?.status
);

export const selectSearchResults = createSelector(
  selectAudienceSearchData,
  (state) => state?.results ?? []
);

export const selectInfluencerTagStatus = createSelector(
  selectInfluencerTagData,
  (state) => state?.status
);
export const selectInfluencerTagResults = createSelector(
  selectInfluencerTagData,
  (state) => state?.results
);

export const selectInfluencerSearchStatus = createSelector(
  selectInfluencerSearchData,
  (state) => state?.status
);

export const selectInfluencerSearchResults = createSelector(
  selectInfluencerSearchData,
  (state) => state?.results
);
export const selectInfluencerSearchTotal = createSelector(
  selectInfluencerSearchData,
  (state) => state?.totalCount ?? 0
);

export const selectFilterData = createSelector(
  selectInfluencerSearchData,
  (state) => ({
    audience_source: state?.audienceSource,
    filter: state?.filter,
  })
);

export const selectAudienceSource = createSelector(
  selectFilterData,
  (state) => state.audience_source
);

export const selectAgeFilter = createSelector(selectFilterData, (state) => ({
  audienceAge: state?.filter?.audience_age,
  influencerAge: state?.filter?.age,
}));

export const selectGenderFilter = createSelector(selectFilterData, (state) => ({
  audienceGender: state?.filter?.audience_gender,
  influencerGender: state?.filter?.gender,
}));

export const selectEthnicityFilter = createSelector(
  selectFilterData,
  (state) => state?.filter?.audience_race
);

export const selectFollowers = createSelector(
  selectFilterData,
  (state) => state?.filter?.followers
);

export const selectFilterHelper = createSelector(
  selectInfluencerState,
  (state) => state?.filterRemoteData ?? {}
);

export const selectRemoteFilterStatus = createSelector(
  selectFilterHelper,
  (state) => state?.status
);

export const selectFilterLocations = createSelector(
  selectFilterHelper,
  (state) => state?.locations ?? []
);

export const selectFilterIntrests = createSelector(
  selectFilterHelper,
  (state) => state?.intrests ?? []
);

export const selectFilterBrands = createSelector(
  selectFilterHelper,
  (state) => state?.brands ?? []
);

export const selectAudienceLocation = createSelector(
  selectFilterData,
  (state) => state?.filter?.audience_geo
);

export const selectInfluencerGeo = createSelector(
  selectFilterData,
  (state) => state?.filter?.geo
);

export const selectBioFilter = createSelector(
  selectFilterData,
  (state) => state?.filter?.text
);

export const selectBrandsFilter = createSelector(selectFilterData, (state) => ({
  brands: state.filter.brand,
  audBrands: state.filter.audience_brand,
}));

export const selectLanguageFilter = createSelector(
  selectFilterData,
  (state) => ({
    langs: state.filter.lang,
    audLangs: state.filter.audience_lang,
  })
);

export const selectFilterLangs = createSelector(
  selectFilterHelper,
  (state) => state?.langs ?? []
);

export const selectFilterLookalikes = createSelector(
  selectFilterHelper,
  (state) => state?.lookalikes ?? []
);

export const selectIntrestsFilter = createSelector(
  selectFilterData,
  (state) => ({
    category: state.filter.brand_category,
    audCategory: state.filter.audience_brand_category,
  })
);

export const selectPartnershipFilter = createSelector(
  selectFilterData,
  (state) => state.filter.ads_brands
);

export const selectLookalikeFilter = createSelector(
  selectFilterData,
  (state) => ({
    audLa: state.filter.audience_relevance,
    la: state.filter.relevance,
  })
);

export const seletRelevanceFilter = createSelector(
  selectFilterData,
  (state) => state.filter.relevance
);

export const seletWithContactsFilter = createSelector(
  selectFilterData,
  (state) => state.filter.with_contact
);

export const selectReportState = createSelector(
  selectInfluencerState,
  (state) => state.reportsData
);

export const selectReportTableData = createSelector(
  selectReportState,
  (state) => state.data
);
export const selectReportTableStatus = createSelector(
  selectReportState,
  (state) => state.status
);
export const selectInfluencerExportStatus = createSelector(
  selectInfluencerState,
  (state) => state.influencerExport
);
