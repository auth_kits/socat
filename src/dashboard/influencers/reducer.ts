import {
  createEntityAdapter,
  createReducer,
  EntityState,
} from "@reduxjs/toolkit";
import * as types from "./action";

interface InfluencerOverview extends types.OverviewData {
  status: types.ELoadingStatus;
}

interface InfluencerEngagement extends types.InfluencerEngagementData {
  status: types.ELoadingStatus;
}

interface InfluencerAudience extends types.InfluencerAudienceData {
  status: types.ELoadingStatus;
}

interface InfluencerPostsData {
  status: types.ELoadingStatus;
  posts: string[];
}

export interface AudienceState {
  selected: types.AudienceType;
  followers?: types.InfluencerAudienceData;
  likes?: types.InfluencerAudienceData;
  extraData?: types.InfluencerAudienceExtraData;
}

interface SearchState {
  status: types.ELoadingStatus;
  query?: string;
}

export interface InfluencerSearchState extends SearchState {
  filter: types.FilterData;
  audienceSource?: types.EAudienceSource;
  results?: types.InfluencerSearchData[];
  totalCount?: number;
}

export interface AudienceSearchState extends SearchState {
  results?: types.InfluencerReportData[];
}

export interface TagSearchState extends SearchState {
  // Change this to tag data when you start receiving it
  results?: types.TagSearchData[];
}

export interface FilterRemoteData {
  // At a time only one filter  is visible so need for multiple statuses
  status: types.ELoadingStatus;
  locations: types.LocationFilter[];
  brands: types.BrandFilter[];
  langs: types.LangFilter[];
  intrests: types.IntrestFilter[];
  lookalikes: types.LookalikeFilter[];
}

export interface ReportDataState {
  status: types.ELoadingStatus;
  data: types.ReportResultData[];
}

export interface InfluencerExportState {
  exportId: string;
  status: types.ELoadingStatus;
}

export interface InfluencersUserState
  extends EntityState<types.InfluencerDetail> {
  errors: any;
  status: types.ELoadingStatus;
  filterRemoteData: FilterRemoteData;
  influencerSearch: InfluencerSearchState;
  reportsData: ReportDataState;
  tagSearch: TagSearchState;
  audienceSearch: AudienceSearchState;
  detail?: types.InfluencerDetail;
  overview?: types.OverviewData;
  engagement?: types.InfluencerEngagementData;
  audience?: AudienceState;
  posts?: {
    top: types.TopPostsData[];
    sponsored: types.TopPostsData[];
  };
  currentId?: string;
  influencerExport: InfluencerExportState;
}

export const adapter = createEntityAdapter<types.InfluencerDetail>({
  selectId: (influencer) => influencer.id,
  // sortComparer: (a, b) => a.email.localeCompare(b.email),
});

export default createReducer<InfluencersUserState>(
  adapter.getInitialState({
    errors: null,
    status: types.ELoadingStatus.INITIAL,
    audienceSearch: {
      status: types.ELoadingStatus.INITIAL,
    },
    tagSearch: {
      status: types.ELoadingStatus.INITIAL,
    },
    influencerSearch: {
      status: types.ELoadingStatus.INITIAL,
      filter: types.INITIAL_FILTER_VALUE.filter,
      audienceSource: types.INITIAL_FILTER_VALUE.audience_source,
    },
    filterRemoteData: {
      status: types.ELoadingStatus.INITIAL,
      locations: [],
      brands: [],
      langs: [],
      intrests: [],
      lookalikes: [],
    },
    reportsData: {
      status: types.ELoadingStatus.INITIAL,
      data: [],
    },
    influencerExport: {
      exportId: "",
      status: types.ELoadingStatus.INITIAL,
    },
  }),
  (builder) => {
    builder.addCase(
      types.setCurrentInfluencerId,
      (state, { payload: { influencerId } }) => {
        state.currentId = influencerId;
      }
    );
    builder.addCase(types.getInfluencerProfile, (state) => {
      state.status = types.ELoadingStatus.LOADING;
    });
    builder.addCase(
      types.getInfluencerProfileSuccess,
      (state, { payload: data }) => {
        state.detail = data?.detail;
        state.overview = data?.overview;
        state.engagement = data?.engagement;
        state.posts = {
          top: data?.posts?.top ?? [],
          sponsored: data?.posts?.sponsored ?? [],
        };
        state.audience = {
          selected: types.AudienceType.FOLLOWERS,
          followers: data?.audience?.followers,
          likes: data?.audience?.likes,
          extraData: data?.audience?.extraData,
        };
        state.status = types.ELoadingStatus.SUCCESS;
      }
    );
    builder.addCase(
      types.getInfluencerProfileError,
      (state, { payload: { message } }) => {
        state.errors = message;
        state.status = types.ELoadingStatus.ERROR;
      }
    );

    builder.addCase(types.setAudienceType, (state, { payload }) => {
      state.audience = {
        ...state.audience,
        selected: payload?.audience,
      };
    });

    builder.addCase(types.getSearchResults, (state, { payload: { query } }) => {
      state.audienceSearch.status = types.ELoadingStatus.LOADING;
      state.audienceSearch.query = query;
    });
    builder.addCase(
      types.getSearchResultsSuccess,
      (state, { payload: { data } }) => {
        state.audienceSearch.status = types.ELoadingStatus.SUCCESS;
        state.audienceSearch.results = data;
      }
    );
    builder.addCase(types.getSearchResultsError, (state, { payload }) => {
      state.influencerSearch.status = types.ELoadingStatus.ERROR;
      state.influencerSearch.results = [];
    });
    builder.addCase(types.setFilterData, (state, { payload }) => {
      state.influencerSearch.filter = payload.filter;
      state.influencerSearch.audienceSource = payload.audience_source;
    });

    builder.addCase(types.getInfluencerSearchResults, (state) => {
      state.influencerSearch.status = types.ELoadingStatus.LOADING;
    });

    builder.addCase(
      types.getInfluencerSearchResultsSuccess,
      (state, { payload: { data, totalCount, append } }) => {
        state.influencerSearch.status = types.ELoadingStatus.SUCCESS;
        if (append) {
          state.influencerSearch.results = [
            ...(state.influencerSearch.results ?? []),
            ...data,
          ];
        } else {
          state.influencerSearch.results = data;
        }
        state.influencerSearch.totalCount = totalCount;
      }
    );
    builder.addCase(types.getInfluencerSearchResultsError, (state) => {
      state.influencerSearch.status = types.ELoadingStatus.ERROR;
    });

    builder.addCase(types.getInfluencerTagResults, (state) => {
      state.tagSearch.status = types.ELoadingStatus.LOADING;
    });

    builder.addCase(types.getInfluencerTagResultsError, (state) => {
      state.tagSearch.status = types.ELoadingStatus.ERROR;
    });

    builder.addCase(
      types.getInfluencerTagResultsSuccess,
      (state, { payload: { data } }) => {
        state.tagSearch.status = types.ELoadingStatus.SUCCESS;
        state.tagSearch.results = data;
      }
    );

    builder.addCase(types.getFilterLocations, (state) => {
      state.filterRemoteData.status = types.ELoadingStatus.LOADING;
    });

    builder.addCase(
      types.getFilterLocationsSuccess,
      (state, { payload: { data } }) => {
        state.filterRemoteData.locations = data ?? [];
        state.filterRemoteData.status = types.ELoadingStatus.SUCCESS;
      }
    );

    builder.addCase(types.getFilterBrands, (state) => {
      state.filterRemoteData.status = types.ELoadingStatus.LOADING;
    });

    builder.addCase(
      types.getFilterBrandsSuccess,
      (state, { payload: { data } }) => {
        state.filterRemoteData.brands = data ?? [];
        state.filterRemoteData.status = types.ELoadingStatus.SUCCESS;
      }
    );

    builder.addCase(types.getFilterLangs, (state) => {
      state.filterRemoteData.status = types.ELoadingStatus.LOADING;
    });

    builder.addCase(
      types.getFilterLangsSuccess,
      (state, { payload: { data } }) => {
        state.filterRemoteData.langs = data ?? [];
        state.filterRemoteData.status = types.ELoadingStatus.SUCCESS;
      }
    );

    builder.addCase(types.getFilterInterests, (state) => {
      state.filterRemoteData.status = types.ELoadingStatus.LOADING;
    });

    builder.addCase(
      types.getFilterInterestSuccess,
      (state, { payload: { data } }) => {
        state.filterRemoteData.intrests = data ?? [];
        state.filterRemoteData.status = types.ELoadingStatus.SUCCESS;
      }
    );

    builder.addCase(types.getFilterLookalikes, (state) => {
      state.filterRemoteData.status = types.ELoadingStatus.LOADING;
    });

    builder.addCase(
      types.getFilterLookalikesuccess,
      (state, { payload: { data } }) => {
        state.filterRemoteData.lookalikes = data ?? [];
        state.filterRemoteData.status = types.ELoadingStatus.SUCCESS;
      }
    );

    builder.addCase(
      types.setFilterSearchError,
      (state, { payload: { message } }) => {
        state.filterRemoteData.status = types.ELoadingStatus.ERROR;
      }
    );

    builder.addCase(types.getReportsData, (state) => {
      state.reportsData.status = types.ELoadingStatus.LOADING;
    });

    builder.addCase(
      types.getReportsSuccess,
      (state, { payload: { data, append } }) => {
        state.reportsData.status = types.ELoadingStatus.SUCCESS;
        state.reportsData.data = append
          ? [...(state.reportsData.data ?? []), ...(data ?? [])]
          : data;
      }
    );

    builder.addCase(types.getReportsError, (state, { payload }) => {
      state.reportsData.status = types.ELoadingStatus.ERROR;
    });

    builder.addCase(
      types.getInfluencerExportResultsSuccess,
      (state, { payload: { exportId, status } }) => {
        state.influencerExport.exportId = exportId;
        state.influencerExport.status = status;
      }
    );
  }
);
