import {
  Avatar,
  Button,
  Card,
  CardContent,
  CardHeader,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Link,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import { Box } from "@material-ui/system";
import { useCallback, useLayoutEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { Link as RouterLink } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { fShortenNumber } from "../../utils/formatNumber";
import { fDate } from "../../utils/formatTime";
import { getReportsData } from "./action";
import { selectReportTableData, selectReportTableStatus } from "./selector";
import { Searchbar } from "../Searchbar";

interface ProceedAction {
  actionType: "unlockSingle" | "unlockMultiple";
  actionData: any;
}

export const ReportPage: React.FC = () => {
  const dispatch = useAppDispatch();

  const status = useAppSelector(selectReportTableStatus);
  const data = useAppSelector(selectReportTableData);
  const [credDialog, setCredDialog] = useState(false);
  const [proceedAction, setProceedAction] = useState<ProceedAction | null>(
    null
  );

  const proceedCallback = useCallback(() => {
    setCredDialog(false);
    dispatch(
      getReportsData({
        offset: proceedAction?.actionData?.offset ?? 0,
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [proceedAction, setCredDialog]);

  const [cancelled, setCancelled] = useState(false);
  useLayoutEffect(() => {
    dispatch(
      getReportsData({
        offset: 0,
      })
    );
  }, [dispatch]);

  console.log(credDialog);

  const totalCount = 1;
  return (
    <Box sx={{ p: 2 }}>
      <Searchbar />
      <Box sx={{ p: 1 }} />
      <Card>
        <CardHeader title="Reports" />
        <CardContent>
          <InfiniteScroll
            dataLength={data?.length ?? 0}
            next={(...args) => {
              setCancelled(false);
              setProceedAction({
                actionType: "unlockMultiple",
                actionData: {
                  credits: "0.6",
                  offset: data?.length ?? 0,
                },
              });
              dispatch(
                getReportsData({
                  offset: data?.length ?? 0,
                })
              );
            }}
            hasMore
            loader={<h4>{cancelled ? null : ""}</h4>}
            endMessage={
              <p style={{ textAlign: "center" }}>
                <b>No more results</b>
              </p>
            }
          >
            <Table stickyHeader>
              <TableHead>
                <TableRow>
                  <TableCell component="th">&nbsp;</TableCell>
                  <TableCell component="th">&nbsp;</TableCell>
                  <TableCell component="th">Followers</TableCell>
                  <TableCell component="th">Engagements</TableCell>
                  <TableCell component="th">Created At</TableCell>

                  <TableCell component="th">
                    {/* View/Analyze Reports */}
                    &nbsp;
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.length > 0 ? (
                  data?.map((res, index) => (
                    <TableRow key={res.id}>
                      <TableCell>{index + 1}</TableCell>
                      <TableCell>
                        <Link
                          underline="none"
                          href={
                            `https://www.instagram.com/${res.username}/` ?? "#"
                          }
                          target="_blank"
                          rel="nofollow"
                        >
                          <Stack
                            direction="row"
                            alignItems="center"
                            spacing={2}
                          >
                            <Avatar alt={res.displayName} src={res.picture} />
                            <Box
                              flexDirection="column"
                              sx={{ display: "flex", position: "relative" }}
                              className=""
                            >
                              {!res.displayName && (
                                <Box className="blurResult" />
                              )}
                              <Typography variant="h6" noWrap>
                                @{res.username ?? "Socat User"}
                              </Typography>
                              <Typography variant="subtitle2" noWrap>
                                {res.displayName ?? "check me out"}
                              </Typography>
                            </Box>
                          </Stack>
                        </Link>
                      </TableCell>
                      <TableCell>{fShortenNumber(res.followers)}</TableCell>
                      <TableCell>{fShortenNumber(res.engagements)}</TableCell>
                      <TableCell>{fDate(res.createAt)}</TableCell>
                      <TableCell>
                        {res.displayName && (
                          <Button
                            // disabled={isEmpty(notAnalyzed)}
                            component={RouterLink}
                            to={`/reports/${res.username}`}
                            variant="contained"
                          >
                            View report
                          </Button>
                        )}
                      </TableCell>
                    </TableRow>
                  ))
                ) : (
                  <TableRow>
                    <TableCell rowSpan={5}>No results found</TableCell>
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </InfiniteScroll>
        </CardContent>
      </Card>
    </Box>
  );
};
