import {
  Avatar,
  Box,
  Button,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  List,
  ListItemAvatar,
  ListItemButton,
  ListItemText,
  Skeleton,
  Tab,
  Tabs,
  Typography,
} from "@material-ui/core";
import ArtTrackIcon from "@material-ui/icons/ArtTrack";
import ConnectWithoutContactIcon from "@material-ui/icons/ConnectWithoutContact";
import DataUsageIcon from "@material-ui/icons/DataUsage";
import EqualizerIcon from "@material-ui/icons/Equalizer";
import GroupsIcon from "@material-ui/icons/Groups";
import MoneyIcon from "@material-ui/icons/Money";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import EmailIcon from "@material-ui/icons/Email";
import TwitterIcon from "@material-ui/icons/Twitter";
import YoutubeIcon from "@material-ui/icons/YouTube";
import ContactPhoneIcon from "@material-ui/icons/ContactPhone";
import RoomIcon from "@material-ui/icons/Room";
import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import { LoadingButton, TabContext, TabPanel } from "@material-ui/lab";
import { styled } from "@material-ui/styles";
import { useLayoutEffect, useState } from "react";
import { Link as RouterLink, useParams, useRouteMatch } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { PrintView } from "../../components/PrintHide";
import { WipDialog } from "../../components/WipDialog";
import { ELoadingStatus, setCurrentInfluencerId } from "./action";
import { AudiencePage } from "./AudiencePage";
import { EngagementPage } from "./EngagementPage";
import { OverviewPage } from "./OverviewPage";
import { PostsPage } from "./PostsPage";
import { selectBasicLoading, selectInfluencerDetail } from "./selector";

const Sidebar = styled(Box)(({ theme }) => ({
  width: 200,
}));

const styles = (theme) => ({
  listItemText: {
    fontSize: "0.7em",
  },
});

export const ProfilePage: React.FC<{ profileId?: string; pdf?: boolean }> = ({
  pdf,
  profileId,
}) => {
  const { path, url } = useRouteMatch();

  const params = useParams<{ id: string }>();
  const routeMatch = useRouteMatch([
    `${path}/overview`,
    `${path}/engagement`,
    `${path}/audience`,
    `${path}/posts`,
  ]);
  const [internalTab, setInternalTab] = useState(`${path}/overview`);
  const dispatch = useAppDispatch();
  const reportPath = profileId
    ? `/reports/${profileId}/pdf`
    : `/reports/${params?.id}/pdf`;
  const [wipAlert, setWipAlert] = useState(false);
  const [contactAlert, setContactAlert] = useState(false);
  useLayoutEffect(() => {
    if (profileId) {
      dispatch(
        setCurrentInfluencerId({
          influencerId: profileId,
        })
      );
    } else if (params?.id) {
      dispatch(
        setCurrentInfluencerId({
          influencerId: params?.id,
        })
      );
    }
  }, [dispatch, profileId, params?.id]);

  const networkStatus = useAppSelector(selectBasicLoading);

  const isLoading = [ELoadingStatus.INITIAL, ELoadingStatus.LOADING].includes(
    networkStatus
  );
  const influencer = useAppSelector(selectInfluencerDetail);
  const currentTab: string = profileId ? internalTab : routeMatch?.path ?? "";
  return (
    <Box>
      <Container maxWidth={false}>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={12} md={2} lg={2}>
            <Sidebar
              sx={{
                height: "100%",
                width: "100% !important",
                display: "flex",
                position: "sticky",
                flexDirection: "column",
                margin: "0 auto",
                textAlign: "center",
                alignItems: "center",
                px: 0,
                py: 3,
              }}
            >
              {isLoading ? (
                <>
                  <Skeleton width="160px" variant="circular" height="160px" />
                  <Skeleton variant="text" sx={{ px: 1, py: 2 }} width="80%" />
                  <Skeleton variant="text" sx={{ px: 1, py: 2 }} width="80%" />
                  <Skeleton
                    variant="rectangular"
                    sx={{ px: 1, py: 4 }}
                    width="80%"
                  />
                </>
              ) : (
                <>
                  <div
                    style={{
                      borderRadius: "50%",
                      backgroundImage: `url(${influencer?.imageUrl})`,
                      backgroundSize: "cover",
                      width: "160px",
                      height: "160px",
                      borderWidth: 2,
                      resize: "both",
                    }}
                  />
                  <Typography
                    variant="h5"
                    sx={{
                      pt: 2,
                      alignItems: "center",
                      display: "flex",
                    }}
                  >
                    {influencer?.displayName}
                    &nbsp;
                    {influencer?.verified && <VerifiedUserIcon color="info" />}
                  </Typography>
                  <Typography
                    variant="body1"
                    sx={{
                      fontSize: {
                        md: 14,
                      },
                      color: "#455a64",
                    }}
                  >
                    {influencer?.tagLine}
                  </Typography>
                  {influencer?.location?.label && (
                    <Typography
                      variant="body1"
                      sx={{
                        fontSize: {
                          md: 14,
                        },
                        color: "#455a64",
                        display: "flex",
                        alignItems: "center",
                        py: 0,
                        px: 0,
                      }}
                    >
                      <RoomIcon color="primary" />
                      {influencer?.location?.label}
                    </Typography>
                  )}

                  <Box sx={{ py: 1, px: 1 }} />
                  <WipDialog
                    open={wipAlert}
                    onClose={() => setWipAlert(false)}
                  />

                  <Dialog
                    open={contactAlert}
                    onClose={() => setContactAlert(false)}
                    aria-labelledby="contact-modal-title"
                    aria-describedby="contact-modal-description"
                  >
                    <DialogTitle id="notable-modal-title">Contacts</DialogTitle>
                    <DialogContent>
                      <List
                        sx={{
                          width: "100%",
                        }}
                      >
                        {influencer?.contact?.map((c) => (
                          <ListItemButton
                            sx={{
                              px: {
                                md: 1,
                              },
                            }}
                            component="a"
                            alignItems="flex-start"
                            key={c.value}
                            href={c.formatted ?? "#"}
                            target="_blank"
                            rel="nofollow"
                          >
                            <ListItemAvatar>
                              {c.type === "facebook" && <FacebookIcon />}
                              {c.type === "instagram" && <InstagramIcon />}
                              {c.type === "email" && <EmailIcon />}
                              {c.type === "youtube" && <YoutubeIcon />}
                              {c.type === "phone" && <ContactPhoneIcon />}
                              {![
                                "facebook",
                                "instagram",
                                "email",
                                "phone",
                                "youtube",
                              ].includes(c.type) && (
                                <ConnectWithoutContactIcon />
                              )}
                            </ListItemAvatar>
                            <ListItemText
                              primary={c.value}
                              secondary={c.formatted}
                            />
                          </ListItemButton>
                        ))}
                      </List>
                    </DialogContent>
                    <DialogActions>
                      <Button
                        onClick={(ev) => {
                          setContactAlert(false);
                        }}
                        autoFocus
                      >
                        Close
                      </Button>
                    </DialogActions>
                  </Dialog>
                  <List
                    sx={{
                      width: "100%",
                      bgcolor: "background.default",
                    }}
                  >
                    {influencer?.socialProfiles?.map((profs) => (
                      <Button
                        rel="nofollow"
                        fullWidth
                        color="inherit"
                        variant="outlined"
                        href={profs.url}
                        target="_blank"
                        sx={{
                          px: {
                            md: 1,
                          },
                          textAlign: "left",
                          mb: 3,
                        }}
                        key={profs.profileType}
                      >
                        <ListItemAvatar>
                          {profs.profileType === "instagram" && (
                            <Avatar
                              sx={{
                                borderRadius: 1,
                              }}
                              alt="Instagram"
                              src="/icon/iconInstagram.svg"
                            />
                          )}
                        </ListItemAvatar>
                        <ListItemText
                          primary={profs?.label}
                          secondary={profs?.count}
                        />
                      </Button>
                    ))}
                    <ListItemButton
                      sx={{
                        px: {
                          md: 1,
                        },
                      }}
                    >
                      <ListItemAvatar>
                        <MoneyIcon />
                      </ListItemAvatar>
                      <ListItemText primary="Pricing" secondary=" " />
                    </ListItemButton>
                    <ListItemButton
                      sx={{
                        px: {
                          md: 1,
                        },
                      }}
                      onClick={() => setContactAlert(true)}
                    >
                      <ListItemAvatar>
                        <ConnectWithoutContactIcon />
                      </ListItemAvatar>
                      <ListItemText
                        primary="Contact"
                        secondary={`${influencer?.contact?.length ?? 0} ways`}
                      />
                    </ListItemButton>
                  </List>
                  {/* <PDFDownloadLink
                    document={<PdfView />}
                    fileName={`${influencer?.displayName}.pdf`}
                  >
                    {({ blob, url: pdfUrl, loading, error }) => (
                      <LoadingButton
                        size="large"
                        variant="contained"
                        type="button"
                        loading={loading}
                        href={url}
                      >
                        Download Report
                      </LoadingButton>
                    )}
                  </PDFDownloadLink> */}
                  <LoadingButton
                    size="large"
                    variant="contained"
                    type="button"
                    loading={false}
                    component={RouterLink}
                    to={reportPath}
                    target="_blank"
                  >
                    Download Report
                  </LoadingButton>
                </>
              )}
            </Sidebar>

            {/* <Box sx={{ mb: 5, mx: 2.5 }}>
                <Link underline="none" component={RouterLink} to="/account">
                  <AccountStyle>
                    <Box sx={{ ml: 2 }}>
                      <Typography
                        variant="subtitle2"
                        sx={{ color: "text.primary" }}
                      >
                        {user?.name}
                      </Typography>
                      <Typography variant="body2" sx={{ color: 'text.secondary' }}>
                {user?.description}
              </Typography>
              </Box>
                  </AccountStyle>
                </Link>
              </Box>

              <NavSection navConfig={sidebarConfig} /> */}
          </Grid>
          <PrintView show hide={pdf}>
            <Grid item xs={12} sm={12} md={10} lg={10}>
              <Tabs value={currentTab} variant="scrollable">
                <Tab
                  icon={<DataUsageIcon />}
                  label="Overview"
                  // onClick={(e) => {
                  //   if (profileId) {
                  //     e.preventDefault();
                  //   }
                  // }}
                  value={`${path}/overview`}
                  {...(profileId
                    ? {
                        onClick: () => setInternalTab(`${path}/overview`),
                      }
                    : {
                        to: `${url}/overview`,
                        component: RouterLink,
                      })}
                />
                <Tab
                  icon={<EqualizerIcon />}
                  label="Engagement"
                  value={`${path}/engagement`}
                  {...(profileId
                    ? {
                        onClick: () => setInternalTab(`${path}/engagement`),
                      }
                    : {
                        to: `${url}/engagement`,
                        component: RouterLink,
                      })}
                />
                <Tab
                  icon={<GroupsIcon />}
                  label="Audience"
                  {...(profileId
                    ? {
                        onClick: () => setInternalTab(`${path}/audience`),
                      }
                    : {
                        to: `${url}/audience`,
                        component: RouterLink,
                      })}
                  value={`${path}/audience`}
                />
                <Tab
                  icon={<ArtTrackIcon />}
                  label="Posts"
                  {...(profileId
                    ? {
                        onClick: () => setInternalTab(`${path}/posts`),
                      }
                    : {
                        to: `${url}/posts`,
                        component: RouterLink,
                      })}
                  value={`${path}/posts`}
                />
              </Tabs>
              <TabContext value={currentTab}>
                <TabPanel value={`${path}/overview`}>
                  <OverviewPage />
                </TabPanel>
                <TabPanel value={`${path}/engagement`}>
                  <EngagementPage />
                </TabPanel>
                <TabPanel value={`${path}/audience`}>
                  <AudiencePage />
                </TabPanel>
                <TabPanel value={`${path}/posts`}>
                  <PostsPage />
                </TabPanel>
              </TabContext>
            </Grid>
          </PrintView>
        </Grid>
      </Container>
    </Box>
  );
};
