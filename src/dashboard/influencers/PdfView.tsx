// import { Document, Page } from "@react-pdf/renderer";
import { LinearProgress, Typography } from "@material-ui/core";
import { LoadingButton } from "@material-ui/lab";
import { Box } from "@material-ui/system";
import html2Pdf from "html2pdf.js/src/index";
import { useCallback, useEffect, useRef, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { AudienceType, ELoadingStatus, setCurrentInfluencerId } from "./action";
import { AgeSplitPage } from "./components/pdf/AgeSplitPage";
import { AudienceInterestAffinityPage } from "./components/pdf/AudienceInterestAffinityPage";
import { AudiencePage } from "./components/pdf/AudiencePage";
import { ContactsPage } from "./components/pdf/ContactsPage";
import { EngagementPage } from "./components/pdf/EngagementPage";
import { EngagementPage2 } from "./components/pdf/EngagementPage2";
import { GenderSplitData } from "./components/pdf/GenderSplitPage";
import { InfluencerBrandAffinityPage } from "./components/pdf/InfluencerBrandAffinityPage";
import { LocationPage } from "./components/pdf/LocationPage";
import { LookalikeDataPage } from "./components/pdf/LookalikesPage";
import { NotableDataPage } from "./components/pdf/NotableDataPage";
import { OverviewPage } from "./components/pdf/OverviewPage";
import { OverviewPage2 } from "./components/pdf/OverviewPage2";
import { PopularDataPage } from "./components/pdf/PopularDataPage";
import { PostsPage } from "./components/pdf/PostsPage";
import { ProfilePage } from "./components/pdf/ProfilePage";
import { selectBasicLoading } from "./selector";

const Doc = (props) => {
  const { targetRef } = props;
  return (
    <Box
      sx={{
        px: 4,
        width: "1400px",
        height: "100%",
        background: "#F4F6F8",
      }}
      ref={targetRef}
    >
      <ProfilePage pdf />
      <OverviewPage pdf />
      <EngagementPage pdf />
      <OverviewPage2 pdf />
      <EngagementPage2 pdf />
    </Box>
  );
};

// const Doc2 = (props) => {
//   const { targetRef } = props;
//   return (
//     <Box
//       sx={{
//         px: 4,
//         width: "1400px",
//         height: "100%",
//         background: "#F4F6F8",
//       }}
//       ref={targetRef}
//     >

//     </Box>
//   );
// };

const Doc3 = (props) => {
  const { targetRef } = props;
  return (
    <Box
      sx={{
        px: 4,
        width: "1400px",
        height: "100%",
        background: "#F4F6F8",
      }}
      ref={targetRef}
    >
      <AudiencePage audienceType={AudienceType.FOLLOWERS} pdf />
      <div className="breakAfterMe" />
      <AudiencePage audienceType={AudienceType.LIKES} pdf />
    </Box>
  );
};

const Doc4 = (props) => {
  const { targetRef } = props;
  return (
    <Box
      sx={{
        px: 4,
        width: "1400px",
        height: "100%",
        background: "#F4F6F8",
      }}
      ref={targetRef}
    >
      <NotableDataPage />
      <LookalikeDataPage />
    </Box>
  );
};

const Doc5 = (props) => {
  const { targetRef } = props;
  return (
    <>
      <Box
        sx={{
          px: 4,
          width: "1400px",
          height: "100%",
          background: "#F4F6F8",
        }}
        ref={targetRef}
      >
        <PopularDataPage />
        <GenderSplitData />
        <AgeSplitPage />
      </Box>
    </>
  );
};

const Doc6 = (props) => {
  const { targetRef } = props;
  return (
    <>
      <Box
        sx={{
          px: 4,
          width: "1400px",
          height: "100%",
          background: "#F4F6F8",
        }}
        ref={targetRef}
      >
        <LocationPage />
      </Box>
    </>
  );
};

const Doc7 = (props) => {
  const { targetRef } = props;
  return (
    <>
      <Box
        sx={{
          px: 4,
          width: "1400px",
          height: "100%",
          background: "#F4F6F8",
        }}
        ref={targetRef}
      >
        <AudienceInterestAffinityPage />
      </Box>
    </>
  );
};

const Doc8 = (props) => {
  const { targetRef } = props;
  return (
    <>
      <Box
        sx={{
          px: 4,
          width: "1400px",
          height: "100%",
          background: "#F4F6F8",
        }}
        ref={targetRef}
      >
        <InfluencerBrandAffinityPage />
      </Box>
    </>
  );
};

const Doc9 = (props) => {
  const { targetRef } = props;
  return (
    <>
      <Box
        sx={{
          px: 4,
          width: "1400px",
          height: "100%",
          background: "#F4F6F8",
        }}
        ref={targetRef}
      >
        <PostsPage />
      </Box>
    </>
  );
};

const Doc10 = (props) => {
  const { targetRef } = props;
  return (
    <>
      <Box
        sx={{
          px: 4,
          width: "1400px",
          height: "100%",
        }}
        ref={targetRef}
      >
        <ContactsPage />
      </Box>
    </>
  );
};

const generateDownloadLink = (dataUrl) => {
  const link = document.createElement("a");
  link.download = "fileName";
  link.href = dataUrl;
  link.click();
};

export const PageView = ({ onStart, onComplete, fileName }) => {
  const ref = useRef<HTMLDivElement>(null);
  const ref1 = useRef<HTMLDivElement>(null);
  const ref2 = useRef<HTMLDivElement>(null);
  const ref3 = useRef<HTMLDivElement>(null);
  const ref4 = useRef<HTMLDivElement>(null);
  const ref5 = useRef<HTMLDivElement>(null);
  const ref6 = useRef<HTMLDivElement>(null);
  const ref7 = useRef<HTMLDivElement>(null);
  const ref8 = useRef<HTMLDivElement>(null);
  const ref9 = useRef<HTMLDivElement>(null);
  const dispatch = useAppDispatch();
  const params = useParams();
  const networkStatus = useAppSelector(selectBasicLoading);
  const [started, setStarted] = useState(false);

  useEffect(() => {
    if (params["id"]) {
      dispatch(
        setCurrentInfluencerId({
          influencerId: params["id"],
        })
      );
    }
  }, [dispatch, params]);

  const onButtonClick = useCallback(() => {
    if (ref.current === null) {
      return;
    }

    setStarted(true);

    const opt = {
      margin: 0,
      filename: fileName,
      enableLinks: true,
      pagebreak: {
        avoid: [".dontBreakMe", "img", "tr"],
        after: [".breakAfterMe"],
        before: [".breakBeforeMe"],
      },
      image: { type: "jpeg", quality: 1 },
      html2canvas: {
        useCORS: true,
        logging: false,
        // foreignObjectRendering: true,
        // windowWidth: ref.current?.getBoundingClientRect().width,
        // height: ref.current?.getBoundingClientRect().height,
        scale: 2,
        imageTimeout: 60000,
        windowWidth: 1400,
      },
      jsPDF: {
        unit: "px",
        hotfixes: ["px_scaling"],
        putOnlyUsedFonts: true,
        compress: true,
        format: [1400, 980],
        orientation: "landscape",
      },
    };

    // New Promise-based usage:
    let doc = html2Pdf().set(opt).from(ref.current).toPdf();
    [ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9].forEach((r, i, a) => {
      doc = doc
        .get("pdf")
        .then((pdf) => {
          // PLEASE DONT DELETE THIS EMPTY THEN IT IS KEPT HERE ON PURPOSE THIS STITCHES MULTIPLE DOCS AND AVOIDS THE MAXIMUM CANVAS SIZE ISSUE
          pdf.addPage();
          onComplete((i * 100) / a.length);
        })
        .from(r.current)
        .toContainer()
        .toCanvas()
        .toPdf();
    });

    doc.save().then(
      () => {
        onComplete(100);
      },
      (err) => {
        console.log("there was an error", err);
        onComplete(100);
      }
    );
  }, [ref, fileName, onComplete]);

  const isLoading = [ELoadingStatus.INITIAL, ELoadingStatus.LOADING].includes(
    networkStatus
  );

  useEffect(() => {
    if (networkStatus === ELoadingStatus.SUCCESS && !started) {
      // if (false) {
      setTimeout(() => {
        // PLEASE dont remove this set time out this is added for rendering the initial page
        onStart();
        onButtonClick();
      }, 500);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [networkStatus]);
  return (
    <>
      <Doc targetRef={ref} />
      {/* <Doc2 targetRef={ref1} /> */}
      <Doc3 targetRef={ref2} />
      <Doc4 targetRef={ref3} />
      <Doc5 targetRef={ref4} />
      <Doc6 targetRef={ref5} />
      <Doc7 targetRef={ref6} />
      <Doc8 targetRef={ref7} />
      <Doc9 targetRef={ref8} />
      <Doc10 targetRef={ref9} />
    </>
  );
  // Uncomment above, comment below
  // return (
  //   <>
  //     <LoadingButton
  //       loading={isLoading}
  //       onClick={() => {
  //         onStart();
  //         onButtonClick();
  //       }}
  //     >
  //       Download Report
  //     </LoadingButton>
  //     <Doc targetRef={ref} />
  //     {/* <Doc2 targetRef={ref1} /> */}
  //     <Doc3 targetRef={ref2} />
  //     <Doc4 targetRef={ref3} />
  //     <Doc5 targetRef={ref4} />
  //     <Doc6 targetRef={ref5} />
  //     <Doc7 targetRef={ref6} />
  //     <Doc8 targetRef={ref7} />
  //     <Doc9 targetRef={ref8} />
  //     <Doc10 targetRef={ref9} />
  //   </>
  // );
};

export const PdfView = () => {
  const ref = useRef<any>();
  // change this to true
  const [loading, setLoading] = useState<ELoadingStatus>(
    ELoadingStatus.INITIAL
  );

  const [progress, setProgress] = useState(0);
  const params = useParams();

  let status = "Fetching Data";

  const history = useHistory();

  switch (loading) {
    case ELoadingStatus.INITIAL:
      status = "Organizing data for the report...";
      break;
    case ELoadingStatus.LOADING:
      status = "Generating report...";
      break;
    case ELoadingStatus.SUCCESS:
      status = "Saving report...";
      break;
    case ELoadingStatus.ERROR:
      status = "There was an error generating the report, please try again";
      break;
    default:
      status = "Fetching data...";
  }
  return (
    <div>
      <Box
        sx={{
          position: "fixed",
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          zIndex: 9999,
          backgroundColor: "#fff",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
        }}
      >
        <Box>
          <Typography variant="h5">{status}</Typography>
        </Box>
        <Box
          sx={{
            width: 400,
          }}
        >
          <Box sx={{ display: "flex", alignItems: "center" }}>
            <Box sx={{ width: "100%", mr: 1 }}>
              <LinearProgress
                variant="determinate"
                color="primary"
                value={progress}
              />
            </Box>
            <Box sx={{ minWidth: 35 }}>
              <Typography variant="body2">{`${Math.round(
                progress
              )}%`}</Typography>
            </Box>
          </Box>
        </Box>
      </Box>
      <PageView
        onStart={() => setLoading(ELoadingStatus.LOADING)}
        fileName={`${params["id"]}.pdf`}
        onComplete={(i: number) => {
          if (i === 100) {
            setLoading(ELoadingStatus.SUCCESS);
            history.goBack();
          }
          setProgress(i);
        }}
      />
    </div>
  );
};
