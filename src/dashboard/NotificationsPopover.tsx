import {
  Avatar,
  Badge,
  Box,
  Button,
  Divider,
  IconButton,
  List,
  ListItemAvatar,
  ListItemButton,
  ListItemText,
  ListSubheader,
  Tooltip,
  Typography,
} from "@material-ui/core";
// material
import { alpha } from "@material-ui/core/styles";
import DoneAllIcon from "@material-ui/icons/DoneAll";
import NotificationsIcon from "@material-ui/icons/Notifications";
import WatchLaterIcon from "@material-ui/icons/WatchLater";
import { noCase } from "change-case";
import { formatDistanceToNow, set, sub } from "date-fns";
import { useRef, useState } from "react";
import { Link as RouterLink } from "react-router-dom";
import { useAppSelector } from "../app/hooks";
import { MenuPopover } from "../components/MenuPopover";
import { Scrollbar } from "../components/Scrollbar";
import { selectNotifications } from "../notification/selector";

enum ENotificationType {
  NONE = "none",
}

interface INotification {
  id: string;
  title: string;
  description: string;
  avatar?: string;
  type: ENotificationType;
  createdAt: Date;
  unread: boolean;
}

const DUMMY_NOTIFICATIONS: INotification[] = [
  {
    id: "some id",
    title: "New user registered",
    description: "waiting for verification",
    avatar: "https://via.placeholder.com/150.png",
    type: ENotificationType.NONE,
    createdAt: set(new Date(), { hours: 10, minutes: 30 }),
    unread: true,
  },
  {
    id: "some-id-2",
    title: "Something just happened",
    description: "check it out",
    avatar: "https://via.placeholder.com/150.png",
    type: ENotificationType.NONE,
    createdAt: sub(new Date(), { hours: 3, minutes: 30 }),
    unread: false,
  },
];

function getAvatarImage(notification: INotification) {
  switch (notification.type) {
    case ENotificationType.NONE:
    default:
      return <img alt={notification.title} src={notification.avatar} />;
  }
}

function renderContent(notification: INotification) {
  const title = (
    <Typography variant="subtitle2">
      {notification.title}
      <Typography
        component="span"
        variant="body2"
        sx={{ color: "text.secondary" }}
      >
        &nbsp; {noCase(notification.description)}
      </Typography>
    </Typography>
  );

  return {
    avatar: getAvatarImage(notification),
    title,
  };
}

interface NotificationItemProps {
  notification: INotification;
}

function NotificationItem({ notification }: NotificationItemProps) {
  const { avatar, title } = renderContent(notification);

  return (
    <ListItemButton
      to="#"
      disableGutters
      component={RouterLink}
      sx={{
        py: 1.5,
        px: 2.5,
        mt: "1px",
        ...(notification.unread && {
          bgcolor: "action.selected",
        }),
      }}
    >
      <ListItemAvatar>
        <Avatar sx={{ bgcolor: "background.neutral" }}>{avatar}</Avatar>
      </ListItemAvatar>
      <ListItemText
        primary={title}
        secondary={
          <Typography
            variant="caption"
            sx={{
              mt: 0.5,
              display: "flex",
              alignItems: "center",
              color: "text.disabled",
            }}
          >
            <Box
              component={WatchLaterIcon}
              sx={{ mr: 0.5, width: 16, height: 16 }}
            />
            {formatDistanceToNow(new Date(notification.createdAt))}
          </Typography>
        }
      />
    </ListItemButton>
  );
}

export default function NotificationsPopover() {
  const anchorRef = useRef(null);
  const [open, setOpen] = useState(false);
  const notifications = useAppSelector(selectNotifications);

  const totalUnRead = notifications.filter(
    (item) => item.unread === true
  ).length;

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleMarkAllAsRead = () => {
    // setNotifications(
    //   notifications.map((notification) => ({
    //     ...notification,
    //     unread: false,
    //   }))
    // );
  };

  return (
    <>
      <IconButton
        ref={anchorRef}
        size="large"
        color={open ? "primary" : "default"}
        onClick={handleOpen}
        sx={{
          ...(open && {
            bgcolor: (theme) =>
              alpha(
                theme.palette.primary.main,
                theme.palette.action.focusOpacity
              ),
          }),
        }}
      >
        <Badge badgeContent={totalUnRead} color="error">
          <NotificationsIcon height={20} width={20} />
        </Badge>
      </IconButton>

      <MenuPopover
        open={open}
        onClose={handleClose}
        anchorEl={anchorRef.current}
        sx={{ width: 360 }}
      >
        <Box sx={{ display: "flex", alignItems: "center", py: 2, px: 2.5 }}>
          <Box sx={{ flexGrow: 1 }}>
            <Typography variant="subtitle1">Notifications</Typography>
            <Typography variant="body2" sx={{ color: "text.secondary" }}>
              You have {totalUnRead} unread messages
            </Typography>
          </Box>

          {totalUnRead > 0 && (
            <Tooltip title=" Mark all as read">
              <IconButton color="primary" onClick={handleMarkAllAsRead}>
                <DoneAllIcon width={20} height={20} />
              </IconButton>
            </Tooltip>
          )}
        </Box>

        <Divider />

        <Scrollbar sx={{ height: { xs: 340, sm: "auto" } }}>
          <List
            disablePadding
            subheader={
              <ListSubheader
                disableSticky
                sx={{ py: 1, px: 2.5, typography: "overline" }}
              >
                New
              </ListSubheader>
            }
          >
            {notifications.slice(0, 2).map((notification) => (
              <NotificationItem
                key={notification.id}
                notification={notification}
              />
            ))}
          </List>

          <List
            disablePadding
            subheader={
              <ListSubheader
                disableSticky
                sx={{ py: 1, px: 2.5, typography: "overline" }}
              >
                Before that
              </ListSubheader>
            }
          >
            {notifications.slice(2, 5).map((notification) => (
              <NotificationItem
                key={notification.id}
                notification={notification}
              />
            ))}
          </List>
        </Scrollbar>

        <Divider />

        <Box sx={{ p: 1 }}>
          <Button
            fullWidth
            disableRipple
            component={RouterLink}
            to="/account/notifications"
          >
            View All
          </Button>
        </Box>
      </MenuPopover>
    </>
  );
}
