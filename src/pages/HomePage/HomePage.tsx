import { Box, Button, ButtonGroup, Container } from "@material-ui/core";
// material
import { styled } from "@material-ui/core/styles";
import { Link as RouterLink, Redirect } from "react-router-dom";
// components

// ----------------------------------------------------------------------

const RootStyle = styled(Box)(({ theme }) => ({
  display: "flex",
  minHeight: "100%",
  alignItems: "center",
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10),
}));

export const HomePage = () => (
  <RootStyle>
    <Container>
      <Redirect to="/login" />
      <Box sx={{ margin: "auto", textAlign: "center" }}>
        <ButtonGroup size="large" variant="contained">
          <Button to="/login" component={RouterLink}>
            Login
          </Button>
          <Button to="/register" component={RouterLink}>
            Register
          </Button>
        </ButtonGroup>
      </Box>
    </Container>
  </RootStyle>
);
