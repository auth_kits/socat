import { Box, Button, Container, Typography } from "@material-ui/core";
// material
import { styled } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
// components

// ----------------------------------------------------------------------

const RootStyle = styled(Box)(({ theme }) => ({
  display: "flex",
  minHeight: "100%",
  alignItems: "center",
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10),
}));

// ----------------------------------------------------------------------

export function WipPage() {
  const history = useHistory();
  return (
    <RootStyle>
      <Container>
        <Box sx={{ maxWidth: 480, margin: "auto", textAlign: "center" }}>
          <Typography sx={{ color: "text.secondary" }}>
            Our engineers are working on it, please check again.
          </Typography>

          <Box
            component="img"
            src="/images/under_construction.svg"
            sx={{ height: 260, mx: "auto", my: { xs: 5, sm: 10 } }}
          />

          <Button
            size="large"
            variant="contained"
            onClick={() => {
              history.goBack();
            }}
          >
            Go back
          </Button>
        </Box>
      </Container>
    </RootStyle>
  );
}
