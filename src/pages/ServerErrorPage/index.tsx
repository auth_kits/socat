import { Box, Button, Container, Typography } from "@material-ui/core";
// material
import { styled } from "@material-ui/core/styles";
import { Link as RouterLink } from "react-router-dom";
// components

// ----------------------------------------------------------------------

const RootStyle = styled(Box)(({ theme }) => ({
  display: "flex",
  minHeight: "100%",
  alignItems: "center",
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10),
}));

// ----------------------------------------------------------------------

export function ServerErrorPage() {
  return (
    <RootStyle>
      <Container>
        <Box sx={{ maxWidth: 480, margin: "auto", textAlign: "center" }}>
          <Typography variant="h3" paragraph>
            Error!
          </Typography>
          <Typography sx={{ color: "text.secondary" }}>
            Sorry, we have encountered an error loading this page.
          </Typography>

          <Box
            component="img"
            src="/images/404.svg"
            sx={{ height: 260, mx: "auto", my: { xs: 5, sm: 10 } }}
          />

          <Button
            to="/"
            size="large"
            variant="contained"
            component={RouterLink}
          >
            Go to Home
          </Button>
        </Box>
      </Container>
    </RootStyle>
  );
}
