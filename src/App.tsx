import React, { useEffect, useLayoutEffect } from "react";
import "react-alice-carousel/lib/alice-carousel.css";

import { Route, Switch } from "react-router-dom";
import "./App.css";
import { useAppDispatch } from "./app/hooks";
import { LoginPage } from "./auth/LoginPage";
import { PrivateRoute } from "./auth/PrivateRoute";
import { RegisterPage } from "./auth/RegisterPage";
import { UserRole } from "./auth/roles";
import { UnPrivateRoute } from "./auth/UnPrivateRoute";
import { AccountPage } from "./dashboard/account";
import { AdminPage } from "./dashboard/admin";
import { AudiencePage } from "./dashboard/audience";
import { DashboardHomePage } from "./dashboard/DashboardHomePage";
import { DashboardLayout } from "./dashboard/DashboardLayout";
import { InfluencersPage } from "./dashboard/influencers";
import { CuratedPage } from "./dashboard/influencers/CuratedPage";
import { HomePage } from "./pages/HomePage";
import { NotFoundPage } from "./pages/NotFoundPage";
import { WipPage } from "./pages/WipPage";

function App() {
  const dispatch = useAppDispatch();
  useLayoutEffect(() => {
    dispatch({
      type: "@@SOCAT@@INIT@@",
    });
  }, [dispatch]);
  return (
    <Switch>
      <Route path="/" exact>
        <HomePage />
      </Route>
      <UnPrivateRoute path="/login">
        <LoginPage />
      </UnPrivateRoute>
      <UnPrivateRoute path="/register">
        <RegisterPage />
      </UnPrivateRoute>
      <UnPrivateRoute path="/forgot-password">
        <LoginPage />
      </UnPrivateRoute>
      <UnPrivateRoute path="/reset-password/:resetToken">
        <LoginPage />
      </UnPrivateRoute>

      <PrivateRoute path="/dashboard">
        <DashboardLayout>
          <DashboardHomePage />
        </DashboardLayout>
      </PrivateRoute>
      <PrivateRoute path="/account">
        <DashboardLayout>
          <AccountPage />
        </DashboardLayout>
      </PrivateRoute>

      <PrivateRoute path="/audience">
        <DashboardLayout>
          <AudiencePage />
        </DashboardLayout>
      </PrivateRoute>
      <PrivateRoute path="/reports">
        <DashboardLayout>
          <InfluencersPage />
        </DashboardLayout>
      </PrivateRoute>
      <PrivateRoute path="/curated">
        <DashboardLayout>
          <CuratedPage />
        </DashboardLayout>
      </PrivateRoute>

      <PrivateRoute
        roles={[UserRole.SUPER_ADMIN, UserRole.ADMIN]}
        path="/admin"
      >
        <DashboardLayout>
          <AdminPage />
        </DashboardLayout>
      </PrivateRoute>

      <Route path="/wip">
        <WipPage />
      </Route>
      <Route path="*">
        <NotFoundPage />
      </Route>
    </Switch>
  );
}

export default App;
