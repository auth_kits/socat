import { createReducer } from "@reduxjs/toolkit";
import {
  ELoadingStatus,
  getCurrentUserInfo,
  loginSuccess,
  setAuthError,
  setCurrentUserInfo,
  setLoading,
  UserInfo,
} from "./action";

export interface AuthState {
  // Changing this will trigger
  authenticated: boolean;
  currentUser?: UserInfo;
  currentUserStatus: ELoadingStatus;
  status: ELoadingStatus;
  errors?: any;
}

export default createReducer<AuthState>(
  {
    authenticated: false,
    status: ELoadingStatus.INITIAL,
    currentUserStatus: ELoadingStatus.INITIAL,
  },
  (builder) => {
    builder.addCase(setLoading, (state, { payload }) => {
      state.status = payload;
    });
    builder.addCase(loginSuccess, (state, { payload }) => {
      state.authenticated = true;
      // state.currentUser = user;
      state.status = ELoadingStatus.SUCCESS;
    });

    builder.addCase(setAuthError, (state, { payload: { message } }) => {
      state.errors = message;
      state.authenticated = false;
      state.status = ELoadingStatus.ERROR;
    });

    builder.addCase(getCurrentUserInfo, (state) => {
      state.currentUserStatus = ELoadingStatus.LOADING;
    });

    builder.addCase(setCurrentUserInfo, (state, { payload: { data } }) => {
      const [userData] = data;
      state.currentUser = {
        ...userData,
        roles: [userData.role_id],
      };
      state.currentUserStatus = ELoadingStatus.SUCCESS;
      state.authenticated = true;
    });
  }
);
