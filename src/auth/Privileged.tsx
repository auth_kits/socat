import React, { useMemo } from "react";
import { Route, RouteProps } from "react-router-dom";
import { useAppSelector } from "../app/hooks";
import { UserRole } from "./roles";
import { selectPermissionForRole } from "./selectors";

interface PrivilegedProps extends RouteProps {
  roles?: UserRole[];
}
export const Privileged: React.FC<PrivilegedProps> = ({
  roles = [],
  ...routeProps
}) => {
  const hasPermissionSelector = useMemo(
    () => selectPermissionForRole(roles),
    [roles]
  );
  const hasPermission = useAppSelector(hasPermissionSelector);

  if (hasPermission) {
    return <Route {...routeProps} />;
  }

  return null;
};
