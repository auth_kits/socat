import { createSelector } from "@reduxjs/toolkit";
import { difference, includes, intersection, isEmpty } from "lodash";
import { RootState } from "../app/store";
import { UserRole } from "./roles";

const selectAuthState = ({ auth }: RootState) => auth;

export const selectAuthenticated = createSelector(
  selectAuthState,
  (auth) => auth.authenticated
);

export const selectCurrentUser = createSelector(
  selectAuthenticated,
  selectAuthState,
  (isAuthenticated, { currentUser }) => (isAuthenticated ? currentUser : null)
);

export const selectStatus = createSelector(
  selectAuthState,
  ({ status }) => status
);

export const selectCurrentUserStatus = createSelector(
  selectAuthState,
  ({ currentUserStatus }) => currentUserStatus
);

// console.log(roleIds, user?.roles, difference(roleIds, user?.roles ?? []).length === 0);
export const selectPermissionForRole = (roleIds: UserRole[]) =>
  createSelector(selectCurrentUser, (user) =>
    isEmpty(roleIds)
      ? true
      : intersection(roleIds, user?.roles ?? []).length > 0
  );

export const selectUserRoles = createSelector(
  selectCurrentUser,
  (user) => user?.roles
);
