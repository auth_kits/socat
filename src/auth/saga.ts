import { isEmpty } from "lodash";
import { all, call, put, takeLatest } from "redux-saga/effects";
import { getApi } from "../app/axios";
import { history } from "../app/history";
import { addSnackMessage } from "../notification/action";
import * as types from "./action";
// import {
//   registerUserService,
//   loginUserService,
// } from "../services/authenticationService";

const USER_TOKEN_NAME = process.env.REACT_APP_TOKEN_NAME ?? "";

export function* registerSaga({
  payload: { email, password, confirmPassword },
}: ReturnType<typeof types.register>) {
  yield put(types.setLoading(types.ELoadingStatus.LOADING));
  try {
    const formData = new FormData();
    formData.append("email_id", email);
    formData.append("password", password);
    formData.append("password_confirmation", confirmPassword);
    // formData.append('phoneNo', phone)
    const response = yield call(getApi().post, "/user/signup", formData, {
      headers: {
        "content-type": "multipart/form-data",
      },
    });
    // navigate to route
    yield [put(types.registerSuccess())];
  } catch (error) {
    yield put(
      types.setAuthError({
        message: "Unable to create your account, please try again in some time",
      })
    );
  }
}

export function* loginSuccessSaga({
  payload: { token },
}: ReturnType<typeof types.loginSuccess>) {
  yield put(types.setLoading(types.ELoadingStatus.SUCCESS));
  localStorage.setItem(USER_TOKEN_NAME, token);
  // reset api client to consider new api requests
  getApi(true);
  yield initSaga();
  yield put(
    addSnackMessage({
      message: "Welcome back!",
      notificationType: "success",
    })
  );
  history.push("/dashboard");
}

export function* logoutSaga({ payload }: ReturnType<typeof types.logout>) {
  yield put({ type: "store/reset" });
  localStorage.removeItem(USER_TOKEN_NAME);
  // reset api client to consider new api requests
  getApi(true);
  if (!isEmpty(payload)) {
    yield put(
      addSnackMessage({
        message: payload ?? "You have been logout!!",
        notificationType: "error",
      })
    );
  }

  history.push("/login");
}

export function* loginSaga({
  payload: { email, password },
}: ReturnType<typeof types.login>) {
  yield put(types.setLoading(types.ELoadingStatus.LOADING));
  try {
    // call axios apis here
    const formData = new FormData();
    formData.append("email_id", email);
    formData.append("password", password);
    const { data } = yield call(getApi().post, "/user/signin", formData, {
      headers: {
        "content-type": "multipart/form-data",
      },
    });
    yield put(
      types.loginSuccess({
        token: data?.data,
      })
    );
  } catch (error) {
    yield put(
      types.setAuthError({
        message: error?.response?.data?.msg ?? "Invalid email or username",
      })
    );
  }
}

export function* currentUserSaga() {
  yield put(types.setLoading(types.ELoadingStatus.LOADING));
  try {
    // call axios apis here
    const { data } = yield call(getApi().get, "user/basic_user_info");
    const newUrl = localStorage.getItem("redirectUrl");
    if (!isEmpty(newUrl)) {
      localStorage.removeItem("redirectUrl");
      console.log("redirecting to", newUrl);
      history.push(newUrl ?? "");
    }
    yield put(types.setCurrentUserInfo(data));
  } catch (error) {
    yield put(types.logout());
  }
}

export function* initSaga() {
  yield put(types.getCurrentUserInfo());
}

export default function* watchUserAuthentication() {
  yield all([
    takeLatest("@@SOCAT@@INIT@@", initSaga),
    takeLatest(types.register, registerSaga),
    takeLatest(types.login, loginSaga),
    takeLatest(types.logout, logoutSaga),
    takeLatest(types.loginSuccess, loginSuccessSaga),
    takeLatest(types.getCurrentUserInfo, currentUserSaga),
  ]);
}
