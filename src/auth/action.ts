import { createAction } from "@reduxjs/toolkit";
import { UserRole } from "./roles";

const PREFIX = "socat/auth";

export enum ELoadingStatus {
  INITIAL,
  LOADING,
  SUCCESS,
  ERROR,
}
export const setLoading = createAction<ELoadingStatus>(`${PREFIX}/setLoading`);

export interface RegisterPayload {
  name: string;
  email: string;
  password: string;
  confirmPassword: string;
  phoneNo: string;
  // address?: string;
}
export const register = createAction<RegisterPayload>(`${PREFIX}/register`);
export const registerSuccess = createAction(`${PREFIX}/registerSuccess`);

export interface LoginPayload {
  email: string;
  password: string;
}
export const login = createAction<LoginPayload>(`${PREFIX}/login`);

export interface UserInfo {
  name: string;
  email: string;
  roles: UserRole[];
  imageUrl?: string;
}

export interface LoginSuccessPayload {
  // JWT token which awill be sent with all the api's
  token: string;
}
export const loginSuccess = createAction<LoginSuccessPayload>(
  `${PREFIX}/loginSuccess`
);

export const logout = createAction<string | undefined>(`${PREFIX}/logoutUser`);

export interface AuthErrorPayload {
  message: string;
}
export const setAuthError = createAction<AuthErrorPayload>(
  `${PREFIX}/setAuthError`
);

export const sendForgotEmail = createAction<{
  email: string;
}>(`${PREFIX}/sendForgotEmail`);

export const resetPassword = createAction<{
  token: string;
  password: string;
  confirmPassword: string;
}>(`${PREFIX}/resetPassword`);

export const getCurrentUserInfo = createAction(`${PREFIX}/getCurrentUserInfo`);

export const setCurrentUserInfo = createAction<{ data: any }>(
  `${PREFIX}/setCurrentUserInfo`
);
export const getCurrentUserInfoError = createAction<{ data: any }>(
  `${PREFIX}/getCurrentUserInfoError`
);
