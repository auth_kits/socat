import { Box, Button, Container, Typography } from "@material-ui/core";
// material
import { styled } from "@material-ui/core/styles";
import { Link as RouterLink } from "react-router-dom";

const RootStyle = styled(Box)(({ theme }) => ({
  display: "flex",
  minHeight: "100%",
  alignItems: "center",
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10),
}));

export const UnAuthorized: React.FC = () => (
  <RootStyle>
    <Container>
      <Box sx={{ maxWidth: 480, margin: "auto", textAlign: "center" }}>
        <Typography variant="h3" paragraph>
          Unauthorized Access!
        </Typography>
        <Typography sx={{ color: "text.secondary" }}>
          Sorry, it seems you don&#39t have access to this content, please
          contact your administrator.
        </Typography>

        <Box
          component="img"
          src="/images/unauthorized.svg"
          sx={{ height: 260, mx: "auto", my: { xs: 5, sm: 10 } }}
        />

        <Button to="/" size="large" variant="contained" component={RouterLink}>
          Go to Home
        </Button>
      </Box>
    </Container>
  </RootStyle>
);
