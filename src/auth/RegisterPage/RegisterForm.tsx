import { Button, Input, Stack, TextField } from "@material-ui/core";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { useAppDispatch } from "../../app/hooks";
import { register } from "../action";

interface IFormInput {
  email: string;
  fullName: string;
  password: string;
  confirmPassword: string;
  phone: string;
  country: string;
  address: string;
}

export const RegisterForm: React.FC = () => {
  const { control, handleSubmit } = useForm<IFormInput>();

  const dispatch = useAppDispatch();
  const onRegister: SubmitHandler<IFormInput> = (data) => {
    dispatch(
      register({
        password: data.password,
        confirmPassword: data.confirmPassword,
        email: data.email,
        name: data.fullName,
        phoneNo: data.phone,
      })
    );
  };
  return (
    <form onSubmit={handleSubmit(onRegister)}>
      <Stack spacing={2}>
        <Controller
          name="fullName"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <TextField label="Name" variant="outlined" {...field} />
          )}
        />
        <Controller
          name="email"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <TextField label="Email" variant="outlined" {...field} />
          )}
        />
        <Controller
          name="phone"
          control={control}
          render={({ field }) => (
            <TextField label="Phone no" variant="outlined" {...field} />
          )}
        />
        <Controller
          name="password"
          control={control}
          render={({ field }) => (
            <TextField
              label="Password"
              variant="outlined"
              type="password"
              {...field}
            />
          )}
        />
        <Controller
          name="confirmPassword"
          control={control}
          render={({ field }) => (
            <TextField
              label="Password"
              variant="outlined"
              type="password"
              {...field}
            />
          )}
        />
        {/* 
        <Controller
          name="country"
          control={control}
          render={({ field }) => (
            <TextField label="Country" variant="outlined" {...field} />
          )}
        />
        <Controller
          name="address"
          control={control}
          render={({ field }) => (
            <TextField
              label="Address"
              variant="outlined"
              multiline
              {...field}
            />
          )}
        /> */}
        <div>
          <Button size="large" variant="contained" type="submit">
            Register
          </Button>
        </div>
      </Stack>
    </form>
  );
};
