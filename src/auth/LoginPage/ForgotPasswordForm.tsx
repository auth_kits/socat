import { Grid, Link, Stack, TextField } from "@material-ui/core";
import { LoadingButton } from "@material-ui/lab";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { Link as RouterLink } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { ELoadingStatus, login, sendForgotEmail } from "../action";
import { selectStatus } from "../selectors";

interface IFormInput {
  email: string;
}

export const ForgotPasswordForm: React.FC = () => {
  const {
    control,
    handleSubmit,
    formState: { isDirty, isValid },
  } = useForm<IFormInput>();

  const networkStatus = useAppSelector(selectStatus);

  const dispatch = useAppDispatch();

  const onForgotPassword: SubmitHandler<IFormInput> = async (data) =>
    dispatch(
      sendForgotEmail({
        email: data.email,
      })
    );
  return (
    <form onSubmit={handleSubmit(onForgotPassword)}>
      <Stack spacing={2}>
        <Controller
          name="email"
          control={control}
          defaultValue=""
          rules={{
            required: true,
          }}
          render={({ field }) => (
            <TextField label="Email" variant="outlined" {...field} />
          )}
        />
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Link
            underline="hover"
            variant="subtitle2"
            component={RouterLink}
            to="/login"
          >
            Login
          </Link>

          <LoadingButton
            size="large"
            variant="contained"
            type="submit"
            loading={networkStatus === ELoadingStatus.LOADING}
            disabled={!isDirty || !isValid}
          >
            Send email
          </LoadingButton>
        </Grid>
      </Stack>
    </form>
  );
};
