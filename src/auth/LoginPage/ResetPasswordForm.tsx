import { Grid, Link, Stack, TextField } from "@material-ui/core";
import { LoadingButton } from "@material-ui/lab";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { Link as RouterLink, useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
  ELoadingStatus,
  login,
  resetPassword,
  sendForgotEmail,
} from "../action";
import { selectStatus } from "../selectors";

interface IFormInput {
  confirmPassword: string;
  password: string;
}

export const ResetPasswordForm: React.FC = () => {
  const {
    control,
    handleSubmit,
    formState: { isDirty, isValid },
  } = useForm<IFormInput>();

  const params = useParams<{ token: string }>();
  const networkStatus = useAppSelector(selectStatus);

  const dispatch = useAppDispatch();

  const onResetPassword: SubmitHandler<IFormInput> = async (data) =>
    dispatch(
      resetPassword({
        token: params.token,
        password: data.password,
        confirmPassword: data.confirmPassword,
      })
    );

  return (
    <form onSubmit={handleSubmit(onResetPassword)}>
      <Stack spacing={2}>
        <Controller
          name="password"
          control={control}
          defaultValue=""
          rules={{
            required: true,
          }}
          render={({ field }) => (
            <TextField
              label="Password"
              type="password"
              variant="outlined"
              {...field}
            />
          )}
        />
        <Controller
          name="confirmPassword"
          control={control}
          defaultValue=""
          rules={{
            required: true,
          }}
          render={({ field }) => (
            <TextField
              label="Confirm Password"
              type="password"
              variant="outlined"
              {...field}
            />
          )}
        />
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Link
            underline="hover"
            variant="subtitle2"
            component={RouterLink}
            to="/login"
          >
            Login
          </Link>

          <LoadingButton
            size="large"
            variant="contained"
            type="submit"
            loading={networkStatus === ELoadingStatus.LOADING}
            disabled={!isDirty || !isValid}
          >
            Send email
          </LoadingButton>
        </Grid>
      </Stack>
    </form>
  );
};
