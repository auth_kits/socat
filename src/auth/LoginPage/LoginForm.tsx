import { Grid, Link, Stack, TextField } from "@material-ui/core";
import { LoadingButton } from "@material-ui/lab";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { Link as RouterLink } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { ELoadingStatus, login } from "../action";
import { selectStatus } from "../selectors";

interface IFormInput {
  email: string;
  password: string;
}

export const LoginForm: React.FC = () => {
  const {
    control,
    handleSubmit,
    formState: { isDirty, isValid },
  } = useForm<IFormInput>();

  const networkStatus = useAppSelector(selectStatus);

  const dispatch = useAppDispatch();

  const onLogin: SubmitHandler<IFormInput> = async (data) =>
    dispatch(
      login({
        email: data.email,
        password: data.password,
      })
    );
  return (
    <form onSubmit={handleSubmit(onLogin)}>
      <Stack spacing={2}>
        <Controller
          name="email"
          control={control}
          defaultValue=""
          rules={{
            required: true,
          }}
          render={({ field }) => (
            <TextField label="Email" variant="outlined" {...field} />
          )}
        />
        <Controller
          name="password"
          control={control}
          rules={{
            required: true,
          }}
          render={({ field }) => (
            <TextField
              label="Password"
              variant="outlined"
              type="password"
              {...field}
            />
          )}
        />
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Link
            underline="hover"
            variant="subtitle2"
            component={RouterLink}
            to="/forgot-password"
          >
            Forgot Password?
          </Link>

          <LoadingButton
            size="large"
            variant="contained"
            type="submit"
            loading={networkStatus === ELoadingStatus.LOADING}
          // disabled={!isDirty || !isValid}
          >
            Login
          </LoadingButton>
        </Grid>
      </Stack>
    </form>
  );
};
