import {
  Box,
  Card,
  CircularProgress,
  Container,
  Grid,
  Link,
  Stack,
  Typography,
} from "@material-ui/core";
// material
import { styled } from "@material-ui/core/styles";
import { Link as RouterLink, Route, Switch } from "react-router-dom";
import { AuthLayout } from "../../components/layouts/AuthLayout";
import { MHidden } from "../../components/MHidden";
import { ForgotPasswordForm } from "./ForgotPasswordForm";
import { LoginForm } from "./LoginForm";
import { ResetPasswordForm } from "./ResetPasswordForm";
// layouts
// components
// import Page from '../components/Page';
// import { MHidden } from '../components/@material-extend';
// import { LoginForm } from '../components/authentication/login';
// import AuthSocial from '../components/authentication/AuthSocial';

// ----------------------------------------------------------------------

const RootStyle = styled(Box)(({ theme }) => ({
  [theme.breakpoints.up("md")]: {
    display: "flex",
  },
}));

const SectionStyle = styled(Card)(({ theme }) => ({
  width: "100%",
  maxWidth: 464,
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  margin: theme.spacing(2, 0, 2, 2),
}));

const ContentStyle = styled("div")(({ theme }) => ({
  maxWidth: 480,
  margin: "auto",
  display: "flex",
  minHeight: "100vh",
  flexDirection: "column",
  justifyContent: "center",
  padding: theme.spacing(12, 0),
}));

// ----------------------------------------------------------------------

export function LoginPage() {
  return (
    <RootStyle title="Login">
      <AuthLayout>
        Don’t have an account? &nbsp;
        <Link
          underline="none"
          variant="subtitle2"
          component={RouterLink}
          to="/register"
        >
          Get started
        </Link>
      </AuthLayout>

      <MHidden width="mdDown">
        <SectionStyle>
          {/* <Typography variant="h3" sx={{ px: 5, mt: 10, mb: 5 }}>
            Hi, Welcome Back
          </Typography> */}
          <img src="/logo512.png" alt="login" />
        </SectionStyle>
      </MHidden>

      <Container maxWidth="sm">
        <ContentStyle>
          <Switch>
            <Route path="/login">
              <Stack sx={{ mb: 5 }}>
                <Typography variant="h4" gutterBottom>
                  Sign in to Social Catalyzers
                </Typography>
                <Typography sx={{ color: "text.secondary" }}>
                  Enter your details below.
                </Typography>
              </Stack>
              <LoginForm />
            </Route>
            <Route path="/forgot-password">
              <Stack sx={{ mb: 5 }}>
                <Typography variant="h4" gutterBottom>
                  Forgot Password?
                </Typography>
                <Typography sx={{ color: "text.secondary" }}>
                  Please provide us your email and we will send you the
                  instructions on how you can retrieve your account.
                </Typography>
              </Stack>
              <ForgotPasswordForm />
            </Route>
            <Route path="/reset-password/:token">
              <Stack sx={{ mb: 5 }} alignItems="center">
                <Typography variant="h4" gutterBottom>
                  Reset Password
                </Typography>

                <ResetPasswordForm />
              </Stack>
            </Route>
          </Switch>

          <MHidden width="smUp">
            <Typography variant="body2" align="center" sx={{ mt: 3 }}>
              Don’t have an account?&nbsp;
              <Link variant="subtitle2" component={RouterLink} to="register">
                Get started
              </Link>
            </Typography>
          </MHidden>
        </ContentStyle>
      </Container>
    </RootStyle>
  );
}
