// This user roles are saved in the user database,
// Norm followed:
// enum value as the roleid in databse.

export enum UserRole {
  SUPER_ADMIN = 100,
  ADMIN = "0",
  MODERATOR = 99,
  USER = "1",
}
