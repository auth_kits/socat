import React, { useEffect, useMemo } from "react";
import { Redirect, Route, RouteProps } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { SkeletonLoader } from "../components/SkeletonLoader";
import { ELoadingStatus } from "../dashboard/influencers/action";
import { getCurrentUserInfo } from "./action";
import { UserRole } from "./roles";
import {
  selectAuthenticated,
  selectPermissionForRole,
  selectStatus,
} from "./selectors";
import { UnAuthorized } from "./UnAuthorized";

export const UnPrivateRoute: React.FC<RouteProps> = (props) => {
  const status = useAppSelector(selectStatus);
  const authenticated = useAppSelector(selectAuthenticated);

  if (authenticated) {
    return <Redirect to="/dashboard" />;
  }
  return <Route {...props} />;
};
