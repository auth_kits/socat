import { isEmpty } from "lodash";
import React, { useMemo } from "react";
import {
  Redirect,
  Route,
  RouteProps,
  useHistory,
  useLocation,
} from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { SkeletonLoader } from "../components/SkeletonLoader";
import { ELoadingStatus } from "./action";
import { UserRole } from "./roles";
import {
  selectAuthenticated,
  selectCurrentUserStatus,
  selectPermissionForRole,
  selectStatus,
} from "./selectors";
import { UnAuthorized } from "./UnAuthorized";

const REDIRECT_TOKEN = "redirectUrl";
interface PrivateRouteProps extends RouteProps {
  roles?: UserRole[];
}
export const PrivateRoute: React.FC<PrivateRouteProps> = ({
  roles = [],
  ...routeProps
}) => {
  const currentUserStatus = useAppSelector(selectCurrentUserStatus);
  const authenticated = useAppSelector(selectAuthenticated);
  const dispatch = useAppDispatch();
  const location = useLocation();
  const history = useHistory();

  const hasPermissionSelector = useMemo(
    () => selectPermissionForRole(roles),
    [roles]
  );
  const hasPermission = useAppSelector(hasPermissionSelector);
  if (
    [ELoadingStatus.INITIAL, ELoadingStatus.LOADING].includes(currentUserStatus)
  ) {
    return (
      <SkeletonLoader loading variant="rectangular">
        <div />
      </SkeletonLoader>
    );
  }
  if (!authenticated) {
    return <Redirect to="/login" />;
  }

  if (hasPermission) {
    return <Route {...routeProps} />;
  }

  // temporary delete this after complete task
  // if (!hasPermission) {
  //   return <Route {...routeProps} />;
  // }
  // console.log(hasPermissionSelector)
  // console.log(hasPermission)
  return (
    <Route {...routeProps}>
      <UnAuthorized />
    </Route>
  );
};
